﻿$(window).load(function () {
    //Hide SignUP Buttons
    if ($("#hdnIsUserLogin").length > 0) {
        $(".sing-up").remove();
    }
    $('#quote-carousel').carousel({
        pause: true,
        interval: 4000,
    });
    //$('#amazon_img').attr('src', '/Content/img/Amazon-Logo.jpg');
});
// Craousel Function Start//
$(window).load(function () {
    try {
        $('#thumbs').carouFredSel({
            responsive: true,
            circular: true,
            infinite: false,
            auto: true,
            prev: '#prev',
            next: '#next',
            scroll: 1,
            items: {
                visible: {
                    min: 2,
                    max: 6
                },
                width: 250,
                height: '80%'
            }
        });
    } catch (e) {

    }
    try {
        $('#thumbs a').click(function () {
            $('#carousel').trigger('slideTo', '#' + this.href.split('#').pop());
            $('#thumbs a').removeClass('selected');
            $(this).addClass('selected');
            return false;
        });
    } catch (e) {

    }
    try {

        $('.owl-carousel').owlCarousel({
            items: 3,
            loop: true,
            margin: 10,
            autoPlay: true,
            autoPlayTimeout: 1000,
            autoplayHoverPause: true
        });

    } catch (e) {

    }
    try {
        var id = window.location.href.split('/').reverse()[0];
        if (id.indexOf('#') >= 0) {
            var offset = 70;
            var target = $(id).offset().top - offset;
            $('html, body').animate({
                scrollTop: target
            }, 500);
            event.preventDefault();
        }
    } catch (e) { }
});
$("header").addClass('header-margin');

