$(window).load(function () {
    $("#status").fadeOut(), $("#preloader").delay(350).fadeOut("slow"), $("body").delay(350).css({
        overflow: "visible"
    })
}), $(".mobile-toggle").click(function () {
    $(".main_header").hasClass("open-nav") ? $(".main_header").removeClass("open-nav") : $(".main_header").addClass("open-nav")
}), $(".main_header li a").click(function () {
    $(".main_header").hasClass("open-nav") && ($(".navigation").removeClass("open-nav"), $(".main_header").removeClass("open-nav"))
}), $("nav a").click(function (e) {
    try {
        console.log("click");
        console.log($(this).attr("href").split("/").reverse()[0]);
        var t = $(this).attr("href").split("/").reverse()[0],
            a = 94,
            n = $(t).offset().top - a;
        $("html, body").animate({
            scrollTop: n
        }, 500), e.preventDefault()
    } catch (o) { }
});
try {
    (new WOW).init()
} catch (e) { }
$(document).ready(function () {
    try {
        $("html").niceScroll({
            cursorwidth: "8",
            cursorborderradius: "none",
            cursorborder: "none",
            cursorcolor: "#3498db",
            mousescrollstep: "60"
        })
    } catch (e) { }
}), $(function () {
    try {
        var e = {
            init: function () {
                try {
                    $("#portfoliolist").mixitup({
                        targetSelector: ".portfolio",
                        filterSelector: ".filter",
                        effects: ["fade"],
                        easing: "snap",
                        onMixEnd: e.hoverEffect()
                    })
                } catch (t) { }
            },
            hoverEffect: function () {
                $("#portfoliolist .portfolio").hover(function () {
                    $(this).find(".label").stop().animate({
                        bottom: 0
                    }, 200, "easeOutQuad"), $(this).find("img").stop().animate({
                        top: -30
                    }, 500, "easeOutQuad")
                }, function () {
                    $(this).find(".label").stop().animate({
                        bottom: -40
                    }, 200, "easeInQuad"), $(this).find("img").stop().animate({
                        top: 0
                    }, 300, "easeOutQuad")
                })
            }
        };
        e.init()
    } catch (t) { }
});
var object = [{
    headline: "HTML & CSS",
    value: 8,
    length: 9,
    description: "Significant experience and knowlage of HTML(5) and CSS functionality and use."
}, {
    headline: "JavaScript & jQuery",
    value: 6,
    length: 5,
    description: "Experience with object-oriented JavaScript. </br> Extended knowlage of DOM manipulation in aiding and extending the UI."
}, {
    headline: "Ruby & Python",
    value: 3,
    length: 5,
    description: "Experience with object-oriented JavaScript. </br> Extended knowlage of DOM manipulation in aiding and extending the UI."
}];
$(document).ready(function () {
    try {
        $("#skillset").skillset({
            object: object,
            duration: 40
        })
    } catch (e) { }
}), $(document).ready(function () {
    try {
        $("#testimonial_carosule").owlCarousel({
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: !0,
            autoPlay: !0,
            transitionStyle: "backSlide"
        })
    } catch (e) { }
}), $(document).ready(function () {
    try {
        $().UItoTop({
            easingType: "easeOutQuart"
        })
    } catch (e) { }
}), $(document).ready(function () {
    $("#submit_btn").click(function () {
        var e = $("input[name=name]").val(),
            t = $("input[name=email]").val(),
            a = $("input[name=phone]").val(),
            n = $("textarea[name=message]").val(),
            o = !0;
        "" == e && ($("input[name=name]").css("border-color", "red"), o = !1), "" == t && ($("input[name=email]").css("border-color", "red"), o = !1), "" == a && ($("input[name=phone]").css("border-color", "red"), o = !1), "" == n && ($("textarea[name=message]").css("border-color", "red"), o = !1), o && (post_data = {
            userName: e,
            userEmail: t,
            userPhone: a,
            userMessage: n
        }, $.post("contact_me.php", post_data, function (e) {
            "error" == e.type ? output = '<div class="error">' + e.text + "</div>" : (output = '<div class="success">' + e.text + "</div>", $("#contact_form input").val(""), $("#contact_form textarea").val("")), $("#result").hide().html(output).slideDown()
        }, "json"))
    }), $("#contact_form input, #contact_form textarea").keyup(function () {
        $("#contact_form input, #contact_form textarea").css("border-color", ""), $("#result").slideUp()
    })
});