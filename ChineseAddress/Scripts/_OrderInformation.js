﻿function SubmitTrackNumber(tracknumber) {
    var trackingNumber = $(tracknumber).text().trim();
    TrackingNumber(trackingNumber);
}
function isAgree() {
    if ($('input[type="checkbox"]').is(':checked') === false) {
        showMessage('Please accept Terms and Conditions', false);
        return false;
    }
    else {
        $('input[type="checkbox"]').val('true')
        $("#frm_rshipment").submit();
        return true;
    }
}
function CancelClick(anchor) {
    confirm('Are you sure you want to cancel this order?', function (result) {
        if (result === true) {
            window.location.href = $(anchor).attr('href');
        }
        else
            return false;
    });

    return false;
}
//Partial
$(document).on('ready', function () {
    $("#fileUpload").fileinput();
    $("a[id^='InitiateLightBox']").click(function () {
        $(this).next("[id^='gallery']").lightGallery({
            thumbnail: true
        });
        $(this).next("[id^='gallery']").children().eq(0)[0].click();
        return false;
    });

    //Subtraction value change
    $("#Orderchargestable").find('td.text-right > strong').each(function (obj, i) {
        if ($(this).text().indexOf('-') !== -1) {
            $(this).text($(this).text().replace('$ -', '(-) $ '));
        }
    });
});
function ChangeIcon(icon) {
    var cancelrequest = $(icon).attr('href').replace('#', '');
    $("#CancelShipment").val(cancelrequest);
    $(icon).children().toggleClass('fa-angle-down fa-angle-up');
}

function SaveNotes(textdata) {
    var data = $(textdata).prev('textarea').val();
    var id = $(textdata).attr('id');
    $.ajax({
        url: '/User/Order/SaveNotes',
        type: 'POST',
        data: JSON.stringify({ "SaveNotesData": data, "RecShipId": id }),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            showMessage(data, true);
        }
    })
}

function correcrprice(ID) {
    RecShipId = $(ID).attr("id");
    localStorage.setItem("RecShipId", RecShipId);
}
function cancelshipment(ID) {
    RecShipId = $(ID).attr("id");
    confirm('Are you sure,you want to confirm cancelshipment ', function (result) {
        if (result == true) {
            $.ajax({
                url: '/User/Shipment/CancelRecivedShipment',
                type: 'POST',
                data: JSON.stringify({ "CancelShipmentID": RecShipId }),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    if (data != null) {
                        showMessage(data, true);
                        location.reload();
                        showMessage('You shipment has been successfully cancel ', true);
                    }
                }
            })
        }
    });
}
// Create FormData object
$(window).load(function () {
    $(document).on("click", ".fileinput-upload-button", function () {
        var fileUpload = $("#fileUpload").get(0);
        var files = fileUpload.files;
        var fileData = new FormData();
        // Looping over all files and add it to FormData object
        for (var i = 0; i < files.length; i++) {
            fileData.append(files[i].name, files[i]);
        }
        fileData.append("recievshipId", localStorage.RecShipId);
        // Adding one more key to FormData object
        $.ajax({
            url: '/User/Shipment/CorrectPrice',
            type: "POST",
            contentType: false, // Not to set any content header
            processData: false, // Not to process data
            data: fileData,
            success: function (result) {
                showMessage(result, true);
                if (result == "")
                { showMessage('Please select at least one shipment', false); }
                else
                {
                    showMessage(result, true);
                    $('button[class="btn btn-default fileinput-remove fileinput-remove-button"]').trigger("click");
                }
            },
            error: function (err) {
                alert(err.statusText);
            }
        });
    });
});