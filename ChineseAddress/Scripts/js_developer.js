﻿$(window).load(function () {
    try {
        /*DropDown Add SearchSelect='true' in control*/
        $("select[SearchSelect='true']").select2({ width: '100%' });
        $('select').on('select2:select', function (evt) {
            $(this).parents('div').next('div').eq(0).find('input').eq(0).focus().select();
        });
        $(document).on('focus', '.select2', function () {
            $(this).siblings('select').select2('open');
        });
    } catch (e) {
    }
});
/*Mesage popup*/
function showMessage(msg, bool, timeout) {
    timeout = (typeof timeout !== 'undefined') ? timeout : 7000;
    if (bool === true) {
        $('#div_msg').html('<div class="alert-success">' + msg + '</div>');
        $.pgwModal({
            target: '#div_msg',
            titleBar: false
        });
        $(".pm-content").css({ "background": "#DFF0D8" })
    }
    else {
        $('#div_msg').html('<div style="color: #dc0400;">' + msg + '</div>');
        $.pgwModal({
            target: '#div_msg',
            titleBar: false
        });
        $(".pm-content").css({ "background": "#ffebeb" })
    }
    setTimeout(function () {
        $.pgwModal('close');
    }, timeout);
}
/* Order tracking number start*/
function TrackingNumber(TrackingNumber) {
    $.ajax({
        url: '/CommonChild/TrackingNumber',
        type: 'POST',
        data: JSON.stringify({ "TrackingNumber": TrackingNumber }),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data != "") {
                window.location.href = "/TrackOrder?Track=" + TrackingNumber
            }
            else {
                showMessage('Invalid or no information found for this tracking number.', false);
            }
        }
    })
}
function TrackingOrderReturnURl(TrackingNumber) {
    $.ajax({
        url: '/CommonChild/TrackingOrderReturnURl',
        type: 'POST',
        data: JSON.stringify({ "TrackingNumber": TrackingNumber }),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data != "") {
                window.location.href = data;
                //window.location.href = "/TrackOrder?Track=" + TrackingNumber
            }
            else {
                showMessage('Invalid or no information found for this tracking number.', false);
            }
        }
    })
}
$(window).load(function () {
    $('#preloader').fadeOut('slow').children().fadeOut('slow');
});
//override confirm with bootbox
window.confirm = function (str, callback) {
    bootbox.confirm({
        title: '<b>Confirm!</b>',
        message: "<p style='font-size: 18px;'>" + str + "</p>",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> No',
                className: 'btn-danger'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Yes',
                className: 'btn-success font-size-inherit  btn-confirm-success'
            }
        },
        callback: function (result) {
            callback(result);
        }
    });
}

function callConfirm(str, callback) {
    var dfrd = new Deferred();
    return deferred.promise();
}
/*Loader Ajax Event Start*/
$(document).ajaxSend(function () {
    $('#preloader').fadeIn('slow').children().fadeIn('slow');
    //$.LoadingOverlay("show", {
    //    color: "rgba(255, 255, 255, 0.7)",
    //    image: "/content/img/preloader_svg.svg"
    //});
});

$(document).ajaxSuccess(function () {
    //$.LoadingOverlay("hide");
    $('#preloader').fadeOut('slow').children().fadeOut('slow');
});

$(document).ajaxComplete(function () {
    //$.LoadingOverlay("hide");
    $('#preloader').fadeOut('slow').children().fadeOut('slow');
});
/*Loader on page Load and onbeforeunload*/;
$(window).load(function () { $('#preloader').fadeOut('slow').children().fadeOut('slow'); });
window.onbeforeunload = function () { $('#preloader').fadeIn('slow').children().fadeIn('slow'); }
/*Loader on page Load and Post Backing End*/

$(window).load(function () {
    try {
        $("img").unveil();
    } catch (e) {
    }
})
$(window).load(function () {
    $('carousel-inner').on('swipe', function () {
        $('.carousel-inner').children()
    })
})
function ShowLoader() {
    $('#preloader').fadeIn('slow').children().fadeIn('slow');
}
function HideLoader() {
    $('#preloader').fadeOut('slow').children().fadeOut('slow');
}
