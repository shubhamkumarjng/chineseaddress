﻿$(function () {
    $("#Country").val("AF");
    $("#Country").trigger("change.select2");
    bindCarriers();
    $("#btnCalculate").click(function () {
        var bool = true;
        $('input[id^="txt"]').each(function () {
            if ($(this).val() === "" || $(this).val() === undefined) {
                $(this).next().removeClass('hidden');
                bool = false;
            } else {
                $(this).next().addClass('hidden');
            }
        });
        if (bool === false)
            return false;

        var varweight = $("#txtWeight").val();
        var varlength = $("#txtLength").val();
        var varwidth = $("#txtWidth").val();
        var varheight = $("#txtHeight").val();

        //LB TO  KG
        //if ($("#ddlType").val().toLowerCase() === "kg") {
        //    varweight = (parseFloat(varweight) * 2.2046226).toFixed(2);
        //}
        //CM TO  INCH
        //if ($("#ddlUnit").val().toLowerCase() === "cm") {
        //    varlength = (parseFloat(varlength) / 2.54).toFixed(2);
        //    varwidth = (parseFloat(varwidth) / 2.54).toFixed(2);
        //    varheight = (parseFloat(varheight) / 2.54).toFixed(2);
        //}

        if ($("#ddlType").val().toLowerCase() === "lb") {
            varweight = (parseFloat(varweight)/ 2.2046226).toFixed(2);
        }
        if ($("#ddlUnit").val().toLowerCase() === "inch") {
            varlength = (parseFloat(varlength) * 2.54).toFixed(2);
            varwidth = (parseFloat(varwidth) * 2.54).toFixed(2);
            varheight = (parseFloat(varheight) * 2.54).toFixed(2);
        }

        $.ajax({
            url: "/Rates/Index",
            type: "Post",
            data: JSON.stringify({ countryCode: $("#Country").val(), weight: varweight, length: varlength, width: varwidth, height: varheight }),
            dataType: "JSON",
            contentType: "application/json; charset/utf-8;",
            success: function (data) {
                debugger;
                if (data === false) {
                    showMessage('Rates are not available for current selection.', false)
                    bindCarriers();
                }
                else {
                    var onlyCarrier = '<div class="row"><div class="col-sm-12 paddingright0px" >';
                    var carriersAndPrice = '';
                    var priceList = '<div class="col-sm-12 col-xs-6"><ul class="list-unstyled">';
                    var listCarrier = '<ul class="list-unstyled">';

                    $(data).each(function (i, obj) {
                        //var LessOneBussinessDay = obj.BussinessDays == 0 ? 0 : obj.BussinessDays - 1;
                        //if (LessOneBussinessDay == 0) {
                        //    listCarrier += '<li ' + (i === 0 ? 'class="margin-minus"' : '') + '><h5>' + obj.Service + '</h5> <em><br/></em></li>';
                        //    priceList += '<li><h4 class="text-blue">' + obj.TotalShipmentCharge + '</h4></li>';
                        //}
                        //else {
                        //    listCarrier += '<li ' + (i === 0 ? 'class="margin-minus"' : '') + '><h5>' + obj.Service + '</h5> <em>' + LessOneBussinessDay + "-" + obj.BussinessDays + " business days" + '</em></li>';
                        //    priceList += '<li><h4 class="text-blue">' + obj.TotalShipmentCharge + '</h4></li>';
                        //}

                        listCarrier += '<li ' + (i === 0 ? 'class="margin-minus"' : '') + '><h5>' + obj.Service + '</h5> <em>' + obj.strBussinessDays + '</em></li>';
                        priceList += '<li><h4 class="text-blue">' + obj.TotalShipmentCharge + '</h4></li>';
                    });
                    listCarrier += '</ul>';
                    onlyCarrier += listCarrier + '</div></div>';
                    carriersAndPrice += '<div class="row"><div class="col-xs-6 visible-xs">' + listCarrier + '</div>' + priceList + '</ul></div></div>'
                    $('#divOnlyCarriers').empty().html(onlyCarrier);
                    $('[id^="divCarriersCharges"]').empty().html(carriersAndPrice);
                    $('div[id^="divCarriersCharges"]').each(function (i, obj) {
                        var margin = 100 + parseFloat($(this).attr('margin'));
                        var textColor = '';
                        switch (i) {
                            case 0:
                                textColor = 'text-green';
                                break;
                            case 1:
                                textColor = 'text-blue';
                                break;
                            case 2:
                                textColor = 'text-green';
                                break;
                            default:
                                textColor = 'text-green';
                                break;
                        }   
                        $(this).find('h4').each(function (i, obj) {
                            $(obj).removeClass('text-green').removeClass('text-blue').addClass(textColor);
                            if (textColor === 'text-blue')
                                obj.innerText = "$" + ((parseFloat(obj.innerText.replace('$', '')) * margin) / 100).toFixed(2).toString();
                            else
                                obj.innerText = '-';
                        });
                    });
                }
            }
        });
    });
});
function bindCarriers() {
    $.getJSON("/Rates/GetAllActiveCarriers", function (data) {
        var onlyCarrier = '<div class="row"><div class="col-sm-12 paddingright0px">';
        var carriersAndPrice = '';
        var priceList = '<div class="col-sm-12 col-xs-6"><ul class="list-unstyled">';
        var listCarrier = '<ul class="list-unstyled">';

        $(data).each(function (i, obj) {
            var LessOneBussinessDay = obj.BussinessDays == 0 ? 0 : obj.BussinessDays - 1;
            if (LessOneBussinessDay == 0) {
                // listCarrier += '<li ' + (i === 0 ? 'class="margin-minus"' : '') + '><h5>' + obj.Service + '</h5> <em>' + (obj.Service.toLowerCase().indexOf('expedited') != -1 ? '2-8 business days' : '1-4 business days') + '</em></li>';
                listCarrier += '<li ' + (i === 0 ? 'class="margin-minus"' : '') + '><h5>' + obj.Service + '</h5><em><br/></em></li>';
                priceList += '<li><h4 class="text-blue">-</h4></li>';
            }
            else {
                listCarrier += '<li ' + (i === 0 ? 'class="margin-minus"' : '') + '><h5>' + obj.Service + '</h5> <em>' + LessOneBussinessDay + "-"  + obj.BussinessDays + "business days" + '</em></li>';
                priceList += '<li><h4 class="text-blue">-</h4></li>';
            }
        });
        listCarrier += '</ul>';
        onlyCarrier += listCarrier + '</div></div>';
        carriersAndPrice += '<div class="row"><div class="col-xs-6 visible-xs">' + listCarrier + '</div>' + priceList + '</ul></div></div>'
        $('#divOnlyCarriers').empty().html(onlyCarrier);
        $('[id^="divCarriersCharges"]').empty().html(carriersAndPrice);
        $('div[id^="divCarriersCharges"]').each(function (i, obj) {
            var textColor = '';
            switch (i) {
                case 0:
                    textColor = 'text-green';
                    break;
                case 1:
                    textColor = 'text-blue';
                    break;
                case 2:
                    textColor = 'text-green';
                    break;
                default:
                    textColor = 'text-green';
                    break;
            }
            $(this).find('h4').each(function (i, obj) {
                $(obj).removeClass('text-green').removeClass('text-blue').addClass(textColor);
            });
        });
    });
}