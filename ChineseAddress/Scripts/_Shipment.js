﻿$(document).ready(function () {
    $('.table.table-striped tbody > tr td span[id^="freestorage"]').each(function (i, obj) {
        var totalCost = ($(obj).text());
        var value = totalCost.replace('Days', '');
        if (value <= 0) {
            $(this).text($(this).text().replace('-', ''));
            $(this).text($(this).text() + ' over');
            $(this).removeClass('.label label-success')
            $(this).addClass('.label label-danger')
        }
        else if (value > 0 && value < 8) {
            $(this).text($(this).text() + ' left');
            $(this).removeClass('.label label-success')
            $(this).addClass('.label label-warning')
        }
        else {
            $(this).text($(this).text() + ' left');
        }
    });
    $("#sel1").change(function () {
        reverseToNext();
        var ID = $("#sel1").val();
        $.ajax({
            url: '/User/Shipment/GetReciveShipmentDetail',
            type: 'POST',
            data: JSON.stringify({ "ID": ID }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $("#FirstNames").text(result.first_name);
                $("#lastNames").text(result.last_name);
                $("#Addres1").text(result.address1 + ',');
                $("#Addres2").text((result.address2 === null ? '' : result.address2));
                $("#CountryCodes").text(result.country_code + ',');
                $("#States").text(result.state + ',');
                $("#Citys").text(result.city + ',');
                $("#PostalCodes").text(result.post_code + ',');
                $("#MobileNumbers").text('M:' + result.mobile1);
            },
        });
    })
});
//Bind Carrier Prices on address change start
//$("#sel1").change(function () {
//    var ID = $("#sel1").val();
//    var ShipmentCheckboxID = GetRecshipmentID()
//    $.ajax({
//        url: '/User/Shipment/GetShipmentMethodCarrierApi',
//        type: 'POST',
//        data: JSON.stringify({ "ID": ID, "ShipmentCheckboxID": ShipmentCheckboxID }),
//        dataType: 'json',
//        contentType: 'application/json; charset=utf-8',
//        success: function (data) {
//            var Target = $("#shipmentapi");
//            Target.empty();
//            $.each(data, function (i, item) {
//                Target.append("<li class='radio'><input type='hidden' id='codevalue' value='" + item.CarrierCode + "'/><input type='hidden' id='services' value='" + item.service + "'/><label style='font-size:1em'><input name='o5' type='radio' value=''><span class='cr'><i class='cr-icon fa fa-circle'></i></span><span id='des'>" + item.carrier + "</span>&nbsp;<span class='label label-default'>" + item.Amount + "</span></label></li>")
//            })
//        },
//    });
//})
//Bind Carrier Prices on address change end

var sumbitform = false;
$("#SubmitOrderButton").click(function (evt) {
    $("#sel1").hide();
    if ($("#SubmitOrderButton")) {
        var Recivedshipid = GetRecshipmentID()
        if (Recivedshipid.length === 0 || typeof Recivedshipid === "undefined") {
            showMessage('Please select at least one shipment', false);
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        } else {
            if (planTitle === 'direct-single-shipment' && Recivedshipid.length > 1) {
                showMessage('Please select only one shipment', false);
                evt.stopPropagation();
                evt.preventDefault();
                return false;
            }
        }
        if (sumbitform === false) {
            if (Recivedshipid.length === 0 || typeof Recivedshipid === "undefined") {

                showMessage('Please select at least one shipment', false);
            }
            else {
                $("#SubmitOrderButton").html($("#SubmitOrderButton").html().replace('Next', 'Submit New Order'))
                ShowLoader();
                var ID = $("#sel1").val();
                $.ajax({
                    url: '/User/Shipment/GetShipmentMethodCarrierApi',
                    type: 'POST',
                    async: false,
                    data: JSON.stringify({ "ID": ID, "ShipmentCheckboxID": Recivedshipid }),
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        var Target = $("#shipmentapi");
                        Target.empty();
                        $.each(data, function (i, item) {
                            var LessOneBussinessDay = item.BussinessDays == 0 ? 0 : item.BussinessDays - 1;
                            if (LessOneBussinessDay == 0) {
                                Target.append("<li class='radio'><input type='hidden' id='codevalue' value='" + item.CarrierCode + "'/><input type='hidden' id='services' value='" + item.service + "'/><label style='font-size:1em'><input name='o5' type='radio' value=''><span class='cr'><i class='cr-icon fa fa-circle'></i></span><span id='des'>" + item.carrier + "</span>&nbsp;<span class='label label-success'>" + item.Amount + "</span></label></li>")
                            }
                            else
                                Target.append("<li class='radio'><input type='hidden' id='codevalue' value='" + item.CarrierCode + "'/><input type='hidden' id='services' value='" + item.service + "'/><label style='font-size:1em'><input name='o5' type='radio' value=''><span class='cr'><i class='cr-icon fa fa-circle'></i></span><span id='des'>" + item.carrier + "</span>&nbsp;<span class='label label-success'>" + item.Amount + "</span>&nbsp" + LessOneBussinessDay + "-" + item.BussinessDays + " business days</label></li>")
                        });
                        HideLoader();
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                sumbitform = true;
                $("#ShippingMethod").show();
                $("#PackagingOptions").show();
                $("#Coupon").show();
                $("#divOrerNote").show();
            }
        }
        else if (sumbitform === true) {
            // check validation carrier checkbox
            var bool = false;
            $('div[id="ShippingMethod"]').find('input').each(function (i, obj) {
                if ($(this).prop('checked') === true) {
                    bool = true;
                }
            });
            if (!bool) {
                //$("#carriercheckbox").removeClass('hidden');
                if ($("#SubmitOrderButton").text().trim() === "Submit New Order") {
                    showMessage('Please select atleast one shipping method', false);
                }
                $("#ShippingMethod").show();
                $("#PackagingOptions").show();
                $("#SubmitOrderButton").html($("#SubmitOrderButton").html().replace('Next', 'Submit New Order'))
                var ShipmentCheckboxID = GetRecshipmentID()
                var ID = $("#sel1").val();
                $.ajax({
                    url: '/User/Shipment/GetShipmentMethodCarrierApi',
                    type: 'POST',
                    data: JSON.stringify({ "ID": ID, "ShipmentCheckboxID": ShipmentCheckboxID }),
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        var Target = $("#shipmentapi");
                        Target.empty();
                        $.each(data, function (i, item) {
                            var LessOneBussinessDay = item.BussinessDays == 0 ? 0 : item.BussinessDays - 1;
                            if (LessOneBussinessDay == 0) {
                                Target.append("<li class='radio'><input type='hidden' id='codevalue' value='" + item.CarrierCode + "'/><input type='hidden' id='services' value='" + item.service + "'/><label style='font-size:1em'><input name='o5' type='radio' value=''><span class='cr'><i class='cr-icon fa fa-circle'></i></span><span id='des'>" + item.carrier + "</span>&nbsp;<span class='label label-success'>" + item.Amount + "</span></label></li>")
                            }
                            else
                                Target.append("<li class='radio'><input type='hidden' id='codevalue' value='" + item.CarrierCode + "'/><input type='hidden' id='services' value='" + item.service + "'/><label style='font-size:1em'><input name='o5' type='radio' value=''><span class='cr'><i class='cr-icon fa fa-circle'></i></span><span id='des'>" + item.carrier + "</span>&nbsp;<span class='label label-success'>" + item.Amount + "</span>&nbsp" + LessOneBussinessDay + "-" + item.BussinessDays + " business days</label></li>")
                        })
                    },
                });
                return false;
            }
            // Get data Address dropdownPanel
            var Addressdropid = $("#sel1").val();
            //Get Shipment Checkbox Data Active Panel//
            var ShipmentCheckboxID = GetRecshipmentID();
            // Get Packageoption checkbox collect data//
            var PacakageOptionArray = [];
            $('div[id="PackagingOptions"]').find('input').each(function (i, obj) {
                if ($(this).prop('checked') === true) {
                    var price = $(this).siblings('.label').text().replace('$', '').trim()
                    var PackageOptionPrice = (price === "" ? "0.0" : price);
                    var PackageOptionTitle = $(this).siblings('text').text().trim();
                    var packageitem = {
                        "PackageOptionPrice": PackageOptionPrice,
                        "PackageOptionTitle": PackageOptionTitle
                    };
                }
                if (typeof packageitem !== "undefined") {
                    PacakageOptionArray.push(packageitem);
                }
            })
            // Get shiping method radio button collect data//
            var ShipmentMethodPackage = [];
            $('div[id="ShippingMethod"]').find('input').each(function (i, obj) {
                if ($(this).prop('checked') === true) {
                    var shipmentMethodPrice = $(this).next('span').next().next().text().replace('$', '');
                    var shipmentdescription = $(this).parent().parent().children().next().val();
                    var shipmentcode = $(this).parent().parent().children().val();
                    var shipMethod = {
                        "shipmentMethodPrice": shipmentMethodPrice,
                        "shipmentdescription": shipmentdescription,
                        "shipmentcode": shipmentcode
                    }
                }
                if (typeof shipMethod !== "undefined") {
                    ShipmentMethodPackage.push(shipMethod);
                }
            });
            var data = JSON.stringify({ ShipmentMethodPackage: ShipmentMethodPackage, PacakageOptionArray: PacakageOptionArray, ShipmentCheckboxID: ShipmentCheckboxID, Addressdropid: Addressdropid, CouponCode: $("#hdnCouponCode").val(), order_notes: $("#order_notes").val() });
            $.ajax({
                url: '/User/Shipment/CreateShipmentOrder',
                type: 'POST',
                data: JSON.stringify({ "data": data }),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    if (data == "") {
                        showMessage('Please select at least one shipment', false);
                    }
                    else if (data === "false") {
                        showMessage('Please select only one shipment', false);
                    }
                    else {
                        window.location.href = "/OrderInformation/" + data
                    }
                }
            })
        }
    }
});
//parital script start
function ReturnPackage() {
    var idarray = GetRecshipmentID();
    if (idarray.length < 1) {
        $("#myreturnpackage").modal('hide');
        showMessage('Please select at least one shipment.', false);
        return false;
    }
    confirm('Are you sure you want to return Package(s)/Shipment(s)? You will automatically charge $' + ReturnShipmentCharge+' for each shipment from your wallet at the time when admin accept return.', function (result) {
        if (result === true) {
            $("#myreturnpackage").find('a').eq(0)[0].click();
            $('#returnpackageid').val(idarray);
            $("#myreturnpackage").modal('show');
            $('a[data-target="#prepaidlabel"]').children().removeClass('fa fa-plus').addClass('fa fa-minus')
        }
    });
}
$(document).ready(function () {
    $('form[id="formdata"]').find('span').addClass('hidden')
})
function Cleardata() {
    $('form[id="formdata"]').find('input').each(function (i, obj) {
        $(this).val('');
    })
}
function SubmitRequestPackageForm() {
    var is_validate = true;
    $('form[id="formdata"]').find('input').each(function (i, obj) {
        if ($(this).val() === "" && $(this).attr("id") !== "Address2" || $(this).attr('id') === "Phone") {
            if ($(this).attr('id') === "Phone") {
                var filter = /^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-.]+\d$/;
                if (filter.test($(this).val())) {
                    $(this).css('border-color', '#ccc');
                    $(this).parent().children().eq(3).addClass('hidden')
                }
                else if ($(this).val() === "") {
                    $(this).css('border-color', 'red');
                    $(this).parent().children().eq(3).addClass('hidden')
                    $(this).parent().children().eq(2).removeClass('hidden')
                    is_validate = false
                }
                else {
                    $(this).css('border-color', 'red');
                    $(this).parent().children().eq(2).addClass('hidden')
                    $(this).parent().children().eq(3).removeClass('hidden')
                    is_validate = false
                }
            }
            else {
                $(this).css('border-color', 'red');
                $(this).parent().children().eq(2).removeClass('hidden')
                is_validate = false;
            }
        }
        else if ($(this).val() != "") {
            $(this).css('border-color', '#ccc');
            $(this).parent().children().eq(2).addClass('hidden')
        }
    });
    if (is_validate) {
        var formrequestdata = new FormData();
        var idarray = [];
        $('div.tab-pane.active').find("input[id^='id'][type='checkbox']:checked").each(function (i, obj) { (idarray.push(obj.id)) })
        formrequestdata.append("returnpackageid", idarray);
        formrequestdata.append("FirstName", $("#FirstName").val());
        formrequestdata.append("LastName", $("#LastName").val());
        formrequestdata.append("Address1", $("#Address1").val());
        formrequestdata.append("Address2", $("#Address2").val());
        formrequestdata.append("City", $("#City").val());
        formrequestdata.append("Zip", $("#Zip").val());
        formrequestdata.append("State", $("#State").val());
        formrequestdata.append("Phone", $("#Phone").val());
        formrequestdata.append("Rma", $("#Rma").val());
        $.ajax({
            url: '/User/Shipment/ReturPackageForm',
            type: "POST",
            contentType: false, // Not to set any content header
            processData: false, // Not to process data
            data: formrequestdata,
            success: function (result) {
                if (result == "")
                { showMessage('Please select at least one shipment.', false); }
                else {
                    showMessage(result, true);
                    $('form[id="formdata"]').find('input').each(function (i, obj) {
                        $(this).val('');
                    });
                }
                setTimeout(function () {
                    location.reload();
                }, 5000);
            },
            error: function (err) {
                alert(err.statusText);
            }
        });
    }
}
function ReadyCheckBox(ready) {
    if ($(ready).parents('thead').find('input[type="checkbox"]').prop('checked') == true) {
        $(ready).parents('thead').next('tbody').children().children().find('input[type="checkbox"]').each(function () { $(this).prop('checked', true) });
    }
    else if ($(ready).parents('thead').find('input[type="checkbox"]').prop('checked') == false) {
        $(ready).parents('thead').next('tbody').children().children().find('input[type="checkbox"]').each(function () { $(this).prop('checked', false) })
    }
}

//Checkbox Events
$("td input[type='checkbox']").change(function () {
    if ($(this).prop('checked') === false) {
        $(this).parents('table').find('thead').find('input[type="checkbox"]').prop('checked', false);
    }
    else {
        if ($(this).parents('tbody').find('input[type="checkbox"]').length === $(this).parents('tbody').find('input[type="checkbox"]:checked').length)
            $(this).parents('table').find('thead').find('input[type="checkbox"]').prop('checked', true);
    }
})
function CheckBoxProccesing(process) {
    if ($(process).parents('thead').find('input[type="checkbox"]').prop('checked') == true) {
        $(process).parents('thead').next('tbody').children().children().find('input[type="checkbox"]').each(function () { $(this).prop('checked', true) });
    }
    else if ($(process).parents('thead').find('input[type="checkbox"]').prop('checked') == false) {
        $(process).parents('thead').next('tbody').children().children().find('input[type="checkbox"]').each(function () { $(this).prop('checked', false) })
    }
}

function ONHoldCheckBox(onhold) {
    if ($(onhold).parents('thead').find('input[type="checkbox"]').prop('checked') == true) {
        $(onhold).parents('thead').next('tbody').children().children().find('input[type="checkbox"]').each(function () { $(this).prop('checked', true) });
    }
    else if ($(onhold).parents('thead').find('input[type="checkbox"]').prop('checked') == false) {
        $(onhold).parents('thead').next('tbody').children().children().find('input[type="checkbox"]').each(function () { $(this).prop('checked', false) })
    }
}

function NotAllowed(notallowed) {
    if ($(notallowed).parents('thead').find('input[type="checkbox"]').prop('checked') == true) {
        $(notallowed).parents('thead').next('tbody').children().children().find('input[type="checkbox"]').each(function () { $(this).prop('checked', true) });
    }
    else if ($(notallowed).parents('thead').find('input[type="checkbox"]').prop('checked') == false) {
        $(notallowed).parents('thead').next('tbody').children().children().find('input[type="checkbox"]').each(function () { $(this).prop('checked', false) })
    }
}
function ChangeIcon(icon) {
    var cancelrequest = $(icon).attr('href').replace('#', '');
    $("#CancelShipment").val(cancelrequest);
    $(icon).children().toggleClass('fa-angle-down fa-angle-up');
}
function CollapeAll(IconChange) {
    $('[role="button"]').trigger('click');
    $(IconChange).children().toggleClass('fa fa-caret-down  fa fa-caret-up');
}
$('a[data-target="#formfill"]').children().click(function () {
    $('a[data-target="#formfill"]').children().toggleClass('fa fa-plus  fa fa-minus')
    if ($('a[data-target="#prepaidlabel"]').parents('div[class="panel panel-default"]').children().eq(1).hasClass('collapse in') === true) {
        $('a[data-target="#prepaidlabel"]').parents('div[class="panel panel-default"]').children().eq(1).removeClass('collapse in').addClass('collapse');
        if ($('a[data-target="#prepaidlabel"]').children().hasClass('fa fa-minus') === true) {
            $('a[data-target="#prepaidlabel"]').children().removeClass('fa fa-minus').addClass('fa fa-plus')
        }
        else if ($('a[data-target="#prepaidlabel"]').children().hasClass('fa fa-plus') === true) {
            $('a[data-target="#prepaidlabel"]').children().removeClass('fa fa-pluss').addClass('fa fa-minus')
        }
    }
})
$('a[data-target="#prepaidlabel"]').children().click(function () {
    $('a[data-target="#prepaidlabel"]').children().toggleClass('fa fa-plus  fa fa-minus')
    if ($('a[data-target="#formfill"]').parents('div[class="panel panel-default"]').children().eq(1).hasClass('collapse in') === true) {
        $('a[data-target="#formfill"]').parents('div[class="panel panel-default"]').children().eq(1).removeClass('collapse in').addClass('collapse');

        if ($('a[data-target="#formfill"]').children().hasClass('fa fa-minus') === true) {
            $('a[data-target="#formfill"]').children().removeClass('fa fa-minus').addClass('fa fa-plus')
        }
        else if ($('a[data-target="#formfill"]').children().hasClass('fa fa-plus') === true) {
            $('a[data-target="#formfill"]').children().removeClass('fa fa-pluss').addClass('fa fa-minus')
        }
    }
})
$("[id='CancelShipment']").click(function () {
    var CancelShipmentID = $(this).parents('tr').prev().find('a').attr('href').replace('#', '')
    confirm('Are you sure you want to dispose of this shipment?', function (result) {
        if (result === true) {
            $.ajax({
                url: '/User/Shipment/CancelRecivedShipment',
                type: 'POST',
                data: JSON.stringify({ "CancelShipmentID": CancelShipmentID }),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    if (data != null) {
                        setTimeout(function () {
                            location.reload();
                        }, 5000);
                        showMessage('You shipment has been successfully canceled!', true);

                    }
                }
            })
        }
    });
});
$("[id='CorrectPrice']").click(function () {
    var CorrectPrice = $(this).parents('tr').prev().find('a').attr('href').replace('#', '')
    $('button[class="btn btn-default fileinput-remove fileinput-remove-button"]').trigger("click");
    localStorage.setItem("CorrectPriceID", CorrectPrice);
});
//Request Picture//
function RequestPicture() {
    var idarray = GetRecshipmentID();
    if (idarray.length < 1) {
        showMessage('Please select at least one shipment.', false);
        return false;
    }
    confirm('Are you sure you want to request shipment picture(s)? You will be charged according to your plan.', function (result) {
        if (result === true)
            $.ajax({
                url: '/User/Shipment/RequestPicture',
                type: 'POST',
                data: JSON.stringify({ "requestpicture": idarray }),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    if (data == "") {
                        showMessage('Please select at least one shipment.', false);
                    }
                    else {
                        $('input[type="checkbox"]:checked').remove();
                        showMessage(data, true);

                    }
                }
            })
    });
}

function SaveNotes(textdata) {
    var data = $(textdata).prev('textarea').val();
    var id = $(textdata).attr('id');
    $.ajax({
        url: '/User/Shipment/SaveNotes',
        type: 'POST',
        data: JSON.stringify({ "SaveNotesData": data, "RecShipId": id }),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            showMessage(data, true);
        }
    })
}
$(document).on('ready', function () {
    $("#fileUpload").fileinput();

    //LightBox start
    $("a[id^='InitiateLightBox']").click(function () {
        $(this).next("[id^='gallery']").lightGallery({
            thumbnail: true
        });
        $(this).next("[id^='gallery']").children().eq(0)[0].click();
        return false;
    });
});
function GetRecshipmentID() {
    var idarray = [];
    $('div.tab-pane.active').find("input[id^='id'][type='checkbox']:checked").each(function (i, obj) { (idarray.push(obj.id)) })
    return idarray;
}
$('div.tab-pane.active input[type="checkbox"]').change(function () {
    reverseToNext();
})
function reverseToNext() {
    $("#ShippingMethod").hide();
    $("#PackagingOptions").hide();
    $("#SubmitOrderButton").html($("#SubmitOrderButton").html().replace('Submit New Order', 'Next'))
}

$(document).ready(function () {
    // Change click tab show hide panels
    $('li[role="presentation"] >a').click(function () {
        if ($(this).attr('id') === 'ReadyTab') {
            if ($('div [id="ready"]').find('td').children('h2').text().trim() === 'No results found') {
                $("#CreateNewOrder").hide();
            }
            else {
                $("#divreturnpackage").show();
                $("#SubmitOrderButton").show();
                $("#CreateNewOrder").show();
            }
        }
        if ($(this).attr('href') === '#OrderCreated') {
            if ($('div [id="OrderCreated"]').find('td').children('h2').text().trim() === 'No results found') {
                $("#divreturnpackage").hide();
                $("#checkrow").hide();
                $("#CreateNewOrder").hide();
            }
            else {
                $("#divreturnpackage").hide();
                $("#checkrow").hide();
                $("#CreateNewOrder").hide();
            }
        }
        if ($(this).attr('href') === '#notallowed') {
            if ($('div [id="notallowed"]').find('td').children('h2').text().trim() === 'No results found') {
                $("#divreturnpackage").hide();
            }
            else {
                $("#divreturnpackage").show();
                $("#CreateNewOrder").hide();
            }
        }
        if ($(this).attr('href') === '#onhold') {
            if ($('div [id="onhold"]').find('td').children('h2').text().trim() === 'No results found') {
                $("#divreturnpackage").hide();
            }
            else {
                $("#divreturnpackage").show();
                $("#CreateNewOrder").hide();
            }
        }
        if ($(this).attr('href') === '#Abandon') {
            if ($('div [id="Abandon"]').find('td').children('h2').text().trim() === 'No results found') {
                $("#divreturnpackage").hide();
                $("#checkrow").hide();
            }
            else {
                $("#divreturnpackage").hide();
                $("#checkrow").hide();
            }
        }
    })

    $(document).on("click", ".fileinput-upload-button", function () {
        // Checking whether FormData is available in browser
        var fileUpload = $("#fileUpload").get(0);
        var filesrec = $("#ReturnpackagefileUpload").get(0);
        var filerecship = filesrec.files;
        var files = fileUpload.files;
        if (fileUpload.files.length === 0) {
            // Create FormData object
            var fileData = new FormData();
            // Looping over all files and add it to FormData object
            for (var i = 0; i < filerecship.length; i++) {
                fileData.append(filerecship[i].name, filerecship[i]);
            }
            var idarray = [];
            $('div.tab-pane.active').find("input[id^='id'][type='checkbox']:checked").each(function (i, obj) { (idarray.push(obj.id)) })
            fileData.append("returnpackageid", idarray);

            // Adding one more key to FormData object
            $.ajax({
                url: '/User/Shipment/ReturPackageForm',
                type: "POST",
                contentType: false, // Not to set any content header
                processData: false, // Not to process data
                data: fileData,
                success: function (result) {
                    showMessage(result, true);
                    if (result == "")
                    { showMessage('Please select at least one shipment.', false); }
                    else {
                        showMessage(result, true);
                        setTimeout(function () {
                            location.reload();
                        }, 5000);
                        $('button[class="btn btn-default fileinput-remove fileinput-remove-button"]').trigger("click");
                    }
                },
                error: function (err) {
                    alert(err.statusText);
                }
            });
        }
        else if (filesrec.files.length === 0) {
            var fileData = new FormData();
            // Looping over all files and add it to FormData object
            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }
            var recivesshipid = localStorage.CorrectPriceID;
            try {
                var removeCheckbox = "#id " + recivesshipid.toString();
                $(removeCheckbox.toString()).remove();
            } catch (e) {

            }
            fileData.append("recievshipId", recivesshipid);
            // Adding one more key to FormData object
            $.ajax({
                url: '/User/Shipment/CorrectPrice',
                type: "POST",
                contentType: false, // Not to set any content header
                processData: false, // Not to process data
                data: fileData,
                success: function (result) {
                    showMessage(result, true);

                    setTimeout(function () {
                        location.reload();
                    }, 5000);
                },
                error: function (err) {
                    alert(err.statusText);
                }
            });
        }
    });
});

function validateCouponCode() {
    if (typeof $("#txtCouponCode").val() === undefined || $("#txtCouponCode").val() === "") {
        showMessage('Please enter valid coupon code.', false);
        $("#hdnCouponCode").val('');
    }
    else {
        $.ajax({
            url: "/User/Shipment/validateCouponCode",
            method: 'POST',
            data: JSON.stringify({ "coupon_code": $("#txtCouponCode").val(), "country_name": $("#CountryCodes").text().substring($("#CountryCodes").text().lastIndexOf(','), 0) }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                showMessage(data.Item2, data.Item1);
                if (data.Item1 === false)
                    $("#hdnCouponCode").val('');
                else
                    $("#hdnCouponCode").val($("#txtCouponCode").val());
            }
        });
    }
}

$(document).ready(function () {
    if ($('div[id="ready"]').find('h2').text().trim() === 'No results found') {
        $("#divreturnpackage").hide();
        $("#SubmitOrderButton").hide();
        $("#CreateNewOrder").hide();
    }
    checkShipmentsUnderProcess();
})

function checkShipmentsUnderProcess() {
    //remove checkbox for those shipments those are under process
    $('input[id*=hfIsUnderSomeProcess]').filter(function () { if ($(this).val() === "True") return true; return false; }).parents('tr').find('input[type="checkbox"]').remove();
}