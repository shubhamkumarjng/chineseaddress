namespace _ChineseAddress.com.Models
{
    public static class ShipperAddressStaticModel
    {
        public static string Address { get; set; } = "3133 Tianhe Ave ";
        public static string City { get; set; } = "Guangzhou";
        public static string State { get; set; } = "Guangzhou";
        public static string Zip { get; set; } = "510000";
        public static string CountryCode { get; set; } = "China";
        public static string Phone { get; set; } = System.Configuration.ConfigurationManager.AppSettings["ClientPhoneNo"];
    }
}