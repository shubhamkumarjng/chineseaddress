using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _ChineseAddress.com
{

    public enum eTopUpWalletStatus
    {
        Pending = 1,
        Accept = 2,
        Reject = 3
        //,cancelled = 4
    }
    public static class enums
    {
        public static IEnumerable<SelectListItem> EnumToSelectListItems<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = (Convert.ToInt32(v)).ToString()
            }).AsEnumerable();
        }
    }
}