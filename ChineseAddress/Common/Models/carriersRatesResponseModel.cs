using _ChineseAddress.com.Repository;
using System.Collections.Generic;

namespace _ChineseAddress.com.Common.Models
{
    public class carriersRatesResponseModel
    {
        public string Carrier { get; set; }
        public string Product { get; set; }
        public string Service { get; set; }
        public string Service_Code { get; set; }
        public string BillableWeight { get; set; }
        public List<tblOrderBox> product { get; set; }
        public decimal TotalShipmentCharge { get; set; }
        public int BussinessDays { get; set; }
        public string strBussinessDays { get; set; }
    }
}