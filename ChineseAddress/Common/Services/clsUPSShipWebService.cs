using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.UPSShipWebService;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;

namespace _ChineseAddress.com.Common.Services
{
    public class clsUPSShipWebService
    {
        #region Properties && Readonly Variables

        /// <summary>
        /// Get SUCCESS or ERROR msg from this acessor
        /// </summary>
        public string _msg { get; set; }

        /// <summary>
        /// Get SUCCESS or ERROR from this acessor
        /// </summary>
        public bool _isSuccess { get; set; } = false;

        private static readonly string _UPSAPIUserName = System.Configuration.ConfigurationManager.AppSettings["UPSAPIUserName"].ToString();
        private static readonly string _UPSAPIPassword = System.Configuration.ConfigurationManager.AppSettings["UPSAPIPassword"].ToString();
        private static readonly string _UPSAPIAccessLicenseNumber = System.Configuration.ConfigurationManager.AppSettings["UPSAPIAccessLicenseNumber"].ToString();
        private static readonly string _UPSAPIShipperNumber = System.Configuration.ConfigurationManager.AppSettings["UPSAPIShipperNumber"].ToString();

        /// <summary>

        /// This Accessor from ToAddress
        /// </summary>
        public tblDeliveryAddress _ToAddress { get; set; }
        public decimal? _Insurancecharges { get; set; }
        public List<tblOrderBox> _Packages { get; set; }
        public string _SuitNo { get; set; }
        public tblOrder _OrderDetail { get; set; }

        #endregion Properties && Readonly Variables

        #region Static Variables

        private static readonly PackagingType _packageType = new PackagingType() { Code = "02" };
        private static readonly ShipUnitOfMeasurementType _packageWeightUnitOfMeasurement = new ShipUnitOfMeasurementType() { Code = "LBS", Description = "Pounds" };
        private static readonly ShipUnitOfMeasurementType _packagesUnitOfMeasurement = new ShipUnitOfMeasurementType() { Code = "IN", Description = "Inches" };

        //response
        private HttpResponse response = HttpContext.Current.Response;

        #endregion Static Variables

        #region CallServiceMetods

        public void CreateShipmentAndGenerateLabel()
        {
            try
            {
                ShipService shpSvc = new ShipService();
                ShipmentRequest shipmentRequest = new ShipmentRequest();
                shpSvc.UPSSecurityValue = getUPSSecurity();
                RequestType request = new RequestType();
                string[] requestOption = { "nonvalidate" };
                request.RequestOption = requestOption;
                shipmentRequest.Request = request;
                
                ShipmentType shipment = new ShipmentType();
                shipment.Description = "Shipping and label web service";
               // IFChargesType insuranceCharges = new IFChargesType();

                //Payment Info  start
                PaymentInfoType paymentInfo = new PaymentInfoType();
                ShipmentChargeType shpmentCharge = new ShipmentChargeType();

                BillShipperType billShipper = new BillShipperType();
                billShipper.AccountNumber = _UPSAPIShipperNumber;
                shpmentCharge.BillShipper = billShipper;
                shpmentCharge.Type = "01";
                ShipmentChargeType[] shpmentChargeArray = { shpmentCharge };
                paymentInfo.ShipmentCharge = shpmentChargeArray;
                shipment.PaymentInformation = paymentInfo;
                //Payment Info  end

                //get static shipment Shipper and from  Address
                Tuple<ShipperType, ShipFromType> ShipperAndFromAddress = getShipperandFromAddress();
                shipment.Shipper = ShipperAndFromAddress.Item1;
                shipment.ShipFrom = ShipperAndFromAddress.Item2;

                //Ship to address
                shipment.ShipTo = getToAddress();

                /**Choosed Service Start*/
                ServiceType service = new ServiceType();
                service.Code = _OrderDetail.service_code;
                shipment.Service = service;
                /**Choosed Service end*/

                /** Packaging options start*/
                shipment.Package = getPackages().ToArray();
                /** Packaging options end*/

                /** Label Specification Start*/
                shipmentRequest.LabelSpecification = getLabelSpecification();
                shipmentRequest.Shipment = shipment;
                /** Label Specification End*/

                string FilePath = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/CarrierXml/UpsXml/{_OrderDetail.id}");
                //Genrate Ups Request xml Start//
                if (!Directory.Exists(FilePath))
                {
                    Directory.CreateDirectory(FilePath);
                    string RequestFilePath = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/CarrierXml/UpsXml/{_OrderDetail.id}/UpsRequest.xml");
                    GenrateXmlSerializer.Serialize(shipmentRequest, RequestFilePath);
                }
                else
                {
                    string RequestFilePath = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/CarrierXml/UpsXml/{_OrderDetail.id}/UpsRequest.xml");
                    GenrateXmlSerializer.Serialize(shipmentRequest, RequestFilePath);
                }
                //Genrate Ups Request xml End//
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                ShipmentResponse shipResponse = shpSvc.ProcessShipment(shipmentRequest);
                // Genrate Ups Response xml Start//
                string ResponseFilePath = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/CarrierXml/UpsXml/{_OrderDetail.id}/UpsResponse.xml");
                GenrateXmlSerializer.Serialize(shipResponse, ResponseFilePath);
                // Genrate Ups Response xml End//
                if (shipResponse.Response.ResponseStatus.Code == "1")
                {
                    _isSuccess = true;
                    SaveShipmentDetailAndLabels(shipResponse);
                }
                else
                {
                    _isSuccess = false;
                    _msg = $"Error occured while calling UPS shipping web service. Code:{shipResponse.Response.ResponseStatus.Code} and Description:{shipResponse.Response.ResponseStatus.Description}";
                }
            }
            catch (Exception ex)
            {
                _isSuccess = false;
                _msg = ((System.Web.Services.Protocols.SoapException)ex).Detail.InnerText;
            }
        }

        private void SaveShipmentDetailAndLabels(ShipmentResponse shipResponse)
        {
            var server = HttpContext.Current.Server;

            //Add tracking no into order table
            _OrderDetail.tracking_no = shipResponse.ShipmentResults.PackageResults[0].TrackingNumber;

            //Add package level  tracking no
            for (int i = 0; i <= shipResponse.ShipmentResults.PackageResults.Length - 1; i++)
            {
                _Packages[i].tracking_no = shipResponse.ShipmentResults.PackageResults[i].TrackingNumber;
            }
            //Create Orders folder.
            if (!Directory.Exists(server.MapPath("~/uploads/Orders")))
                Directory.CreateDirectory(server.MapPath("~/uploads/Orders"));

            //Create Order Number folder under orders folder.
            string orderfolder = server.MapPath($"~/uploads/Orders/{_OrderDetail.id}");
            if (!Directory.Exists(orderfolder))
                Directory.CreateDirectory(orderfolder);
            int count = 1;
            PdfDocument doc = new PdfDocument();
            foreach (PackageResultsType PackResult in shipResponse.ShipmentResults.PackageResults)
            {
                using (MemoryStream imgStream = new MemoryStream(Convert.FromBase64String(PackResult.ShippingLabel.GraphicImage)))
                {
                    System.Drawing.Image i = System.Drawing.Image.FromStream(imgStream);
                    // rotate Image 90' Degree
                    i.RotateFlip(System.Drawing.RotateFlipType.Rotate270FlipXY);
                    // save it to its actual path
                    string gifName = $"{PackResult.TrackingNumber}_Pack{count++}.gif";
                    string GifFilePath = orderfolder + "\\" + gifName;
                    i.Save(GifFilePath, System.Drawing.Imaging.ImageFormat.Gif);
                    // release Image File
                    i.Dispose();
                    //Create pdf
                    PdfPage page = new PdfPage();
                    doc.Pages.Add(page);
                    XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[count - 2]);
                    XImage img = XImage.FromFile(GifFilePath);
                    page.Height = img.PixelHeight;
                    page.TrimMargins = new TrimMargins() { Left = 15, Right = 15, Top = 15, Bottom = 15 };
                    xgr.DrawImage(img, 0, 0);
                }
            }
            Context4InshipEntities context = new Context4InshipEntities();
            doc.Save(orderfolder + "\\" + _OrderDetail.tracking_no + "_Label.pdf");
        }

        #endregion CallServiceMetods

        #region Common and miscellaneous methods

        /// <summary>
        /// UPSSecurity
        /// </summary>
        /// <returns></returns>
        private static UPSSecurity getUPSSecurity()
        {
            UPSSecurity upss = new UPSSecurity();

            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = _UPSAPIAccessLicenseNumber;
            upss.ServiceAccessToken = upssSvcAccessToken;

            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = _UPSAPIUserName;
            upssUsrNameToken.Password = _UPSAPIPassword;
            upss.UsernameToken = upssUsrNameToken;
            return upss;
        }

        /// <summary>
        /// Get Static From and ToAddress
        /// </summary>
        /// <returns></returns>
        private Tuple<ShipperType, ShipFromType> getShipperandFromAddress()
        {
            //Shipper Address
            ShipperType shipper = new ShipperType();
            shipper.ShipperNumber = _UPSAPIShipperNumber;

            ShipAddressType shipperAddress = new ShipAddressType();
            shipperAddress.AddressLine = new string[] { ShipperAddressStaticModel.Address + " " + _SuitNo };
            shipperAddress.City = ShipperAddressStaticModel.City;
            shipperAddress.PostalCode = ShipperAddressStaticModel.Zip;
            shipperAddress.StateProvinceCode = ShipperAddressStaticModel.State;
            shipperAddress.CountryCode = ShipperAddressStaticModel.CountryCode;
            shipper.Name = "4InShip";
            shipper.AttentionName = "Jorfili";
            shipper.Phone = new ShipPhoneType() { Number = ShipperAddressStaticModel.Phone };
            shipper.Address = shipperAddress;

            //From Address
            ShipFromType shipFrom = new ShipFromType();
            ShipAddressType shipFromAddress = new ShipAddressType();
            shipFromAddress.AddressLine = new[] { ShipperAddressStaticModel.Address + " " + _SuitNo };
            shipFromAddress.City = ShipperAddressStaticModel.City;
            shipFromAddress.PostalCode = ShipperAddressStaticModel.Zip;
            shipFromAddress.StateProvinceCode = ShipperAddressStaticModel.State;
            shipFromAddress.CountryCode = ShipperAddressStaticModel.CountryCode;
            shipFrom.Address = shipFromAddress;
            shipFrom.Name = "4InShip";
            shipFrom.AttentionName = "4InShip";
            shipFrom.Phone = new ShipPhoneType() { Number = ShipperAddressStaticModel.Phone };
            return Tuple.Create(shipper, shipFrom);
        }

        /// <summary>
        /// Get To Address from _TOAddress Accessors
        /// </summary>
        /// <returns></returns>
        private ShipToType getToAddress()
        {
            ShipToType shipTo = new ShipToType();
            try
            {
                ShipToAddressType shipToAddress = new ShipToAddressType();
                List<string> lstAddress = new List<string>();
                if (_ToAddress.address1 == null)
                {
                    lstAddress.Add("");
                }
                else
                {
                    lstAddress.Add(_ToAddress.address1);
                    //lstAddress.AddRange(_ToAddress.address1.Split(','));
                }
                if (_ToAddress.address2 != null)
                {
                    lstAddress.Add(_ToAddress.address2);
                    //lstAddress.AddRange(_ToAddress.address2.Split(','));
                }
                shipToAddress.AddressLine = lstAddress.ToArray();
                shipToAddress.City = (_ToAddress.city ?? "");
                shipToAddress.PostalCode = (_ToAddress.postal_code ?? "");
                //shipToAddress.StateProvinceCode = (_ToAddress.state ?? "");
                shipToAddress.CountryCode = _ToAddress.country_code;
                shipTo.Address = shipToAddress;
                shipTo.Phone = new ShipPhoneType() { Number = _ToAddress.mobile1 };
                shipTo.Name = _ToAddress.first_name + " " + _ToAddress.last_name;
                shipTo.AttentionName = _ToAddress.first_name + " " + _ToAddress.last_name;
            }
            catch (Exception ex)
            {
                _msg = ex.ToString();
                _isSuccess = false;
            }
            return shipTo;
        }

        private List<PackageType> getPackages()
        {
            List<PackageType> lstpackage = new List<PackageType>();
            try
            {
                if (_Packages != null)
                {
                    foreach (tblOrderBox _package in _Packages)
                    {
                        DimensionsType packageDimension = new DimensionsType();
                        PackageWeightType packageWeight = new PackageWeightType();

                        PackageType package = new PackageType();
                        //Pass Insurance to carrier start// 
                        //package.PackageServiceOptions = new PackageServiceOptionsType();
                        //package.PackageServiceOptions.Insurance = new InsuranceType();
                        //package.PackageServiceOptions.Insurance.ExtendedFlexibleParcelIndicator = new InsuranceValueType();
                        //package.PackageServiceOptions.Insurance.ExtendedFlexibleParcelIndicator.CurrencyCode = "USD";
                        //package.PackageServiceOptions.Insurance.ExtendedFlexibleParcelIndicator.MonetaryValue = "500";
                        //if (_ordercharges != null)
                        //{
                        //    InternationalFormType objInternationalFormType = new InternationalFormType();
                        //    objInternationalFormType.InsuranceCharges = new IFChargesType();
                        //    objInternationalFormType.InsuranceCharges.MonetaryValue = Convert.ToString(_ordercharges.sell_price);
                        //}
                        //Pass Insurance to carrier End//
                        //Add Package weight
                        packageWeight.Weight = Convert.ToString(_package.actual_weight);
                        packageWeight.UnitOfMeasurement = _packageWeightUnitOfMeasurement;
                        package.PackageWeight = packageWeight;
                        ////Add Package Dim-Weight
                        //packageWeight.Weight = Convert.ToString(_package.dim_weight);
                        //package.DimWeight = packageWeight;
                        //Add Dimension
                        if (_package.length != null)
                        {
                            packageDimension.Length = Convert.ToString(_package.length);
                            packageDimension.Width = Convert.ToString(_package.width);
                            packageDimension.Height = Convert.ToString(_package.height);
                            packageDimension.UnitOfMeasurement = _packagesUnitOfMeasurement;
                            package.Dimensions = packageDimension;
                        }
                        //Add Package Type
                        package.Packaging = _packageType;

                        lstpackage.Add(package);
                    }
                }
            }
            catch (Exception ex)
            {
                _msg = ex.ToString();
                _isSuccess = false;
            }
            return lstpackage;
        }

        private static LabelSpecificationType getLabelSpecification()
        {
            LabelSpecificationType labelSpec = new LabelSpecificationType();
            //LabelStockSizeType labelStockSize = new LabelStockSizeType();
            //labelStockSize.Height = "6";
            //labelStockSize.Width = "4";
            //labelSpec.LabelStockSize = labelStockSize;
            LabelImageFormatType labelImageFormat = new LabelImageFormatType();
            labelImageFormat.Code = "GIF";
            labelSpec.LabelImageFormat = labelImageFormat;
            return labelSpec;
        }

        #endregion Common and miscellaneous methods
    }
}