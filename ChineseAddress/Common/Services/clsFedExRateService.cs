using _ChineseAddress.com.Common.Models;
using _ChineseAddress.com.FedExRateWebService;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;

namespace _ChineseAddress.com.Common.Services
{
    public class clsFedExRateService
    {
        public static string _FailMsg { get; set; }
        public bool _Success { get; set; } = false;
        private static readonly string _FedExApiKey = ConfigurationManager.AppSettings["FedExKey"];
        private static readonly string _FedExPassword = ConfigurationManager.AppSettings["FedExPassword"];
        private static readonly string _FedExAccountNumber = ConfigurationManager.AppSettings["FedExAccountNumber"];
        private static readonly string _FedExMeterNumber = ConfigurationManager.AppSettings["FedExMeterNumber"];

        Context4InshipEntities Context = new Context4InshipEntities();
        public DateTime DeliverdDates { get; set; }
        public DateTime TodayDates { get; set; } = DateTime.Now;
        public int Bussinessdays { get; set; }
        public DateTime GetDay { get; set; }
        /// <summary>
        /// Carrier List
        /// </summary>
        public tblCarriersDetail _Carrier { get; set; } = null;

        /// <summary>
        /// This Accessor from ToAddress
        /// </summary>
        public tblDeliveryAddress _ToAddress { get; set; }

        public List<tblOrderBox> _Packages { get; set; }
        public string _SuitNo { get; set; }

        public List<carriersRatesResponseModel> _GetRatesApi()
        {
            List<carriersRatesResponseModel> Carrierrates = new List<carriersRatesResponseModel>();
            RateRequest request = CreateRateRequest();
            //
            RateService service = new RateService();
            try
            {
                // ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                RateReply reply = service.getRates(request);
                if (reply.HighestSeverity == NotificationSeverityType.SUCCESS || reply.HighestSeverity == NotificationSeverityType.NOTE || reply.HighestSeverity == NotificationSeverityType.WARNING)
                {
                    _Success = true;
                    foreach (RateReplyDetail rateReplyDetail in reply.RateReplyDetails)
                    {
                        var CompareDefaultDate = "01-01-0001 00:00:00";
                        if (rateReplyDetail.DeliveryTimestamp == Convert.ToDateTime(CompareDefaultDate))
                        {
                            Bussinessdays = 0;
                        }
                        else
                        {
                            var TodayDays = DateTime.Now.Date;
                            DeliverdDates = rateReplyDetail.DeliveryTimestamp.Date;
                            Bussinessdays = Convert.ToInt32((DeliverdDates - TodayDays).TotalDays);
                        }
                        for (int i = 0; i < rateReplyDetail.RatedShipmentDetails.Length; i++)
                        {
                            decimal CarrierRates = Convert.ToDecimal((rateReplyDetail.RatedShipmentDetails[i]).ShipmentRateDetail.TotalNetCharge.Amount);
                            Carrierrates.Add(new carriersRatesResponseModel() { Service = Convert.ToString(rateReplyDetail.ServiceType), TotalShipmentCharge = CarrierRates, Service_Code = Convert.ToString((int)rateReplyDetail.ServiceType), Carrier = "FedEx", BussinessDays = Bussinessdays });
                        }
                    }
                }
                else if (reply.HighestSeverity == NotificationSeverityType.ERROR || reply.HighestSeverity == NotificationSeverityType.WARNING || reply.HighestSeverity == NotificationSeverityType.FAILURE)
                {
                    _FailMsg = "";
                    for (int i = 0; i < reply.Notifications.Length; i++)
                    {
                        _FailMsg += reply.Notifications[i].Message.ToString() + "\r\n";
                    }
                    _Success = false;
                }
            }
            catch (SoapException e)
            {
                _FailMsg = e.Detail.InnerText;
                _Success = false;
            }
            catch (Exception e)
            {
                _FailMsg = e.Message.ToString();
                _Success = false;
            }
            return Carrierrates.ToList();
        }

        private void _SetFromAddress(RateRequest _Ratesrequest)
        {
            try
            {
                _Ratesrequest.RequestedShipment.Shipper = new Party();
                _Ratesrequest.RequestedShipment.Shipper.Address = new Address();
                _Ratesrequest.RequestedShipment.Shipper.Address.StreetLines = new string[1] { ShipperAddressStaticModel.Address };
                _Ratesrequest.RequestedShipment.Shipper.Address.CountryCode = ShipperAddressStaticModel.CountryCode;
                _Ratesrequest.RequestedShipment.Shipper.Address.City = ShipperAddressStaticModel.City;
                _Ratesrequest.RequestedShipment.Shipper.Address.PostalCode = ShipperAddressStaticModel.Zip;
                _Ratesrequest.RequestedShipment.Shipper.Address.StateOrProvinceCode = ShipperAddressStaticModel.State;
            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }

        private void _SetShiptoAddress(RateRequest _Ratesrequest)
        {
            try
            {
                _Ratesrequest.RequestedShipment.Recipient = new Party();
                _Ratesrequest.RequestedShipment.Recipient.Address = new Address();
                _Ratesrequest.RequestedShipment.Recipient.Address.StreetLines = new string[2] { _ToAddress.address1 ?? "", _ToAddress.address2 ?? "" };
                _Ratesrequest.RequestedShipment.Recipient.Address.City = _ToAddress.city ?? "";
                // _Ratesrequest.RequestedShipment.Recipient.Address.StateOrProvinceCode = _ToAddress.state;
                _Ratesrequest.RequestedShipment.Recipient.Address.PostalCode = _ToAddress.postal_code ?? "";
                _Ratesrequest.RequestedShipment.Recipient.Address.CountryCode = _ToAddress.country_code ?? "";
            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }

        private RateRequest CreateRateRequest()
        {
            // Build a RateRequest
            RateRequest request = new RateRequest();
            try
            {
                //
                request.WebAuthenticationDetail = new WebAuthenticationDetail();
                request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
                request.WebAuthenticationDetail.UserCredential.Key = _FedExApiKey; // Replace "XXX" with the Key
                request.WebAuthenticationDetail.UserCredential.Password = _FedExPassword; // Replace "XXX" with the Password

                //
                request.ClientDetail = new ClientDetail();
                request.ClientDetail.AccountNumber = _FedExAccountNumber; // Replace "XXX" with the client's account number
                request.ClientDetail.MeterNumber = _FedExMeterNumber; // Replace "XXX" with the client's meter number
                request.TransactionDetail = new TransactionDetail();
                Guid _guid = Guid.NewGuid();
                request.TransactionDetail.CustomerTransactionId = _guid.ToString().Substring(0, 8); // This is a reference field for the customer.  Any value can be used and will be provided in the response.
                                                                                                    //
                request.Version = new VersionId();
                //
                request.ReturnTransitAndCommit = true;
                request.ReturnTransitAndCommitSpecified = true;
                //
                SetShipmentDetails(request);
                //
            }
            catch (Exception ex)
            {
                _FailMsg = ex.ToString();
                _Success = false;
            }
            return request;
        }

        private void SetShipmentDetails(RateRequest request)
        {
            //request.RequestedShipment.DropoffType = DropoffType.REGULAR_PICKUP; //Drop off types are BUSINESS_SERVICE_CENTER, DROP_BOX, REGULAR_PICKUP, REQUEST_COURIER, STATION
            // request.RequestedShipment.ServiceType = ServiceType.INTERNATIONAL_PRIORITY; // Service types are STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND ...
            try
            {
                request.RequestedShipment = new RequestedShipment();
                request.RequestedShipment.ShipTimestamp = DateTime.Now; // Shipping date and time
                request.RequestedShipment.ShipTimestampSpecified = true;

                if (_Carrier != null)
                {
                    // RequestedShipment _service = new RequestedShipment();
                    request.RequestedShipment.ServiceType = (ServiceType)Convert.ToInt32(_Carrier.code);
                    request.RequestedShipment.ServiceTypeSpecified = true;
                }
                else
                {
                    request.RequestedShipment.ServiceTypeSpecified = false;
                }
                request.RequestedShipment.PackagingType = PackagingType.YOUR_PACKAGING; // Packaging type FEDEX_BOK, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
                request.RequestedShipment.PackagingTypeSpecified = false;

                var DayOfWeek = DateTime.Now.DayOfWeek.ToString();
                var GetTodayDate = DateTime.Now;
                var CarrierHolidayDate = Context.tblcarrierholidays.AsQueryable().Where(x => x.holidaydate == GetTodayDate).FirstOrDefault();
                if (DayOfWeek.ToLower() == "sunday")
                {
                    GetDay = GetTodayDate.AddDays(1);
                }
                else if (DayOfWeek.ToLower() == "saturday")
                {
                    GetDay = GetTodayDate.AddDays(2);
                }
                else if (DayOfWeek.ToLower() == "saturday" && CarrierHolidayDate != null)
                {
                    GetDay = GetTodayDate.AddDays(3);
                }
                else if (CarrierHolidayDate != null)
                {
                    GetDay = (DateTime)CarrierHolidayDate.holidaydate;
                }
                if (GetDay.Day != 1)
                {
                    request.RequestedShipment.ShipTimestamp = GetDay;
                }
                else
                {
                    request.RequestedShipment.ShipTimestamp = DateTime.Now;
                }

                _SetFromAddress(request);
                //
                _SetShiptoAddress(request);
                //
                request.RequestedShipment.RequestedPackageLineItems = _GetPackages(request).ToArray<RequestedPackageLineItem>();
                request.RequestedShipment.PackageCount = Convert.ToString(_Packages.Count());
            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
            }
        }
       
        public List<RequestedPackageLineItem> _GetPackages(RateRequest request)
        {
            List<RequestedPackageLineItem> _lstPackges = new List<RequestedPackageLineItem>();
            Weight _packageweight = new Weight();
            Dimensions _packageDemansionWeight = new Dimensions();
            try
            {
                if (_Packages != null)
                {
                    foreach (var _package in _Packages)
                    {
                        // package weight
                        RequestedPackageLineItem package = new RequestedPackageLineItem();
                        package.GroupPackageCount = "1";
                        _packageweight.Units = WeightUnits.LB;
                        _packageweight.UnitsSpecified = true;
                        _packageweight.Value = Convert.ToDecimal(Math.Ceiling(_package.actual_weight));
                        _packageweight.ValueSpecified = true;
                        package.Weight = _packageweight;
                        // package dimensions
                        _packageDemansionWeight = new Dimensions();
                        _packageDemansionWeight.Length = Convert.ToString(Math.Ceiling(_package.length ?? 0));
                        _packageDemansionWeight.Width = Convert.ToString(Math.Ceiling(_package.width ?? 0));
                        _packageDemansionWeight.Height = Convert.ToString(Math.Ceiling(_package.height ?? 0));
                        _packageDemansionWeight.Units = LinearUnits.IN;
                        _packageDemansionWeight.UnitsSpecified = true;
                        package.Dimensions = _packageDemansionWeight;
                        _lstPackges.Add(package);
                    }
                }
            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
            return _lstPackges.ToList();
        }
    }
}
