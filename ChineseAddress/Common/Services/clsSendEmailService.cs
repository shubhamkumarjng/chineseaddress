using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace _ChineseAddress.com.Common.Services
{
    public class clsSendEmailService
    {
        public string SendEmail(string[] to, string[] cc, string[] bcc, string body, string subject, string[] attachments = null)
        {
            string returnString = "";
            try
            {
                subject = ConfigurationManager.AppSettings["Environment"] + " " + subject;
                MailMessage email = new MailMessage();

                SmtpClient smtp = new SmtpClient();

                // draft the email
                email.From = new MailAddress(ConfigurationManager.AppSettings["FromMail"].ToString());
                if (to != null)
                {
                    foreach (string item in to)
                    {
                        email.To.Add(new MailAddress(item));
                    }
                }
                if (cc != null)
                {
                    foreach (string item in cc)
                    {
                        email.CC.Add(new MailAddress(item));
                    }
                }
                if (bcc != null)
                {
                    foreach (string item in bcc)
                    {
                        email.Bcc.Add(new MailAddress(item));
                    }
                }
                //every email bcc to bccEmails
                foreach (string bccEmail in Convert.ToString(ConfigurationManager.AppSettings["bccEmails"]).Split(','))
                {
                    if (!string.IsNullOrEmpty(bccEmail))
                        email.Bcc.Add(new MailAddress(bccEmail));
                }

                if (attachments != null)
                {
                    foreach (string attachment in attachments)
                    {
                        email.Attachments.Add(new Attachment(attachment));
                    }
                }
                email.Subject = subject;
                email.Body = body;
                email.IsBodyHtml = true;

                //Encode body  
                email.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, new System.Net.Mime.ContentType("text/html")));
                email.SubjectEncoding = System.Text.Encoding.UTF8;
                email.BodyEncoding = System.Text.Encoding.UTF8;

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                //Synchronous method
                //smtp.Send(email);
                //Asynchronous method
                Task.Run(() => { SendEmailAsync(smtp, email); });

                returnString = "Email Successfully Send";
            }
            catch (Exception ex)
            {
                returnString = "Error: " + ex.Message.ToString();
            }
            return returnString;
        }

        private async void SendEmailAsync(SmtpClient smtp, MailMessage email)
        {
            smtp.Send(email);
        }
    }
}