using _ChineseAddress.com.Common.Models;
using _ChineseAddress.com.FedExShipWebService;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services.Protocols;

    public class clsFedExShipWebService
    {
        public string _FailMsg { get; set; }
        public bool _Success { get; set; } = false;
        private static readonly string _FedExApiKey = ConfigurationManager.AppSettings["FedExKey"];
        private static readonly string _FedExPassword = ConfigurationManager.AppSettings["FedExPassword"];
        private static readonly string _FedExAccountNumber = ConfigurationManager.AppSettings["FedExAccountNumber"];
        private static readonly string _FedExMeterNumber = ConfigurationManager.AppSettings["FedExMeterNumber"];



        /// <summary>
        /// This Accessor from ToAddress
        /// </summary>
        public tblCarriersDetail _Carrier { get; set; } = null;
        public tblDeliveryAddress _ToAddress { get; set; }
        public List<tblOrderBox> _Packages { get; set; }
        public tblOrder _OrderDetail { get; set; }
        public string _SuitNo { get; set; }
        public int _OrderID { get; set; }

        public string[] _Description { get; set; }
        public string AddDiscription { get; set; }
        public decimal? _Insurancecharges { get; set; }
        public tblAddressBook _Addresbook { get; set; }
        public int _Count { get; set; } = 0;
        tblShipmentDetail _shipmentdetail = new tblShipmentDetail();
        Context4InshipEntities Context = new Context4InshipEntities();
        public void _getShipApi()
        {
            try
            {
                PdfDocument pdfDoc = new PdfDocument();

                for (int i = 0; i < _Packages.Count; i++)
                {
                    _Count = i;

                    ProcessShipmentRequest request = CreateShipmentRequest();
                    ShipService service = new ShipService();
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                    ProcessShipmentReply reply = service.processShipment(request);
                    // Save FedExShip Request Xml Start//
                    string FilePath = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/CarrierXml/FedExXml/{_OrderID}");
                    if (!Directory.Exists(FilePath))
                    {
                        Directory.CreateDirectory(FilePath);
                        string RequestFilePath = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/CarrierXml/FedExXml/{_OrderID}/" + _Count + "FedExRequest.xml");
                        GenrateXmlSerializer.Serialize(request, RequestFilePath);
                    }
                    else
                    {
                        string RequestFilePath = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/CarrierXml/FedExXml/{_OrderID}/" + _Count + "FedExRequest.xml");
                        GenrateXmlSerializer.Serialize(request, RequestFilePath);
                    }
                    // Save FedExShip Request Xml End//

                    //// Save FedExShip Response Xml Start//
                    string ResponseFilePath = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/CarrierXml/FedExXml/{_OrderID}/" + _Count + "FedExResponse.xml");
                    GenrateXmlSerializer.Serialize(reply, ResponseFilePath);
                    //// Save FedExShip Response Xml End//

                    if ((reply.HighestSeverity != NotificationSeverityType.ERROR) && (reply.HighestSeverity != NotificationSeverityType.FAILURE))
                    {
                        foreach (CompletedPackageDetail packageDetail in reply.CompletedShipmentDetail.CompletedPackageDetails)
                        {
                            using (MemoryStream ms = new MemoryStream(packageDetail.Label.Parts[0].Image))
                            {
                                int count = 0;
                                //System.Drawing.Image n = System.Drawing.Image.FromStream(ms);
                                foreach (PdfPage item in PdfReader.Open(ms, PdfDocumentOpenMode.Import).Pages)
                                {
                                    count++;
                                    if (count != 3)
                                    {
                                        PdfPage page = new PdfPage();
                                        page = item;
                                        pdfDoc.Pages.Add(page);
                                    }

                                }
                            }
                            if (_Count == 0)
                            {
                                _OrderDetail.tracking_no = packageDetail.TrackingIds[0].TrackingNumber;
                            }
                            tblOrderBox objOrderBox = new tblOrderBox();
                            int ID = Convert.ToInt32(_Packages[_Count].id);
                            var GetOrderboxes = Context.tblOrderBoxes.AsQueryable().Where(x => x.id == ID).Select(x => x).FirstOrDefault();
                            GetOrderboxes.tracking_no = packageDetail.TrackingIds[0].TrackingNumber;
                            Context.Entry(GetOrderboxes).State = System.Data.Entity.EntityState.Modified;
                            Context.SaveChanges();
                        }

                    }
                    else if (reply.HighestSeverity == NotificationSeverityType.ERROR || reply.HighestSeverity == NotificationSeverityType.WARNING || reply.HighestSeverity == NotificationSeverityType.FAILURE)
                    {
                        _FailMsg = "";
                        for (int j = 0; j < reply.Notifications.Length; j++)
                        {
                            _FailMsg += reply.Notifications[j].Message.ToString() + "\r\n";
                        }
                        _Success = false;
                    }

                }
                // Save outbound shipping label
                string Path = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/Orders/{_OrderID}");
                if (!Directory.Exists(Path))
                {
                    Directory.CreateDirectory(Path);
                    string filePath = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/Orders/{_OrderID}/") + _OrderDetail.tracking_no + "_Label.pdf";
                    pdfDoc.Save(filePath);
                }
                else
                {
                    string filePath = System.Web.HttpContext.Current.Server.MapPath($"~/uploads/Orders/{_OrderID}/") + _OrderDetail.tracking_no + "_Label.pdf";
                    pdfDoc.Save(filePath);
                }
                _Success = true;



            }
            catch (SoapException ex)
            {
                _FailMsg = ex.Detail.InnerText;
                _Success = false;
            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }

        private ProcessShipmentRequest CreateShipmentRequest()
        {
            ProcessShipmentRequest request = new ProcessShipmentRequest();
            try
            {
                request.WebAuthenticationDetail = new WebAuthenticationDetail();
                request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
                request.WebAuthenticationDetail.UserCredential.Key = _FedExApiKey;
                request.WebAuthenticationDetail.UserCredential.Password = _FedExPassword;
                request.ClientDetail = new ClientDetail();
                request.ClientDetail.AccountNumber = _FedExAccountNumber;
                request.ClientDetail.MeterNumber = _FedExMeterNumber;
                request.TransactionDetail = new TransactionDetail();
                Guid _guid = Guid.NewGuid();
                request.TransactionDetail.CustomerTransactionId = _guid.ToString().Substring(0, 8);
                request.Version = new VersionId();

                _SetshipmentDetails(request);

                _SetFromAddress(request);

                _SetShiptoAddress(request);



                _SetPayment(request);

                _SetLabelDetails(request);

                _SetPackageLineItems(request);

                _CustomerReference(request);

                _SetCustomsClearanceDetails(request);

            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
            return request;
        }
        private void _SetshipmentDetails(ProcessShipmentRequest request)
        {
            try
            {

                request.RequestedShipment = new RequestedShipment();
                request.RequestedShipment.ShipTimestamp = DateTime.Now;
                request.RequestedShipment.DropoffType = DropoffType.REGULAR_PICKUP;
                request.RequestedShipment.ServiceType = (ServiceType)Convert.ToInt32(_OrderDetail.service_code);
                request.RequestedShipment.PackagingType = PackagingType.YOUR_PACKAGING;
                request.RequestedShipment.PackageCount = "1";

            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }
       
        private void _SetFromAddress(ProcessShipmentRequest request)
        {
            try
            {
                request.RequestedShipment.Shipper = new Party();
                request.RequestedShipment.Shipper.Contact = new Contact();
                request.RequestedShipment.Shipper.Contact.CompanyName = "4InShip";
                request.RequestedShipment.Shipper.Contact.PersonName = "Jorfili";
                request.RequestedShipment.Shipper.Contact.PhoneNumber = ShipperAddressStaticModel.Phone;
                request.RequestedShipment.Shipper.Address = new Address();
                request.RequestedShipment.Shipper.Address.StreetLines = new string[1] { ShipperAddressStaticModel.Address + " " + _SuitNo };
                request.RequestedShipment.Shipper.Address.CountryCode = ShipperAddressStaticModel.CountryCode;
                request.RequestedShipment.Shipper.Address.City = ShipperAddressStaticModel.City;
                request.RequestedShipment.Shipper.Address.PostalCode = ShipperAddressStaticModel.Zip;
                request.RequestedShipment.Shipper.Address.StateOrProvinceCode = ShipperAddressStaticModel.State;
            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }
        private void _SetShiptoAddress(ProcessShipmentRequest request)
        {
            try
            {
                request.RequestedShipment.Recipient = new Party();
                request.RequestedShipment.Recipient.Contact = new Contact();
                request.RequestedShipment.Recipient.Contact.PersonName = _ToAddress.first_name + " " + _ToAddress.last_name;
                //request.RequestedShipment.Recipient.Contact.CompanyName = "chineseaddress";
                request.RequestedShipment.Recipient.Contact.PhoneNumber = _ToAddress.mobile1;

                request.RequestedShipment.Recipient.Address = new Address();
                request.RequestedShipment.Recipient.Address.StreetLines = new string[2] { _ToAddress.address1, _ToAddress.address2 };
                request.RequestedShipment.Recipient.Address.City = _ToAddress.city;
                // _Ratesrequest.RequestedShipment.Recipient.Address.StateOrProvinceCode = _ToAddress.state;
                request.RequestedShipment.Recipient.Address.PostalCode = _ToAddress.postal_code;
                request.RequestedShipment.Recipient.Address.CountryCode = _ToAddress.country_code;
            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }
        private void _SetPayment(ProcessShipmentRequest request)
        {
            try
            {
                request.RequestedShipment.ShippingChargesPayment = new Payment();
                request.RequestedShipment.ShippingChargesPayment.PaymentType = PaymentType.THIRD_PARTY;
                request.RequestedShipment.ShippingChargesPayment.Payor = new Payor();
                request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty = new Party();
                request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.AccountNumber = _FedExAccountNumber;
            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }
        private void _SetLabelDetails(ProcessShipmentRequest request)
        {
            try
            {

                request.RequestedShipment.LabelSpecification = new LabelSpecification();
                request.RequestedShipment.LabelSpecification.ImageType = ShippingDocumentImageType.PDF; // Image types PDF, PNG, DPL, ...
                request.RequestedShipment.LabelSpecification.ImageTypeSpecified = true;
                request.RequestedShipment.LabelSpecification.LabelFormatType = LabelFormatType.COMMON2D;
                request.RequestedShipment.LabelSpecification.LabelStockType = LabelStockType.PAPER_4X6;
                request.RequestedShipment.LabelSpecification.LabelPrintingOrientation = LabelPrintingOrientationType.BOTTOM_EDGE_OF_TEXT_FIRST;
                request.RequestedShipment.LabelSpecification.LabelPrintingOrientationSpecified = true;

            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }

        private void _SetPackageLineItems(ProcessShipmentRequest request)
        {

            try
            {
                request.RequestedShipment.RequestedPackageLineItems = new RequestedPackageLineItem[1];
                request.RequestedShipment.RequestedPackageLineItems[0] = new RequestedPackageLineItem();
                request.RequestedShipment.RequestedPackageLineItems[0].SequenceNumber = "1";
                // request.RequestedShipment.RequestedPackageLineItems[0].
                // Package weight information
                request.RequestedShipment.RequestedPackageLineItems[0].Weight = new Weight();
                request.RequestedShipment.RequestedPackageLineItems[0].Weight.Value = Convert.ToDecimal(Math.Ceiling(_Packages[_Count].actual_weight));
                request.RequestedShipment.RequestedPackageLineItems[0].Weight.Units = WeightUnits.LB;
                // package dimensions
                request.RequestedShipment.RequestedPackageLineItems[0].Dimensions = new Dimensions();
                request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Length = Convert.ToString(Math.Ceiling(_Packages[_Count].length ?? 0));
                request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Width = Convert.ToString(Math.Ceiling(_Packages[_Count].width ?? 0));
                request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Height = Convert.ToString(Math.Ceiling(_Packages[_Count].height ?? 0));
                request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Units = LinearUnits.IN;
              
            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }

        }
        private void _CustomerReference(ProcessShipmentRequest request)
        {
            request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences = new CustomerReference[1] { new CustomerReference() };
            request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences[0].CustomerReferenceType = CustomerReferenceType.CUSTOMER_REFERENCE;
            request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences[0].Value = _OrderDetail.reference_no;
        }
        private void _SetCustomsClearanceDetails(ProcessShipmentRequest request)
        {
            try
            {
                request.RequestedShipment.CustomsClearanceDetail = new CustomsClearanceDetail();
                request.RequestedShipment.CustomsClearanceDetail.DutiesPayment = new Payment();
                request.RequestedShipment.CustomsClearanceDetail.DutiesPayment.PaymentType = PaymentType.RECIPIENT;
                request.RequestedShipment.CustomsClearanceDetail.DutiesPayment.Payor = new Payor();
                request.RequestedShipment.CustomsClearanceDetail.DutiesPayment.Payor.ResponsibleParty = new Party();
                request.RequestedShipment.CustomsClearanceDetail.DutiesPayment.Payor.ResponsibleParty.AccountNumber = _FedExAccountNumber;
                request.RequestedShipment.CustomsClearanceDetail.DutiesPayment.Payor.ResponsibleParty.Contact = new Contact();
                request.RequestedShipment.CustomsClearanceDetail.DutiesPayment.Payor.ResponsibleParty.Address = new Address();
                request.RequestedShipment.CustomsClearanceDetail.DutiesPayment.Payor.ResponsibleParty.Address.CountryCode = "US";
                request.RequestedShipment.CustomsClearanceDetail.DocumentContent = InternationalDocumentContentType.NON_DOCUMENTS;

                request.RequestedShipment.CustomsClearanceDetail.CustomsValue = new Money();
                request.RequestedShipment.CustomsClearanceDetail.CustomsValue.Amount = Convert.ToDecimal(_OrderDetail.payable_amount ?? 0m);
                request.RequestedShipment.CustomsClearanceDetail.CustomsValue.Currency = "USD";

                //request.RequestedShipment.CustomsClearanceDetail.InsuranceCharges = new Money();
                //if (_Insurancecharges != null)
                //{
                //    request.RequestedShipment.CustomsClearanceDetail.InsuranceCharges.Amount = _Insurancecharges??0.0m;
                //    request.RequestedShipment.CustomsClearanceDetail.InsuranceCharges.Currency = "USD";
                //}

                _SetCommodityDetails(request);

            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }
       
        private void _SetCommodityDetails(ProcessShipmentRequest request)
        {
            try
            {

                if (_Count == 0)
                {

                    Context4InshipEntities Context = new Context4InshipEntities();
                    List<tblShipmentDetail> objtblShipmentDetail = new List<tblShipmentDetail>();
                    var GetRecshipId = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == _OrderID).Select(x => x).ToList();
                    foreach (var item in GetRecshipId)
                    {
                        List<tblShipmentDetail> ShipmentDetail = new List<tblShipmentDetail>();
                        ShipmentDetail = Context.tblShipmentDetails.AsQueryable().Where(q => q.Fk_shipment_id == item.id).ToList();
                        foreach (var items in ShipmentDetail)
                        {
                            objtblShipmentDetail.Add(new tblShipmentDetail() { description = items.description, purchase_price = items.purchase_price, quantity = items.quantity });
                        }
                    }
                    _shipmentdetail.purchase_price = objtblShipmentDetail.Sum(x => x.purchase_price);
                    _shipmentdetail.quantity = (objtblShipmentDetail.Sum(x => x.quantity));
                    _Description = objtblShipmentDetail.Select(x => x.description).ToArray();
                    for (int i = 0; i < _Description.Length; i++)
                    {
                        AddDiscription += _Description[i] + ",";
                        if (i == _Description.Count() - 1)
                        {
                            AddDiscription.Remove(AddDiscription.LastIndexOf(","), 1);
                        }
                    }
                }

                request.RequestedShipment.CustomsClearanceDetail.Commodities = new Commodity[1] { new Commodity() };
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].NumberOfPieces = "1";
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].Description = AddDiscription;
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].CountryOfManufacture = "US";
                //
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].Weight = new Weight();
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].Weight.Value = Convert.ToDecimal(_Packages[_Count].actual_weight);
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].Weight.Units = WeightUnits.LB;
                //
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].Quantity = _shipmentdetail.quantity;
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].QuantitySpecified = true;
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].QuantityUnits = "EA";
                //
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].UnitPrice = new Money();
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].UnitPrice.Amount = Convert.ToDecimal(_shipmentdetail.purchase_price);
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].UnitPrice.Currency = "USD";
                //
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].CustomsValue = new Money();
                // request.RequestedShipment.CustomsClearanceDetail.Commodities[0].CustomsValue.Amount =_shipmentdetail.purchase_price;
                request.RequestedShipment.CustomsClearanceDetail.Commodities[0].CustomsValue.Currency = "USD";

            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }
        private void SaveLabel(string labelFileName, byte[] labelBuffer)
        {
            try
            {

                // Save label buffer to file
                FileStream LabelFile = new FileStream(labelFileName, FileMode.Create);
                LabelFile.Write(labelBuffer, 0, labelBuffer.Length);
                LabelFile.Close();

            }
            catch (Exception ex)
            {
                _FailMsg = ex.Message.ToString();
                _Success = false;
            }
        }
        private void saveFile(string fileName, params byte[][] bytes)
        {
            try
            {
                PdfDocument outputDocument = new PdfDocument();
                for (int i = 0; i < bytes.Length; i++)
                {
                    using (MemoryStream stream = new MemoryStream(bytes[i]))
                    {
                        PdfDocument inputDocument = PdfReader.Open(stream, PdfDocumentOpenMode.Import);
                        foreach (PdfPage page in inputDocument.Pages)
                        {
                            outputDocument.AddPage(page); //throws the exception !!!
                        }
                    }
                }
                outputDocument.Save(fileName);
            }
            catch (Exception ex)
            {
                _FailMsg = ex.ToString();
                _Success = false;
            }
        }

        //private static void ShowTrackingDetails(TrackingId[] TrackingIds)
        //{
        //    // Tracking information for each package
        //    if (TrackingIds != null)
        //    {
        //        for (int i = 0; i < TrackingIds.Length; i++)
        //        {
        //            Console.WriteLine("Tracking # {0} Form ID {1}", TrackingIds[i].TrackingNumber, TrackingIds[i].FormId);
        //        }
        //    }
        //}

    }
