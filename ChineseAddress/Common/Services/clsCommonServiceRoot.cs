using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Xml;
using System.Xml.Serialization;

namespace _ChineseAddress.com.Common.Services
{
    /// <summary>
    /// Common like invoice refno
    /// </summary>
    public class clsCommonServiceRoot
    {

        private Context4InshipEntities Context;



        public clsCommonServiceRoot()
        {
            Context = new Context4InshipEntities();
        }

        public string GetInvoiceNumber()
        {
            string strInvoiceNumber = Convert.ToString(Context.tblInvoices.AsQueryable().Where(x => x.invoice_number != null).OrderByDescending(x => x.Id).Select(x => x.invoice_number).FirstOrDefault());
            if (strInvoiceNumber == null)
            {
                return "INV-0000001";
            }
            return "INV-" + (Convert.ToInt32(strInvoiceNumber.Replace("INV-", "")) + 1).ToString().PadLeft(7, '0');
        }

        public string[] CheckRole()
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            string[] User = authTicket.UserData.Split(';');
            return User;
        }

        //public IEnumerable<SelectListItem> GetMonths()
        //{
        //    IEnumerable<SelectListItem> months = new[]{
        //         new SelectListItem{ Text="01"},
        //         new SelectListItem{ Text="02"},
        //         new SelectListItem{ Text="03"},
        //         new SelectListItem{ Text="04"},
        //         new SelectListItem{ Text="05"},
        //         new SelectListItem{ Text="06"},
        //         new SelectListItem{ Text="07"},
        //         new SelectListItem{ Text="08"},
        //         new SelectListItem{ Text="09"},
        //         new SelectListItem{ Text="10"},
        //         new SelectListItem{ Text="11"},
        //         new SelectListItem{ Text="12"}
        //     };
        //    return months;
        //}
        //public List<SelectListItem> GetYears()
        //{
        //    List<SelectListItem> Years = new List<SelectListItem>();
        //    for (int i = 2017; i <= 2050; i++)
        //    {
        //        Years.Add(new SelectListItem { Text = i.ToString() });
        //    }
        //    return Years;
        //}

        public string GetInvoicePaidStatusLabel(int? paid_status)
        {
            if ((paid_status ?? 0) == 0)
            {
                return "<span class='label label-danger'>Failed</span>";
            }
            else if (paid_status == 1)
            {
                return "<span class='label label-warning'>Pending</span>";
            }
            else if (paid_status == 2)
            {
                return "<span class='label label-success'>Success</span>";
            }
            return "";
        }

        public void CreateOrderCustomPerfomaInvoice(int Orderid)
        {
            try
            {
                var Server = HttpContext.Current.Server;
                var GettblOrders = Context.tblOrders.AsQueryable().Where(x => x.id == Orderid).Select(x => x).FirstOrDefault();
                var Getinvoice = Context.tblInvoices.AsQueryable().Where(x => x.Fk_customer_id == GettblOrders.Fk_customer_id && x.Id == GettblOrders.Fk_invoice_id).Select(x => x).FirstOrDefault();
                var GetCustomerRefernceNumber = Context.tblCustomers.AsQueryable().Where(x => x.Id == GettblOrders.Fk_customer_id).Select(x => x.customer_reference).FirstOrDefault();
                var CustomerDeliveryAddress = Context.tblDeliveryAddresses.AsQueryable().Where(x => x.id == GettblOrders.Fk_delivery_address_id).Select(x => x).FirstOrDefault();
                //var GetAdreessbookData = Context.tblAddressBooks.Where(x => x.Fk_customer_Id == GettblOrders.Fk_customer_id).Select(x => x).FirstOrDefault();
                var GetReciveshipmentid = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == Orderid).Select(x => x.id).ToList();
                var GetCountryName = (from tbdel in Context.tblDeliveryAddresses join tbord in Context.tblOrders on tbdel.id equals tbord.Fk_delivery_address_id join tbcountry in Context.tblCountryMasters on tbdel.country_code equals tbcountry.country_code select new { tbord.id, tbcountry.country_name }).Where(x => x.id == Orderid).FirstOrDefault();
                var NetWeight = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == Orderid).Sum(x => x.billable_weight);
                var GetCountPackages = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == Orderid).ToList().Count;
                StringBuilder shipmentdetails = new StringBuilder();
                decimal TotalValues = 0;
                foreach (var item in GetReciveshipmentid)
                {
                    var GetShipmentDetails = Context.tblShipmentDetails.AsQueryable().Where(x => x.Fk_shipment_id == item).Select(x => x).ToList();
                    GetShipmentDetails.ForEach(x =>
                    {
                        decimal Totalvaluecurrency = x.purchase_price * x.quantity;
                        shipmentdetails.Append("<tr><td>" + x.description + "</td><td>China</td><td>" + x.comodity_code + "</td><td>" + x.harmonizedCode + "</td><td>" + x.quantity + "</td><td>" + x.purchase_price + "</td></tr>");//<td>" + Totalvaluecurrency + "</td>
                        TotalValues += Totalvaluecurrency;
                    });
                }
                Hashtable hashTable = new Hashtable();
                hashTable.Add("#invoicedate#", Getinvoice.invoice_date.Value.ToString("yyyy/MM/dd"));
                hashTable.Add("#byname#", "ChineseAddress");
                hashTable.Add("#byaddress#", "3133 Tianhe Ave, Suite" + GetCustomerRefernceNumber);
                hashTable.Add("#bycity#", "Guangzhou,  510000");
                hashTable.Add("#bycountry#", "China");
                hashTable.Add("#bytelephone#", Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ClientPhoneNo"]));
                hashTable.Add("#toname#", CustomerDeliveryAddress.first_name + " " + CustomerDeliveryAddress.last_name);
                hashTable.Add("#toaddress#", CustomerDeliveryAddress.address1 + "," + CustomerDeliveryAddress.address2);
                hashTable.Add("#tocity#", CustomerDeliveryAddress.city + ", " + CustomerDeliveryAddress.postal_code);
                hashTable.Add("#tocountry#", GetCountryName.country_name);
                hashTable.Add("#totelephone#", CustomerDeliveryAddress.mobile1);
                hashTable.Add("#shipmentdetail#", shipmentdetails);
                hashTable.Add("#demototal#", TotalValues);
                hashTable.Add("#billableweight#", NetWeight ?? 0.0m);
                hashTable.Add("#carrier#", GettblOrders.carrier);
                hashTable.Add("#reasonforexport#", ".................................");
                hashTable.Add("#termsofdelivery#", ".................................");
                hashTable.Add("#numberandkindpackage#", GetCountPackages);
                hashTable.Add("#name#", "");
                hashTable.Add("#placeanddate#", "");
                hashTable.Add("#signature#", "");
                string Invoicetemplate = (new clsEmailTemplateService()).GetEmailTemplate("customproforma_template", hashTable);
                string CssFile = HttpContext.Current.Server.MapPath("~/Content/css/CustomPerforma.min.css");
                string FilePath = Server.MapPath("~/Uploads/Orders/" + "/" + Orderid + "/") + Orderid + "_Customproforma.pdf";
                clsHTMLtoPDF objclsHTMLtoPDF = new clsHTMLtoPDF();
                objclsHTMLtoPDF.ConvertHTMLToPDFCustomerPerforma(Invoicetemplate, CssFile, FilePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendOrderDispatchedEmail(int Orderid, string TrackingNumber)
        {
            var GetCustomerEmail = (from tbcust in Context.tblCustomers
                                    join tbord in Context.tblOrders on tbcust.Id equals tbord.Fk_customer_id
                                    join tbAddressbook in Context.tblAddressBooks on tbcust.Id equals tbAddressbook.Fk_customer_Id
                                    select new { tbcust.email, tbAddressbook.first_name, tbAddressbook.last_name, tbord.id, tbord.Fk_customer_id, tbord.reference_no, tbord.tracking_no }).Where(x => x.id == Orderid).FirstOrDefault();
            if (GetCustomerEmail != null)
            {
                clsEmailTemplateService clscommn = new clsEmailTemplateService();
                string subject = "chineseaddress - Order Dispatch";
                clscommn = new clsEmailTemplateService();
                Hashtable htTemplate = new Hashtable();
                htTemplate.Add("#FirstName#", GetCustomerEmail.first_name);
                htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
                htTemplate.Add("#referenceno#", GetCustomerEmail.reference_no);
                htTemplate.Add("#trackingno#", GetCustomerEmail.tracking_no);
                string mailbody = (new clsEmailTemplateService()).GetEmailTemplate("awaitingdispatch", htTemplate);
                try
                {
                    (new clsSendEmailService()).SendEmail(new[] { GetCustomerEmail.email }, null, null, mailbody, subject);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public int GetFreeStorageDays(int ID, DateTime RecivedDateTime)
        {
            int TotalDays = 0;
            var GetPlansDays = Context.tblCustomerPlanLinkings.Where(x => x.Fk_Customer_Id == ID).Select(x => x).FirstOrDefault();
            if (GetPlansDays != null)
            {
                int TotalstorageDays = Convert.ToInt32((DateTime.Now.Date - RecivedDateTime).TotalDays);
                TotalDays = Convert.ToInt32(GetPlansDays.free_storage_days) - TotalstorageDays;
            }
            return TotalDays;
        }
        public void Errorhandling(Exception exception, string userName)
        {
            Context4InshipEntities objcontext = new Context4InshipEntities();

            tblExceptionlog objtblExceptionlog = new tblExceptionlog();
            try
            {
                var context = HttpContext.Current;
                var UserAgent = HttpContext.Current.Request.UserAgent;
                string strHostName = System.Net.Dns.GetHostName();
                string hostName = Dns.GetHostName();
                string Url = HttpContext.Current.Request.Url.ToString();
                objtblExceptionlog.ErrorDate = DateTime.Now;
                var ex = new StackTrace(exception, true);
                var frame = ex.GetFrame(0);
                objtblExceptionlog.ErrorLineNumber = Convert.ToString(frame.GetFileLineNumber());
                objtblExceptionlog.ErrorMessage = exception.Message.ToString();
                objtblExceptionlog.ErrorPage = frame.GetFileName();
                if (userName != null)
                    objtblExceptionlog.UserName = userName;
                objtblExceptionlog.IpAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();
                objtblExceptionlog.UserAgent = UserAgent;
                objtblExceptionlog.Url = Url;
                objcontext.Configuration.ValidateOnSaveEnabled = false;
                objcontext.tblExceptionlogs.Add(objtblExceptionlog);
                objcontext.SaveChanges();
            }
            catch (Exception ex)
            {
                objcontext.Entry(objtblExceptionlog).State = EntityState.Modified;
                objcontext.SaveChanges();
                throw ex;
            }
        }

        public static decimal EvaluteMathematicExpression(string expression, decimal length, decimal width, decimal height)
        {
            expression = expression.Replace("l", Convert.ToString(length)).Replace("w", Convert.ToString(width)).Replace("h", Convert.ToString(height));
            NCalc.Expression _exp = new NCalc.Expression(expression);
            return Convert.ToDecimal(_exp.Evaluate());
        }

        public ViewCustomerModel getCustomerViaEmail(string email)
        {
            ViewCustomerModel viewCustomerModel = (from c in Context.tblCustomers
                                                   join ca in Context.tblAddressBooks
                                                   on c.Id equals ca.Fk_customer_Id
                                                   where c.email == email && ca.is_default == true
                                                   select new ViewCustomerModel { email = c.email, status = c.status, FirstName = ca.first_name, LastName = ca.last_name }).FirstOrDefault();
            return viewCustomerModel;
        }

        public decimal getPlanChargePriceByCustomerId(int custId, string ChargeName)
        {
            try
            {
                var charge = (from cpl in Context.tblCustomerPlanLinkings
                              join p in Context.tblPlans on cpl.plan_title equals p.title
                              join pol in Context.tblPlanOptionLinkings on p.Id equals pol.Fk_plan_id
                              join po in Context.tblPackagingOptions on pol.Fk_packaging_option_id equals po.Id
                              orderby cpl.Id descending
                              where po.title == ChargeName
                              select pol.price).FirstOrDefault();
                return (charge ?? 0.00m);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool IsUserTopUpWithMinAmount(int ID)
        {
            try
            {
                decimal TotalCredit = (Context.tblRewardAmounts.AsQueryable().Where(x => x.Fk_CustomerId == ID).Sum(x => x.credit) ?? 0.0m);
                if (TotalCredit >= 50)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
public static class GenrateXmlSerializer
{
    public static string Serialize<T>(T filter, string FilePath)
    {
        string xml = null;
        using (StringWriter sw = new StringWriter())
        {

            XmlSerializer xs = new XmlSerializer(typeof(T));
            xs.Serialize(sw, filter);
            try
            {
                xml = sw.ToString();

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xml);
        doc.Save(FilePath);
        return xml;
    }
}