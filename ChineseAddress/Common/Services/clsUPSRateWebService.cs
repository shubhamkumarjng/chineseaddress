using _ChineseAddress.com.Common.Models;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.UPSRateWebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace _ChineseAddress.com.Common.Services
{
    public class clsUPSRateWebService : Areas.Admin.Models.clsCommon
    {
        #region Properties && Readonly Variables

        /// <summary>
        /// Get SUCCESS or ERROR msg from this acessor
        /// </summary>
        public string _msg { get; set; }

        /// <summary>
        /// Get SUCCESS or ERROR from this acessor
        /// </summary>
        public bool _isSuccess { get; set; } = false;

        private static readonly string _UPSAPIUserName = System.Configuration.ConfigurationManager.AppSettings["UPSAPIUserName"].ToString();
        private static readonly string _UPSAPIPassword = System.Configuration.ConfigurationManager.AppSettings["UPSAPIPassword"].ToString();
        private static readonly string _UPSAPIAccessLicenseNumber = System.Configuration.ConfigurationManager.AppSettings["UPSAPIAccessLicenseNumber"].ToString();
        private static readonly string _UPSAPIShipperNumber = System.Configuration.ConfigurationManager.AppSettings["UPSAPIShipperNumber"].ToString();

        /// <summary>
        /// Carrier List
        /// </summary>
        public tblCarriersDetail _Carrier { get; set; } = null;
        public int Bussinessdays { get; set; } = 0;
        /// <summary>
        /// This Accessor from ToAddress
        /// </summary>
        public tblDeliveryAddress _ToAddress { get; set; }

        public List<tblOrderBox> _Packages { get; set; }
        public string _SuitNo { get; set; }
        private StringBuilder sb = new StringBuilder();

        #endregion Properties && Readonly Variables

        #region Static Variables

        private static readonly CodeDescriptionType _packageType = new CodeDescriptionType() { Code = "02" };
        private static readonly CodeDescriptionType _packageWeightUnitOfMeasurement = new CodeDescriptionType() { Code = "LBS", Description = "Pounds" };
        private static readonly CodeDescriptionType _packagesUnitOfMeasurement = new CodeDescriptionType() { Code = "IN", Description = "Inches" };

        //response
        private HttpResponse response = HttpContext.Current.Response;

        #endregion Static Variables

        #region CallServiceMetods
        public DateTime GetDay { get; set; }
         Context4InshipEntities Context = new Context4InshipEntities();
        public List<carriersRatesResponseModel> CalcRate()
        {
            List<carriersRatesResponseModel> carrierrates = new List<carriersRatesResponseModel>();
            try
            {
                RateService rate = new RateService();
                RateRequest rateRequest = new RateRequest();

                //getUPSSecurity
                rate.UPSSecurityValue = getUPSSecurity();
                //getRequsetType
                if (_Carrier != null)
                {
                    rateRequest.Request = getRequestType(new[] { "Rate" });
                }
                else
                    rateRequest.Request = getRequestType(new[] { "Shoptimeintransit" });//Shoptimeintransit

                //Add Shipper,from and To Address into Shipment Type object
                ShipmentType shipment = new ShipmentType();

                //get static shipment Shipper and from  Address
                Tuple<ShipperType, ShipFromType> ShipperAndFromAddress = getShipperandFromAddress();
                shipment.Shipper = ShipperAndFromAddress.Item1;
                shipment.ShipFrom = ShipperAndFromAddress.Item2;

                //To Address
                shipment.ShipTo = getToAddress();

                if (_Carrier != null)
                {
                    CodeDescriptionType service = new CodeDescriptionType();
                    service.Code = _Carrier.code;
                    service.Description = _Carrier.carrier_name;
                    shipment.Service = service;
                }
                //Below code uses dummy date for reference. Please udpate as required.

                shipment.Package = getPackages().ToArray<PackageType>();


                //Add Below code to get negotiate rates
                shipment.ShipmentRatingOptions = new ShipmentRatingOptionsType();
                shipment.ShipmentRatingOptions.NegotiatedRatesIndicator = "";

                //ShipmentTotalWeight Required when Type "Shoptimeintransit"
                shipment.ShipmentTotalWeight = (new ShipmentWeightType() { UnitOfMeasurement = _packageWeightUnitOfMeasurement, Weight = _Packages.Sum(x => x.actual_weight).ToString() });

                shipment.DeliveryTimeInformation = new TimeInTransitRequestType();
                shipment.DeliveryTimeInformation.PackageBillType = "03";

                shipment.DeliveryTimeInformation.Pickup = new PickupType();
                var DayOfWeek = DateTime.Now.DayOfWeek.ToString();
                var GetTodayDate = DateTime.Now;
                var CarrierHolidayDate = Context.tblcarrierholidays.AsQueryable().Where(x => x.holidaydate == GetTodayDate).FirstOrDefault();
                if (DayOfWeek.ToLower() == "sunday")
                {
                    GetDay = GetTodayDate.AddDays(1);
                }
                else if (DayOfWeek.ToLower() == "saturday")
                {
                    GetDay = GetTodayDate.AddDays(2);
                }
                else if (DayOfWeek.ToLower() == "saturday" && CarrierHolidayDate != null)
                {
                    GetDay = GetTodayDate.AddDays(3);
                }
                else if (CarrierHolidayDate != null)
                {
                    GetDay = (DateTime)CarrierHolidayDate.holidaydate;
                }
                if (GetDay.Day != 1)
                {
                    shipment.DeliveryTimeInformation.Pickup.Date = GetDay.ToString("yyyyMMdd");
                }
                else
                {
                    shipment.DeliveryTimeInformation.Pickup.Date = DateTime.Now.ToString("yyyyMMdd");
                }
                shipment.DeliveryTimeInformation.Pickup.Time = DateTime.Now.ToString("HHmmss");

                shipment.InvoiceLineTotal = new InvoiceLineTotalType();
                shipment.InvoiceLineTotal.CurrencyCode = "USD";
                shipment.InvoiceLineTotal.MonetaryValue = "10";

                rateRequest.Shipment = shipment;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                RateResponse rateResponses = rate.ProcessRate(rateRequest);
                if (rateResponses != null)
                {
                    if (rateResponses.Response.ResponseStatus.Code == "1")
                    {
                        _isSuccess = true;
                        ChargesType res_ct = new ChargesType();
                        foreach (RatedShipmentType ratedShipment in rateResponses.RatedShipment)
                        {
                            if (ratedShipment.TimeInTransit != null)
                            {
                                if (ratedShipment.TimeInTransit.ServiceSummary[0].EstimatedArrival.BusinessDaysInTransit != null)
                                {
                                    Bussinessdays = Convert.ToInt32(ratedShipment.TimeInTransit.ServiceSummary[0].EstimatedArrival.TotalTransitDays);
                                }
                            }
                            res_ct = (ratedShipment.NegotiatedRateCharges == null ? ratedShipment.TotalCharges : ratedShipment.NegotiatedRateCharges.TotalCharge);
                            carrierrates.Add(new carriersRatesResponseModel() { TotalShipmentCharge = Convert.ToDecimal(res_ct.MonetaryValue), Carrier = "UPS", Service_Code = ratedShipment.Service.Code, Service = ratedShipment.Service.Description,BussinessDays= Bussinessdays });
                        }
                        return carrierrates;
                    }
                    else
                    {
                        _isSuccess = false;
                        _msg = $"Error occured while calculationg ups carrier prices. Code:{rateResponses.Response.ResponseStatus.Code} and Description:{rateResponses.Response.ResponseStatus.Description}";
                    }
                }
                else
                {
                    _msg = $"Error occured while calculationg ups carrier prices.";
                    _isSuccess = false;
                }
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                sb.Clear();
                sb.AppendLine("SoapException Message= " + ex.Message);
                sb.AppendLine("SoapException Category:Code:Message= " + ex.Detail.LastChild.InnerText);
                sb.AppendLine("SoapException XML String for all= " + ex.Detail.LastChild.OuterXml);
                sb.AppendLine("SoapException StackTrace= " + ex.StackTrace);
                _msg = sb.ToString();
                _isSuccess = false;
            }
            //catch (System.ServiceModel.CommunicationException ex)
            //{
            //    _isSuccess = false;
            //    sb.Clear();
            //    sb.AppendLine("CommunicationException= " + ex.Message);
            //    sb.AppendLine("CommunicationException-StackTrace= " + ex.StackTrace);
            //    _msg = ErrorMsg(sb.ToString());
            //}
            catch (Exception ex)
            {
                sb.Clear();
                sb.AppendLine(" Generaal Exception= " + ex.Message);
                sb.AppendLine(" Generaal Exception-StackTrace= " + ex.StackTrace);
                _msg = sb.ToString();
                _isSuccess = false;
            }
            return carrierrates;
        }

        //public string CreateShipmentAndGenerateLabel()
        //{
        //    UPSShipWebService.ShipService shpSvc = new UPSShipWebService.ShipService();
        //    UPSShipWebService.ShipmentRequest shipmentRequest = new UPSShipWebService.ShipmentRequest();
        //    shpSvc.UPSSecurityValue = getUPSSecurity<UPSShipWebService.UPSSecurity>();

        //    return "";
        //}

        #endregion CallServiceMetods

        #region Common and miscellaneous methods

        /// <summary>
        /// UPSSecurity
        /// </summary>
        /// <returns></returns>
        private static UPSSecurity getUPSSecurity()
        {
            UPSSecurity upss = new UPSSecurity();

            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = _UPSAPIAccessLicenseNumber;
            upss.ServiceAccessToken = upssSvcAccessToken;

            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = _UPSAPIUserName;
            upssUsrNameToken.Password = _UPSAPIPassword;
            upss.UsernameToken = upssUsrNameToken;
            return upss;
        }

        /// <summary>
        /// get Request Types like Rate,Shop etc
        /// </summary>
        /// <param name="Types"></param>
        /// <returns></returns>
        private RequestType getRequestType(string[] Types)
        {
            RequestType request = new RequestType();
            try
            {
                request.RequestOption = Types;//"Rate","Rateintransit","Shoptimeintransit"
            }
            catch (Exception ex)
            {
                _msg = ex.ToString();
                _isSuccess = false;
            }

            return request;
        }

        /// <summary>
        /// Get Static From Address
        /// </summary>
        /// <returns></returns>
        private static Tuple<ShipperType, ShipFromType> getShipperandFromAddress()
        {
            //Shipper Address
            ShipperType shipper = new ShipperType();
            shipper.ShipperNumber = _UPSAPIShipperNumber;

            AddressType shipperAddress = new AddressType();
            shipperAddress.AddressLine = new string[] { ShipperAddressStaticModel.Address };
            shipperAddress.City = ShipperAddressStaticModel.City;
            shipperAddress.PostalCode = ShipperAddressStaticModel.Zip;
            shipperAddress.StateProvinceCode = ShipperAddressStaticModel.State;
            shipperAddress.CountryCode = ShipperAddressStaticModel.CountryCode;

            shipper.Address = shipperAddress;

            //From Address
            ShipFromType shipFrom = new ShipFromType();
            ShipAddressType shipFromAddress = new ShipAddressType();
            shipFromAddress.AddressLine = new[] { ShipperAddressStaticModel.Address };
            shipFromAddress.City = ShipperAddressStaticModel.City;
            shipFromAddress.PostalCode = ShipperAddressStaticModel.Zip;
            shipFromAddress.StateProvinceCode = ShipperAddressStaticModel.State;
            shipFromAddress.CountryCode = ShipperAddressStaticModel.CountryCode;
            shipFrom.Address = shipFromAddress;
            return Tuple.Create(shipper, shipFrom);
        }

        /// <summary>
        /// Get To Address from _TOAddress Accessors
        /// </summary>
        /// <returns></returns>
        public ShipToType getToAddress()
        {
            ShipToType shipTo = new ShipToType();
            try
            {
                ShipToAddressType shipToAddress = new ShipToAddressType();
                // shipToAddress.AddressLine = new[] { (_ToAddress.address1 ?? ""), (_ToAddress.address2 ?? "") };
                shipToAddress.City = (_ToAddress.city ?? "");
                shipToAddress.PostalCode = (_ToAddress.postal_code ?? "");
                //shipToAddress.StateProvinceCode = (_ToAddress.state ?? "AE");
                shipToAddress.CountryCode = _ToAddress.country_code;
                shipTo.Address = shipToAddress;
            }
            catch (Exception ex)
            {
                _msg = ex.ToString();
                _isSuccess = false;
            }
            return shipTo;
        }

        public List<PackageType> getPackages()
        {
            List<PackageType> lstpackage = new List<PackageType>();
            try
            {
                DimensionsType packageDimension = new DimensionsType();
                PackageWeightType packageWeight = new PackageWeightType();

                if (_Packages != null)
                {
                    foreach (tblOrderBox _package in _Packages)
                    {
                        PackageType package = new PackageType();

                        //Add Package weight
                        packageWeight.Weight = Convert.ToString(_package.actual_weight);
                        packageWeight.UnitOfMeasurement = _packageWeightUnitOfMeasurement;
                        package.PackageWeight = packageWeight;
                        ////Add Package Dim-Weight
                        //packageWeight.Weight = Convert.ToString(_package.dim_weight);
                        //package.DimWeight = packageWeight;
                        //Add Dimension
                        if (_package.length != null)
                        {
                            packageDimension.Length = Convert.ToString(_package.length);
                            packageDimension.Width = Convert.ToString(_package.width);
                            packageDimension.Height = Convert.ToString(_package.height);
                            packageDimension.UnitOfMeasurement = _packagesUnitOfMeasurement;
                            package.Dimensions = packageDimension;
                        }
                        //Add Package Type
                        package.PackagingType = _packageType;

                        lstpackage.Add(package);
                    }
                }
            }
            catch (Exception ex)
            {
                _msg = ex.ToString();
                _isSuccess = false;
            }
            return lstpackage;
        }

        #endregion Common and miscellaneous methods
    }
}