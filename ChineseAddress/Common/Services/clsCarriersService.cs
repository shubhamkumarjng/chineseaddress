using _ChineseAddress.com.Common.Models;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using NCalc;

namespace _ChineseAddress.com.Common.Services
{
    public class clsCarriersService : Areas.Admin.Models.clsCommon
    {
        #region Properties

        private Context4InshipEntities context;
        public int? _orderId { get; set; } = null;
        public int[] _ReceivedShipmentIds = null;
        public tblCarriersDetail _carrierDetail { get; set; }
        public tblOrder _orderdetails { get; set; }
        public decimal? _Insurancecharges { get; set; }
        public tblDeliveryAddress _ToAddress { get; set; }
        public List<tblOrderBox> _Packages { get; set; }
        public List<carriersRatesResponseModel> _carriersRates { get; set; }
        public string _SuitNo { get; set; }
        public string _msg { get; set; }
        public bool _IsSuccess { get; set; }
        public static string _ClientEmail { get; set; }
        #endregion Properties

        public clsCarriersService()
        {
            context = new Context4InshipEntities();
        }

        public void getCarrierPrices()
        {
            try
            {
                if (_orderId != null)
                {
                    _ToAddress = (from odr in context.tblOrders
                                  join da in context.tblDeliveryAddresses on odr.Fk_delivery_address_id equals da.id
                                  where odr.id == _orderId
                                  select da).FirstOrDefault();
                    _carrierDetail = (from odr in context.tblOrders
                                      join cd in context.tblCarriersDetails
                                      on odr.service_code equals cd.code
                                      where odr.carrier == cd.carrier_name
                                      && odr.product == cd.product
                                      && odr.service == cd.service
                                      && (cd.status ?? false) == true
                                      && odr.id == _orderId
                                      select cd).FirstOrDefault();
                    _Packages = (from odr in context.tblOrders
                                 join ob in context.tblOrderBoxes
                                 on odr.id equals ob.Fk_order_id
                                 where odr.id == _orderId
                                 select ob).ToList();
                    //Add Insurance on carrier start//
                    decimal? TotalShipmentPrice = 0;
                    var GetShipmentDetailData = context.tblShipmentDetails.Where(x => x.Fk_shipment_id == _orderId).Select(x => x).ToList();
                    GetShipmentDetailData.ForEach(x =>
                    {
                        decimal SumTotalShipment = (x.purchase_price) * (x.quantity);
                        TotalShipmentPrice += SumTotalShipment;
                    });
                    _Insurancecharges = TotalShipmentPrice;
                    //Add Insurance on carrier start//
                    _orderdetails = context.tblOrders.AsQueryable().Where(x => x.id == _orderId).Select(x => x).FirstOrDefault();
                    //Call particular rate method by carrier name
                    InvokeAllCarrierRatesFunctionsNew(_orderdetails.service_code);
                    //MethodInfo info = this.GetType().GetMethod($"get{_orderdetails.carrier}Rates");
                    //Action getRates = new Action(delegate () { info.Invoke(this, null); });
                    //getRates.Invoke();
                    //if (_carriersRates == null)
                    //    _carriersRates = new List<carriersRatesResponseModel>();
                    //if (_carriersRates.Any())
                    //    _IsSuccess = true;
                    //else
                    //    _IsSuccess = false;
                }
                else if (_ReceivedShipmentIds != null)
                {
                    List<tblReceivedShipment> lstreceivedShipment = context.tblReceivedShipments.AsQueryable().Where(x => _ReceivedShipmentIds.Contains(x.id)).Select(x => x).ToList();

                    if (lstreceivedShipment.Any())
                    {
                        if (_Packages == null)
                            _Packages = new List<tblOrderBox>();
                        foreach (tblReceivedShipment x in lstreceivedShipment)
                        {
                            _Packages.Add(new tblOrderBox() { height = x.height, length = x.length, width = x.width, actual_weight = (x.weight ?? 0) });
                        }
                        InvokeAllCarrierRatesFunctionsNew();
                    }
                }
                else
                {
                    //Public Rates Page
                    InvokeAllCarrierRatesFunctionsNew();
                }
            }
            catch (Exception ex)
            {
                _msg = ex.Message;
                _IsSuccess = false;
            }
        }

        private void InvokeAllCarriersRatesFunction()
        {
            try
            {
                IEnumerable<string> lstCarriersDetail = context.tblCarriersDetails.Where(x => x.status == true).Select(x => x.carrier_name).Distinct().AsEnumerable();
                List<Action> lstAction = new List<Action>();
                foreach (string Carrier_Name in lstCarriersDetail)
                {
                    MethodInfo info = this.GetType().GetMethod($"get{Carrier_Name}Rates");
                    if (info != null)
                    {
                        lstAction.Add(new Action(delegate () { info.Invoke(this, null); }));
                    }
                }
                if (lstAction.Any())
                {
                    try
                    {
                        foreach (Action item in lstAction)
                        {
                            item.Invoke();
                        }
                        if (_carriersRates != null)
                        {
                            if (_carriersRates.Any())
                                _IsSuccess = true;
                            else
                                _IsSuccess = false;
                        }
                        else
                            _IsSuccess = false;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// shubham CA 
        /// </summary>
        private void InvokeAllCarrierRatesFunctionsNew(string service_code = null)
        {
            IEnumerable<tblCarriersDetail> lstCarriersDetail = context.tblCarriersDetails.Where(x => x.status == true && x.code == (service_code ?? x.code)).Select(x => x).Distinct().AsEnumerable();
            carriersRatesResponseModel _carrierRate;
            foreach (var _Carrier in lstCarriersDetail)
            {
                //_CarrierDetail.vol_weight_factor
                _carrierRate = new carriersRatesResponseModel() { Carrier = _Carrier.carrier_name, Product = _Carrier.product, Service = _Carrier.service, Service_Code = _Carrier.code };

                foreach (tblOrderBox _package in _Packages)
                {
                    calcBillableWeight(_Carrier.vol_weight_factor, _package);
                    var retPrice = context.sp_ef_calcRates(_carrierRate.Service, _carrierRate.Service_Code, _package.billable_weight, _ToAddress.country_code);
                    var objtblPrice = retPrice.AsEnumerable().Select(x => x).FirstOrDefault();
                    if (objtblPrice != null)
                    {
                        _carrierRate.TotalShipmentCharge += objtblPrice.price;
                        if (_carrierRate.product == null)
                            _carrierRate.product = new List<tblOrderBox>();
                        _carrierRate.product.Add(_package);
                        _carrierRate.strBussinessDays = objtblPrice.delivery_time;

                    }
                }
                if (_carrierRate.TotalShipmentCharge > 0)
                {
                    if (_carriersRates == null)
                    {
                        _carriersRates = new List<carriersRatesResponseModel>();
                        _IsSuccess = true;
                    }

                    _carriersRates.Add(_carrierRate);
                }

            }
            if (_carriersRates == null || _carriersRates.Any() == false)
            {
                _msg = "Price not caluculate for this shipment";
            }

        }
        private void calcBillableWeight(string expression, tblOrderBox _Package)
        {
            try
            {
                expression = expression.Replace("l", Convert.ToString(_Package.length)).Replace("w", Convert.ToString(_Package.width)).Replace("h", Convert.ToString(_Package.height));
                Expression _exp = new Expression(expression);
                _Package.dim_weight = Convert.ToDecimal(_exp.Evaluate());
                if (_Package.dim_weight > _Package.actual_weight)
                    _Package.billable_weight = _Package.dim_weight;
                else
                    _Package.billable_weight = _Package.actual_weight;
            }
            catch { }
        }
        public void getUPSRates()
        {
            try
            {
                clsUPSRateWebService objUPSCarrier = new clsUPSRateWebService();
                objUPSCarrier._ToAddress = _ToAddress;
                objUPSCarrier._Carrier = _carrierDetail;
                objUPSCarrier._Packages = _Packages;
                List<carriersRatesResponseModel> upsCarierRates = objUPSCarrier.CalcRate();
                if (objUPSCarrier._isSuccess == true)
                {
                    _IsSuccess = true;
                    if (_carriersRates != null)
                    {
                        _carriersRates.AddRange(upsCarierRates);
                    }
                    else
                    {
                        _carriersRates = new List<carriersRatesResponseModel>();
                        _carriersRates.AddRange(upsCarierRates);
                    }
                }
                else
                {
                    _IsSuccess = false;
                    _msg = objUPSCarrier._msg;
                    string UserEmail = CheckCurrentUserLogin();
                    SendEmailApiError(_orderdetails.service, _carrierDetail.carrier_name, _msg, UserEmail, _orderdetails.reference_no);
                }
            }
            catch (Exception ex)
            {
                _msg = (ex.Message);
                _IsSuccess = false;
                string UserEmail = CheckCurrentUserLogin();
                if (_orderdetails != null)
                    SendEmailApiError(_orderdetails.service, _carrierDetail.carrier_name, _msg, UserEmail, _orderdetails.reference_no);
            }
        }

        public void getFedExRates()
        {
            clsFedExRateService objclsFedExRateService = new clsFedExRateService();
            objclsFedExRateService._ToAddress = _ToAddress;
            objclsFedExRateService._Carrier = _carrierDetail;
            objclsFedExRateService._Packages = _Packages;
            List<carriersRatesResponseModel> FedexcarrierRates = objclsFedExRateService._GetRatesApi();
            if (objclsFedExRateService._Success == true)
            {
                _IsSuccess = true;
                if (_carriersRates != null)
                {
                    _carriersRates.AddRange(FedexcarrierRates);
                }
                else
                {
                    _carriersRates = new List<carriersRatesResponseModel>();
                    _carriersRates.AddRange(FedexcarrierRates);
                }
            }
            else
            {
                _msg = clsFedExRateService._FailMsg;
                string UserEmail = CheckCurrentUserLogin();
                if (_orderdetails != null)
                    SendEmailApiError(_orderdetails.service, _carrierDetail.carrier_name, _msg, UserEmail, _orderdetails.reference_no);
            }
        }


        public void CallShippingApi()
        {
            System.Reflection.MethodInfo theMethod = this.GetType().GetMethod(_orderdetails.carrier + "ShippingApiFunc",
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            if (theMethod != null)
                theMethod.Invoke(this, null);
            else
            {
            }
        }

        private void UPSShippingApiFunc()
        {
            try
            {

                clsUPSShipWebService objclsUPSShipWebService = new clsUPSShipWebService();
                objclsUPSShipWebService._OrderDetail = _orderdetails;
                objclsUPSShipWebService._SuitNo = _SuitNo;
                objclsUPSShipWebService._ToAddress = _ToAddress;
                objclsUPSShipWebService._Packages = _Packages;
                objclsUPSShipWebService._Insurancecharges = _Insurancecharges;
                objclsUPSShipWebService.CreateShipmentAndGenerateLabel();
                if (objclsUPSShipWebService._isSuccess)
                {
                    _orderdetails = objclsUPSShipWebService._OrderDetail;
                    _IsSuccess = true;
                }
                else
                {
                    _msg = objclsUPSShipWebService._msg.ToString();
                    _IsSuccess = false;
                    string UserEmail = CheckCurrentUserLogin();
                    SendEmailApiError(_orderdetails.service, _orderdetails.carrier, _msg, UserEmail, _orderdetails.reference_no);
                }

            }
            catch (Exception ex)
            {
                _msg = ex.ToString();
                string UserEmail = CheckCurrentUserLogin();
                SendEmailApiError(_orderdetails.service, _orderdetails.carrier, _msg, UserEmail, _orderdetails.reference_no);
            }
        }
        private void FedExShippingApiFunc()
        {
            try
            {

                clsFedExShipWebService objclsFedExShipWebService = new clsFedExShipWebService();
                objclsFedExShipWebService._ToAddress = _ToAddress;
                objclsFedExShipWebService._Carrier = _carrierDetail;
                objclsFedExShipWebService._Packages = _Packages;
                objclsFedExShipWebService._OrderID = _orderdetails.id;
                objclsFedExShipWebService._OrderDetail = _orderdetails;
                objclsFedExShipWebService._Insurancecharges = _Insurancecharges;
                objclsFedExShipWebService._SuitNo = _SuitNo;
                objclsFedExShipWebService._getShipApi();
                if (objclsFedExShipWebService._Success == true)
                {
                    _orderdetails = objclsFedExShipWebService._OrderDetail;
                    _IsSuccess = true;
                }
                else
                {
                    _msg = objclsFedExShipWebService._FailMsg.ToString();
                    _IsSuccess = false;
                    string UserEmail = CheckCurrentUserLogin();
                    SendEmailApiError(_orderdetails.service, _orderdetails.carrier, _msg, UserEmail, _orderdetails.reference_no);
                }

            }
            catch (Exception ex)
            {
                _msg = ex.ToString();
                string UserEmail = CheckCurrentUserLogin();
                SendEmailApiError(_orderdetails.service, _orderdetails.carrier, _msg, UserEmail, _orderdetails.reference_no);
            }
        }
        public void SendEmailApiError(string _CarrierServices, string _CarrierName, string _Msg, string UserName, string OrderRefernceNo)
        {

            try
            {
                clsSendEmailService objclsSendEmailService = new clsSendEmailService();
                _ClientEmail = ConfigurationManager.AppSettings["ClientMail"].ToString();
                clsEmailTemplateService objclsEmailTemplateService = new clsEmailTemplateService();
                Hashtable objhashtable = new Hashtable();
                objhashtable.Add("#CarrierName#", _CarrierName);
                objhashtable.Add("#Msg#", _Msg);
                objhashtable.Add("#UserName#", UserName);
                objhashtable.Add("#OrderRefernceNo#", OrderRefernceNo);
                objhashtable.Add("#Date#", DateTime.Now);
                objhashtable.Add("#CarrierServices#", _CarrierServices);
                string htmlTemaplate = objclsEmailTemplateService.GetEmailTemplate("apierroremail", objhashtable);
                objclsSendEmailService.SendEmail(new[] { _ClientEmail }, null, null, htmlTemaplate, "4InShip - Api Error");

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public string CheckCurrentUserLogin()
        {
            FormsAuthenticationTicket authTicket;
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            string UserEmail;
            if (authCookie != null)
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                string[] UserName = authTicket.UserData.Split(';');

                if (UserName.Length == 3)
                {
                    UserEmail = "Admin";
                }
                else
                    UserEmail = UserName[3];
            }
            UserEmail = "Anonymous User";
            return UserEmail;
        }

    }
}