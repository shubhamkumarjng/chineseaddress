using System;
using System.Collections;
using System.Text;

namespace _ChineseAddress.com.Common.Services
{
    public class clsEmailTemplateService
    {
        /// <summary>
        /// Define Template here and TemlateKey in a small characters
        /// </summary>
        /// <param name="TemplateKey"></param>
        /// <returns></returns>
        public StringBuilder GetTemplate(string TemplateKey)
        {
            StringBuilder sb = new StringBuilder();

            if (TemplateKey == "signup")
            {
                sb.Append(@"
            <div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 16px; line-height:26px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                        <span style=""font-size: 16px;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                            Thank you for signing up with ChineseAddresss.<br /><br />
                            If you have any questions, please reach us by email at: <a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a> or by phone at: +8618871488894.<br />
                            You may also visit our <a href=""http://www.Chineseaddress.com/faqs""> FAQs</a> and <a href=""http://www.Chineseaddress.com/termsandconditions"">Terms and Services</a>  for more information.<br /><br />
                            To get started, please log in at <a href=""http://www.Chineseaddress.com""> www.Chineseaddress.com </a> with your login as bellow:<br/>
                            Username: <strong>#username#</strong>  <br/>
                            Password: <strong>#password#.</strong> <br /><br />
                            We look forward to helping you shop, ship, and save money with our forwarding and consolidation services.
                        </span><br /><br />
                        <span style=""font-size:16px"">Sincerely,</span><br />
                        <span style=""font-size:16px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "recivedshipment")
            { sb.Append(@"<div style='padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;'>
    <div>
        <p style='font-size: 16px; line-height:26px;'>
            <span style='font-size: 16px; font-family: gotham, helvetica, arial, sans-serif !important;'>
                Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                Your shipment with following information is received by ChineseAddress.<br />
                Carrier:<strong>#carrier#</strong><br />
                Sender:<strong>#sender#</strong><br />
                Tracking Number:<strong>#trackingno#</strong><br />
                If you have any questions about your shipment, please contact us by email at:<a href='mailto:info@ChineseAddresss.com'>info@ChineseAddresss.com</a>  or by phone at: +8618871488894.
            </span><br /><br />
            <span style='font-size:16px'>Sincerely,</span><br />
            <span style='font-size:16px'>The ChineseAddress Staff</span>
        </p>
    </div>
</div> "); }
            else if (TemplateKey == "ordercreate")
            {
                sb.Append(@"<div style='padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;'>
    <div>
        <p style='font-size: 13px; line-height:26px; font-family: gotham, helvetica, arial, sans-serif !important;'>
            <span style='font-size: 14px;'>
                Dear <strong> #FirstName#  #LastName# </strong><br /><br /><br/>
                Your order is created with following information.<br />
                Order Number:<strong>#order_no#</strong><br />
                Shipping Method:<strong>#shipping_method#</strong><br />
                Status:<strong>#status#</strong><br />
                If you have any questions about your shipment, please contact us by email at:<a href='mailto:info@ChineseAddresss.com'>info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
            </span><br /><br />
            <span style='font-size:15px'>Sincerely,</span><br />
            <span style='font-size:15px'>The ChineseAddress Staff</span>
        </p>
    </div>
</div>");
            }
            else if (TemplateKey == "orderproccessing")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                            Your order is currently being processed.  Your reference number is:  <strong>#referenceno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "orderonhold")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                           Your order is currently on hold.  Your reference number is:  <strong>#referenceno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "orderunhold")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                          Your order has been released.  Your reference number is: <strong>#referenceno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "orderabandon")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                          Your order has been abandoned or cancelled. <br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "restricted")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                         Your order is restricted and cannot be shipped.  Your reference number is: <strong>#referenceno#</strong><br />
                          <strong>Restricted reason:</strong> #Msg#  Click here to check  <a href=""http://www.Chineseaddress.com/privacy"">Privacy Policy</a> <br/>
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "invoicepricechanged")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                         Your invoice price has changed.  Your tracking number is: <strong>#trackingno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "invoicepricecancel")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                         Your invoice price has been cancelled.  Your tracking number is: <strong>#trackingno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "confirmorder")
            {
                sb.Append(@"<div style=""padding: 15px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                         Your order is confirmed and is awaiting payment.  Your reference number is: <strong>#refernceno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "awaitingdispatch")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                        Your order is awaiting dispatch.  Your reference number is: <strong>#referenceno#</strong>  no.<br />
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "orderpayment")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                          Your order has been dispatched.  Your reference number is: <strong>#referenceno#</strong>  no. Your invoice is attached for your records.<br />
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "shipmentreturnaccept")
            {
                sb.Append(@"<div style="" padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
	<div>
		<p style="" font-size: 13px; line-height:26px;"">
			<span style="" font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
				Dear <strong> #FirstName#  #LastName# </strong><br /><br />
				Your shipment with Id #id# has been returned successfully. Your have charged $#charge# to return this shipment.<br />
				If you have any questions about your shipment, please contact us by email at: <a href="" mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
			</span><br />
			<span style="" font-size:15px"">Sincerely,</span><br />
			<span style="" font-size:15px"">The ChineseAddress Staff</span>
		</p>
	</div>
</div>");
            }
            else if (TemplateKey == "shipmentreturnreject")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                         Your return shipment request for shipment with Id #id# has been rejected #reason#.<br />
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "pictureuploaded")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                         A picture of your order has been uploaded. Please sign in to <a href=""https://www.chineseaddress.com/"">www.Chineseaddress.com</a> for more details.<br />
                          Your tracking number is: <strong>#trackingno#</strong>.</br>
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "refferalinvitation")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #name#</strong><br />
                        You have been invited by #Useremail# to try  <a href=""https://www.chineseaddress.com""> www.Chineseaddress.com </a>.<br />
                        <a href=""#Links#"">Click here</a> avail your discount Signup fees <strong>#ReferalDiscount#</strong>%<br/>
                        To start shopping, shipping, and saving money on your international orders, visit our page.</br>
                         We look forward to delivering your favorite US items right to your door soon.
                        </span><br />

                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "contactus")
            {
                sb.Append(@"<div style="" padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
    <div>
        <p style="" font-size: 13px; line-height:26px; font-family: gotham, helvetica, arial, sans-serif !important;"">
            <span style="" font-size: 14px;"">
                Dear Admin<br />
                You received a query with following details:-<br />
                Name: <strong>#name# </strong><br />
                Phone: <strong>#phone# </strong><br />
                Email: <strong>#email# </strong><br />
                Message: <strong>#message# </strong>
            </span><br /><br />
            <span style="" font-size:15px"">Sincerely,</span><br />
            <span style="" font-size:15px"">The ChineseAddress Staff</span>
        </p>
    </div>
</div>");
            }
            else if (TemplateKey == "forgotpassword")
            {
                sb.Append(@"<div style='padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;'>
    <div>
        <p style='font-size: 13px; line-height:26px;'>
            <span style='font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;'>
                Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                You have recently let us know that you forgot your password. <br />
                Your have been registered with following credentials<br/>
                Username:<strong>#email#</strong><br />
                Password:<strong>#recoverpasswored# </strong> <br /><br />
                Please log in at www.Chineseaddress.com to update your password.<br />
                If you have any questions about your shipment, please contact us by email at: <a href='mailto:info@ChineseAddresss.com'>info@ChineseAddresss.com</a>  or by phone at: +8618871488894.
            </span><br /><br />
            <span style='font-size:15px'>Sincerely,</span><br />
            <span style='font-size:15px'>The ChineseAddress Staff</span>
        </p>
    </div>
</div>");
            }
            else if (TemplateKey == "refersignup")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                           Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                          You have received reward points for referring a new customer.  To show our thanks, we have added <strong>#rewardpoint# </strong> to your account.<br />
                          If you have any questions about your shipment, please contact us by email at: <a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "rewardpoint")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                           Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                         You currently have reward points: <strong>#rewardpoints#</strong><br />
                          If you have any questions about your shipment, please contact us by email at: <a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "apierroremail")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                           <strong>Shipping Api Error</strong><br />
                             <strong>Carrier Name :</strong> #CarrierName#<br/>
                             <strong>Carrier Services :</strong> #CarrierServices# <br/>
                               <strong>User :</strong>  #UserName#<br/>
                               <strong>Order RefNo :</strong> #OrderRefernceNo#<br/>
                                <strong>Message :</strong> #Msg#<br/>
                                <strong>Date :</strong>#Date#
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "notificationemail")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                            Tracking Number: <strong>#tracking#</strong><br />
                            Carrier: <strong>#carrier#</strong><br />
                            Sender: <strong>#sender#</strong><br />
                             Message:  <strong>#Msg#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "paymentfailed")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                           Dear <strong> #FirstName#  #LastName# </strong><br /><br />
                         Payment failed :<strong>#failedMsg#</strong><br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "customproforma_template")
            {
                sb.Append(@"<html lang='en'>
<body>
    <div class='container'>
        <table class='tbl1'>
            <tr>
                <th class='h1 noborder'>
                    Proforma Invoice
                </th>
                <th class='noborder'>
                </th>
            </tr>
            <tr>
                <th class='noborder'>
                    Date: #invoicedate#
                </th>
                <th class='noborder'>
                    CARRIER: #carrier#
                </th>
            </tr>
        </table>
        <div class='clearfix'>&nbsp;</div>
        <div class='clearfix'>&nbsp;</div>
        <table cellpadding='8' class='tbl2'>
            <tr>
                <th class='underline'>
                    <b>SENT BY</b>
                </th>
                <th class='underline'>
                    <b>SENT TO</b>
                </th>
            </tr>
            <tr>
                <td>
                    Name: <strong>#byname#</strong>
                </td>
                <td>
                    Name: <strong>#toname#</strong>
                </td>
            </tr>
            <tr>
                <td>
                    Address: <strong>#byaddress#</strong>
                </td>
                <td>
                    Address: <strong>#toaddress#</strong>
                </td>
            </tr>
            <tr>
                <td>
                    City/Postal Code: <strong>#bycity#</strong>
                </td>
                <td>
                    City/Postal Code: <strong>#tocity#</strong>
                </td>
            </tr>
            <tr>
                <td>
                    Country: <strong>#bycountry#</strong>
                </td>
                <td>
                    Country: <strong>#tocountry#</strong>
                </td>
            </tr>
            <tr>
                <td>
                    Telephone/Fax: <strong>#bytelephone#</strong>
                </td>
                <td>
                    Telephone/Fax: <strong>#totelephone#</strong>
                </td>
            </tr>
        </table>
        <div class='clearfix'>&nbsp;</div>
        <div class='clearfix'>&nbsp;</div>
        <table cellpadding='8' id='desc' class='tbl3'>
            <thead>
                <tr>
                    <th>
                        <b>FULL DESCRIPTION OF GOODS</b>
                    </th>
                    <th>
                        <b>ORIGIN COUNTRY</b>
                    </th>
                    <th>
                        <b>CUSTOMS COMMODITY CODE</b>
                    </th>
                    <th>
                        <b>HARMONIZED CODE</b>
                    </th>
                    <th>
                        <b>QUANTITY</b>
                    </th>
                    <th>
                        <b>UNIT VALUE AND CURRENCY</b>
                    </th>
                    <!--<th>
                        <b>TOTAL VALUE AND CURRENCY</b>
                    </th>-->
                </tr>
            </thead>
            <tbody style='text-align:center;'>
                #shipmentdetail#
                <tr>
                    <td colspan='4' style='text-align:right;'>
                        TOTAL VALUE AND CURRENCY
                    </td>
                    <td colspan='2' style='text-align:center;'>
                        #demototal#
                    </td>
                </tr>
            </tbody>
        </table>
        <div class='clearfix'>&nbsp;</div>
        <div class='clearfix'>&nbsp;</div>
        <table class='noborder tbl4' cellpadding='8'>
            <tr class='noborder'>
                <th class='noborder colspn'>
                    <b>Value for customs purposes only.</b>
                </th>
            </tr>
            <tr class='noborder'>
                <th class='noborder colspn'>
                    <b>No commercial value.</b>
                </th>
            </tr>
            <tr class='noborder'>
                <td class='noborder'>
                    REASON FOR EXPORT:
                </td>
                <td class='noborder'>
                    #reasonforexport#
                </td>
            </tr>
            <tr class='noborder'>
                <td class='noborder'>
                    TERMS OF DELIVERY(INCOTERMS 2000) :
                </td>
                <td class='noborder'>
                    #termsofdelivery#
                </td>
            </tr>
            <tr class='noborder'>
                <td class='noborder'>
                    NUMBER AND KIND OF PACKAGES :
                </td>
                <td class='noborder'>
                    #numberandkindpackage#
                </td>
            </tr>
            <tr class='noborder'>
                <td class='noborder'>
                    NET WEIGHT :
                </td>
                <td class='noborder'>
                    #billableweight# (KG)
                </td>
            </tr>
            <tr class='noborder'>
                <td class='noborder'>
                    <strong>Place and date :</strong>
                </td>
                <td class='noborder'>
                    #placeanddate#
                </td>
            </tr>
            <tr class='noborder'>
                <td class='noborder'>
                    <strong>Name:</strong>
                </td>
                <td class='noborder'>
                    #name#
                </td>
            </tr>
            <tr class='noborder'>
                <td class='noborder'>
                    <strong>Signature :</strong>
                </td>
                <td class='noborder'>
                    #signature#
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
");
            }
            else if (TemplateKey == "signupinvoice")
            {
                sb.Append(@"<table class='topTable'>
    <tr>
        <td align='right' colspan='3'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class='imgDiv' style='width:30%'>
            <img height='35px' src='http://www.Chineseaddress.com/Content/img/logo.png' />
        </td>
        <td class='divH1' style='width:30%'>
            <h1 class='h1'>
                Invoice
            </h1>
        </td>
        <td class='tdRight' style='width:30%'>
            <b>Invoice Number :</b>  #invoicenumber#<br />
            <b>Date :</b>#invoicedate#<br />
        </td>
    </tr>
    <tr>
        <td align='right' colspan='3'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align='right' colspan='3'>
            &nbsp;
        </td>
    </tr>

    <tr>
        <td align='left' valign='bottom'>
            <b>From Address :</b>
            <br />
            #fromAddress1#<br />
            #fromAddress2#<br />
            Guangzhou<br />
            #fromzipcode#<br />
            #fromcountry#<br />
            #fromphoneno#
        </td>
        <td align='left'>
            &nbsp;
        </td>
        <td class='tdRight'>
            <b>To Address :</b><br />
            #firstName#  #lastName#<br />
            #address1#<br />
            #city#,#state#<br />
            #postalcode#<br />
            #country#<br />
            #mobileNo#<br />
        </td>
    </tr>
    <tr>
        <td align='right' colspan='3'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align='right' colspan='3'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan='4' class='tdH2'>
            <h2 class='h2'>
                Invoice Details
            </h2>
        </td>
    </tr>
    <tr>
        <td align='right' colspan='3'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align='right' colspan='3'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align='left' valign='top' colspan='3'>
            <table cellpadding='12' id='desc' style='width: 100%; text-align: left; border: 1px solid black;'>
                <thead>
                    <tr align='left' valign='top'>
                        <th width='25%'>
                            Plan
                        </th>
                        <th width='35%'>
                            Date
                        </th>
                        <th width='25%'>
                            Pay Amount
                        </th>
                        <th width='10%'>
                            Type
                        </th>
                    </tr>
                    <tr align='left' valign='top' style='font-weight: bold;'>
                        <td>
                            #plan#
                        </td>
                        <td>
                            #date#
                        </td>
                        <td>
                            $ #paymentamount#
                        </td>
                        <td>
                            #type#
                        </td>
                    </tr>
                    <tr>
                        <td align='left' valign='top' colspan='6'>
                            <br />
                            <center>
                                <span style='font-size: larger;'>
                                    Thank you for using ChineseAddresss.com, we appreciate your
                                    experience with us.
                                </span>
                            </center>
                            <br />
                            <br />
                            *No charges have been raised based on the current subscribed plan as this comes under promotional plan of $0.00 amount.
                            <br />
                            <br />
                            <center>
                                <span style='font-size: small;'>
                                    ChineseAddresss.com is registered
                                    in China No. LCC_Number_Here
                                </span>
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td align='left' valign='top' colspan='3'>
                            &nbsp;
                        </td>
                    </tr>
                </thead>
            </table>
        </td>
    </tr>
</table>
");
            }
            else if (TemplateKey == "orderinvoice")
            {
                sb.Append(@"<div class='margin20px'>
    <table class='topTable' style='width:100%;'>
        <tr>
            <td class='imgDiv' width='40%'>
                <img src='https://www.chineseaddress.com/Content/img/logo.png' style='height:20px' />
            </td>
            <td class='divH1' width='20%'>
                <h1 class='h1' style='text-align:center'>
                    Invoice
                </h1>
            </td>
            <td class='tdRight' width='40%' style='text-align:right'>
                <b>Invoice Number :</b>#invoicno#<br />
                <b>Date :</b>#invoicedate#<br />
            </td>
        </tr>
        <tr>
            <td align='right' colspan='3'>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align='right' colspan='3'>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align='left' valign='bottom'>
                <b>From Address :</b> <br />
                Chineseaddress<br />
                3133 Tianhe Ave<br />
                Suite #suite#<br />
                Guangzhou<br />
                510000<br />
                China<br />
                8618871488894
            </td>
            <td align='left'>
                &nbsp;
            </td>
            <td class='tdRight' style='text-align:right'>
                <b>To Address :</b><br />
                #toname#<br />
                #toaddress#<br />
                #tocity#,#tostate#<br />
                #tozip#<br />
                #tocountry#<br />
                #totelephone#<br />
            </td>
        </tr>
        <tr>
            <td align='right' colspan='3'>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align='right' colspan='3'>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan='3' class='tdH2'>
                <h2 class='h2' style='text-align:center'>
                    Order Details
                </h2>
            </td>
        </tr>
        <tr>
            <td align='right' colspan='3'>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align='right' colspan='3'>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align='left' valign='top' colspan='3'>
                <table cellpadding='12' id='desc' class='Percent100' style='width:100%;text-align:center; border:1px solid black;font-size:small'>
                    <thead>
                        <tr align='left' valign='top' style='font-size:small'>
                            <th width='20%'>
                                <!--width='15%'-->
                                Order Ref No
                            </th>
                            <th width='15%'>
                                <!--width='10%'-->
                                Date
                            </th>
                            <!--<th>
                                Tracking No
                            </th>-->
                            <th width='15%'>
                                <!--width='20%'-->
                                Package(s)
                            </th>
                            <th width='30%'>
                                <!--width='20%'-->
                                Consolidate Shipment(s)
                            </th>
                            <th width='20%'>
                                <!-- width='20%' -->
                                Pay Amount
                            </th>
                            <!--<th width='0%'>
                                Wallet Consume Amount
                            </th>-->
                        </tr>
                        <tr align='left' valign='top'>
                            <td width='20%' style='text-align:left'>
                                #ordrefno#
                            </td>
                            <td width='15%' style='text-align:left'>
                                #date#
                            </td>
                            <!--<td>
                                #trackingno#
                            </td>-->
                            <td width='15%' style='text-align:left'>
                                #packages#
                            </td>
                            <td width='30%' style='text-align:left'>
                                #shipments#
                            </td>
                            <td width='20%' style='text-align:left'>
                                US$ #wallet_amount#<!--#amount#-->
                            </td>
                            <!--<td>
                                US$ #wallet_amount#
                            </td>-->
                        </tr>
                        <tr>
                            <td style='text-align:right' valign='top' colspan='4'>
                                <b>Invoice Total</b>
                            </td>
                            <td style='text-align:left' valign='top' colspan='1'>
                                US$ #invoicetotal#
                            </td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' colspan='6'>
                                <br />
                                <center>
                                    <span style='font-size: larger;'>
                                        Thank you for using ChineseAddresss.com, we appreciate your
                                        experience with us.
                                    </span>
                                </center>
                                <br />
                                <br />
                                *These charges have been raised based on the size and weight declared for these
                                shipments. If the carrier advises that the size or weight is larger then additional
                                carriage and administration charges will be due. If additional services post collection
                                are provided by the carrier for these shipments additional charges may be due in
                                accordance with our terms and conditions.
                                <br />
                                <br />
                                <center>
                                    <span style='font-size: small;'>
                                        ChineseAddresss.com is registered
                                        in China No. LCC_Number_Here
                                    </span>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' colspan='3'>
                                &nbsp;
                            </td>
                        </tr>
                    </thead>
                </table>
            </td>
        </tr>
    </table>
</div>");
            }
            else if (TemplateKey == "topupinvoice")
            {
                sb.Append(@"<table class='topTable'>
    <tr>
        <td class='imgDiv' style='width:30%'>
            <img height='35px' src='http://www.Chineseaddress.com/Content/img/logo.png' />
        </td>
        <td class='divH1' style='width:30%'>
            <h1 class='h1'>
                Invoice
            </h1>
        </td>
        <td class='tdRight' style='width:30%'>
            <b>Invoice Number :</b>#invoicno#<br />
            <b>Date :</b>#invoicedate#<br />
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align='left' valign='bottom'>
            <b>From Address :</b> <br />
            Chineseaddress<br />
            3133 Tianhe Ave<br />
            Suite #suite#<br />
            Guangzhou<br />
            510000<br />
            China<br />
            8618871488894
        </td>
        <td align='left'>
            &nbsp;
        </td>
        <td class='tdRight'>
            <b>To Address :</b><br />
            #toname#<br />
            #toaddress#<br />
            #tocity#,#tostate#<br />
            #tozip#<br />
            #tocountry#<br />
            #totelephone#<br />
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan='4' class='tdH2'>
            <h2 class='h2' style='text-align:center;'>
                Invoice Details
            </h2>
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align='left' valign='top' colspan='3'>
            <table cellpadding='12' id='desc' style='width: 100%; text-align: left; border: 1px solid black;'>
                <thead>
                    <tr align='left' valign='top'>
                        <th width='30%'>
                            Payment Method
                        </th>
                        <th width='30%'>
                            Payment Date
                        </th>
                        <th width='25%'>
                            Payment Amount
                        </th>
                        <th>
                            Type
                        </th>
                    </tr>
                    <tr align='left' valign='top' style='font-weight: bold;'>
                        <td>
                            #payment_method#
                        </td>
                        <td>
                            #date#
                        </td>
                        <td>
                            $ #amount#
                        </td>
                        <td>
                            #type#
                        </td>
                    </tr>
                    <tr>
                        <td align='left' valign='top' colspan='6'>
                            <br />
                            <center>
                                <span style='font-size: larger;'>
                                    Thank you for using ChineseAddresss.com, we appreciate your
                                    experience with us.
                                </span>
                            </center>
                            <br />
                            <br />
                            *These charges have been raised based on the topup wallet request.
                            <br />
                            <br />
                            <center>
                                <span style='font-size: small;'>
                                    ChineseAddresss.com is registered
                                    in China No. LCC_Number_Here
                                </span>
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td align='left' valign='top' colspan='3'>
                            &nbsp;
                        </td>
                    </tr>
                </thead>
            </table>
        </td>
    </tr>
</table>
");
            }
            else if (TemplateKey == "return_shipment_invoice")
            {
                sb.Append(@"<table class='topTable'>
    <tr>
        <td class='imgDiv' style='width:30%'>
            <img height='35px' src='http://www.Chineseaddress.com/Content/img/logo.png' />
        </td>
        <td class='divH1' style='width:30%'>
            <h1 class='h1'>
                Invoice
            </h1>
        </td>
        <td class='tdRight' style='width:30%'>
            <b>Invoice Number :</b>#invoicno#<br />
            <b>Date :</b>#invoicedate#<br />
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align='left' valign='bottom'>
            <b>From Address :</b> <br />
            Chineseaddress<br />
            3133 Tianhe Ave<br />
            Suite #suite#<br />
            Guangzhou<br />
            510000<br />
            China<br />
            8618871488894
        </td>
        <td align='left'>
            &nbsp;
        </td>
        <td class='tdRight'>
            <b>To Address :</b><br />
            #toname#<br />
            #toaddress#<br />
            #tocity#,#tostate#<br />
            #tozip#<br />
            #tocountry#<br />
            #totelephone#<br />
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan='4' class='tdH2'>
            <h2 class='h2' style='text-align:center;'>
                Invoice Details
            </h2>
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan='3' align='right'>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align='left' valign='top' colspan='3'>
            <table cellpadding='12' id='desc' style='width: 100%; text-align: left; border: 1px solid black;'>
                <thead>
                    <tr align='left' valign='top'>
                        <th width='15%'>
                            Payment Method
                        </th>
                        <th width='20%'>
                            Payment Date
                        </th>
                        <th width='25%'>
                            Payment Amount
                        </th>
                        <th width='10%'>
                            Shipment Id
                        </th>
                        <th width='25%'>
                            Type
                        </th>
                    </tr>
                    <tr align='left' valign='top' style='font-weight: bold;'>
                        <td>
                            #payment_method#
                        </td>
                        <td>
                            #date#
                        </td>
                        <td>
                            $ #amount#
                        </td>
                            <td>
                            #id#
                        </td>
                        <td>
                            #type#
                        </td>
                    </tr>
                    <tr>
                        <td align='left' valign='top' colspan='6'>
                            <br />
                            <center>
                                <span style='font-size: larger;'>
                                    Thank you for using ChineseAddresss.com, we appreciate your
                                    experience with us.
                                </span>
                            </center>
                            <br />
                            <br />
                            *These charges have been raised based on the topup wallet request.
                            <br />
                            <br />
                            <center>
                                <span style='font-size: small;'>
                                    ChineseAddresss.com is registered
                                    in China No. LCC_Number_Here
                                </span>
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td align='left' valign='top' colspan='3'>
                            &nbsp;
                        </td>
                    </tr>
                </thead>
            </table>
        </td>
    </tr>
</table>
");
            }
            else if (TemplateKey == "commonuser")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                        <span style=""font-size: 14px;"">
                            Dear <span class='text-capitalize'>#user#</span>,<br />
                                    #content#
                        </span><br />
                        If you have any questions, please contact us by email at:<a href=""mailto:info@ChineseAddresss.com"">info@ChineseAddresss.com</a>  or by phone at: +8618871488894.<br/>
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "commonadmin")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                        <span style=""font-size: 14px;"">
                          Dear Admin,<br />
                                    #content#
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The ChineseAddress Staff</span>
                    </p>
                </div>
            </div>");
            }
            if (TemplateKey == "signupinvoice" || TemplateKey == "customproforma_template" || TemplateKey == "orderinvoice" || TemplateKey == "topupinvoice" || TemplateKey == "return_shipment_invoice")
            {
                return sb;
            }

            string HeaderTemplatedtstring = HeaderTemplate();
            string FooterTemplatestring = FooterTemplate();
            StringBuilder AppendTempalte = new StringBuilder(HeaderTemplatedtstring + sb + FooterTemplatestring);
            return AppendTempalte;
        }

        public string GetEmailTemplate(string TemplateKey, Hashtable TemplateData)
        {
            try
            {

                StringBuilder FormTempleate = GetTemplate(TemplateKey.ToLower());
                foreach (DictionaryEntry d in TemplateData)
                {
                    FormTempleate = FormTempleate.Replace(Convert.ToString(d.Key), Convert.ToString(d.Value));
                }
                return FormTempleate.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "";

        }

        public static string HeaderTemplate()
        {
            return @"<html><head>
   <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
</head>
<body>
    <div style=""float: left;margin: 53px 0 0;width: 100%;"">
        <div style=""background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
            margin: auto;width: 90%;border-color: #517FBB !important; min-height: 239px !important; border-radius:none!important;"">
            <div style=""border-bottom: 1px solid transparent;border-top-left-radius: 3px;border-top-right-radius: 3px;border-bottom-color:#517FBB!important; padding: 15px 10px;background-color: #E5E5E5 !important;font-size:15px;"">
                <img src=""http://www.Chineseaddress.com/Content/img/logo.png"" height=""35px""/>
            </div>";
        }

        public static string FooterTemplate()
        {
            return @"</div>  </div> </body ></html>";
        }
    }
}