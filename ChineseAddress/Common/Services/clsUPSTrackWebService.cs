using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.UPSTrackWebSerice;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;

namespace _ChineseAddress.com.Common.Services
{
    public class clsUPSTrackWebService
    {
        #region Static Properties

        private static readonly string _UPSAPIUserName = System.Configuration.ConfigurationManager.AppSettings["UPSAPIUserName"].ToString();
        private static readonly string _UPSAPIPassword = System.Configuration.ConfigurationManager.AppSettings["UPSAPIPassword"].ToString();
        private static readonly string _UPSAPIAccessLicenseNumber = System.Configuration.ConfigurationManager.AppSettings["UPSAPIAccessLicenseNumber"].ToString();
        private static readonly string _UPSAPIShipperNumber = System.Configuration.ConfigurationManager.AppSettings["UPSAPIShipperNumber"].ToString();

        #endregion Static Properties

        #region Properies

        /// <summary>
        /// Get SUCCESS or ERROR msg from this acessor
        /// </summary>
        public string _msg { get; set; }

        /// <summary>
        /// Get SUCCESS or ERROR from this acessor
        /// </summary>
        public bool _isSuccess { get; set; } = false;

        public List<TrackingDetailModel> _TrackingDetail { get; set; }
        
        public string _trckingNo { get; set; } = "1Z12345E0305271640";

        #endregion Properies

        public clsUPSTrackWebService()
        {
            _TrackingDetail = new List<TrackingDetailModel>();
        }

        public void TrackerOrder()
        {
            try
            {
                TrackService track = new TrackService();
                TrackRequest tr = new TrackRequest();

                track.UPSSecurityValue = getUPSSecurity();

                RequestType request = new RequestType();
                string[] requestOption = { "1" };
                request.RequestOption = requestOption;
                tr.Request = request;
                tr.InquiryNumber = _trckingNo;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                TrackResponse TrackingResponse = track.ProcessTrack(tr);
                foreach (PackageType package in TrackingResponse.Shipment[0].Package)   
                {
                    TrackingDetailModel objTrackingDetail = new TrackingDetailModel();
                    List<TrackingDetailModel.Address> lstDetail = new List<TrackingDetailModel.Address>();
                    Context4InshipEntities Context = new Context4InshipEntities();
                    objTrackingDetail.TrackingNumber = package.TrackingNumber;
                    var GetReciverby = (from tbord in Context.tblOrders
                                        join tbaddress in Context.tblAddressBooks on tbord.Fk_customer_id equals tbaddress.Fk_customer_Id
                                        select new { tbord.tracking_no, tbaddress.first_name, tbaddress.last_name }).Where(x => x.tracking_no == package.TrackingNumber).FirstOrDefault();
                    if(GetReciverby!=null)
                    objTrackingDetail.Reciverby = GetReciverby.first_name + " " + GetReciverby.last_name;
                    foreach (ActivityType activity in package.Activity)
                    {
                        TrackingDetailModel.Address objAddress = new TrackingDetailModel.Address();
                        objAddress.Date = DateTime.ParseExact(activity.Date + activity.Time, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);
                        if (activity.ActivityLocation != null)
                        {
                            var GetCountryName = Context.tblCountryMasters.Where(x => x.country_code == activity.ActivityLocation.Address.CountryCode).FirstOrDefault();
                            objAddress.City = activity.ActivityLocation.Address.City;
                            objAddress.State = activity.ActivityLocation.Address.StateProvinceCode;
                            objAddress.Country = (GetCountryName !=null)?GetCountryName.country_name:"";
                            objAddress.PostalCode = activity.ActivityLocation.Address.PostalCode;
                        }
                        objAddress.status = activity.Status.Description;
                        lstDetail.Add(objAddress);
                    }
                    objTrackingDetail.AddressDetail = lstDetail;
                    _TrackingDetail.Add(objTrackingDetail);
                }
                _isSuccess = true;
                //Console.WriteLine("The transaction was a " + trackRespon  se.Response.ResponseStatus.Description);
                //Console.WriteLine("Shipment Service " + trackResponse.Shipment[0].Service.Description);
            }
            catch (Exception ex)
            {
                _isSuccess = false;
                _msg = ex.Message.ToString();
            }
        }

        #region Common and miscellaneous methods

        /// <summary>
        /// UPSSecurity
        /// </summary>
        /// <returns></returns>
        private static UPSSecurity getUPSSecurity()
        {
            UPSSecurity upss = new UPSSecurity();

            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = _UPSAPIAccessLicenseNumber;
            upss.ServiceAccessToken = upssSvcAccessToken;

            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = _UPSAPIUserName;
            upssUsrNameToken.Password = _UPSAPIPassword;
            upss.UsernameToken = upssUsrNameToken;
            return upss;
        }

        #endregion Common and miscellaneous methods

    }
}