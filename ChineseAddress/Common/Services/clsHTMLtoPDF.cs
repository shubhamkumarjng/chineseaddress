using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace _ChineseAddress.com.Common.Services
{
    public class clsHTMLtoPDF
    {

        public void ConvertHTMLToPDF(string _htmlString, string _cssPath, string _pdfSavePath, bool OrderInvoice)
        {
            var cssText = File.ReadAllText(_cssPath);
            var html = _htmlString;
            using (var memoryStream = new MemoryStream())
            {
                if (OrderInvoice == true)
                {
                    var document = new Document(PageSize.A4, 5f, 0f, 5f, 2f);
                    var writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();
                    using (var cssMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(cssText)))
                    {
                        using (var htmlMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(html)))
                        {
                            XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, htmlMemoryStream, cssMemoryStream);
                        }
                    }
                    document.Close();
                    byte[] yourByteArray = memoryStream.ToArray();
                    File.WriteAllBytes(_pdfSavePath, yourByteArray);

                }
                else 
                {
                    var document = new Document(PageSize.A4, 30f, 10f, 30f, 10f);
                    var writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();
                    using (var cssMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(cssText)))
                    {
                        using (var htmlMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(html)))
                        {
                            XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, htmlMemoryStream, cssMemoryStream);
                        }
                    }
                    document.Close();
                    byte[] yourByteArray = memoryStream.ToArray();
                    File.WriteAllBytes(_pdfSavePath, yourByteArray);
                }

            }
        }
        public void ConvertHTMLToPDFCustomerPerforma(string _htmlString, string _cssPath, string _pdfSavePath)
        {
            var cssText = File.ReadAllText(_cssPath);
            var html = _htmlString;
            using (var memoryStream = new MemoryStream())
            {
               
                    var document = new Document(PageSize.A4, 30f, 10f, 20f, 10f);
                    var writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();
                    using (var cssMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(cssText)))
                    {
                        using (var htmlMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(html)))
                        {
                            XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, htmlMemoryStream, cssMemoryStream);
                        }
                    }
                    document.Close();
                    byte[] yourByteArray = memoryStream.ToArray();
                    File.WriteAllBytes(_pdfSavePath, yourByteArray);

            }
        }
    }
}