using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.FedExTrackingWebService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services.Protocols;
using _ChineseAddress.com.Repository;

namespace _ChineseAddress.com.Common.Services
{
    public class clsFedExTrackingWebService
    {

        public string _FailMsg { get; set; }
        public bool _Success { get; set; } = false;
        private static readonly string _FedExApiKey = ConfigurationManager.AppSettings["FedExKey"];
        private static readonly string _FedExPassword = ConfigurationManager.AppSettings["FedExPassword"];
        private static readonly string _FedExAccountNumber = ConfigurationManager.AppSettings["FedExAccountNumber"];
        private static readonly string _FedExMeterNumber = ConfigurationManager.AppSettings["FedExMeterNumber"];


        List<Address> _objAddress = new List<Address>();
        Context4InshipEntities Context = new Context4InshipEntities();
        public string TrackingNumber { get; set; } = "403934084723025";

        public List<TrackingDetailModel> _GetTrackingNumberDetails()
        {
            List<TrackingDetailModel> _TrackingDetail = new List<TrackingDetailModel>();
            try
            {
                TrackRequest request = CreateTrackRequest();
                TrackService service = new TrackService();

                //
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                TrackReply reply = service.track(request);
                if (reply.HighestSeverity == NotificationSeverityType.SUCCESS || reply.HighestSeverity == NotificationSeverityType.NOTE || reply.HighestSeverity == NotificationSeverityType.WARNING)
                {
                    TrackingDetailModel objTrackingDetail = new TrackingDetailModel();
                   // TrackingDetailModel objTracking = new TrackingDetailModel();
                    objTrackingDetail.TrackingNumber = TrackingNumber;
                    var GetReciverby = (from tbord in Context.tblOrders
                                        join tbaddress in Context.tblAddressBooks on tbord.Fk_customer_id equals tbaddress.Fk_customer_Id
                                        select new { tbord.tracking_no, tbaddress.first_name, tbaddress.last_name }).Where(x => x.tracking_no == TrackingNumber).FirstOrDefault();
                    if(GetReciverby!=null)
                    objTrackingDetail.Reciverby = GetReciverby.first_name + " " + GetReciverby.last_name;
                    List<TrackingDetailModel.Address> lstDetail = new List<TrackingDetailModel.Address>();
                    foreach (CompletedTrackDetail completedTrackDetail in reply.CompletedTrackDetails)
                    {
                        foreach (TrackEvent _trackDetail in completedTrackDetail.TrackDetails[0].Events)
                        {
                            TrackingDetailModel.Address objAddress = new TrackingDetailModel.Address();
                            objAddress.City = _trackDetail.Address.City;
                            objAddress.Country = _trackDetail.Address.CountryName;
                            objAddress.State = _trackDetail.Address.StateOrProvinceCode;
                            objAddress.PostalCode = _trackDetail.Address.PostalCode;
                            objAddress.Date = _trackDetail.Timestamp;
                            objAddress.status = _trackDetail.EventDescription;

                            lstDetail.Add(objAddress);
                        }

                    }
                    objTrackingDetail.AddressDetail = lstDetail;
                    //objTrackingDetail.Reciverby = objTracking.Reciverby;
                    _TrackingDetail.Add(objTrackingDetail);
                }
                _Success = true;
            }
            catch (SoapException e)
            {
                _FailMsg = e.Detail.InnerText;
                _Success = false;
            }
            catch (Exception e)
            {
                _FailMsg = e.ToString();
                _Success = false;
            }
            return _TrackingDetail;
        }
        private TrackRequest CreateTrackRequest()
        {
            try
            {

                TrackRequest request = new TrackRequest();

                request.WebAuthenticationDetail = new WebAuthenticationDetail();
                request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
                request.WebAuthenticationDetail.UserCredential.Key = _FedExApiKey; // Replace "XXX" with the Key
                request.WebAuthenticationDetail.UserCredential.Password = _FedExPassword;// Replace "XXX" with the Password

                request.ClientDetail = new ClientDetail();
                request.ClientDetail.AccountNumber = _FedExAccountNumber; // Replace "XXX" with the client's account number
                request.ClientDetail.MeterNumber = _FedExMeterNumber; // Replace "XXX" with the client's meter number

                request.TransactionDetail = new TransactionDetail();
                request.TransactionDetail.CustomerTransactionId = new Guid().ToString();
                request.Version = new VersionId();

                request.SelectionDetails = new TrackSelectionDetail[1] { new TrackSelectionDetail() };
                request.SelectionDetails[0].PackageIdentifier = new TrackPackageIdentifier();
                request.SelectionDetails[0].PackageIdentifier.Value = TrackingNumber /* _TrackingDetail.TrackingNumber*/;

                request.SelectionDetails[0].PackageIdentifier.Type = TrackIdentifierType.TRACKING_NUMBER_OR_DOORTAG;

                request.SelectionDetails[0].ShipDateRangeBegin = DateTime.Parse(DateTime.Now.ToString());

                request.SelectionDetails[0].ShipDateRangeEnd = request.SelectionDetails[0].ShipDateRangeBegin.AddDays(0);
                request.SelectionDetails[0].ShipDateRangeBeginSpecified = false;
                request.SelectionDetails[0].ShipDateRangeEndSpecified = false;
                request.ProcessingOptions = new TrackRequestProcessingOptionType[1];
                request.ProcessingOptions[0] = TrackRequestProcessingOptionType.INCLUDE_DETAILED_SCANS;
                return request;
            }
            catch (Exception ex)
            {
                _FailMsg = ex.ToString();
                _Success = false;
            }
            return null;
        }
    }
}