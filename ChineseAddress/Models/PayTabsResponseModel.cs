using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _ChineseAddress.com.Models
{
    public class PayTabsResponseModel
    {
        public string result { get; set; }
        public string response_code { get; set; }
        public string pt_invoice_id { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string reference_no { get; set; }
        public string transaction_id { get; set; }
    }
}