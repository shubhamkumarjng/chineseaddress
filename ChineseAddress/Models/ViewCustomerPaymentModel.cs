using System;
using System.ComponentModel.DataAnnotations;

namespace _ChineseAddress.com.Models
{
    public class ViewCustomerPaymentModel
    {
        public int Fk_customer_id { get; set; }
        public int Id { get; set; }
        public int billingId { get; set; }
        public string Adress2 { get; set; }
        [MaxLength(11, ErrorMessage = "First Name cannot be longer than 10 characters.")]
        [Required(ErrorMessage = "First Name is required")]
        public string first_name { get; set; }
        [MaxLength(11, ErrorMessage = "Last Name cannot be longer than 10 characters.")]
        [Required(ErrorMessage = "Last Name is required")]
        public string last_name { get; set; }

        [Required(ErrorMessage = "Email  is required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string email { get; set; }

        [Required(ErrorMessage = "Mobile Number is required")]
        [MaxLength(15, ErrorMessage = "MobileNumber cannot be longer than 10 digits.")]
        [RegularExpression(@"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-.]+\d$", ErrorMessage = "Entered phone format is not valid.")]
        public string mobile { get; set; }
        [Required(ErrorMessage = "Address is required")]
        [MaxLength(35, ErrorMessage = "Address is maximum 35 characters long")]
        [RegularExpression("^[a-zA-Z0-9-,/;.'&quot; ]*$", ErrorMessage = "Unacceptable character in Address field. Please use only a-z,0-9 or -',/;.&quot;")]
        public string address { get; set; }

        [Required(ErrorMessage = "Country is required")]
        public string Country { get; set; }

        [Required(ErrorMessage = "State is required")]
        public string state { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string city { get; set; }

        [Required(ErrorMessage = "Zip Code is required")]
        public string post_code { get; set; }

        [Required(ErrorMessage = "Credit Card is required")]
        public string CreditCard { get; set; }

        [Required(ErrorMessage = "Expire Month is required")]
        public string ExpireMonth { get; set; }

        [Required(ErrorMessage = "Expire Year is required")]
        public string ExpireYear { get; set; }

        [Required(ErrorMessage = "CVV Code is required")]
        public string CVCCode { get; set; }

        public System.DateTime created_on { get; set; }

        [Display(Name = "Terms and Conditions")]
        public bool Terms { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string PlanType { get; set; }
        public decimal? CouponValue { get; set; }
        public decimal? PayableAmount { get; set; }
        public decimal? PlanAmount { get; set; }
        public string Plan { get; set; }
        public int? CreditCardId { get; set; }
        public int referrer_id { get; set; }
    }
}