using System.ComponentModel.DataAnnotations;

namespace _ChineseAddress.com.Models
{
    public class PlansOptionModel
    {
        public int p_id { get; set; }
        public string title { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0.00}")]
        public decimal price { get; set; }

        public string description { get; set; }
        public bool IsSignUp { get; set; }
    }
}