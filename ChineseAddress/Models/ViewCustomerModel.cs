using _ChineseAddress.com.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace _ChineseAddress.com.Models
{
    public class ViewCustomerModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Username/Email is required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Enter a valid Username/Email address")]
        [IsEmailExists(ErrorMessage = "Username/Email is already exists")]
        public string email { get; set; }

        [MinLength(6, ErrorMessage = "Password must be minimum 6 character")]
        [Required(ErrorMessage = "Password is required")]
        public string password { get; set; }

        [MinLength(6, ErrorMessage = "Confirm Password must be minimum 6 character")]
        [Required(ErrorMessage = "Confirm Password is required")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("password", ErrorMessage = "Your password and confirmation password do not match")]
        public string ConfirmPassword { get; set; }
        [MaxLength(11, ErrorMessage = "First Name cannot be longer than 10 characters.")]
        [Required(ErrorMessage = "First Name is required")]
        public string FirstName { get; set; }
        [MaxLength(11, ErrorMessage = "Last Name cannot be longer than 10 characters.")]
        [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [MaxLength(35, ErrorMessage = "Address is maximum 35 characters long")]
        [RegularExpression("^[a-zA-Z0-9-,/;.'&quot; ]*$", ErrorMessage = "Unacceptable character in Address field. Please use only a-z,0-9 or -',/;.&quot;")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Country is required")]
        public string Country { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [Required(ErrorMessage = "State is required")]
        public string State { get; set; }

        [Required(ErrorMessage = "Zip Code is required")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Mobile Number is required")]
        [MaxLength(15, ErrorMessage = "MobileNumber cannot be longer than 10 digits.")]
        [RegularExpression(@"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-.]+\d$", ErrorMessage = "Entered phone format is not valid.")]
        public string MobileNumber { get; set; }

        public bool? status { get; set; }
        public string Company { get; set; }
        public string CuponCode { get; set; }
        public string AccountExpDate { get; set; }
        public int Plan_id { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class IsEmailExistsAttribute : ValidationAttribute, IClientValidatable
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(value)))
            {
                if ((new CustomerServices(new AuthorizeNetService())).isUserExists(value.ToString()))
                    return ValidationResult.Success;
            }
            return new ValidationResult(string.Format(ErrorMessageString, validationContext.DisplayName));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "isemailexists"
            };

            yield return rule;
        }
    }
}