using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _ChineseAddress.com.Repository;

namespace _ChineseAddress.com.Areas.Admin.Services
{
    public class TrackingService
    {
        public List<TrackingDetailModel> _TrackingDetail { get; set; }
        Context4InshipEntities Context = new Context4InshipEntities();
        public bool _isSuccess { get; set; }
        public string _msg { get; set; }

        private string GetCarrierName(string TrackingNumber)
        {
            try
            {
                string carrierName = Context.tblOrders.AsQueryable().Where(x => x.tracking_no == TrackingNumber).Select(x => x.carrier).FirstOrDefault();
                if (string.IsNullOrEmpty(carrierName))
                {
                    carrierName = (from tob in Context.tblOrderBoxes
                                   join todr in Context.tblOrders
                                   on tob.Fk_order_id equals todr.id
                                   where tob.tracking_no == TrackingNumber
                                   select todr.carrier).FirstOrDefault();
                }
                return carrierName ?? "UPS";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TrackingDetailModel TrackOrder(string TrackingNumber)
        {
            try
            {
                string carrierName = GetCarrierName(TrackingNumber);

                //Call particular tacking method
                System.Reflection.MethodInfo theMethod = this.GetType().GetMethod($"Track{carrierName}Order",
            System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                theMethod.Invoke(this, new[] { TrackingNumber });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return new TrackingDetailModel();
        }

        private void TrackUPSOrder(string Track)
        {
            clsUPSTrackWebService objclsTrackWebService = new clsUPSTrackWebService();
            objclsTrackWebService._trckingNo = Track;
            objclsTrackWebService.TrackerOrder();
            if (objclsTrackWebService._isSuccess)
            {
                _isSuccess = true;
                _TrackingDetail = objclsTrackWebService._TrackingDetail;
            }
            else
            {
                _isSuccess = objclsTrackWebService._isSuccess;
                _msg = objclsTrackWebService._msg;
            }
        }
        private void TrackFedExOrder(string Track)
        {

            clsFedExTrackingWebService _objclsFedExTrackingWebService = new clsFedExTrackingWebService();
            _objclsFedExTrackingWebService.TrackingNumber = Track;
            List<TrackingDetailModel> objTrackingDetail = _objclsFedExTrackingWebService._GetTrackingNumberDetails();
            if (_objclsFedExTrackingWebService._Success == true)
            {
                _isSuccess = true;
                _TrackingDetail = objTrackingDetail;
            }
        }
    }
}