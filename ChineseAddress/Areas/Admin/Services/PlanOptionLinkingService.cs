using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Services
{
    public class PlanOptionLinkingService : clsCommon
    {
        private Context4InshipEntities Context = new Context4InshipEntities();

        public List<ViewModeltblPlanOptionLinking> GetLinkingOptions()
        {
            try
            {
                List<ViewModeltblPlanOptionLinking> LinkingOptionsList = (from link in Context.tblPlanOptionLinkings join plan in Context.tblPlans on link.Fk_plan_id equals plan.Id join pack in Context.tblPackagingOptions on link.Fk_packaging_option_id equals pack.Id select new ViewModeltblPlanOptionLinking { PlanTitle = plan.title, packagingOptionTitle = pack.title, Id = link.Id, Price = link.price }).ToList();
                return LinkingOptionsList;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public tblPlanOptionLinking GetLinkingOptionsById(int Id)
        {
            try
            {
                tblPlanOptionLinking List = Context.tblPlanOptionLinkings.AsQueryable().Where(x => x.Id == Id).FirstOrDefault();
                List.price = Convert.ToDecimal(string.Format("{0:0.00}", List.price));
                return List;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                return null;
            }
        }

        public string InserUpdateLinkingOptions(tblPlanOptionLinking tblplanoptionlinking)
        {
            try
            {
                if (tblplanoptionlinking.Id != 0)
                {
                    tblPlanOptionLinking tbl = Context.tblPlanOptionLinkings.AsQueryable().Where(x => x.Id == tblplanoptionlinking.Id).FirstOrDefault();
                    if (tbl != null)
                    {
                        tbl.Id = tblplanoptionlinking.Id;
                        tbl.Fk_plan_id = tblplanoptionlinking.Fk_plan_id;
                        tbl.Fk_packaging_option_id = tblplanoptionlinking.Fk_packaging_option_id;
                        tbl.price = tblplanoptionlinking.price;
                        Context.Entry(tbl).State = System.Data.Entity.EntityState.Modified;
                        Context.SaveChanges();
                        return SuccessMsg("Updated successfully");
                    }
                }
                else
                {
                    Context.tblPlanOptionLinkings.Add(tblplanoptionlinking);
                }
                Context.SaveChanges();
                return SuccessMsg("Inserted successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public List<SelectListItem> GetPlan()
        {
            var stands = Context.tblPlans.ToList();
            List<SelectListItem> objplan = new List<SelectListItem>();
            objplan.Add(new SelectListItem() { Text = "Select Plan", Value = "" });
            foreach (var item in stands)
            {
                objplan.Add(new SelectListItem() { Value = Convert.ToString(item.Id), Text = item.title });
            }

            return objplan.ToList();
        }

        public List<SelectListItem> GetPackagingOptions()
        {
            var stands = Context.tblPackagingOptions.ToList();
            List<SelectListItem> objplan = new List<SelectListItem>();
            objplan.Add(new SelectListItem() { Text = "Select Packagingoption", Value = "" });
            foreach (var item in stands)
            {
                objplan.Add(new SelectListItem() { Value = Convert.ToString(item.Id), Text = item.title });
            }

            return objplan.ToList();
        }

        public string DeletePlanOptionLinkingById(int Id)
        {
            try
            {
                if (Id != 0)
                {
                    var ItemToRemove = Context.tblPlanOptionLinkings.AsQueryable().Where(x => x.Id == Id).FirstOrDefault();
                    if (ItemToRemove != null)
                    {
                        Context.Entry(ItemToRemove).State = System.Data.Entity.EntityState.Deleted;
                    }
                }
                Context.SaveChanges();
                return SuccessMsg("Deleted Successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }
    }
}