using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Common.Models;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.Repository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace _ChineseAddress.com.Areas.Admin.Services
{
    public class OrderService : clsCommon
    {
        private Context4InshipEntities Context = new Context4InshipEntities();

        public string InsertUpdateOrder(tblOrder Order)
        {
            try
            {
                if (Order.id != 0)
                {
                    var Ids = Order.id;
                    var tbl = Context.tblOrders.AsQueryable().Where(x => x.id == Ids).FirstOrDefault();
                    if (tbl != null)
                    {
                        tbl.id = Order.id;
                        tbl.reference_no = Order.reference_no;
                        tbl.Fk_customer_id = Order.Fk_customer_id;
                        tbl.Fk_invoice_id = Order.Fk_invoice_id;
                        tbl.carrier = Order.carrier;
                        tbl.product = Order.product;
                        tbl.service = Order.service;
                        tbl.pickup_date = Order.pickup_date;
                        tbl.pickup_cut_off_time = Order.pickup_cut_off_time;
                        tbl.booking_time = Order.booking_time;
                        tbl.delivery_date = Order.delivery_date;
                        tbl.delivery_time = Order.delivery_time;
                        tbl.payable_amount = Order.payable_amount;
                        tbl.tracking_no = Order.tracking_no;
                        tbl.billing_weight = Order.billing_weight;
                        tbl.is_delivered = Order.is_delivered;
                        tbl.signature = Order.signature;
                        Context.Entry(tbl).State = System.Data.Entity.EntityState.Modified;
                        return SuccessMsg("Updated successfully");
                    }
                }
                else
                {
                    Context.tblOrders.Add(Order);
                }
                Context.SaveChanges();
                return ErrorMsg("Saved successfully");
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public List<tblOrder> GetTblOrderById(int Id)
        {
            var lst = (from list in Context.tblOrders.AsQueryable().Where(x => x.id == Id) select list).ToList();
            return lst.ToList();
        }

        public string GetCarrierLogoByOrderId(int Id)
        {
            return Convert.ToString((from ord in Context.tblOrders
                                     join cd in Context.tblCarriersDetails on ord.service_code equals cd.code
                                     where ord.id == Id
                                     select cd.logo).FirstOrDefault());
        }

        public List<Models.ViewModelOrder> GetOrders()
        {
            try
            {
                List<Models.ViewModelOrder> OrderList = (from ord in Context.tblOrders
                                                         join cus in Context.tblCustomers on ord.Fk_customer_id equals cus.Id
                                                         join
                                                         deladd in Context.tblDeliveryAddresses on ord.Fk_delivery_address_id equals deladd.id
                                                         select new Models.ViewModelOrder
                                                         {
                                                             delivery_Address = deladd.address_name,
                                                             Fk_customer_id = cus.Id,
                                                             Customer_Email = cus.email,
                                                             Customer_reference = cus.customer_reference,
                                                             Order_id = ord.id,
                                                             Order_Refernce_No = ord.reference_no,
                                                             carrier = ord.carrier,
                                                             product = ord.product,
                                                             service = ord.service,
                                                             pickup_date = ord.pickup_date,
                                                             pickup_cut_off_time = ord.pickup_cut_off_time,
                                                             booking_time = ord.booking_time,
                                                             payable_amount = ord.payable_amount,
                                                             tracking_no = ord.tracking_no,
                                                             billing_weight = ord.billing_weight,
                                                             is_delivered = ord.is_delivered,
                                                             signature = ord.signature,
                                                             status = ord.status,
                                                             createdOn = ord.created_on,
                                                             urgent = ord.is_urgent
                                                         }).OrderByDescending(x => x.urgent).ThenByDescending(d => d.createdOn).ToList();
                return OrderList.ToList();
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                return null;
            }
        }

        public string DeletetblTblOrder(int Id)
        {
            try
            {
                var itemToRemove = Context.tblOrders.AsQueryable().SingleOrDefault(x => x.id == Id);
                if (itemToRemove != null)
                {
                    Context.Entry(itemToRemove).State = System.Data.Entity.EntityState.Deleted;
                }
                Context.SaveChanges();
                return SuccessMsg("Deleted successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public string CreateOrderBox(tblOrderBox tblorderbox)
        {
            try
            {
                if (tblorderbox.id != 0)
                {
                }
                else
                {
                    Context.tblOrderBoxes.Add(tblorderbox);
                }
                Context.SaveChanges();
                return SuccessMsg("Order box has been created successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public ViewModelOrderInformation GetOrderInformation(int Id)
        {
            var totalship = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == Id).Select(x => x).Count();
            var totalpackage = Context.tblOrderBoxes.Where(x => x.Fk_order_id == Id).Select(x => x.Fk_order_id).Count();
            decimal totalBillablewieight = (decimal)(Context.tblOrderBoxes.Where(x => x.Fk_order_id == Id).Sum(x => x.billable_weight) ?? 0.0m);
            var data = (from ord in Context.tblOrders join del in Context.tblDeliveryAddresses on ord.Fk_delivery_address_id equals del.id select new { ord.id, ord.reference_no, ord.created_on, ord.tracking_no, ord.status, ord.carrier, ord.order_notes, del.first_name, del.last_name, del.address1, del.address2, del.country_code, del.city, del.mobile1, del.state, del.postal_code}).Where(x => x.id == Id).ToList();
            var GetCountryName = (from tbdel in Context.tblDeliveryAddresses
                                  join tbord in Context.tblOrders
                                  on tbdel.id equals tbord.Fk_delivery_address_id
                                  join tbcountry in Context.tblCountryMasters
                                  on tbdel.country_code equals tbcountry.country_code
                                  select new { tbord.id, tbcountry.country_name }).Where(x => x.id == Id).FirstOrDefault();
            ViewModelOrderInformation objviewmodelinfo = new ViewModelOrderInformation();
            foreach (var item in data)
            {
                objviewmodelinfo.id = item.id;
                objviewmodelinfo.orderno = item.reference_no;
                objviewmodelinfo.shipmethod = item.carrier;
                objviewmodelinfo.status = item.status;
                objviewmodelinfo.firstName = item.first_name;
                objviewmodelinfo.lastName = item.last_name;
                objviewmodelinfo.address1 = item.address1;
                objviewmodelinfo.address2 = item.address2;
                objviewmodelinfo.country = (GetCountryName == null ? "" : GetCountryName.country_name);
                objviewmodelinfo.city = item.city;
                objviewmodelinfo.phone = item.mobile1;
                objviewmodelinfo.State = item.state;
                objviewmodelinfo.PostalCode = item.postal_code;
                objviewmodelinfo.totalshipment = Convert.ToString(totalship);
                objviewmodelinfo.trackingnumber = Convert.ToString((item.tracking_no ?? ""));
                objviewmodelinfo.orderon = item.created_on;
                objviewmodelinfo.billweight = Convert.ToString(Math.Floor(totalBillablewieight));
                objviewmodelinfo.totalpackage = Convert.ToString(totalpackage);
                objviewmodelinfo.order_notes = item.order_notes;
            }
            return objviewmodelinfo;
        }

        public List<tblOrderCharge> GetTblOrdeChangedByFkId(int id)
        {
            try
            {
                List<tblOrderCharge> obj = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == id).ToList();
                return obj;
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
                var errs = ex.Message; ;
                throw;
            }
        }

        public string ChangeStatusToProcessingById(int id)
        {
            try
            {
                if (id != 0)
                {
                    var itemToremove = Context.tblOrders.AsQueryable().Where(x => x.id == id).FirstOrDefault();
                    if (itemToremove != null)
                    {
                        itemToremove.status = 2;
                        Context.Entry(itemToremove).State = System.Data.Entity.EntityState.Modified;
                    }
                }
                Context.SaveChanges();
                var GetCustomerEmail = (from tbcust in Context.tblCustomers
                                        join tbord in Context.tblOrders on tbcust.Id equals tbord.Fk_customer_id
                                        join tbAddressbook in Context.tblAddressBooks on tbcust.Id equals tbAddressbook.Fk_customer_Id
                                        select new { tbcust.email, tbAddressbook.first_name, tbAddressbook.is_default, tbAddressbook.last_name, tbord.id, tbord.reference_no }).Where(x => x.id == id && x.is_default == true).FirstOrDefault();
                // var GetCustomerEmail = (from tbcust in Context.tblCustomers join tbord in Context.tblOrders on tbcust.Id equals tbord.Fk_customer_id select new { tbcust.email, tbord.id,tbord.reference_no }).Where(x => x.id == id).FirstOrDefault();
                string subject = "chineseaddress - Order Processing !";
                Hashtable htTemplate = new Hashtable();
                htTemplate.Add("#FirstName#", GetCustomerEmail.first_name);
                htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
                htTemplate.Add("#referenceno#", GetCustomerEmail.reference_no);
                string mailbody = (new clsEmailTemplateService()).GetEmailTemplate("orderproccessing", htTemplate);
                (new clsSendEmailService()).SendEmail(new[] { GetCustomerEmail.email }, null, null, mailbody, subject);
                return SuccessMsg("Status has been changed successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public List<ViewRecivedShipmentModel> CreateOrderRecivedShipment(int OrderID)
        {
            try
            {
                tblOrder objorder = new tblOrder();
                ViewRecivedShipmentModel objrecshipment = new ViewRecivedShipmentModel();
                List<ViewRecivedShipmentModel> objViewRecivedShipmentModelList = new List<ViewRecivedShipmentModel>();
                objorder.id = OrderID;
                var RecivedshipmentList = Context.tblReceivedShipments.Where(x => x.Fk_order_id == OrderID).ToList();
                foreach (var item in RecivedshipmentList)
                {
                    objrecshipment.RecivedShipment_ImagePath = Context.tblShipmentImages.Where(x => x.Fk_shipment_id == item.id).Select(x => x.file_path).ToList<dynamic>();

                    objViewRecivedShipmentModelList.Add(new ViewRecivedShipmentModel() { sender = item.sender, received_date = item.received_date, length = item.length, weight = item.weight, width = item.width, height = item.height, box_condition = item.box_condition, staff_comments = item.staff_comments, status = item.status, tracking = item.tracking, ID = item.id, RecivedShipment_ImagePath = objrecshipment.RecivedShipment_ImagePath });
                }
                return objViewRecivedShipmentModelList.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ViewRecivedShipmentModel> GetShipmentDetails(int OrderID)
        {
            try
            {
                List<ViewRecivedShipmentModel> ShipmentDetails
                    = (from tblrec
                    in Context.tblReceivedShipments
                       join tblship in Context.tblShipmentDetails
                       on tblrec.id equals tblship.Fk_shipment_id
                       where tblrec.Fk_order_id == OrderID
                       select new ViewRecivedShipmentModel
                       {
                           description = tblship.description,
                           purchase_price = tblship.purchase_price,
                           quantity = tblship.quantity,
                           Fk_shipment_id = tblship.Fk_shipment_id
                       }).ToList();
                return ShipmentDetails.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SaveOrderBoxes(tblOrderBox objtblorder)
        {
            try
            {
                decimal? DimentionalWeight = 0;
                decimal? SumDimentionalWeight = 0;
                // Get VolumeWeightFactor in tblcarrierdetails
                var GetVolumefactor = Context.Database.SqlQuery<string>("select tbCdatial.vol_weight_factor from tblorder tbord inner join tblCarriersDetail tbCdatial on tbord.carrier=tbCdatial.carrier_name and tbord.product=tbCdatial.product and tbord.service=tbCdatial.service and tbord.service_code=tbCdatial.code where tbord.id=" + objtblorder.id).FirstOrDefault();
                tblOrderBox objtblOrderBox = new tblOrderBox();
                var GetOrderboxesrecord = Context.tblOrderBoxes.Where(x => x.Fk_order_id == objtblorder.id).Select(x => x).ToList();
                Context.tblOrderBoxes.RemoveRange(GetOrderboxesrecord);
                Context.SaveChanges();
                for (int i = 0; i < objtblorder.Atualweight.Length; i++)
                {
                    objtblOrderBox.Fk_order_id = objtblorder.id;
                    objtblOrderBox.actual_weight = objtblorder.Atualweight[i];
                    objtblOrderBox.height = objtblorder.heights[i];
                    objtblOrderBox.width = objtblorder.widths[i];
                    objtblOrderBox.length = objtblorder.lengths[i];
                    DimentionalWeight = clsCommonServiceRoot.EvaluteMathematicExpression(GetVolumefactor, (objtblOrderBox.length ?? 0.0M), (objtblOrderBox.width ?? 0.0M), (objtblOrderBox.height ?? 0.0M));
                    //DimentionalWeight = (objtblOrderBox.length) * (objtblOrderBox.width) * (objtblOrderBox.height) / (Convert.ToDecimal(GetVolumefactor));
                    objtblOrderBox.dim_weight = (decimal)DimentionalWeight;
                    if (DimentionalWeight > objtblOrderBox.actual_weight)
                    {
                        objtblOrderBox.billable_weight = Math.Floor((decimal)DimentionalWeight);
                        SumDimentionalWeight += Math.Floor((decimal)DimentionalWeight);
                    }
                    else
                    {
                        objtblOrderBox.billable_weight = Math.Floor(objtblOrderBox.actual_weight);
                        SumDimentionalWeight += Math.Floor((decimal)objtblOrderBox.actual_weight);
                    }
                    Context.Configuration.ValidateOnSaveEnabled = false;
                    Context.tblOrderBoxes.Add(objtblOrderBox);
                    Context.SaveChanges();
                }
                var GetOrderBillableWeight = Context.tblOrders.AsQueryable().Where(x => x.id == objtblorder.id).Select(x => x).FirstOrDefault();
                GetOrderBillableWeight.billing_weight = (decimal)SumDimentionalWeight;
                Context.Entry(GetOrderBillableWeight).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
                return SuccessMsg("Order boxe(s) has been saved successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message.ToString());
            }
        }

        public List<tblOrderBox> GetOrderboxesData(int OrderID)
        {
            List<tblOrderBox> OrderboxesList = Context.tblOrderBoxes.Where(x => x.Fk_order_id == OrderID).ToList();
            return OrderboxesList.ToList();
        }

        public string SaveOrderCharges(string ChargePrice, string ChargeName, int id)
        {
            try
            {
                if (ChargePrice != "")
                {
                    tblOrderCharge objtblordercharges = new tblOrderCharge();
                    objtblordercharges.charge_name = ChargeName;
                    objtblordercharges.Fk_order_id = id;
                    objtblordercharges.sell_price = Convert.ToDecimal(ChargePrice);
                    Context.tblOrderCharges.Add(objtblordercharges);
                    Context.SaveChanges();
                    // Update payableAmount Order Table
                    UpdatePayableAmount(id);
                }
                return SuccessMsg("Order charges has been saved successfully");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string RestrictedShipments(int Recshipid, int RestricOrderID)
        {
            try
            {
                var RestrictedReshipment = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == RestricOrderID && x.id == Recshipid).FirstOrDefault();
                RestrictedReshipment.status = 5;
                Context.Entry(RestrictedReshipment).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
                return SuccessMsg("Shipment has been restricted successfully");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RestrictedOrder(int RestricOrderID, string RestrictMsg)
        {
            try
            {
                var RestrictedOrder = Context.tblOrders.AsQueryable().Where(x => x.id == RestricOrderID).Select(x => x).FirstOrDefault();
                RestrictedOrder.status = 7;
                Context.Entry(RestrictedOrder).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
                //var CustomerEmailAddress = Context.tblCustomers.AsQueryable().Where(x => x.Id == RestrictedOrder.Fk_customer_id).Select(x => x.email).FirstOrDefault();
                var GetCustomerEmail = (from tbcust in Context.tblCustomers
                                        join tbord in Context.tblOrders on tbcust.Id equals tbord.Fk_customer_id
                                        join tbdelivery in Context.tblDeliveryAddresses on tbord.Fk_delivery_address_id equals tbdelivery.id
                                        select new { tbcust.email, tbdelivery.first_name, tbdelivery.last_name, tbord.id, tbord.reference_no }).Where(x => x.id == RestricOrderID).FirstOrDefault();
                if (GetCustomerEmail != null)
                {
                    clsEmailTemplateService objclsEmailTemplateService = new clsEmailTemplateService();
                    clsSendEmailService objclsSendEmailService = new clsSendEmailService();
                    string subject = "chineseaddress - Order and  Shipment Restricted !";
                    System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                    hashTable.Add("#FirstName#", GetCustomerEmail.first_name);
                    hashTable.Add("#LastName#", GetCustomerEmail.last_name);
                    hashTable.Add("#referenceno#", GetCustomerEmail.reference_no);
                    if (!string.IsNullOrEmpty(RestrictMsg))
                    {
                        hashTable.Add("#Msg#", RestrictMsg);
                    }
                    else
                        hashTable.Add("#Msg#", "");
                    objclsSendEmailService.SendEmail(new[] { GetCustomerEmail.email }, null, null, objclsEmailTemplateService.GetEmailTemplate("restricted", hashTable), subject);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCustomerEmailById(int Id)
        {
            var CustomerEmail = (from ord in Context.tblOrders join cust in Context.tblCustomers on ord.Fk_customer_id equals cust.Id select new { Email = cust.email, ord.id }).Where(x => x.id == Id).FirstOrDefault();
            return CustomerEmail.Email;
        }

        public int ChangeStatusToDispatchByID(int Id)
        {
            try
            {
                tblOrder OrderTable = Context.tblOrders.AsQueryable().Where(x => x.id == Id).FirstOrDefault();
                //List<tblReceivedShipment> ShipmentItem = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == Id).ToList();
                //byte shipmentStatus = Convert.ToByte(enumReceivShipmentStatus.Order_Created);
                //ShipmentItem.ForEach(x => x.status = shipmentStatus);
                if (OrderTable != null)
                {
                    OrderTable.status = Convert.ToByte(enumorder.Dispatched);
                    Context.Entry(OrderTable).State = System.Data.Entity.EntityState.Modified;
                }
                Context.SaveChanges();
                return OrderTable.Fk_customer_id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ChangeStatusToProcessingByID(int Id)
        {
            try
            {
                tblOrder OrderTable = Context.tblOrders.AsQueryable().Where(x => x.id == Id).FirstOrDefault();
                List<tblReceivedShipment> ShipmentItem = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == Id).ToList();
                byte shipmentStatus = Convert.ToByte(enumReceivShipmentStatus.Processing);
                ShipmentItem.ForEach(x => x.status = shipmentStatus);
                if (OrderTable != null)
                {
                    OrderTable.status = Convert.ToByte(enumorder.Proccessing);
                    Context.Entry(OrderTable).State = System.Data.Entity.EntityState.Modified;
                }
                Context.SaveChanges();
                return OrderTable.Fk_customer_id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ChangeStatusToOnHoldByID(int Id)
        {
            try
            {
                tblOrder OrderTable = Context.tblOrders.AsQueryable().Where(x => x.id == Id).FirstOrDefault();
                List<tblReceivedShipment> ShipmentItem = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == Id).ToList();
                byte shipmentStatus = Convert.ToByte(enumReceivShipmentStatus.On_Hold);
                ShipmentItem.ForEach(x => x.status = shipmentStatus);
                if (OrderTable != null)
                {
                    OrderTable.status = Convert.ToByte(enumorder.On_Hold);
                    Context.Entry(OrderTable).State = System.Data.Entity.EntityState.Modified;
                }
                Context.SaveChanges();
                return OrderTable.Fk_customer_id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string[] CalculateOrderCharges(int OrderID)
        {
            try
            {
                string[] message = new string[3];
                clsCarriersService objclsCarriersService = new clsCarriersService();
                objclsCarriersService._orderId = OrderID;
                objclsCarriersService.getCarrierPrices();
                if (objclsCarriersService._IsSuccess == true)
                {
                    if (objclsCarriersService._carriersRates != null && objclsCarriersService._carriersRates.Any())
                    {
                        carriersRatesResponseModel shippingChargesModel = objclsCarriersService._carriersRates.FirstOrDefault();
                        var GetCustomerPlan = (from tbord in Context.tblOrders join tbcustlink in Context.tblCustomerPlanLinkings on tbord.Fk_customer_id equals tbcustlink.Fk_Customer_Id select new { tbord.id, tbcustlink.plan_title }).AsQueryable().Where(x => x.id == OrderID).Select(x => x.plan_title).FirstOrDefault();
                        var GetMarginProfit = Context.tblPlans.AsQueryable().Where(x => x.title == GetCustomerPlan).Select(x => x.profit_margin).FirstOrDefault();
                        message[0] = "Shipping charges calculated successfully.";
                        message[1] = Convert.ToString(shippingChargesModel.TotalShipmentCharge);
                        message[2] = Convert.ToString(GetMarginProfit);
                    }
                    else
                    {
                        var GetCustomerPlan = (from tbord in Context.tblOrders join tbcustlink in Context.tblCustomerPlanLinkings on tbord.Fk_customer_id equals tbcustlink.Fk_Customer_Id select new { tbord.id, tbcustlink.plan_title }).AsQueryable().Where(x => x.id == OrderID).Select(x => x.plan_title).FirstOrDefault();
                        var GetMarginProfit = Context.tblPlans.AsQueryable().Where(x => x.title == GetCustomerPlan).Select(x => x.profit_margin).FirstOrDefault();
                        message[0] = "Error occured while calculating shipping charges";
                        message[2] = Convert.ToString(GetMarginProfit);
                    }
                }
                else
                {
                    var GetCustomerPlan = (from tbord in Context.tblOrders join tbcustlink in Context.tblCustomerPlanLinkings on tbord.Fk_customer_id equals tbcustlink.Fk_Customer_Id select new { tbord.id, tbcustlink.plan_title }).AsQueryable().Where(x => x.id == OrderID).Select(x => x.plan_title).FirstOrDefault();
                    var GetMarginProfit = Context.tblPlans.AsQueryable().Where(x => x.title == GetCustomerPlan).Select(x => x.profit_margin).FirstOrDefault();
                    message[0] = objclsCarriersService._msg;
                    message[2] = Convert.ToString(GetMarginProfit);
                }
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SaveShippingCharge(int Orderid, decimal BuyPrice, decimal SellPrice)
        {
            tblOrderCharge charge = Context.tblOrderCharges.AsQueryable().Where(x => x.charge_name == "Shipping Charges" && x.Fk_order_id == Orderid).FirstOrDefault();
            if (charge != null)
            {
                charge.buy_price = BuyPrice;
                charge.sell_price = SellPrice;
                Context.Entry(charge).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
                tblOrder objorder = Context.tblOrders.AsQueryable().Where(x => x.id == Orderid).FirstOrDefault();
                objorder.save_amount = SellPrice;
                Context.Entry(objorder).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();

            }
            else
            {
                charge = new tblOrderCharge();
                charge.charge_name = "Shipping Charges";
                charge.buy_price = BuyPrice;
                charge.sell_price = SellPrice;
                charge.Fk_order_id = Orderid;
                Context.tblOrderCharges.Add(charge);
                Context.SaveChanges();
                tblOrder objorder = Context.tblOrders.AsQueryable().Where(x => x.id == Orderid).FirstOrDefault();
                objorder.save_amount = SellPrice;
                Context.Entry(objorder).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
            }
            // Update PayableAmount OrderCharges Table
            UpdatePayableAmount(Orderid);
            return "Order charges has been saved successfully";
        }

        //public Hashtable GetPerformaInvoiceReport(int Orderid)
        //{
        //    try
        //    {
        //        var GettblOrders = Context.tblOrders.AsQueryable().Where(x => x.id == Orderid).Select(x => x).FirstOrDefault();
        //        var Getinvoice = Context.tblInvoices.AsQueryable().Where(x => x.Fk_customer_id == GettblOrders.Fk_customer_id).Select(x => x).FirstOrDefault();
        //        var GetCustomerRefernceNumber = Context.tblCustomers.AsQueryable().Where(x => x.Id == GettblOrders.Fk_customer_id).Select(x => x.customer_reference).FirstOrDefault();
        //        var CustomerDeliveryAddress = Context.tblDeliveryAddresses.AsQueryable().Where(x => x.id == GettblOrders.Fk_delivery_address_id).Select(x => x).FirstOrDefault();
        //        var GetAdreessbookData = Context.tblAddressBooks.Where(x => x.Fk_customer_Id == GettblOrders.Fk_customer_id).Select(x => x).FirstOrDefault();
        //        var GetReciveshipmentid = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == Orderid).Select(x => x.id).ToList();
        //        var GetCountryName = (from tbdel in Context.tblDeliveryAddresses join tbord in Context.tblOrders on tbdel.id equals tbord.Fk_delivery_address_id join tbcountry in Context.tblCountryMasters on tbdel.country_code equals tbcountry.country_code select new { tbord.id, tbcountry.country_name }).Where(x => x.id == Orderid).FirstOrDefault();
        //        var NetWeight = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == Orderid).Sum(x => x.billable_weight);
        //        var GetCountPackages = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == Orderid).ToList().Count;
        //        StringBuilder shipmentdetails = new StringBuilder();
        //        decimal TotalValues = 0;
        //        foreach (var item in GetReciveshipmentid)
        //        {
        //            var GetShipmentDetails = Context.tblShipmentDetails.AsQueryable().Where(x => x.Fk_shipment_id == item).Select(x => x).ToList();
        //            GetShipmentDetails.ForEach(x =>
        //            {
        //                decimal Totalvaluecurrency = x.purchase_price * x.quantity;
        //                shipmentdetails.Append("<tr><td>" + x.description + "</td><td>China</td><td>" + x.comodity_code + "</td><td>" + x.quantity + "</td><td>" + x.purchase_price + "</td><td>" + Totalvaluecurrency + "</td></tr>");
        //                TotalValues += Totalvaluecurrency;
        //            });
        //        }
        //        System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
        //        hashTable.Add("#invoicedate#", Getinvoice.invoice_date.Value.ToString("yyyy/mm/dd"));
        //        hashTable.Add("#byname#", "ChineseAddress");
        //        hashTable.Add("#byaddress#", "2115 S Union Ave/	Suite" + GetCustomerRefernceNumber);
        //        hashTable.Add("#bycity#", "	Alliance/44601");
        //        hashTable.Add("#bycountry#", "US");
        //        hashTable.Add("#bytelephone#", Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ClientPhoneNo"]));
        //        hashTable.Add("#toname#", GetAdreessbookData.first_name + "/" + GetAdreessbookData.last_name);
        //        hashTable.Add("#toaddress#", CustomerDeliveryAddress.address1);
        //        hashTable.Add("#tocity#", CustomerDeliveryAddress.city + "/" + CustomerDeliveryAddress.postal_code);
        //        hashTable.Add("#tocountry#", GetCountryName.country_name);
        //        hashTable.Add("#totelephone#", CustomerDeliveryAddress.mobile1);
        //        hashTable.Add("#shipmentdetail#", shipmentdetails);
        //        hashTable.Add("#demototal#", TotalValues);
        //        hashTable.Add("#billableweight#", NetWeight ?? 0.0m);
        //        hashTable.Add("#carrier#", GettblOrders.carrier);
        //        hashTable.Add("#reasonforexport#", ".................................");
        //        hashTable.Add("#termsofdelivery#", ".................................");
        //        hashTable.Add("#numberandkindpackage#", GetCountPackages);
        //        hashTable.Add("#name#", "");
        //        hashTable.Add("#placeanddate#", "");
        //        hashTable.Add("#signature#", "");
        //        return hashTable;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public string ConfirmOrder(int id)
        {
            try
            {
                string message = "";
                var PaymentAwaitingStatus = Context.tblOrders.AsQueryable().Where(x => x.id == id).FirstOrDefault();
                PaymentAwaitingStatus.status = 3;
                Context.Entry(PaymentAwaitingStatus).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
                //Save Coupon Charges in orderCharges table//
                var GetCouponData = Context.tblCoupons.AsQueryable().Where(x => x.coupon_code == PaymentAwaitingStatus.coupon_code).FirstOrDefault();
                if (GetCouponData != null)
                {
                    System.Data.Entity.Core.Objects.ObjectParameter objIsValidparameter = new System.Data.Entity.Core.Objects.ObjectParameter("Returnparameter", typeof(string));
                    // ObjectParameter objParam = new ObjectParameter("Returnparameter ", typeof(int));
                    Context.usp_AdminCoupon(id, GetCouponData.coupon_code, objIsValidparameter);
                    UpdatePayableAmount(id);
                    string returnPerameterValue = Convert.ToString(objIsValidparameter.Value);
                    if (returnPerameterValue != null && returnPerameterValue != "")
                    {
                        var RemoveCouponOrdertbl = Context.tblOrders.AsQueryable().Where(x => x.id == id).Select(x => x).FirstOrDefault();
                        RemoveCouponOrdertbl.coupon_code = "";
                        Context.Entry(RemoveCouponOrdertbl).State = System.Data.Entity.EntityState.Modified;
                        Context.SaveChanges();
                        message = SuccessMsg("Order has been confirmed successfully but " + returnPerameterValue);
                    }
                }
                if (message == "")
                {
                    message = SuccessMsg("Order has been confirmed successfully.");
                }
                return message;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<tblOrderBox> GetTblOrdeBoxesByFkId(int id)
        {
            try
            {
                List<tblOrderBox> obj = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == id).ToList();
                return obj;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string SaveTrackingNumber(tblOrder tblorder)
        {
            try
            {
                var Orderdata = Context.tblOrders.Where(x => x.id == tblorder.id).Select(x => x).FirstOrDefault();
                Orderdata.tracking_no = tblorder.tracking_no;
                Context.Configuration.ValidateOnSaveEnabled = false;
                Context.Entry(Orderdata).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
                return "Notes has been successfully  saved";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePayableAmount(int OrderID)
        {
            try
            {

                var Getordertbl = Context.tblOrders.AsQueryable().Where(x => x.id == OrderID).Select(x => x).FirstOrDefault();
                var GetCouponValue = Context.tblOrderCharges.AsQueryable().Where(x => x.charge_name.ToLower() == "coupon" && x.Fk_order_id == OrderID).FirstOrDefault();
                if (GetCouponValue != null)
                {
                    var GetOrderCharges = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == OrderID && x.charge_name.ToLower() != "coupon").Sum(x => x.sell_price);
                    if (Math.Abs(Convert.ToDecimal(GetCouponValue.sell_price)) > GetOrderCharges)
                    {
                        Getordertbl.payable_amount = 0.0m;
                        Context.Entry(Getordertbl).State = System.Data.Entity.EntityState.Modified;
                        Context.SaveChanges();
                    }
                    else if (GetOrderCharges > Math.Abs(Convert.ToDecimal(GetCouponValue.sell_price)))
                    {
                        Getordertbl.payable_amount = (GetOrderCharges) - (Math.Abs(Convert.ToDecimal(GetCouponValue.sell_price)));
                        Context.Entry(Getordertbl).State = System.Data.Entity.EntityState.Modified;
                        Context.SaveChanges();
                    }
                }
                else
                {
                    var GetSumOrdercharges = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == OrderID).Sum(x => x.sell_price);
                    Getordertbl.payable_amount = GetSumOrdercharges;
                    Context.Entry(Getordertbl).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public decimal GetOrderCharges(int OrderID)
        {
            decimal Charges = 0.0m;
            var GetSumOrdercharges = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == OrderID).Sum(x => x.sell_price);
            Charges = (GetSumOrdercharges ?? 0.0m);
            if (Charges <= 0.00m)
            {
                Charges = 0.00m;
            }
            //var GetCouponValue = Context.tblOrderCharges.AsQueryable().Where(x => x.charge_name.ToLower() == "coupon" && x.Fk_order_id == OrderID).FirstOrDefault();
            //var GetOrderCharges = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == OrderID && x.charge_name.ToLower() != "coupon").Sum(x => x.sell_price);
            //if (GetCouponValue != null)
            //{

            //    Charges = Convert.ToDecimal(GetOrderCharges??0.0m);
            //    if (Math.Abs(Convert.ToDecimal(GetCouponValue.sell_price)) > GetOrderCharges)
            //    {
            //        Charges = 0.00m;
            //    }

            //}
            //else
            //{
            //    var GetSumOrdercharges = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == OrderID).Sum(x => x.sell_price);
            //    Charges = Convert.ToDecimal(GetSumOrdercharges);
            //}
            return Charges;
        }
    }
}