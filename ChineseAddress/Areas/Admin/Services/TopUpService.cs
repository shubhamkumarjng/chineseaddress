using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.Repository;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace _ChineseAddress.com.Areas.Admin.Services
{

    public class TopUpService : clsCommon
    {
        [Dependency]
        public IWalletService<tblTopUp, tblTopUpMethod> _repositoryUserWalletService { get; set; }

        private Context4InshipEntities Context = new Context4InshipEntities();

        public List<tblTopUpMethod> getTopUpMethods()
        {
            return Context.tblTopUpMethods.OrderByDescending(x => x.id).ToList();
        }

        public tblTopUpMethod getTopUpMethodById(int id)
        {
            return Context.tblTopUpMethods.Where(x => x.id == id).FirstOrDefault();
        }

        public string SaveUpadateTopUpMethod(tblTopUpMethod objTopUp)
        {
            try
            {
                if (objTopUp.id == 0)
                {
                    Context.tblTopUpMethods.Add(objTopUp);
                    Context.SaveChanges();
                    return SuccessMsg("Top Up Method has been saved successfully.");
                }
                else
                {
                    Context.Entry(objTopUp).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();
                    return SuccessMsg("Top Up Method has been updated successfully.");
                }
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message.ToString());
            }
        }

        public string ChangeStatustblTopUpMethods(int id)
        {
            try
            {
                var objChangeStatusTopUpMethod = Context.tblTopUpMethods.AsQueryable().Where(x => x.id == id).SingleOrDefault();
                if (objChangeStatusTopUpMethod != null)
                {
                    objChangeStatusTopUpMethod.status = !objChangeStatusTopUpMethod.status;
                    Context.Entry(objChangeStatusTopUpMethod).Property(x => x.status).IsModified = true;
                    Context.SaveChanges();
                    return SuccessMsg("Status has been changed successfully.");
                }
                return SuccessMsg("Error occured while changing status.");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message.ToString());
            }
        }
        

    }
}