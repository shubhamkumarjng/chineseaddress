using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Services
{
    public class CouponService : _ChineseAddress.com.Areas.Admin.Models.clsCommon
    {
        private Context4InshipEntities Context = new Context4InshipEntities();

        public IEnumerable<SelectListItem> getAllCustomers()
        {
            try
            {
                return (from x in Context.tblCustomers
                        select new SelectListItem
                        {
                            Text = x.customer_reference + "/" + x.email,
                            Value = System.Data.Entity.SqlServer.SqlFunctions.StringConvert((double)x.Id).Trim()
                        }).AsEnumerable();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string AddUpdateCoupon(tblCoupon objCoupon)
        {
            try
            {
                if (objCoupon.id == 0)
                {
                    Context.tblCoupons.Add(objCoupon);
                    Context.SaveChanges();
                    return SuccessMsg("Coupon created successfully.");
                }
                else if (objCoupon.id != 0)
                {
                    Context.Entry(objCoupon).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();
                    return SuccessMsg("Coupon updated successfully.");
                }
                else
                    return ErrorMsg("Error Occured while saved coupon");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public tblCoupon getCouponById(int id)
        {
            try
            {
                return Context.tblCoupons.AsQueryable().Where(x => x.id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblCoupon> GetCouponDetails()
        {
            try
            {
                dynamic GetCouponList = (from tbcoupon in Context.tblCoupons
                                         join tbcust in Context.tblCustomers on tbcoupon.customer_id equals tbcust.Id
                                         into temp
                                         from data in temp.DefaultIfEmpty()
                                         select new
                                         {
                                             country_to = tbcoupon.country_to,
                                             coupon_code = tbcoupon.coupon_code,
                                             coupon_type = tbcoupon.coupon_type,
                                             reccuring = tbcoupon.reccuring,
                                             coupon_value = tbcoupon.coupon_value,
                                             customer_name = data.customer_reference == null ? "--" : (data.customer_reference + "/" + data.email),
                                             end_date = tbcoupon.end_date,
                                             start_date = tbcoupon.start_date,
                                             status = tbcoupon.status,
                                             max_order_value = tbcoupon.max_order_value,
                                             min_order_value = tbcoupon.min_order_value,
                                             new_users_only = tbcoupon.new_users_only,
                                             uniq_per_user = tbcoupon.uniq_per_user,
                                             id = tbcoupon.id
                                         }).ToList<dynamic>();
                //List<tblCoupon> GetCouponList = Context.tblCoupons.AsQueryable().OrderByDescending(x => x.id).ToList();
                List<tblCoupon> objtblCoupon = new List<tblCoupon>();
                foreach (var item in GetCouponList)
                {
                    string country_to = (string)item.country_to;
                    var GetCountryName = Context.tblCountryMasters.AsQueryable().Where(x => x.country_code == country_to).Select(x => x.country_name).FirstOrDefault();
                    objtblCoupon.Add(new tblCoupon() { country_to = GetCountryName, coupon_code = item.coupon_code, coupon_type = item.coupon_type, coupon_value = item.coupon_value, status = item.status, start_date = item.start_date, end_date = item.end_date, min_order_value = item.min_order_value, max_order_value = item.max_order_value, reccuring = item.reccuring, new_users_only = item.new_users_only, uniq_per_user = item.uniq_per_user, customer_name = item.customer_name, id = item.id });
                }
                return objtblCoupon.OrderByDescending(x=>x.id).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsCouponExist(string coupon_code, int id)
        {
            try
            {
                return Context.tblCoupons.AsQueryable().Any(x => x.coupon_code == coupon_code && x.id != id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}