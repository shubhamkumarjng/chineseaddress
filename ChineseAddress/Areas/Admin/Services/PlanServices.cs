using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _ChineseAddress.com.Areas.Admin.Services
{
    public class PlanServices : clsCommon
    {
        private Context4InshipEntities Context = new Context4InshipEntities();

        public string InsertUpdatePlan(tblPlan objtblPlan)
        {
            try
            {
                if (objtblPlan.Id != 0)
                {
                    tblPlan tbl = Context.tblPlans.AsQueryable().Where(x => x.Id == objtblPlan.Id).FirstOrDefault();
                    tbl.Id = objtblPlan.Id;
                    // tbl.title = objtblPlan.title;
                    tbl.description = objtblPlan.description;
                    tbl.price = objtblPlan.price;
                    tbl.free_storage_days = objtblPlan.free_storage_days;
                    tbl.is_recurring = objtblPlan.is_recurring;
                    tbl.recurring_duration = 1;
                    tbl.profit_margin = objtblPlan.profit_margin;
                    tbl.display_order = objtblPlan.display_order;
                    tbl.modified_on = DateTime.Now;
                    tbl.status = objtblPlan.status;
                    tbl.modified_on = DateTime.Now;
                    Context.Entry(tbl).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();
                    return SuccessMsg("Updated successfully");
                }
                else
                {
                    objtblPlan.recurring_duration = 1;
                    objtblPlan.created_on = DateTime.Now;
                    Context.tblPlans.Add(objtblPlan);
                }
                Context.SaveChanges();
                return SuccessMsg("Saved successfully");
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public List<tblPlan> GettblPlans()
        {
            return Context.tblPlans.AsQueryable().OrderBy(x => x.display_order).ToList();
        }

        public string ChangeStatusTblPlanById(int Id)
        {
            try
            {
                var ItemToRemove = Context.tblPlans.AsQueryable().SingleOrDefault(x => x.Id == Id);
                if (ItemToRemove != null)
                {
                    ItemToRemove.status = !ItemToRemove.status;
                    Context.Entry(ItemToRemove).State = System.Data.Entity.EntityState.Modified;
                }
                Context.SaveChanges();
                return SuccessMsg("Status has been changed successfully");
            }
            catch (Exception ex)

            {
                return SuccessMsg(ex.Message);
            }
        }

        public string ChangeIS_RecurringTblPlanById(int Id)
        {
            try
            {
                var ItemToRemove = Context.tblPlans.AsQueryable().SingleOrDefault(x => x.Id == Id);
                if (ItemToRemove != null)
                {
                    ItemToRemove.is_recurring = !ItemToRemove.is_recurring;
                    Context.Entry(ItemToRemove).State = System.Data.Entity.EntityState.Modified;
                }
                Context.SaveChanges();
                return SuccessMsg("Status has been changed successfully");
            }
            catch (Exception ex)

            {
                return SuccessMsg(ex.Message);
            }
        }

        public tblPlan GetblPlansById(int id)
        {
            return Context.tblPlans.AsQueryable().Where(x => x.Id == id).OrderByDescending(x => x.created_on).FirstOrDefault();
        }
    }
}