using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _ChineseAddress.com.Areas.Admin.Services
{
    public class InvoiceService
    {
        private Context4InshipEntities Context = new Context4InshipEntities();

        public string InsertUpdateTblInvoice(tblInvoice tblinvoice)
        {
            try
            {
                if (tblinvoice.Id != 0)
                {
                    var tbl = Context.tblInvoices.Where(x => x.Id == tblinvoice.Id).FirstOrDefault();
                    if (tbl != null)
                    {
                        //tbl.Id = tblpackagingoption.Id;
                        //tbl.title = tblpackagingoption.title;
                        //tbl.description = tblpackagingoption.description;
                        //tbl.status = true;
                        //tbl.modified_on = DateTime.Now;
                        Context.Configuration.ValidateOnSaveEnabled = false;
                        Context.Entry(tbl).State = System.Data.Entity.EntityState.Modified;
                        return "Updated successfully";
                    }
                }
                else
                {
                    tblinvoice.invoice_date = DateTime.Now;
                    tblinvoice.paid_on = DateTime.Now;
                    Context.tblInvoices.Add(tblinvoice);
                    //  Context.Configuration.ValidateOnSaveEnabled = false;
                }
                Context.SaveChanges();
                return "Saved successfully";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
      
        public List<ViewModelInvoice> GetTblInvoiceDetail()
        {
            //int InvoiceId;
            var InvoiveList = (from inv in Context.tblInvoices
                               join cus in Context.tblCustomers on inv.Fk_customer_id equals cus.Id
                               join bill in Context.tblBillingAddresses on inv.Fk_billing_address_id equals bill.Id
                               join link in Context.tblCustomerPlanLinkings on inv.Fk_customer_id equals link.Fk_Customer_Id
                               join ord in Context.tblOrders on inv.Id equals ord.Fk_invoice_id
                               into tempOrder
                               from data in tempOrder.DefaultIfEmpty()
                               select new
                               {
                                   cus.email,
                                   cus.customer_reference,
                                   cus.Id,
                                   bill.address,
                                   InvoiceId=inv.Id,
                                   inv.invoice_number,
                                   inv.invoice_date,
                                   inv.invoice_type,
                                   inv.transaction_status,
                                   inv.paid_status,
                                   inv.custom_guid,
                                   inv.paid_on,
                                   inv.payment_method,
                                   inv.payment_amount,
                                   inv.transaction_id,
                                   inv.transaction_response,
                                   inv.plan_type,
                                   reference_no = (data == null ? "--" : data.reference_no),
                                   orid = (data == null ? 0 : data.id)
                               }).OrderByDescending(x => x.invoice_date).ToList();
            List<ViewModelInvoice> objtblInvoice = new List<ViewModelInvoice>();
            foreach (var item in InvoiveList)
            {
                objtblInvoice.Add(new ViewModelInvoice() { orderId =item.orid, orderNO = item.invoice_type == "signup" ? item.plan_type : item.reference_no, Id = item.Id, customer = item.customer_reference, billingaddress = item.address, invoicenumber = item.invoice_number, invoice_date = item.invoice_date, paid_status = Convert.ToInt32(item.paid_status), paid_on = item.paid_on, payment_method = item.payment_method, payment_amount = item.payment_amount, transaction_id = item.transaction_id, custom_guid = item.custom_guid, invoice_type = item.invoice_type, transaction_status = item.transaction_status, transaction_response = item.transaction_response, customeremail = item.email });
            }
            return objtblInvoice.ToList();
        }
    }
}