using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _ChineseAddress.com.Areas.Admin.Services
{
    public class PackagingOptionsService : clsCommon
    {
        private Context4InshipEntities Context = new Context4InshipEntities();

        public string InsertUpdatePackagingOptions(tblPackagingOption tblpackagingoption)
        {
            try
            {
                if (tblpackagingoption.Id != 0)
                {
                    tblPackagingOption tbl = Context.tblPackagingOptions.AsQueryable().Where(x => x.Id == tblpackagingoption.Id).FirstOrDefault();
                    if (tbl != null)
                    {
                        tbl.Id = tblpackagingoption.Id;
                        tbl.title = tblpackagingoption.title;
                        tbl.description = tblpackagingoption.description;
                        tbl.status = true;
                        tbl.modified_on = DateTime.Now;
                        tbl.is_shipment = tblpackagingoption.is_shipment;
                        tbl.is_signup = tblpackagingoption.is_signup;
                        Context.Configuration.ValidateOnSaveEnabled = false;
                        Context.Entry(tbl).State = System.Data.Entity.EntityState.Modified;
                        Context.SaveChanges();
                        return SuccessMsg("Updated successfully");
                    }
                }
                else
                {
                    tblpackagingoption.created_on = DateTime.Now;
                    tblpackagingoption.status = true;
                    Context.tblPackagingOptions.Add(tblpackagingoption);
                    //Context.Configuration.ValidateOnSaveEnabled = false;
                }
                Context.SaveChanges();
                return SuccessMsg("Saved successfully");
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public List<tblPackagingOption> GetblPackagingOptionsById(int Id)
        {
            var lst = (from list in Context.tblPackagingOptions.AsQueryable().Where(x => x.Id == Id) select list).ToList();
            return lst.ToList();
        }

        public List<tblPackagingOption> GettblPackagingOptions()
        {
            return Context.tblPackagingOptions.OrderByDescending(x => x.created_on).ToList();
        }

        public string ChangeStatustblPackagingOptions(int Id)
        {
            try
            {
                var itemToRemove = Context.tblPackagingOptions.SingleOrDefault(x => x.Id == Id);
                if (itemToRemove != null)
                {
                    Context.Configuration.ValidateOnSaveEnabled = false;
                    itemToRemove.status = !itemToRemove.status;
                    Context.Entry(itemToRemove).State = System.Data.Entity.EntityState.Modified;
                }
                Context.SaveChanges();
                return SuccessMsg("Status changed successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public string ChangeSignuptblPackagingOptions(int Id)
        {
            try
            {
                var itemToRemove = Context.tblPackagingOptions.SingleOrDefault(x => x.Id == Id);
                if (itemToRemove != null)
                {
                    Context.Configuration.ValidateOnSaveEnabled = false;
                    itemToRemove.is_signup = !itemToRemove.is_signup;
                    Context.Entry(itemToRemove).State = System.Data.Entity.EntityState.Modified;
                }
                Context.SaveChanges();
                return SuccessMsg("Status changed successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public string ChangeShipmentptblPackagingOptions(int Id)
        {
            try
            {
                var itemToRemove = Context.tblPackagingOptions.SingleOrDefault(x => x.Id == Id);
                if (itemToRemove != null)
                {
                    itemToRemove.is_shipment = !(itemToRemove.is_shipment ?? false);
                    Context.Entry(itemToRemove).State = System.Data.Entity.EntityState.Modified;
                }
                Context.SaveChanges();
                return SuccessMsg("Status changed successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }
    }
}