using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _ChineseAddress.com.Areas.Admin.Services
{
    public class Customerservice : clsCommon
    {
        private Context4InshipEntities Context = new Context4InshipEntities();

        public List<tblCustomer> GetCustomerDetails()
        {
            return Context.tblCustomers.OrderByDescending(x => x.Id).ToList();
        }

        public string ChangeStatustblCustomer(int id)
        {
            try
            {
                var itemToRemove = Context.tblCustomers.AsQueryable().Where(x => x.Id == id).FirstOrDefault();
                if (itemToRemove != null)
                {
                 

                    if (itemToRemove!=null)
                    {
                        var GetRecivedShipmnetRestrictedRecord = Context.tblReceivedShipments.AsQueryable().Where(x => x.status == (int)enumReceivShipmentStatus.Restricted).Where(q => q.Fk_Customer_Id == id).ToList();
                        var GetRecivedShipmnetNotRestrictedAndDisposedoff = Context.tblReceivedShipments.AsQueryable().Where(x => x.status != (int)enumReceivShipmentStatus.Restricted || x.status != (int)enumReceivShipmentStatus.Dispossed_Off).Where(q => q.Fk_Customer_Id == id).ToList();
                        var GetRecivedShipmnetDisposOff = Context.tblReceivedShipments.AsQueryable().Where(x =>  x.status != (int)enumReceivShipmentStatus.Dispossed_Off).Where(q => q.Fk_Customer_Id == id).ToList();
                        var GetOrderRecord = Context.tblOrders.AsQueryable().Where(x => x.status == (int)enumorder.Restricted && x.Fk_customer_id == id).ToList();
                        if(GetRecivedShipmnetDisposOff.Count()!=0 && GetOrderRecord.Count()==0 && GetRecivedShipmnetRestrictedRecord.Count()==0 && GetRecivedShipmnetNotRestrictedAndDisposedoff.Count()==0)
                        {
                            if (itemToRemove.status != null)
                            {
                                Context.Configuration.ValidateOnSaveEnabled = false;
                                itemToRemove.status = itemToRemove.status = !itemToRemove.status;
                                Context.Entry(itemToRemove).State = System.Data.Entity.EntityState.Modified;
                                Context.SaveChanges();
                                if (itemToRemove.status == false)
                                {
                                    return SuccessMsg("Customer InActive Successfully.");
                                }
                                else
                                    return SuccessMsg("Customer Active Successfully.");
                            }
                        }
                        if (GetOrderRecord.Count()!=0)
                        {
                            foreach (var item in GetOrderRecord)
                            {
                                var GetRecivedShipmentAccordingOrder = Context.tblReceivedShipments.AsQueryable().Where(q => q.status != (int)enumReceivShipmentStatus.Restricted).Where(x => x.Fk_order_id == item.id).ToList();
                                if (GetRecivedShipmentAccordingOrder.Count() != 0)
                                {
                                    return ErrorMsg("orders of this user under process.");
                                }
                                else
                                {
                                    SaveCustomerProcessing(GetOrderRecord, GetRecivedShipmnetRestrictedRecord);
                                    Context.Configuration.ValidateOnSaveEnabled = false;
                                    itemToRemove.status = !itemToRemove.status;
                                    Context.Entry(itemToRemove).State = System.Data.Entity.EntityState.Modified;
                                    Context.SaveChanges();
                                    return SuccessMsg("Customer InActive Successfully.");
                                }
                                   
                            }
                        }
                        else if (GetRecivedShipmnetNotRestrictedAndDisposedoff.Count() != 0)
                        {
                            return ErrorMsg("you should  not mark this user as inactive because one or more shipments.");
                        }
                        else
                        {
                            if (itemToRemove.status != null)
                            {
                                Context.Configuration.ValidateOnSaveEnabled = false;
                                itemToRemove.status = itemToRemove.status = !itemToRemove.status;
                                Context.Entry(itemToRemove).State = System.Data.Entity.EntityState.Modified;
                                Context.SaveChanges();
                                if (itemToRemove.status == false)
                                {
                                    return SuccessMsg("Customer InActive Successfully.");
                                }
                                else
                                    return SuccessMsg("Customer Active Successfully.");
                            }
                        }
                    }
                }
                return SuccessMsg("Customer InActive Successfully.");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public string ChangePaymentStatustblCustomer(int id)
        {
            try
            {
                tblCustomer itemToremove = Context.tblCustomers.AsQueryable().Where(x => x.Id == id).FirstOrDefault();
                if (itemToremove != null)
                {
                    Context.Configuration.ValidateOnSaveEnabled = false;
                    itemToremove.payment_status = !itemToremove.payment_status;
                    Context.Entry(itemToremove).State = System.Data.Entity.EntityState.Modified;
                }
                Context.SaveChanges();
                return SuccessMsg("Payment status changed successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }
        public void SaveCustomerProcessing(List<tblOrder>GetOrderRecord,List<tblReceivedShipment> GetRecivedShipmnetRecord)
        {
            foreach (var item in GetRecivedShipmnetRecord)
            {
                item.status = (int)enumReceivShipmentStatus.isInctive;
                Context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
            }
            foreach (var item in GetOrderRecord)
            {
                item.status = (int)enumorder.isInctive;
                Context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
            }
        }
    }
}