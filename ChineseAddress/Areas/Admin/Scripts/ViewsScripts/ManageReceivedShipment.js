﻿$('a[href^="#CorrectPrice"]').click(function () {
    var shipmentID = $(this).attr('id');
    $.ajax({
        type: "Post",
        contentType: 'application/json; charset=utf-8',
        url: "/Admin/ReceivedShipment/AwatingPriceCorrection",
        data: JSON.stringify({ "shipmentID": shipmentID }),
        success: (function (data) {
            var target = $("#CorrectPriceDetails");
            target.empty();
            target.append("<div class='row' style='padding-bottom:10px'><div class='col-sm-3 col-xs-12'><strong>Description</strong></div><div class='col-sm-3 col-xs-12'><strong>Commodity Code</strong></div><div class='col-sm-3 col-xs-12'><strong>$ Price</strong></div><div class='col-sm-3 col-xs-12'><strong>Quantity</strong></div></div>")
            $.each(data, function (i, item) {
                target.append("<div class='row'><input type='hidden' value=" + item.Fk_shipment_id + " name='Fk_ShipmentID' id='ReciveshipmentID'><input type='hidden' value=" + item.id + " name='ShipmentID'><div class='col-lg-3'><label>" + item.description + "</label></div><div class='col-lg-3'><label>" + item.comodity_code + "</label></div><div class='col-lg-3' style='padding-bottom: 6px;'><input type='number' class='form-control' value=" + item.purchase_price + " name='CorrectPrice'></div><div class='col-lg-3'><label>" + item.quantity + "</label></div></div>")
            })
        })
    })
})
$("#SubmitCorrectPrice").click(function () {
    $.ajax({
        type: 'POST',
        url: '/Admin/ReceivedShipment/SaveCorrectPrice',
        data: $('#CorrectPriceform').serialize(),
        success: function (data) {
            window.location.reload();
        }
    });
})
$("#CancelCorrectPrice").click(function () {
    $.ajax({
        type: "Post",
        contentType: 'application/json;charset=utf-8',
        url: "/Admin/ReceivedShipment/CancelCorrectPrice",
        data: JSON.stringify({ "shipmentID": $("#ReciveshipmentID").val() }),
        success: (function (data) {
            window.location.reload();
        })
    })
});
$('a[data-target="#CorrectPrice"]').click(function () {
    var thisa = $(this);
    var path = "/Uploads/CorrectPrice/" + thisa.attr("id") + "/" + thisa.attr("data-file-path");
    $("#ViewCorrectPrice").attr('href', path);
})
		//$("#ViewCorrectPrice").click(function () {
		//	$.ajax({
		//		type: "Post",
		//		contentType: 'application/json;charset=utf-8',
		//		url: "/Admin/ReceivedShipment/ViewCorrectPrice",
		//		data: JSON.stringify({ "shipmentID": $("#ReciveshipmentID").val() }),
		//		success: (function (data) {
		//			//var target = $("#CorrectPricelabel");
		//			if (data.correct_price_file_path != null) {
		//				//target.empty();
		//				//target.append("<img src='/Uploads/CorrectPrice/" + data.id + "/" + data.correct_price_file_path + "' style=width:100%; />");
		//				//CorrectPrice IN  WINDOW
		//				var a = $('<a></a>').attr("href", "/Uploads/CorrectPrice/" + data.id + "/" + data.correct_price_file_path);
		//				a.trigger('click');

		//				//window.open("/Uploads/CorrectPrice/" + data.id + "/" + data.correct_price_file_path);
		//			}
		//		})
		//	})
		//})



//second

//custom filtering

//Status Filter
$.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {
        var selOption = $("#ddlstatus").val();
        if (typeof selOption === "undefined") {
            return true;
        }
        else if (data[9].trim() === selOption) {
            return true;
        }
        else if (selOption === "0") {
            return true;
        }
        else
            return false;

    }
);

//Free Storage Days
$.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {
        var txtfreeStorageStatus = $("#ddlfreeStorageStatus").val();
        if (typeof txtfreeStorageStatus == "undefined") {
            return true;
        }
        else if (txtfreeStorageStatus === "0") {
            return true;
        }
        else if ((data[8].trim().indexOf('Days left') > -1 && parseInt(data[7].trim()) > 0 && parseInt(data[7].trim()) <= 7 && txtfreeStorageStatus.trim() === 'Expiring Soon')) {
            return true;
        }
        else if ((data[8].trim().indexOf('Days over') > -1 && txtfreeStorageStatus.trim() === 'Expired')) {
            return true;
        }
        else
            return false;

    }
);

//Shipment Picture Filter
$.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {
        var selOption = $("#ddlFilterPictures").val();
        if (typeof selOption === "undefined") {
            return true;
        }
        else if (data[7].trim() === selOption) {
            return true;
        }
        else if (selOption === "0") {
            return true;
        }
        else
            return false;

    }
);

//Correct Shipment Price Filter
$.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {
        var selOption = $("#ddlCorrectPrice").val();
        if (typeof selOption === "undefined") {
            return true;
        }
        else if (data[6].trim() === selOption) {
            return true;
        }
        else if (selOption === "0") {
            return true;
        }
        else
            return false;

    }
);

var table;
$(window).load(function () {
    drawTable();
});
function drawTable() {
    table = $("#tblReceivedShipment").DataTable({
        "dom": "<'row'<'col-sm-2'l><'col-sm-3'<'#divstatus.dataTables_filter'>><'col-sm-3'<'#freeStorageStatus.dataTables_filter'>><'col-sm-2'f>><'row'<'col-sm-6'<'#divFilterPictures.dataTables_filter pull-left'>><'col-sm-6'<'#divCorrectPrice.dataTables_filter pull-right'>>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "bDestroy": true,
        responsive: true,
        "order": [],
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: -1 },
            { responsivePriority: 3, targets: -1 }
        ]

    });
    $('#divfree').html('Free Storage :');
    $('#divfree').css('padding', '9px 0px');
    $('#freeStorageStatus').css('padding', '1px 50px 0px 0px');
    //bind filter Status dropdown in  divstatus start
    $('<label />').attr('id', 'lblStatus').html('Status :').appendTo('#divstatus');
    $select = $('<select id="ddlstatus" class="form-control input-md" />').appendTo('#lblStatus')
    $('<option />').val('0').text('All Status').appendTo($select);
    let statusReceivedShipment = ["Ready", "Processing", "Payment Awaiting", "On Hold", "Restricted", "Abandon", "Return Pending", "Returned", "Order Created"];
    statusReceivedShipment.forEach(function (item) {
        $select.append('<option value="' + item + '">' + item + '</option>')
    });
    //bind filter Status dropdown in  divstatus end
    //bind filter freeStorageStatus dropdown  start
    $('<label />').attr('id', 'lblfreeStorageStatus').html('Free Storage :').appendTo('#freeStorageStatus');
    $select = $('<select id="ddlfreeStorageStatus" class="form-control input-md" />').appendTo('#lblfreeStorageStatus')
    $('<option />').val('0').text('All Free Storage').appendTo($select);
    let statusFreeStorageDays = ["Expiring Soon", "Expired"];
    statusFreeStorageDays.forEach(function (item) {
        $select.append('<option value="' + item + '">' + item + '</option>')
    });
    //bind filter freeStorageStatus dropdown  end

    //bind filter shipment picture dropdown  start
    $('<label />').attr('id', 'lblShipmentPictures').html('Shipment Pictures :').appendTo('#divFilterPictures');
    $select = $('<select id="ddlFilterPictures" class="form-control input-md" />').appendTo('#divFilterPictures')
    $('<option />').val('0').text('All').appendTo($select);
    let statusShipmentPictures = ["Requested Images", "Add Images", "View"];
    statusShipmentPictures.forEach(function (item) {
        $select.append('<option value="' + item + '">' + item + '</option>')
    });
    //bind filter shipment picture dropdown  end

    //bind filter Correct Price dropdown  start
    $('<label />').attr('id', 'lblCorrectPrice').html('Correct Shipment Price :').appendTo('#divCorrectPrice');
    $select = $('<select id="ddlCorrectPrice" class="form-control input-md" />').appendTo('#lblCorrectPrice')
    $('<option />').val('0').text('All').appendTo($select);
    let optionsCorrectPrice = ["Awating Price Correction", "Price Updated"];
    optionsCorrectPrice.forEach(function (item) {
        $select.append('<option value="' + item + '">' + item + '</option>')
    });
    //bind filter Correct Price dropdown  end

    // Event listener to the two range filtering inputs to redraw on input
    $('#ddlfreeStorageStatus').change(function () {
        table.draw();
    });
    $('#ddlstatus').change(function () {
        table.draw();
    });

    $('#ddlFilterPictures').change(function () {
        table.draw();
    });
    $('#ddlCorrectPrice').change(function () {
        table.draw();
    });

}
function refresh() {
    $("#parent-container").load(location.href + " #child-container", null, function () {
        drawTable();
    });
}
function openpopupcomments(staff_comments) {
    $("#myModalcomments").modal('show')
    $("#divcomments").text(staff_comments);
}
function openpopup(id) {
    $.ajax({
        type: "post",
        url: "/ReceivedShipment/ReceivedShipmentDetailById",
        data: { 'id': id },
        success: function (data) {
            var target = $("#tblReceivedShipmentPopup");
            target.empty();
            target.append("<thead><tr><th>Description</th><th>Comodity Code</th><th>Harmonized Code</th><th>Price</th><th>Quantity</th></thead>");
            target.append("<tbody>")
            $.each(data, function (i, item) {
                target.append("<tr><td>" + item.description + "</td><td>" + item.comodity_code + "</td><td>" + item.harmonizedCode + "</td><td>" + item.purchase_price + "</td><td>" + item.quantity + "</td></tr></tbody>")
            });
        },
        error: function (data) {
            alert(data.x);
        }
    });
}
function openpopupReturn(id, refrence_number) {
    //check  user wallet
    var returnCharge = 0.0;
    var cust_id = 0;
    $.ajax({
        type: "post",
        url: "/ReceivedShipment/checkReturnAmountInUserWallet",
        data: { 'id': id, "Customer_Reference": refrence_number },
        success: function (response) {
            console.log(response.issuccess);
            console.log(response.returnCharge);
            console.log(response.cust_id);
            cust_id = response.cust_id;
            returnCharge = response.returnCharge;

            $.ajax({
                type: "post",
                url: "/ReceivedShipment/ReturnShipmentByFkId",
                data: { 'id': id },
                success: function (data) {
                    var target = $("#tblReceivedShipmentPopupReturn");
                    target.empty();
                    $.each(data, function (i, item) {
                        if (response.issuccess == true) {
                            if (item.prepaid_label_filename != null) {
                                target.empty();
                                target.append("<div class='row'><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>" + "<a href='/Uploads/ReturnPackages/" + item.Fk_shipment_id + "/" + item.prepaid_label_filename + "'class='btn btn-primary' target='_blank'>Click here to view return shipment label</a></div>");
                            }
                            if (item.first_name != null) {
                                target.append("<div class='row' style='margin-top:10px;'><div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'><strong>First Name</strong><input type='text' class='form-control' value='" + item.first_name + "' placeholder='First Name' readonly /></div>" +
                                    "<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'><strong>Last Name</strong><input type='text' class='form-control' value='" + item.last_name + "' placeholder='Last Name' readonly /></div>" +
                                    "<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'style='padding-top:6px;'><strong>Address 1</strong><input type='text' class='form-control' value='" + item.address1 + "' placeholder='Address1'  readonly /></div>" +
                                    "<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'style='padding-top:6px;'><strong>Address 2</strong><input type='text' class='form-control' value='" + item.address2 + "' placeholder='Address2' readonly /></div>" +
                                    "<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'style='padding-top:6px;'><strong>City</strong><input type='text' class='form-control' value='" + item.city + "'placeholder='City' readonly /></div>" +
                                    "<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'style='padding-top:6px;'><strong>State</strong><input type='text' class='form-control' value='" + item.state + "'placeholder='State' readonly /></div>" +
                                    "<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'style='padding-top:6px;'><strong>Zip</strong><input type='text' class='form-control' value='" + item.postal_code + "' placeholder='Zip' readonly /></div>" +
                                    "<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'style='padding-top:6px;'><strong>Mobile</strong><input type='text' class='form-control' value='" + item.mobile + "' placeholder='Mobile' readonly /></div>" +
                                    "<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'style='padding-top:6px;'><strong>Rma</strong><input type='text' class='form-control' value='" + item.rma + "' placeholder='Rma' readonly /></div>")
                            }
                        }
                        if (response.issuccess == true) {
                            target.append("<div class='row'>")
                            target.append("<div class='col-md-12' style='margin:19px 0px 6px 93px;'><a href='javascript:void(0)' onclick='ChangeStatus(" + item.id + "," + cust_id + "," + returnCharge + ")' class='btn btn-success'><i class='fa fa-retweet'> Accept Return Request</i></a>&nbsp &nbsp;<a href='javascript:void(0)' onclick='RejectShipment(" + item.id + "," + 'true' + ")' class='btn btn-danger'><i class='fa fa-close'></i> Reject Return Request</i></a></div>")
                            target.append("</div>");
                        }
                        else {
                            target.append("<div class='row'>")
                            target.append("<div class='col-md-12 text-yellow'><h4>User does not have a enough balance in his wallet.</<h4></div>");
                            target.append("<div class='col-md-12'><a href='javascript:void(0)' onclick='RejectShipment(" + item.id + "," + 'false' + ")' class='btn btn-danger'><i class='fa fa-close'></i> Reject Return Request</i></a></div>");
                            target.append("</div>");
                        }

                    });
                },
                error: function (data) {
                    alert(data.x);
                }

            });


        }
    });
}


function DeleteData(id) {

    confirm("You you sure you want delete Shipment?", function (r) {
        if (r === true) {
            var parent = $(this).parent().parent();
            $.ajax({
                type: "post",
                url: "/ReceivedShipment/DeleteReceivedShipmentById",
                data: { 'id': id },
                success: function (id) {
                    if (id !== "0") {
                        table.row($("a[id='Delete " + id + "']").parents('tr[role="row"]')).remove().draw();
                        showMessage("Shipment deleted successfully", true);
                    }
                },
                error: function (data) {
                    alert(data.x);
                }
            });
        }
    });

}

function ChangeStatus(id, cust_id, returnCharge) {
    var parent = $(this).parent().parent();
    $.ajax({
        type: "post",
        url: "/ReceivedShipment/ChangeTblShipmentReturnStatus",
        data: { 'id': id, "cust_id": cust_id, "returnCharge": returnCharge },
        success: function (data) {
            if (data != null) {
                showMessage('Accept Reject Return Request has been submited successfully', true);
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
            location.reload();
        },
        error: function (data) {
            alert(data.x);
        }
    });
}
function RejectShipment(id, isUserHaveBalance) {
    console.log(isUserHaveBalance);
    $.ajax({
        type: "post",
        url: "/ReceivedShipment/RejectRequestShipment",
        data: { 'id': id, 'isUserHaveBalance': isUserHaveBalance },
        success: function (data) {
            if (data != null) {
                $("#myModals").modal('hide');
                showMessage(data, true);
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        },
        error: function (data) {
            alert(data.x);
        }
    })
}

//third

$('td span[id^="freestorage"]').each(function (i, obj) {
    var totalCost = ($(obj).text());
    var value = totalCost.replace('Days', '');
    if (value <= 0) {
        $(this).text($(this).text().replace('-', ''));
        $(this).text($(this).text() + ' Days over');
        $(this).removeClass('.label label-success')
        $(this).addClass('.label label-danger')
    }
    else if (value > 0 && value < 8) {
        $(this).text($(this).text() + ' Days left');
        $(this).removeClass('.label label-success')
        $(this).addClass('.label label-warning')
    }
    else {
        $(this).text($(this).text() + ' Days left');
    }
});