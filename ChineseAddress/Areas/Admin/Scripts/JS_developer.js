﻿$(document).ready(function () {
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
    //Drop Down Start
    $("select[SearchSelect='true']").select2({ width: '100%' });

    //focus next
    $('select').on('select2:select', function (evt) {
        $(this).parents('div').next('div').eq(0).find('input').eq(0).focus().select();
    });
    //open on focus
    $(document).on('focus', '.select2', function () {
        $(this).siblings('select').select2('open');
    });

    //Drop Down End
    //Date Picker Start
    var FromEndDate = new Date();
    $('input[calendar="date"]').datepicker({
        format: 'yyyy/mm/dd',
        endDate: FromEndDate,
        autoclose: true
    });
    $('input[calendar="enabledDates"]').datepicker({
        format: 'yyyy/mm/dd',
        startDate: FromEndDate,
        autoclose: true
    });
    //Date Picker End
})
function showMessage(msg, bool, timeout) {
    timeout = (typeof timeout !== 'undefined') ? timeout : 7000;

    if (bool === true) {
        $('#div_msg').html('<div>' + msg + '</div>');
        $.pgwModal({
            target: '#div_msg',
            titleBar: false
        });
        $(".pm-content").css({ "background": "#DFF0D8" })
    }
    else {
        $('#div_msg').html('<div style="color: #dc0400;">' + msg + '</div>');
        $.pgwModal({
            target: '#div_msg',
            titleBar: false
        });
        $(".pm-content").css({ "background": "#ffebeb" })
    }
    setTimeout(function () {
        $.pgwModal('close');
    }, timeout);
}
//function TrackingNumber(TrackingNumber) {
//    $.ajax({
//        url: '/Tracking/Index',
//        type: 'POST',
//        data: JSON.stringify({ "TrackingNumber": TrackingNumber }),
//        dataType: 'json',
//        contentType: 'application/json; charset=utf-8',
//        success: function (data) {
//            if (data !== "") {
//                window.open(data);
//            }
//        }
//    })
//}
function TrackingOrderReturnURl(TrackingNumber) {
    $.ajax({
        url: '/CommonChild/TrackingOrderReturnURl',
        type: 'POST',
        data: JSON.stringify({ "TrackingNumber": TrackingNumber }),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data != "") {
                window.location.href = data;
                //window.location.href = "/TrackOrder?Track=" + TrackingNumber
            }
            else {
                showMessage('Invalid or no information found for this tracking number.', false);
            }
        }
    })
}
function Cancel_Click() {
    window.location = window.location.href;
}

//override confirm with bootbox
window.confirm = function (str, callback) {
    bootbox.confirm({
        title: '<b>Confirm!</b>',
        message: "<p style='font-size: 18px;'>" + str + "</p>",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> No',
                className: 'btn-danger'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Yes',
                className: 'btn-success font-size-inherit btn-confirm-success'
            }
        },
        callback: function (result) {
            callback(result);
        }
    });
}

/*Loader Ajax Event Start*/
$(document).ajaxSend(function () {
    $('#preloader').fadeIn('slow').children().fadeIn('slow');
    //$.LoadingOverlay("show", {
    //    color: "rgba(255, 255, 255, 0.7)",
    //    image: "/content/img/preloader_svg.svg"
    //});
});

$(document).ajaxSuccess(function () {
    //$.LoadingOverlay("hide");
    $('#preloader').fadeOut('slow').children().fadeOut('slow');
});

$(document).ajaxComplete(function () {
    //$.LoadingOverlay("hide");
    $('#preloader').fadeOut('slow').children().fadeOut('slow');

    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
});
/*Loader Ajax Event End*/
/*Loader Show Hide End*/

/*Cancel Button click start*/
/*Cancel Button click end*/

/*Prevent validation on Key Up start*/
$(function () {
    $.validator.setDefaults({
        onkeyup: false
    });
});

/*Prevent validation on Key Up end*/
/*Loader on page Load and onbeforeunload*/;
$(document).ready(function () {
    $('#preloader').fadeOut('slow').children().fadeOut('slow');
})
$(window).load(function () {
    try {
        $('#preloader').fadeOut('slow').children().fadeOut('slow');
    } catch (e) {
        console.log(e);
    }
});
window.onload = function () {
    try {
        $('#preloader').fadeOut('slow').children().fadeOut('slow');
    } catch (e) {
        console.log(e);
    }
}
window.onbeforeunload = function () { $('#preloader').fadeIn('slow').children().fadeIn('slow'); }
/*Loader on page Load and Post Backing End*/