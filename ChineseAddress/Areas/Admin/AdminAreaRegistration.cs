using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "UploadWebCamImage",
                "Admin/ReceivedShipment/UploadWebCamImage/{id}",
                new { controller = "ReceivedShipment", action = "UploadWebCamImage", id = UrlParameter.Optional }
            );
        }
    }
}