using _ChineseAddress.com.Areas.Admin.Services;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin")]
    public class AdminUsersController : Controller
    {
        // GET: Admin/AdminUsers
        private AdminUsersService Context = new AdminUsersService();

        public ActionResult Index(int? Id)
        {
            if (Id != null)
            {
                return View(Context.GettbltblAdminUserById(Id ?? 0).SingleOrDefault());
            }
            return View();
        }

        [ValidateAntiForgeryToken]
        public ActionResult InsertTblAdminUsers(tblAdminUser tbladminuser)
        {
            try
            {
                if (tbladminuser != null)
                {
                    ViewBag.Message = Context.InsertUpdateAdminUsers(tbladminuser);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            tblAdminUser roll = new tblAdminUser();
            ModelState.Clear();
            return View("Index");
        }

        public ActionResult UpdateTblAdminUsers(tblAdminUser tbladminuser)
        {
            try
            {
                if (tbladminuser != null)
                {
                    Context.InsertUpdateAdminUsers(tbladminuser);
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
            tblAdminUser roll = new tblAdminUser();
            ModelState.Clear();
            return View("Index", roll);
        }

        public ActionResult AdminUsers()
        {
            tblAdminUser roll = new tblAdminUser();

            List<tblAdminUser> lst = Context.GettbltblAdminUser().OrderBy(x => x.created_on).ToList();

            return View("GetAdminUsersDetail", lst);
        }

        public JsonResult DeleteAdminUsersDetailById(int Id)
        {
            if (Id != 0)
            {
                Context.DeletetblAdminUser(Id);
            }
            return Json(Id.ToString(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeStatusTblUserById(int Id)
        {
            if (Id != 0)
            {
                ViewBag.Message = Context.ChangeStatusTblUserById(Id);
            }
            return Json(Id.ToString(), JsonRequestBehavior.AllowGet);
        }
    }
}