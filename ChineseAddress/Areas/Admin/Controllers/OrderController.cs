using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Areas.Admin.Services;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.Repository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin ,Staff")]
    [OutputCache(Duration = 0)]
    public class OrderController : Controller
    {
        // GET: Admin/Order
        private OrderService Context = new OrderService();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsertTblOrde(tblOrder Order)
        {
            try
            {
                if (Order != null)
                {
                    ViewBag.Message = Context.InsertUpdateOrder(Order);
                    var err = ViewBag.Message;
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            tblOrder roll = new tblOrder();
            ModelState.Clear();
            return View("Index", roll);
        }

        public ActionResult UpdateTblPackagingOptions(tblOrder Order)
        {
            try
            {
                if (Order != null)
                {
                    Context.InsertUpdateOrder(Order);
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
            tblOrder roll = new tblOrder();
            ModelState.Clear();
            return View("Index", roll);
        }

        [HttpGet]
        public ActionResult Index()
        {
            List<ViewModelOrder> obj = Context.GetOrders();
            return View(obj);
        }

        [HttpGet]
        public JsonResult DeleteOrderById(int Id)
        {
            if (Id != 0)
            {
                Context.DeletetblTblOrder(Id);
            }
            return Json(Id.ToString(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChangeStatus(int id)
        {
            Context.ChangeStatusToProcessingById(id);
            return RedirectToAction("Manageorder", "Order", new { id });
        }

        [HttpGet]

        public ActionResult Manageorder(int id)
        {
            TempData["orderID"] = id;
            TempData.Peek("orderID");
            ViewBag.CreateOrderDetails = Context.CreateOrderRecivedShipment(id);
            //ViewBag.Message = Context.ChangeStatusToProcessingById(id);
            ViewModelOrderInformation orderInfo = Context.GetOrderInformation(id);
            ViewBag.OrderCharges = Context.GetTblOrdeChangedByFkId(id);
            ViewBag.OrderBoxes = Context.GetTblOrdeBoxesByFkId(id);
            ViewBag.OrderShipmentDetails = Context.GetShipmentDetails(id);
            ViewBag.GetOrderBoxesList = Context.GetOrderboxesData(id);
            ViewBag.CarrierLogo = Context.GetCarrierLogoByOrderId(id);
            List<tblOrderCharge> items = new List<tblOrderCharge>(ViewBag.OrderCharges);
            decimal Ordercharges = Context.GetOrderCharges(id);
            if (orderInfo.miscellaneous_charge_name != null)
            {
                orderInfo.totalcharges = (decimal)orderInfo.miscellaneous_charge_Price + Ordercharges;
            }
            else
            {
                orderInfo.totalcharges = Ordercharges;
            }
            return View(orderInfo);
        }

        [HttpPost]
        public ActionResult Orderboxes(tblOrderBox objtblOrderBox)
        {
            TempData["Ordercharges"] = Context.SaveOrderBoxes(objtblOrderBox);
            int Id = Convert.ToInt32(TempData.Peek("orderID"));
            return RedirectToAction("Manageorder", "Order", new { Id });
        }

        [HttpPost]
        public JsonResult MiscellaneousCharges(string ChargePrice, string ChargeName)
        {
            int id = Convert.ToInt32(TempData.Peek("orderID"));
            ViewBag.Message = Context.SaveOrderCharges(ChargePrice, ChargeName, id);
            return Json("Order charges has been saved successfully", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RestrictedOrder(string[] Recivshipid, string RestrictedOrderid, string RestrictMsg)
        {
            int OrderId = Convert.ToInt32(RestrictedOrderid);

            string message = "";
            if (Recivshipid == null)
            {
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            if (Recivshipid.Length != 0)
            {
                for (int i = 0; i < Recivshipid.Length; i++)
                {
                    if (Recivshipid[i] != "")
                    {
                        int id = Convert.ToInt32(Recivshipid[i].Replace("id", ""));
                        message = Context.RestrictedShipments(id, OrderId);
                    }
                }
                Context.RestrictedOrder(OrderId, RestrictMsg);
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CalculatePriceApi(string OrderID)
        {
            string[] message = new string[3];
            message = Context.CalculateOrderCharges(Convert.ToInt32(OrderID));
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConfirmOrder()
        {
            Context4InshipEntities objContext = new Context4InshipEntities();
            string message = "";
            int Orderid = Convert.ToInt32(TempData.Peek("orderID"));

            message = Context.ConfirmOrder(Orderid);
            var GetCustomerEmail = (from tbcust in objContext.tblCustomers
                                    join tbord in objContext.tblOrders on tbcust.Id equals tbord.Fk_customer_id
                                    join tbAddressbook in objContext.tblAddressBooks on tbcust.Id equals tbAddressbook.Fk_customer_Id
                                    select new { tbcust.email, tbAddressbook.first_name, tbAddressbook.last_name, tbAddressbook.is_default, tbord.id, tbord.Fk_customer_id, tbord.reference_no }).Where(x => x.id == Orderid && x.is_default == true).FirstOrDefault();
            TempData["CouponMsg"] = message;
            //string Cust_Email = Context.GetCustomerEmailById(Orderid);
            if (GetCustomerEmail != null)
            {
                clsEmailTemplateService clscommn = new clsEmailTemplateService();
                clsSendEmailService clssendemailservice = new clsSendEmailService();
                string subject = "chineseaddress - Order Confirm/Awaiting Payment !";
                Hashtable htTemplate = new Hashtable();
                htTemplate.Add("#FirstName#", GetCustomerEmail.first_name);
                htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
                htTemplate.Add("#refernceno#", GetCustomerEmail.reference_no);
                string mailbody = (new clsEmailTemplateService()).GetEmailTemplate("confirmorder", htTemplate);

                try
                {
                    (new clsSendEmailService()).SendEmail(new[] { GetCustomerEmail.email }, null, null, mailbody, subject);
                    ViewBag.Message = "showMessage('Confirmation email sent successfully.',true)";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message.ToString();
                }
            }
            return Redirect("Index");
        }

        public JsonResult SaveShippingCharges(string OrderID, string SellPrice, string BuyPrice)
        {
            string message = "";

            message = Context.SaveShippingCharge(Convert.ToInt32(OrderID), Convert.ToDecimal(SellPrice), Convert.ToDecimal(BuyPrice));
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DispatcheOrderById()
        {
            try
            {
                System.Collections.Hashtable hashTable = new System.Collections.Hashtable();

                Context4InshipEntities contxt = new Context4InshipEntities();
                clsEmailTemplateService objclsEmailTemplateService = new clsEmailTemplateService();
                HttpFileCollectionBase files = Request.Files;
                if (Request.Form["Orderid"] != null)
                {
                    string Orderid = Request.Form["Orderid"];
                    string TrackingNumber = Request.Form["trackingnumber"];
                    int OrdID = Convert.ToInt32(Orderid);

                    #region Change order status

                    var Cust_ID = Context.ChangeStatusToDispatchByID(OrdID);

                    #endregion Change order status

                    #region Save Tracking Number

                    SaveTrackingNo(TrackingNumber, OrdID);

                    #endregion Save Tracking Number

                    #region Create Invoicelabel and CutomerProforma

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string extension = Path.GetExtension(file.FileName);
                        string fileName = TrackingNumber + "_label" + extension;
                        if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/Orders" + "/") + Orderid))
                        {
                            Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/Orders" + "/") + Orderid);
                        }
                        file.SaveAs(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/Orders/" + "/" + Orderid + "/") + fileName.ToString());
                    }
                    clsCommonServiceRoot _objclsCommonServiceRoot = new clsCommonServiceRoot();
                    _objclsCommonServiceRoot.CreateOrderCustomPerfomaInvoice(Convert.ToInt32(Orderid));
                    ViewBag.Message = "Order dispatched successfully";

                    #endregion Create Invoicelabel and CutomerProforma

                    #region Send Email to Customer order disparched

                    // _objclsCommonServiceRoot.SendOrderDispatchedEmail(Convert.ToInt32(Orderid), TrackingNumber);

                    #endregion Send Email to Customer order disparched
                }
            }
            catch (Exception ex)
            {
                _ChineseAddress.com.Controllers.IPNSilentController objIPN = new com.Controllers.IPNSilentController();
                //objIPN.CreatePaymentResponses( ""+ex.Message, "PayTabsIPN.txt");
                ViewBag.Message = ex.Message;
            }
            return Json(ViewBag.Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UnHoldOrderById(int Id)
        {
            Context4InshipEntities objContext = new Context4InshipEntities();
            var Cust_ID = Context.ChangeStatusToProcessingByID(Id);
            //var Cust_Email = Context.GetCustomerEmailById(Cust_ID);
            List<ViewModelOrder> obj = Context.GetOrders();

            string Cust_Email = Context.GetCustomerEmailById(Id);
            clsEmailTemplateService clscommn = new clsEmailTemplateService();
            clsSendEmailService clssendemailservice = new clsSendEmailService();
            //var GetCustomerEmail = (from tbcust in objContext.tblCustomers join tbord in objContext.tblOrders on tbcust.Id equals tbord.Fk_customer_id select new { tbcust.email, tbord.id, tbord.reference_no }).Where(x => x.id == Id).FirstOrDefault();
            var GetCustomerEmail = (from tbcust in objContext.tblCustomers
                                    join tbord in objContext.tblOrders on tbcust.Id equals tbord.Fk_customer_id
                                    join tbAddressbook in objContext.tblAddressBooks on tbcust.Id equals tbAddressbook.Fk_customer_Id
                                    select new { tbcust.email, tbAddressbook.first_name, tbAddressbook.last_name, tbAddressbook.is_default, tbord.id, tbord.Fk_customer_id, tbord.reference_no }).Where(x => x.id == Id && x.is_default == true).FirstOrDefault();
            if (GetCustomerEmail != null)
            {
                string subject = "chineseaddress - Order Unhold";
                Hashtable htTemplate = new Hashtable();
                htTemplate.Add("#FirstName#", GetCustomerEmail.first_name);
                htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
                htTemplate.Add("#referenceno#", GetCustomerEmail.reference_no);
                string mailbody = (new clsEmailTemplateService()).GetEmailTemplate("orderunhold", htTemplate);
                try
                {
                    (new clsSendEmailService()).SendEmail(new[] { GetCustomerEmail.email }, null, null, mailbody, subject);
                    ViewBag.Message = "showMessage('Confirmation email sent successfully.',true)";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message.ToString();
                }
            }
            return RedirectToAction("Index", obj);
        }

        public ActionResult PutOnHold(int Id)
        {
            Context4InshipEntities objContext = new Context4InshipEntities();
            var Cust_ID = Context.ChangeStatusToOnHoldByID(Id);
            List<ViewModelOrder> obj = Context.GetOrders();
            //string Cust_Email = Context.GetCustomerEmailById(Id);
            clsEmailTemplateService clscommn = new clsEmailTemplateService();
            clsSendEmailService clssendemailservice = new clsSendEmailService();
            var GetCustomerEmail = (from tbcust in objContext.tblCustomers
                                    join tbord in objContext.tblOrders on tbcust.Id equals tbord.Fk_customer_id
                                    join tbAddressbook in objContext.tblAddressBooks on tbcust.Id equals tbAddressbook.Fk_customer_Id
                                    select new { tbcust.email, tbAddressbook.first_name, tbAddressbook.is_default, tbAddressbook.last_name, tbord.id, tbord.Fk_customer_id, tbord.reference_no }).Where(x => x.id == Id && x.is_default == true).FirstOrDefault();
            if (GetCustomerEmail != null)
            {
                string subject = "chineseaddress4Inship - Order Hold";
                string body = "Dear " + GetCustomerEmail.first_name + " " + GetCustomerEmail.last_name + ",<br/> your order " + GetCustomerEmail.reference_no + " no has been put On Hold";
                Hashtable htTemplate = new Hashtable();
                htTemplate.Add("#FirstName#", GetCustomerEmail.first_name);
                htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
                htTemplate.Add("#referenceno#", GetCustomerEmail.reference_no);
                htTemplate.Add("#content#", body);
                string mailbody = (new clsEmailTemplateService()).GetEmailTemplate("orderonhold", htTemplate);
                try
                {
                    (new clsSendEmailService()).SendEmail(new[] { GetCustomerEmail.email }, null, null, mailbody, subject);
                    ViewBag.Message = "showMessage('Confirmation email sent successfully.',true)";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message.ToString();
                }
            }
            return RedirectToAction("Index", obj);
        }

        public FileResult DownloadCustomproforma(int Id)
        {
            return new FilePathResult("~/Uploads/Orders/" + Id + "/" + "" + Id + "_Customproforma.pdf", "Application/pdf");
        }

        public FileResult DownloadInvoicelabel(int Id)
        {
            Context4InshipEntities contxt = new Context4InshipEntities();
            var TrackingNumber = contxt.Database.SqlQuery<string>("select tracking_no from tblorder where id =" + Id).FirstOrDefault<string>();
            return new FilePathResult("~/Uploads/Orders/" + Id + "/" + "" + TrackingNumber + "_label.pdf", "Application/pdf");
        }

        public FileResult DownloadInvoice(int Id)
        {
            return new FilePathResult("~/Uploads/Orders/" + Id + "/" + "" + Id + "_Invoice.pdf", "Application/pdf");
        }

        private void SaveTrackingNo(string TrackingNo, int OrderId)
        {
            tblOrder objtblOrder = new tblOrder();
            objtblOrder.id = OrderId;
            objtblOrder.tracking_no = TrackingNo;
            string Message = Context.SaveTrackingNumber(objtblOrder);
        }
    }
}