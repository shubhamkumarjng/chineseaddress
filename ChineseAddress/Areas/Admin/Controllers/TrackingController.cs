using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.Areas.Admin.Services;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin ,Staff")]
    public class TrackingController : Controller
    {
        // GET: Admin/Tracking
        Context4InshipEntities Context = new Context4InshipEntities();
        TrackingService _repo = new TrackingService();
       
        public ActionResult Index(string id)
        {
            string TrackingNumber = id;
            var isTrackingExists = Context.tblOrders.AsQueryable().Where(x => x.tracking_no == TrackingNumber).Select(x => x.tracking_no).Union(Context.tblOrderBoxes.AsQueryable().Where(x => x.tracking_no == TrackingNumber).Select(x => x.tracking_no)).FirstOrDefault();
            if (isTrackingExists != null)
            {

                _repo.TrackOrder(TrackingNumber);
                if (!_repo._isSuccess)
                {
                    ViewBag.Message = $"showMessage('No information found for this tracking number.',false)";
                }
            }
            else
                ViewBag.Message = $"showMessage('Invalid tracking number.',false)";
            return View(_repo._TrackingDetail);
        }
    }
}