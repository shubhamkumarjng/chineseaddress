using _ChineseAddress.com.Areas.Admin.Services;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin")]
    public class PackagingOptionsController : Controller
    {
        // GET: Admin/PackagingOptions
        private PackagingOptionsService Context = new PackagingOptionsService();

        public ActionResult Index(int? Id)
        {
            if (Id != 0)
            {
                return View(Context.GetblPackagingOptionsById(Id ?? 0).SingleOrDefault());
            }
            return View(new _ChineseAddress.com.Repository.tblPackagingOption());
        }

        public ActionResult InsertTblPackagingOptions()
        {
            return View("Index");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertTblPackagingOptions(tblPackagingOption tblpackagingoption, string Issignup, string Isshipment)
        {
            try
            {
                if (tblpackagingoption != null)
                {
                    ViewBag.Message = Context.InsertUpdatePackagingOptions(tblpackagingoption);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            tblPackagingOption roll = new tblPackagingOption();
            ModelState.Clear();
            return View("Index");
        }

        public ActionResult UpdateTblPackagingOptions(tblPackagingOption tblpackagingoption)
        {
            try
            {
                if (tblpackagingoption != null)
                {
                    ViewBag.Message = Context.InsertUpdatePackagingOptions(tblpackagingoption);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            tblPackagingOption roll = new tblPackagingOption();
            ModelState.Clear();
            return View("Index");
        }

        [HttpGet]
        public ActionResult PackagingOptions()
        {
            List<tblPackagingOption> lst = Context.GettblPackagingOptions().ToList();

            return View("GetPackagingOptionDetail", lst);
        }

        public JsonResult ChangeStatusPackagingOptionById(int Id)
        {
            if (Id != 0)
            {
                ViewBag.Message = Context.ChangeStatustblPackagingOptions(Id);
            }
            return Json(Id.ToString(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeSignupStatusPackagingOptionById(int Id)
        {
            if (Id != 0)
            {
                ViewBag.Message = Context.ChangeSignuptblPackagingOptions(Id);
            }
            return Json(Id.ToString(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeshipmentStatusPackagingOptionById(int Id)
        {
            if (Id != 0)
            {
                ViewBag.Message = Context.ChangeShipmentptblPackagingOptions(Id);
            }
            return Json(Id.ToString(), JsonRequestBehavior.AllowGet);
        }
    }
}