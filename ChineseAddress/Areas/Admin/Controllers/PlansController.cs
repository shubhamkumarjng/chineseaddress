using _ChineseAddress.com.Areas.Admin.Services;
using _ChineseAddress.com.Repository;
using System.Collections.Generic;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin")]
    public class PlansController : Controller
    {
        // GET: Admin/Home
        private PlanServices Context = new PlanServices();

        public ActionResult Index(int? Id)
        {
            if (Id != null)
            {
                var GetTblplanSinglerecord = Context.GetblPlansById(Id ?? 0);
                return View(GetTblplanSinglerecord);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(tblPlan objtblPlan, string IsRecurring, string Status)
        {
            if (objtblPlan != null)
            {
                if (IsRecurring == "on")
                {
                    objtblPlan.is_recurring = true;
                }
                else
                {
                    objtblPlan.is_recurring = false;
                }
                if (Status == "on")
                {
                    objtblPlan.status = true;
                }
                else
                {
                    objtblPlan.status = false;
                }
                ViewBag.Message = Context.InsertUpdatePlan(objtblPlan);
            }
            ModelState.Clear();
            return View();
        }

        [HttpGet]
        public ActionResult Plans()
        {
            List<tblPlan> lst = Context.GettblPlans();
            return View("GetPlanDetail", lst);
        }

        public JsonResult ChangeStatusTblPlanById(int id)
        {
            if (id != 0)
            {
                ViewBag.Message = Context.ChangeStatusTblPlanById(id);
            }
            return Json(id, ToString(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeIS_RecurringTblPlanById(int id)
        {
            if (id != 0)
            {
                ViewBag.Message = Context.ChangeIS_RecurringTblPlanById(id);
            }
            return Json(id, ToString(), JsonRequestBehavior.AllowGet);
        }
    }
}