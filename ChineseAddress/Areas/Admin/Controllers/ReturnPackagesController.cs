using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Areas.Admin.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin ,Staff")]
    public class ReturnPackagesController : Controller
    {
        // GET: Admin/ReturnPackages
        private ReturnPackagesService Context = new ReturnPackagesService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReturnPackages()
        {
            List<View_ModelReturnPackage> lst = Context.GettblShipmentReturnDetail().ToList();
            return View("ReturnPackages", lst);
        }
    }
}