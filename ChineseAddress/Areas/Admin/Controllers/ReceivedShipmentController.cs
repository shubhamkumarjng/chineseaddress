using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Areas.Admin.Services;
using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Repository;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin ,Staff")]
    public class ReceivedShipmentController : Controller
    {
        // GET: Admin/ReceivedShipment
        [Dependency]
        public IWalletService<tblTopUp, tblTopUpMethod> _repositoryUserWalletService { get; set; }
        private ReceivedShipmentService Context = new ReceivedShipmentService();
        private Context4InshipEntities _context = new Context4InshipEntities();

        public ActionResult Index(int? Id)
        {
            GetTblCustomer();
            ViewBag.ItemType = null;
            tblReceivedShipment objrecivedshipment = new tblReceivedShipment();
            objrecivedshipment.received_date = DateTime.Now;
            if (Id != null)
            {
                var data = Context.GetblReceivedShipmentById(Id ?? 0).ToList();
                var datas = Context.GetblReceivedShipmentDataById(Id ?? 0).ToList();


                if (datas != null && datas.Count != 0)
                {
                    objrecivedshipment.Fk_Customer_Id = datas[0].Fk_Customer_Id;
                    objrecivedshipment.sender = datas[0].sender;
                    objrecivedshipment.tracking = datas[0].tracking;
                    objrecivedshipment.carrier = datas[0].carrier;
                    objrecivedshipment.weight = datas[0].weight;
                    objrecivedshipment.height = datas[0].height;
                    objrecivedshipment.length = datas[0].length;
                    objrecivedshipment.width = datas[0].width;
                    objrecivedshipment.box_condition = datas[0].box_condition;
                    objrecivedshipment.staff_comments = datas[0].staff_comments;
                    objrecivedshipment.shelf_no = datas[0].shelf_no;
                    ViewBag.ItemType = datas[0].status;
                    objrecivedshipment.id = datas[0].id;
                    objrecivedshipment.created_on = datas[0].created_on;
                    objrecivedshipment.received_date = datas[0].received_date;
                }
                List<Repository.tblShipmentDetail> objshipcList = new List<Repository.tblShipmentDetail>();
                ArrayList lstarray = new ArrayList();
                for (int i = 0; i <= data.Count - 1; i++)
                {
                    tblShipmentDetail objShipmentDetail = new tblShipmentDetail();
                    objShipmentDetail.description = data[i].description;
                    objShipmentDetail.quantity = data[i].quantity;
                    objShipmentDetail.purchase_price = data[i].purchase_price;
                    objShipmentDetail.comodity_code = data[i].comodity_code;
                    objShipmentDetail.harmonizedCode = data[i].harmonizedCode;
                    objshipcList.Add(objShipmentDetail);
                    //objrecList.Add(new tblReceivedShipment() { Description = data[i].description, Quantity = Convert.ToInt32(data[i].quantity), PurchasePrice = Convert.ToDecimal(data[i].purchase_price), ComodityCode = data[i].comodity_code });
                }
                ViewBag.ShipmentList = objshipcList.ToList();
                return View(objrecivedshipment);
            }

            return View(objrecivedshipment);
        }

        [HttpGet]
        public void InsertTblReceivedShipment()
        {
            RedirectToAction("Index", "ReceivedShipment");
        }

        [HttpPost]
        public ActionResult InsertTblReceivedShipment(tblReceivedShipment receivedshipment, bool isAddPackageImages = false)
        {
            try
            {
                if (receivedshipment != null)
                {
                    receivedshipment.height = (receivedshipment.height == null || receivedshipment.height == 0 ? 1 : receivedshipment.height);
                    receivedshipment.length = (receivedshipment.length == null || receivedshipment.length == 0 ? 1 : receivedshipment.length);
                    receivedshipment.width = (receivedshipment.width == null || receivedshipment.width == 0 ? 1 : receivedshipment.width);

                    ViewBag.Message = Context.InsertUpdateReceivedShipment(receivedshipment);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            tblReceivedShipment roll = new tblReceivedShipment();
            ModelState.Clear();
            GetTblCustomer();
            if (isAddPackageImages)
            {
                return RedirectToAction("AddEditPckagesImages", new { Id = receivedshipment.id, isChargeable = "false" });
            }
            return View("Index");
        }

        public JsonResult GetTblAdminUser()
        {
            var lst = Context.GettblAdminUser();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        private void GetTblCustomer()
        {
            try
            {
                ViewBag.Fk_Customer_Id = Context.Customer();
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
        }

        public ActionResult ReceivedShipments()
        {
            List<ViewModelreceivedShipment> lst = Context.GetblReceivedShipment().ToList();
            return View("GetReceivedShipmentDetail", lst);
        }

        public JsonResult DeleteReceivedShipmentById(int Id)
        {

            if (Id != 0)
            {
                Context.DeletetblReceivedShipments(Id);
            }
            return Json(Id.ToString(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeTblShipmentReturnStatus(int Id, int cust_id, decimal returnCharge)
        {
            if (Id != 0)
            {
                Context.ChangeTblShipmentReturnStatus(Id, cust_id, returnCharge);
            }
            return Json("Reload", JsonRequestBehavior.AllowGet);
        }

        public JsonResult RejectRequestShipment(int Id, bool isUserHaveBalance)
        {
            string Message = Context.RejectRequest(Id, isUserHaveBalance);
            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReceivedShipmentDetailById(int Id)
        {
            List<Repository.tblShipmentDetail> lst = Context.GetTblShipmentDetailById(Id);
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        //Return Shipment 
        public JsonResult checkReturnAmountInUserWallet(int Id, string Customer_Reference)
        {
            bool ret = false;
            int cust_id = _context.tblCustomers.AsNoTracking().AsQueryable().Where(x => x.customer_reference == Customer_Reference).Select(x => x.Id).FirstOrDefault();
            _ChineseAddress.com.Common.Services.clsCommonServiceRoot clsCommonService = new Common.Services.clsCommonServiceRoot();
            var charge = clsCommonService.getPlanChargePriceByCustomerId(cust_id, "Return Shipment");
            var walletBalance = _repositoryUserWalletService.yourBalance(cust_id);
            if (walletBalance >= charge)
            {
                ret = true;
            }
            return Json(new { issuccess = ret, cust_id = cust_id, returnCharge = charge }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ReturnShipmentByFkId(int Id)
        {
            List<tblShipmentReturnDetail> lst = Context.GetTblShipmentReturnByFkId(Id);
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddEditPckagesImages(int Id, bool IsChargeable = true)
        {
            try
            {
                if (Request.InputStream.Length > 0)
                {
                    using (StreamReader reader = new StreamReader(Request.InputStream))
                    {
                        string hexString = Server.UrlEncode(reader.ReadToEnd());
                        ViewBag.hexaString = hexString;
                        Context.InsertUpdateTblShipmentImagesByBytes(Id, hexString);
                    }
                }
                TempData["IsChargeable"] = IsChargeable;
                TempData["Ids"] = Id;
                List<tblShipmentImage> lst = Context.GetTblShipmentImagesByfkId(Id);
                if (lst != null)
                {
                    return View(lst);
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
            return View();
        }


        [HttpPost]
        public ActionResult AddEditPckagesImages(HttpPostedFileBase[] input7)
        {
            try
            {
                if (input7 != null)
                {
                    int Fk_Id = Convert.ToInt32(TempData.Peek("Ids"));
                    ViewBag.message = Context.InsertUpdateTblShipmentImages(Fk_Id, input7, Convert.ToBoolean(TempData.Peek("isChargeable")));
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
            return RedirectToAction("AddEditPckagesImages", new { Id = Convert.ToInt32(TempData.Peek("Ids")), IsChargeable = Convert.ToBoolean(TempData.Peek("isChargeable")) });
        }

        public void UploadWebCamImage(int Id)
        {
            if (Request.InputStream.Length > 0)
            {
                using (StreamReader reader = new StreamReader(Request.InputStream))
                {
                    string hexString = Server.UrlEncode(reader.ReadToEnd());
                    Context.InsertUpdateTblShipmentImagesByBytes(Id, hexString, Convert.ToBoolean(TempData.Peek("isChargeable")));
                }
            }
            TempData["Ids"] = Id;
        }

        public JsonResult AwatingPriceCorrection(int shipmentID)
        {
            List<tblShipmentDetail> ShimentDeatilsList = Context.GetAwatingCorrectionDetails(shipmentID);
            return Json(ShimentDeatilsList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCorrectPrice(tblShipmentDetail objtblShipmentDetail)
        {
            string Message = Context.SaveCPrice(objtblShipmentDetail);
            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CancelCorrectPrice(string shipmentID)
        {
            tblReceivedShipment objtblReceivedShipment = new tblReceivedShipment();
            objtblReceivedShipment.id = Convert.ToInt32(shipmentID);
            string Message = Context.CancelCorrect_Price(objtblReceivedShipment);
            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ViewCorrectPrice(string shipmentID)
        {
            tblReceivedShipment objtblReceivedShipment = new tblReceivedShipment();
            objtblReceivedShipment.id = Convert.ToInt32(shipmentID);
            objtblReceivedShipment = Context.ViewCorrectPrice_label(objtblReceivedShipment);
            return Json(objtblReceivedShipment, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletePckagesImages(string file_path)
        {
            int Fk_Id = Convert.ToInt32(TempData["Ids"]);
            try
            {
                if (file_path != null)
                {
                    ViewBag.Message = Context.DeleteTblShipmentImages(Fk_Id, file_path);
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
            return RedirectToAction("AddEditPckagesImages", new { id = Fk_Id });
        }
    }
}