using _ChineseAddress.com.Areas.Admin.Services;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.Services;
using System;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin")]
    public class CouponController : Controller
    {
        private CouponService objCouponService;

        public CouponController()
        {
            objCouponService = new CouponService();
        }

        // GET: Admin/Coupon
        [HttpGet]
        public ActionResult Index(int? id)
        {
            tblCoupon objCouponDetail = new tblCoupon();
            if (id != null)
            {
                TempData["Coupon_Id"] = id;
                objCouponDetail = objCouponService.getCouponById(id ?? 0);
            }
            else
            {
                if (TempData["Coupon_Id"] != null)
                {
                    TempData["Coupon_Id"] = null;
                }
            }
            BindCountries();

            return View(objCouponDetail);
        }

        [HttpPost]
        public ActionResult Index(tblCoupon objCoupon)
        {
            if (TempData["Coupon_Id"] != null)
            {
                objCoupon.id = Convert.ToInt32(TempData["Coupon_Id"]);
            }
            if (objCoupon.coupon_status != "")
            {
                objCoupon.status = Convert.ToInt16(objCoupon.coupon_status);
            }

            ViewBag.Message = objCouponService.AddUpdateCoupon(objCoupon);

            BindCountries();
            return View(objCoupon);
        }

        [OutputCache(Duration = 216000)]
        private void BindCountries()
        {
            try
            {
                ViewBag.country_to = (new CountryServices()).Country();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ManageCoupon()
        {
            try
            {
                return View(objCouponService.GetCouponDetails());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult IsCouponExist(string coupon_code, int? id)
        {
            bool isExist = objCouponService.IsCouponExist(coupon_code, (id ?? 0));
            return Json(!isExist, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult validateMinMaxOrder(double max_order_value, double min_order_value)
        {
            bool isExist = false;
            if (min_order_value <= max_order_value)
                isExist = true;
            return Json(isExist, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult validateStartEndDate(DateTime start_date, DateTime end_date)
        {
            bool isValid = false;
            if (start_date <= end_date)
                isValid = true;
            return Json(isValid, JsonRequestBehavior.AllowGet);
        }
    }
}