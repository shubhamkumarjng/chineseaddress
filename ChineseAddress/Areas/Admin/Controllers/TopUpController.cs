using _ChineseAddress.com.Areas.Admin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _ChineseAddress.com.Repository;
using Microsoft.Practices.Unity;
using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Common.Services;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin")]
    public class TopUpController : Controller
    {
        private readonly TopUpService serviceContext = new TopUpService();
        [Dependency]
        public IWalletService<tblTopUp, tblTopUpMethod> _repositoryUserWalletService { get; set; }
        // GET: Admin/TopUp
        public ActionResult Index()
        {
            var listTopUpDetail = _repositoryUserWalletService.getTopUpDetail();
            return View(listTopUpDetail);
        }

        /// <summary>
        /// Add Top Up wallet
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ActionResult Add(int? id, string type)
        {
            tblTopUp objTopUp = new tblTopUp();
            try
            {
                //Customer Drop Down 
                ReceivedShipmentService objReceivedShipmentService = new ReceivedShipmentService();
                ViewBag.Fk_Customer_Id = objReceivedShipmentService.Customer();

                ViewBag.eTopUpListItem = enums.EnumToSelectListItems<eTopUpWalletStatus>();

                ViewBag.TopUpMethodId = _repositoryUserWalletService.getTopUpMethodsAsItems();
                if ((id ?? 0) != 0)
                {
                    objTopUp = _repositoryUserWalletService.getTopUpDetailById((id ?? 0));
                    if (!_repositoryUserWalletService._isSucceeded)
                        TempData["msg"] = _repositoryUserWalletService._msg;

                    if (objTopUp != null)
                    {
                        if (!string.IsNullOrEmpty(type))
                        {
                            if (type == "accept")
                            {
                                objTopUp.status = (int)eTopUpWalletStatus.Accept;
                            }
                            else if (type == "reject")
                            {
                                objTopUp.status = (int)eTopUpWalletStatus.Reject;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["msg"] = $"'showMessage({ex.Message.ToString()})',false);";
                throw ex;
            }
            return View(objTopUp);
        }

        /// <summary>
        /// Add Top Up wallet Post
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult Add(tblTopUp _tblTopUp)
        {
            //_tblTopUp.Fk_Customer_Id = Convert.ToInt32(Session["UserId"]);
            //_tblTopUp.status = (int)eTopUpWalletStatus.Pending;
            _repositoryUserWalletService.AddUpdateTopUpWalletRequest(_tblTopUp, true);

            TempData["msg"] = _repositoryUserWalletService._msg;

            if (_repositoryUserWalletService._isSucceeded)
            {

                SendEmailTopUpStatus(_tblTopUp);

                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult AddEditMethod(int id = 0)
        {
            tblTopUpMethod objtblTopUpMethod = new tblTopUpMethod();
            if (id != 0)
            {
                objtblTopUpMethod = serviceContext.getTopUpMethodById(id);
            }
            return View(objtblTopUpMethod);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEditMethod(tblTopUpMethod objTopUp)
        {
            ViewBag.Message = serviceContext.SaveUpadateTopUpMethod(objTopUp);

            return View();
        }

        public ActionResult ManageMethods()
        {
            List<tblTopUpMethod> lstTopUpMethod = serviceContext.getTopUpMethods();
            return View(lstTopUpMethod);
        }

        public JsonResult ChangeStatusTopUpMethodById(int Id)
        {
            if (Id != 0)
            {
                ViewBag.Message = serviceContext.ChangeStatustblTopUpMethods(Id);
            }
            return Json(Id.ToString(), JsonRequestBehavior.AllowGet);
        }

        public void SendEmailTopUpStatus(tblTopUp _tblTopUp)
        {
            Context4InshipEntities Context = new Context4InshipEntities();
            if (_tblTopUp.status == (int)eTopUpWalletStatus.Accept)
            {
                tblCustomer customer = Context.tblCustomers.AsQueryable().Where(X => X.Id == _tblTopUp.Fk_Customer_Id).FirstOrDefault();
                string firstName = Context.tblAddressBooks.AsQueryable().Where(x => x.Fk_customer_Id == customer.Id).OrderBy(x => x.created_on).Select(x => x.first_name).FirstOrDefault();

                _tblTopUp = _repositoryUserWalletService.getTopUpDetailById(_tblTopUp.Id);
                //Send Sign Email Start
                clsSendEmailService objclsSendEmailService = new clsSendEmailService();

                string subject = "chineseaddress - Top Up Request Approved";
                System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append($"This is an automatic email just to let you know that your top Up request with  reference no <strong>" + _tblTopUp.transaction_id + "</strong> has been approved.<br/>Your updated balance is $" + _repositoryUserWalletService.yourBalance(_tblTopUp.Fk_Customer_Id).ToString("F") + ".");
                hashTable.Add("#user#", firstName);
                hashTable.Add("#content#", sb.ToString());
                //Send Sign Email End
                objclsSendEmailService.SendEmail(new[] { Convert.ToString(customer.email) }, null, null, (new clsEmailTemplateService().GetEmailTemplate("commonuser", hashTable)), subject);
            }
            else if (_tblTopUp.status == (int)eTopUpWalletStatus.Reject)
            {
                tblCustomer customer = Context.tblCustomers.AsQueryable().Where(X => X.Id == _tblTopUp.Fk_Customer_Id).FirstOrDefault();
                string firstName = Context.tblAddressBooks.AsQueryable().Where(x => x.Fk_customer_Id == customer.Id).OrderBy(x => x.created_on).Select(x => x.first_name).FirstOrDefault();
                _tblTopUp = _repositoryUserWalletService.getTopUpDetailById(_tblTopUp.Id);
                //Send Sign Email Start
                clsSendEmailService objclsSendEmailService = new clsSendEmailService();

                string subject = "chineseaddress - Top Up Request Disapproved";
                System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append($"This is an automatic email just to let you know your top Up request with  reference no <strong>" + _tblTopUp.transaction_id + "</strong> has been disapproved by reason " + _tblTopUp.admin_remark + ".");
                hashTable.Add("#user#", firstName);
                hashTable.Add("#content#", sb.ToString());
                //Send Sign Email End
                objclsSendEmailService.SendEmail(new[] { Convert.ToString(customer.email) }, null, null, (new clsEmailTemplateService().GetEmailTemplate("commonuser", hashTable)), subject);
            }
            else if (_tblTopUp.status == (int)eTopUpWalletStatus.Pending)
            {
                if (_tblTopUp.Id == 0)
                    _repositoryUserWalletService.sendEmailOnAddEditTopUpRequest(_tblTopUp);
                else
                    _repositoryUserWalletService.sendEmailOnAddEditTopUpRequest(_tblTopUp, false);
            }

        }
    }
}