using _ChineseAddress.com.Areas.Admin.Services;
using _ChineseAddress.com.Repository;
using System.Collections.Generic;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin ,Staff")]
    public class CustomerController : Controller
    {
        // GET: Admin/Customer
        private Customerservice Context = new Customerservice();

        [HttpGet]
        public ActionResult Index()
        {
            List<tblCustomer> custList = Context.GetCustomerDetails();
            return View("Index", custList);
        }

        public JsonResult ChangeStatusCustomerById(int id)
        {
            if (id != 0)
            {
                ViewBag.Message = Context.ChangeStatustblCustomer(id);
                string Status =ViewBag.Message ;
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        public JsonResult ChangepaymentStatusCustomerById(int Id)
        {
            if (Id != 0)
            {
                ViewBag.Message = Context.ChangePaymentStatustblCustomer(Id);
            }
            return Json(Id.ToString(), JsonRequestBehavior.AllowGet);
        }
    }
}