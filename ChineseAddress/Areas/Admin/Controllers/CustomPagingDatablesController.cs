﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _4InShip.com.Repository;
using _4InShip.com.Areas.Admin.Models;
using PagedList;
namespace _4InShip.com.Areas.Admin.Controllers
{
    public class CustomPagingDatablesController : Controller
    {
        // GET: Admin/CustomPagingDatables
        public ActionResult Index(DataTablesParamModel param)
        {

            return View();

        }
        public ActionResult Result_Index(DataTablesParamModel param)
        {
            Context4InshipEntities context = new Context4InshipEntities();
            var AllCustomer = context.tblCustomers.ToList();
            var Customer = AllCustomer.ToSkipTake(param.iDisplayStart, param.iDisplayLength);
            var result = from c in Customer
                         select new[] { c.email };

            //search
            //if (!string.IsNullOrEmpty(param.sSearch))
            //{
            //    Customer = AllCustomer.Where(c => c.Name.Contains(param.sSearch)
            //                         ||
            //              c.Address.Contains(param.sSearch)
            //                         ||
            //                         c.Town.Contains(param.sSearch));
            //}
            //else
            //{
            //    filteredCompanies = allCompanies;
            //}


            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = AllCustomer.Count(),
                iTotalDisplayRecords = AllCustomer.Count(),
                aaData = result
            },
                JsonRequestBehavior.AllowGet);
        }

    }

}