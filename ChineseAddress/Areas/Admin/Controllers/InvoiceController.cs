using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Areas.Admin.Services;
using System.Collections.Generic;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin ,Staff")]
    public class InvoiceController : Controller
    {
        // GET: Admin/Invoice
        private InvoiceService Context = new InvoiceService();

        public ActionResult Index()
        {
            List<ViewModelInvoice> lst = Context.GetTblInvoiceDetail();
            return View(lst);
        }
    }
}