using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Areas.Admin.Services;
using _ChineseAddress.com.Repository;
using System.Collections.Generic;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin")]
    public class PlanOptionLinkingController : Controller
    {
        // GET: Admin/PlanOptionLinking
        private PlanOptionLinkingService Context = new PlanOptionLinkingService();

        [HttpGet]
        public ActionResult Linkoptionswithplan()
        {
            List<ViewModeltblPlanOptionLinking> LinkingOptionsList = Context.GetLinkingOptions();
            return View(LinkingOptionsList);
        }

        [HttpGet]
        public ActionResult Index(int? Id)
        {
            Fk_plan_id();
            Fk_packaging_option_id();
            if (Id != null)
            {
                tblPlanOptionLinking LinkingOptionsList = Context.GetLinkingOptionsById(Id ?? 0);
                return View(LinkingOptionsList);
            }

            return View();
        }

        [HttpPost]
        public ActionResult Index(tblPlanOptionLinking tbllinkingoptionModel)
        {
            if (tbllinkingoptionModel != null)
            {
                ViewBag.Message = Context.InserUpdateLinkingOptions(tbllinkingoptionModel);
            }
            Fk_plan_id();
            Fk_packaging_option_id();
            return View();
        }

        private void Fk_plan_id()
        {
            ViewBag.Fk_plan_id = Context.GetPlan();
        }

        private void Fk_packaging_option_id()
        {
            ViewBag.Fk_packaging_option_id = Context.GetPackagingOptions();
        }

        [HttpPost]
        public JsonResult DeleteOptionLinkingById(int id)
        {
            if (id != 0)
            {
                ViewBag.Message = Context.DeletePlanOptionLinkingById(id);
            }
            return Json(id, ToString(), JsonRequestBehavior.AllowGet);
        }
    }
}