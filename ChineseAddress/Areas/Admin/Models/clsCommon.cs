using _ChineseAddress.com.Repository;
using System.Collections.Generic;

namespace _ChineseAddress.com.Areas.Admin.Models
{
    public class clsCommon
    {
        public string SuccessMsg(string msg)
        {
            return string.Format("showMessage('{0}',true);", msg.Replace("'", @""""));
        }

        public string ErrorMsg(string msg)
        {
            return string.Format("showMessage('{0}',false);", msg.Replace("'", @""""));
        }

        public string Status(string Statustype)
        {
            if (Statustype == "1")
            {
                Statustype = "label-success" + "," + "Ready";
            }
            else if (Statustype == "2")
            {
                Statustype = "label-primary" + "," + "Processing";
            }
            else if (Statustype == "3")
            {
                Statustype = "label-default" + "," + "Payment Awaiting";
            }
            else if (Statustype == "4")
            {
                Statustype = "label-warning" + "," + "On Hold";
            }
            else if (Statustype == "5")
            {
                Statustype = "label-danger" + "," + "Restricted";
            }
            else if (Statustype == "7")
            {
                Statustype = "label-danger" + "," + "Abandon";
            }
            else if (Statustype == "8")
            {
                Statustype = "label-warning" + "," + "Return Pending";
            }
            else if (Statustype == "9")
            {
                Statustype = "label-danger" + "," + "Returned";
            }
            else if (Statustype == "10")
            {
                Statustype = "label-success" + "," + "Order Created";
            }
            return Statustype;
        }

        public string OrderStatus(string Statustype)
        {
            if (Statustype == "1")
            {
                Statustype = "label-primary" + "," + "In Queue";
            }
            else if (Statustype == "2")
            {
                Statustype = "label-primary" + "," + "Processing";
            }
            else if (Statustype == "3")
            {
                Statustype = "label-default" + "," + "Awaiting Payment";
            }
            else if (Statustype == "4")
            {
                Statustype = "label-success" + "," + "Awaiting Dispatch ";
            }
            else if (Statustype == "5")
            {
                Statustype = "label-success" + "," + "Dispatched";
            }
            else if (Statustype == "6")
            {
                Statustype = "label-success" + "," + "Delivered";
            }
            else if (Statustype == "7")
            {
                Statustype = "label-danger" + "," + "Restricted";
            }
            else if (Statustype == "8")
            {
                Statustype = "label-danger" + "," + "Awaiting Confirmation";
            }
            else if (Statustype == "9")
            {
                Statustype = "label-danger" + "," + "Abandoned";
            }
            else if (Statustype == "10")
            {
                Statustype = "label-warning" + "," + "On Hold";
            }
            return Statustype;
        }

        public string GetChargeByChargename(Dictionary<string, tblOrderCharge> objs, string chargename)
        {
            if (objs.ContainsKey(chargename))
            {
                tblOrderCharge sellprice = objs[chargename];
                return "$" + sellprice.sell_price.ToString();
            }
            else
            {
                return "$0.00";
            }
        }

        public static int getLoggedInUserId()
        {
            return (int)System.Web.HttpContext.Current.Session["UserId"];
        }
    }
}