using System;

namespace _ChineseAddress.com.Areas.Admin.Models
{
    public class ViewModeltblPlanOptionLinking
    {
        public string PlanTitle { get; set; }
        public string packagingOptionTitle { get; set; }
        public Nullable<decimal> Price { get; set; }
        public int Id { get; set; }
    }
}