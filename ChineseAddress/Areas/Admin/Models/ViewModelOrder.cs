using System;

namespace _ChineseAddress.com.Areas.Admin.Models
{
    public class ViewModelOrder
    {
        public int id { get; set; }

        public Nullable<System.DateTime> createdOn { get; set; }
        public int Fk_customer_id { get; set; }
        public string Customer_Email { get; set; }
        public int Order_id { get; set; }
        public int Fk_invoice_id { get; set; }
        public string Order_Refernce_No { get; set; }
        public string delivery_Address { get; set; }
        public string car { get; set; }
        public string product { get; set; }
        public string service { get; set; }
        public Nullable<System.DateTime> pickup_date { get; set; }
        public Nullable<System.DateTime> pickup_cut_off_time { get; set; }
        public Nullable<System.DateTime> booking_time { get; set; }
        public Nullable<System.DateTime> delvery_date { get; set; }
        public Nullable<System.DateTime> delvery_time { get; set; }
        public decimal? payable_amount { get; set; }
        public string tracking_no { get; set; }
        public decimal billing_weight { get; set; }
        public Nullable<bool> is_delivered { get; set; }
        public string signature { get; set; }
        public string Customer_reference { get; set; }
        public int status { get; set; }
        public string sender { get; set; }
        public string tracking { get; set; }
        public string carrier { get; set; }
        public string AssignedCustomer { get; set; }
        public string freeleftDays { get; set; }
        public bool? urgent { get; set; }
    }
}