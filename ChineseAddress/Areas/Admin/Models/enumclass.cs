namespace _ChineseAddress.com.Areas.Admin.Models
{
    public enum enumReceivShipmentStatus
    {
        Ready = 1,
        Processing = 2,
        Payment_Awaiting = 3,
        On_Hold = 4,
        Restricted = 5,
        Dispossed_Off = 7,
        Return_Pending = 8,
        Returned = 9,
        Order_Created = 10,
        isInctive = 11
    }

    public class enumsecond
    {
        private enumReceivShipmentStatus enumclass;

        public void Setenum(enumReceivShipmentStatus value)
        {
            enumclass = value;
        }

        public enumReceivShipmentStatus Getenum()
        {
            return enumclass;
        }
    }

    public enum enumorder
    {
        In_Queue = 1,
        Proccessing = 2,
        Awaiting_Payment = 3,
        Awaiting_Dispatched = 4,
        Dispatched = 5,
        Delivered = 6,
        Restricted = 7,
        Awaiting_Confirmation = 8,
        Dispossed_Off = 9,
        On_Hold = 10,
        isInctive=11
    }

    public enum enamnotification
    {
        Expire_Soon = 1,
        Expired = 2
    }
    public class enumseconds
    {
        private enumorder enumclass;

        public void Setenum(enumorder value)
        {
            enumclass = value;
        }

        public enumorder Getenum()
        {
            return enumclass;
        }
    }
}