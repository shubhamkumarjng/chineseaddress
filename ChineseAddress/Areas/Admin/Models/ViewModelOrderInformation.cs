using System;

namespace _ChineseAddress.com.Areas.Admin.Models
{
    public class ViewModelOrderInformation
    {
        public int id { get; set; }
        public DateTime? orderon { get; set; }
        public string orderno { get; set; }
        public string shipmethod { get; set; }
        public int status { get; set; }
        public string trackingnumber { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string address1 { get; set; }
        public string city { get; set; }
        public string address2 { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        // public List<tblOrderCharge> OrderChargeDetails { get; set; }
        public string totalshipment { get; set; }

        public string totalpackage { get; set; }
        public string billweight { get; set; }
        public decimal totalcharges { get; set; }
        public string miscellaneous_charge_name { get; set; }
        public decimal? miscellaneous_charge_Price { get; set; }
        public string order_notes { get; set; }
    }
}