using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace _ChineseAddress.com.Repository
{
    [MetadataType(typeof(tblCouponMetaData))]
    public partial class tblCoupon
    {
        public string coupon_status { get; set; }
        public string customer_name { get; set; }
    }

    public class tblCouponMetaData
    {
        [Required(ErrorMessage = "Coupon Code is required")]
        [MaxLength(9)]
        [Remote("IsCouponExist", "Coupon", "Admin", ErrorMessage = "Coupon Code already exist", AdditionalFields = "max_order_value")]
        public string coupon_code { get; set; }

        [Required(ErrorMessage = "Coupon value/Percentage is required")]
        public double coupon_value { get; set; }

        [DisplayName("Order Min. Value")]
        public Nullable<double> min_order_value { get; set; }

        [Display(Name = "Order Max. Value")]
        //[System.ComponentModel.DataAnnotations.Compare("min_order_value", ErrorMessage = "Order Maximum Value must be greater than equal to Order Minimum Value")]
        [Remote("validateMinMaxOrder", "Coupon", "Admin", ErrorMessage = "Order Maximum Value must be greater than equal to Order Minimum Value", AdditionalFields = "min_order_value")]
        public Nullable<double> max_order_value { get; set; }

        public Nullable<DateTime> start_date { get; set; }

        [Remote("validateStartEndDate", "Coupon", "Admin", ErrorMessage = "End Date must be greater than equal to Start Date", AdditionalFields = "start_date")]
        public Nullable<DateTime> end_date { get; set; }
    }
}