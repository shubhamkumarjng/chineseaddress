using System;
using System.ComponentModel.DataAnnotations;

namespace _ChineseAddress.com.Repository
{
    [MetadataType(typeof(TblLinkingOptionProperties))]
    public partial class tblPlanOptionLinking
    {
    }

    public class TblLinkingOptionProperties
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Plan is required")]
        public int Fk_plan_id { get; set; }

        [Required(ErrorMessage = "Packaging Option is required")]
        public int Fk_packaging_option_id { get; set; }

        [Required(ErrorMessage = "Price is required")]
        public Nullable<decimal> price { get; set; }
    }
}