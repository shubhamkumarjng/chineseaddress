namespace _ChineseAddress.com.Repository
{
    public partial class tblShipmentDetail
    {
        public string[] CorrectPrice { get; set; }
        public string[] Fk_ShipmentID { get; set; }
        public string[] ShipmentID { get; set; }
    }

    public class ShipMentDetails
    {
        public int id { get; set; }
        public int Fk_shipment_id { get; set; }
        public string comodity_code { get; set; }
        public string description { get; set; }
        public int quantity { get; set; }
        public decimal purchase_price { get; set; }
        public string harmonizedCode { get; set; }
    }
}