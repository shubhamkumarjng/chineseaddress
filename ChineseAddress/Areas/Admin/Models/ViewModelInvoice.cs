namespace _ChineseAddress.com.Areas.Admin.Models
{
    public class ViewModelInvoice
    {
        public int Id { get; set; }
        public int orderId { get; set; }
        public string customer { get; set; }
        public string orderNO { get; set; }
        public string customeremail { get; set; }
        public string billingaddress { get; set; }
        public string invoicenumber { get; set; }
        public System.DateTime? invoice_date { get; set; }
        public int? paid_status { get; set; }
        public System.DateTime? paid_on { get; set; }
        public string payment_method { get; set; }
        public decimal payment_amount { get; set; }
        public string transaction_id { get; set; }
        public string custom_guid { get; set; }
        public string invoice_type { get; set; }
        public string transaction_status { get; set; }
        public string transaction_response { get; set; }
    }
}