using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace _ChineseAddress.com.Repository
{
    [MetadataType(typeof(tblTopUpMethodMetaData))]
    public partial class tblTopUpMethod
    {
    }
    internal sealed class tblTopUpMethodMetaData
    {
        [Required(ErrorMessage ="Method Name is required")]
        public string method_name { get; set; }
        [Required(ErrorMessage = "Method Code is required")]
        public string method_code { get; set; }
        [Required(ErrorMessage = "Method Description is required")]
        [AllowHtml]
        public string method_description { get; set; }
    }
}