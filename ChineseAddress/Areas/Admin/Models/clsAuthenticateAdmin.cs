using System;
using System.Web;
using System.Web.Security;

namespace _ChineseAddress.com.Areas.Admin.Models
{
    public class clsAuthenticateAdmin
    {
        public void CheckUserAuthenticated()
        {
            HttpSessionStateBase Session = new HttpSessionStateWrapper(HttpContext.Current.Session);
            FormsIdentity fi;
            fi = (FormsIdentity)HttpContext.Current.User.Identity;
            string[] ud = fi.Ticket.UserData.Split(';');
            Session["AdminRole"] = ud[0].ToString();
            Session["AdminId"] = Convert.ToInt32((string.IsNullOrEmpty(ud[1]) ? "0" : ud[1]));
            Session["AdminUsername"] = ud[2];
        }

        public bool isUserWithAdminRole()
        {
            HttpSessionStateBase Session = new HttpSessionStateWrapper(HttpContext.Current.Session);
            if (Convert.ToString(Session["AdminRole"]) == "Admin")
                return true;
            else
                return false;
        }
    }
}