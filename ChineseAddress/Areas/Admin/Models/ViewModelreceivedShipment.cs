using System;
using System.Collections.Generic;

namespace _ChineseAddress.com.Areas.Admin.Models
{
    public class ViewModelreceivedShipment
    {
        public int id { get; set; }
        public string RefernceNumber { get; set; }
        public List<dynamic> chkimage { get; set; }
        public List<dynamic> chkimageNonChargeable { get; set; }

        public int temp { get; set; }
        public string Fk_assigned_user { get; set; }
        public System.DateTime received_date { get; set; }
        public string sender { get; set; }
        public string tracking { get; set; }
        public string carrier { get; set; }
        public Nullable<decimal> weight { get; set; }
        public Nullable<decimal> height { get; set; }
        public Nullable<decimal> length { get; set; }
        public Nullable<decimal> width { get; set; }
        public string box_condition { get; set; }
        public string staff_comments { get; set; }
        public string user_notes { get; set; }
        public string modified_by { get; set; }
        public int Numberofitems { get; set; }
        public int Correct_Price_Status { get; set; }
        public string correct_price_file_path { get; set; }
        public string Created_By { get; set; }
        public string status { get; set; }
        public int freestoragedays { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }

        public Nullable<System.DateTime> modified_on { get; set; }

        public Nullable<bool> is_requested_picture { get; set; }
        public string shelf_no { get; set; }
    }
}