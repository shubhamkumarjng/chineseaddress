using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.Repository;
using Microsoft.Practices.Unity;
using PagedList;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;

namespace _ChineseAddress.com.Areas.User.Services
{
    public class OrderService : clsCommon, IOrder<ViewModelOrderinfo, tblOrderCharge, ViewRecivedShipmentModel, ViewOrderModel, tblReceivedShipment, tblOrderBox>
    {
        private UserCommanClass objUserCommanClass = new UserCommanClass();

        [Dependency]
        public Context4InshipEntities Context { get; set; }

        [Dependency]
        public IWalletService<tblTopUp, tblTopUpMethod> _repoWalletService { get; set; }

        public IPagedList<ViewOrderModel> GettblShipmentReturnDetail(int CutomerID, int PageNo)
        {
            IPagedList<ViewOrderModel> OrderInformation =
       (from tblord in Context.tblOrders
        join tblDaddress in Context.tblDeliveryAddresses on tblord.Fk_delivery_address_id equals tblDaddress.id
        join tblinv in Context.tblInvoices on tblord.Fk_invoice_id equals tblinv.Id
        into oida
        from item in oida.DefaultIfEmpty()
        select new ViewOrderModel { Fk_Customer_id = tblord.Fk_customer_id, OrderRefernece = tblord.reference_no, Created_on = tblord.created_on, TrackingNumber = tblord.tracking_no, Status = tblord.status, Order_id = tblord.id, Carrier = tblord.carrier, Estimate_Price = tblord.estimated_orderprice, Save_Amount = tblord.save_amount, DeliveredAddress = tblDaddress.address_name, PaybelAmount = tblord.payable_amount }).Where(x => x.Fk_Customer_id == CutomerID).OrderByDescending(x => x.Created_on).ToList().ToPagedList(PageNo, MvcApplication.global_Page_Size);
            //List<ViewOrderModel> OrderInformation = (from tblord in Context.tblOrders join tblinv in Context.tblInvoices on tblord.Fk_customer_id equals tblinv.Fk_customer_id join tblDaddress in Context.tblDeliveryAddresses on tblord.Fk_delivery_address_id equals tblDaddress.id select new ViewOrderModel { Fk_Customer_id = tblord.Fk_customer_id, OrderRefernece = tblord.reference_no, Created_on = tblord.created_on, TrackingNumber = tblord.tracking_no, Status = tblord.status, Order_id = tblord.id, Carrier = tblord.carrier, DeliveredAddress = tblDaddress.address_name, PaybelAmount = tblinv.payment_amount }).Where(x => x.Fk_Customer_id == CutomerID).OrderByDescending(x => x.Created_on).ToList();
            return OrderInformation;
        }

        public ViewModelOrderinfo GetOrderInformation(int Id)
        {
            try
            {
                int totalship = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == Id).Select(x => x).Count();
                int totalpackage = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == Id).Select(x => x).Count();
                var billweight = (Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == Id).Select(x => x).Sum(x => x.billable_weight) ?? 0.00m);
                decimal balance = _repoWalletService.yourBalance(Convert.ToInt32(HttpContext.Current.Session["UserId"]));
                return (from ord in Context.tblOrders
                        join del in Context.tblDeliveryAddresses on ord.Fk_delivery_address_id equals del.id
                        join cd in Context.tblCarriersDetails on ord.service_code equals cd.code
                        join cm in Context.tblCountryMasters on del.country_code equals cm.country_code
                        where cd.service == ord.service && cd.product == ord.product
                        select new ViewModelOrderinfo()
                        {
                            id = ord.id,
                            City = del.city,
                            Postal_Code = del.postal_code,
                            trackingnumber = ord.tracking_no,
                            orderno = ord.reference_no,
                            status = ord.status,
                            shipmethod = ord.carrier,
                            name = del.first_name + " " + del.last_name,
                            address1 = del.address1,
                            address2 = del.address2,
                            state = del.state,
                            country = cm.country_name,
                            phone = del.mobile1,
                            orderon = ord.created_on,
                            carrier_logo = cd.logo,
                            totalshipment = SqlFunctions.StringConvert((double)totalship),
                            totalpackages = SqlFunctions.StringConvert((double)totalpackage),
                            billableweight = billweight,
                            WalletBalance = balance,
                            totalamount = (ord.payable_amount ?? 0.0m)
                            
                            //,order_notes = ord.order_notes

                        }).Where(x => x.id == Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public dynamic GetTblOrdeChargedByFkId(int id)
        {
            try
            {
                ViewModelOrderinfo objviewmodelinfo = new ViewModelOrderinfo();
                List<ViewModelOrderCharges> objOrderCharges = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == id).Select(x => new ViewModelOrderCharges { charge_name = (x.charge_name ?? ""), sell_price = (x.sell_price ?? 0.0m) }).ToList();
                return objOrderCharges;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                throw;
            }
        }
        public decimal GetOrderCharges(int id)
        {
            decimal Charges = 0.0m;
            var GetCouponValue = Context.tblOrderCharges.AsQueryable().Where(x => x.charge_name.ToLower() == "coupon" && x.Fk_order_id == id).FirstOrDefault();
            if (GetCouponValue != null)
            {
                var GetOrderCharges = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == id && x.charge_name.ToLower() != "coupon").Sum(x => x.sell_price);
                if (Math.Abs(Convert.ToDecimal(GetCouponValue.sell_price)) > GetOrderCharges)
                {
                    Charges = 0.00m;
                }
                else
                {
                    var GetSumOrdercharges = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == id).Sum(x => x.sell_price);
                    Charges = Convert.ToDecimal(GetSumOrdercharges);
                }

            }
            else
            {
                var GetSumOrdercharges = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == id).Sum(x => x.sell_price);
                Charges = Convert.ToDecimal(GetSumOrdercharges);
            }
            return Charges;
        }
        public List<tblOrderBox> GetTblOrdeBoxesByFkId(int id)
        {
            try
            {
                List<tblOrderBox> obj = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == id).ToList();
                return obj;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ViewRecivedShipmentModel> CreateOrderRecivedShipment(int OrderID)
        {
            try
            {
                tblOrder objorder = new tblOrder();
                ViewRecivedShipmentModel objrecshipment = new ViewRecivedShipmentModel();
                List<ViewRecivedShipmentModel> objViewRecivedShipmentModelList = new List<ViewRecivedShipmentModel>();
                objorder.id = OrderID;
                var RecivedshipmentList = Context.tblReceivedShipments.Where(x => x.Fk_order_id == OrderID).ToList();
                foreach (var item in RecivedshipmentList)
                {
                    objrecshipment.RecivedShipment_ImagePath = Context.tblShipmentImages.Where(x => x.Fk_shipment_id == item.id).Select(x => x.file_path).ToList<dynamic>();
                    objViewRecivedShipmentModelList.Add(new ViewRecivedShipmentModel()
                    {
                        sender = item.sender,
                        received_date = item.received_date,
                        length = item.length,
                        weight = item.weight,
                        width = item.width,
                        height = item.height,
                        box_condition = item.box_condition,
                        staff_comments = item.staff_comments,
                        status = item.status,
                        tracking = item.tracking,
                        ID = item.id,
                        user_notes = item.user_notes,
                        RecivedShipment_ImagePath = objrecshipment.RecivedShipment_ImagePath
                    });
                }
                return objViewRecivedShipmentModelList.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SaveNotes(tblReceivedShipment objtblReceivedShipment)
        {
            try
            {
                var Reshipmentdata = Context.tblReceivedShipments.Where(x => x.id == objtblReceivedShipment.id).Select(x => x).FirstOrDefault();
                Reshipmentdata.user_notes = objtblReceivedShipment.user_notes;
                Context.Configuration.ValidateOnSaveEnabled = false;
                Context.Entry(Reshipmentdata).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
                return "Notes has been successfully  saved";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ViewRecivedShipmentModel> GetShipmentDetails()
        {
            try
            {
                List<ViewRecivedShipmentModel> ShipmentDetails
                    = (from tblrec in
                       Context.tblReceivedShipments
                       join tblship in Context.tblShipmentDetails on tblrec.id equals tblship.Fk_shipment_id
                       select new ViewRecivedShipmentModel
                       {
                           description = tblship.description,
                           purchase_price = tblship.purchase_price,
                           quantity = tblship.quantity,
                           Fk_shipment_id = tblship.Fk_shipment_id
                       }).ToList();
                return ShipmentDetails.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CancleOrder(int Id)
        {
            try
            {
                objUserCommanClass.DeleteCreateOrder(Id);
                return SuccessMsg("Deleted successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public string ConfirmOrder(int OrderID)
        {
            string message = "";
            try
            {
                var UpdateOrdertblstatus = Context.tblOrders.Where(x => x.id == OrderID).ToList();
                var Updaterecivedshipstatus = Context.tblReceivedShipments.Where(x => x.Fk_order_id == OrderID).ToList();
                if (UpdateOrdertblstatus.Count != 0 || Updaterecivedshipstatus.Count != 0)
                {
                    UpdateOrdertblstatus.ForEach(x => { x.status = 1; });
                    Updaterecivedshipstatus.ForEach(x => { x.status = 10; });
                    Context.SaveChanges();
                    var GetCustomerEmail = (from tbcust in Context.tblCustomers
                                            join tbord in Context.tblOrders on tbcust.Id equals tbord.Fk_customer_id
                                            join tbAddressbook in Context.tblAddressBooks on tbcust.Id equals tbAddressbook.Fk_customer_Id
                                            select new { tbcust.email, tbAddressbook.first_name, tbAddressbook.is_default, tbAddressbook.last_name, tbord.id }).Where(x => x.id == OrderID && x.is_default == true).FirstOrDefault();
                    // var GetCustomerEmail = (select new { tbcust.email, tbord.id }).Where(x => x.id == OrderID).FirstOrDefault();
                    if (GetCustomerEmail != null)
                    {
                        string subject = "chineseaddress - Order Created!";
                        clsEmailTemplateService clscommn = new clsEmailTemplateService();
                        Hashtable htTemplate = new Hashtable();
                        htTemplate.Add("#header#", "chineseaddress - Order Confirm");
                        htTemplate.Add("#FirstName#", GetCustomerEmail.first_name);
                        htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
                        htTemplate.Add("#order_no#", UpdateOrdertblstatus[0].reference_no);
                        htTemplate.Add("#shipping_method#", UpdateOrdertblstatus[0].carrier);
                        htTemplate.Add("#status#", "Queue");
                        string mailbody = (new clsEmailTemplateService()).GetEmailTemplate("ordercreate", htTemplate);
                        (new clsSendEmailService()).SendEmail(new[] { GetCustomerEmail.email }, null, null, mailbody, subject);
                    }
                    return message = SuccessMsg("Order has been created successfully");
                }
                else
                {
                    return message = ErrorMsg("One of your shipment already exists in another order ");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}