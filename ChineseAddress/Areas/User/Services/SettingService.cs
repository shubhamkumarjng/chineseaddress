using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.IServices;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.Services;
using Microsoft.Practices.Unity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _ChineseAddress.com.Areas.User.Services
{
    public class SettingService : Admin.Models.clsCommon, ISettingService<tblAddressBook, tblCustomer, tblCreditCard, tblPlan, PlansOptionModel, tblRewardPoint, ViewCustomerPaymentModel, tblCreditCardViewModel, tblCustomerPlanLinking>
    {
        #region Properties

        public bool is_Success { get; set; }

        #endregion Properties

        private IAuthorizeNetService repo;

        public SettingService(AuthorizeNetService repos)
        {
            repo = repos;
        }

        [Dependency]
        public Context4InshipEntities Context { get; set; }

        public string CreateUpdateNewAddress(tblAddressBook tbladdressbook)
        {
            try
            {
                if (tbladdressbook.Id != 0)
                {
                    var tbl = Context.tblAddressBooks.AsQueryable().Where(x => x.Id == tbladdressbook.Id).FirstOrDefault();
                    tbl.Id = tbladdressbook.Id;
                    tbl.first_name = tbladdressbook.first_name;
                    tbl.last_name = tbladdressbook.last_name;
                    tbl.address1 = tbladdressbook.address1;
                    tbl.address2 = tbladdressbook.address2;
                    tbl.address_name = tbladdressbook.address_name;
                    tbl.company = tbladdressbook.company;
                    tbl.country_code = tbladdressbook.country_code;
                    tbl.city = tbladdressbook.city;
                    tbl.state = tbladdressbook.state;
                    tbl.mobile1 = tbladdressbook.mobile1;
                    tbl.mobile2 = tbladdressbook.mobile2;
                    tbl.post_code = tbladdressbook.post_code;
                    tbl.tax_id = tbladdressbook.tax_id;
                    Context.Configuration.ValidateOnSaveEnabled = false;
                    Context.Entry(tbl).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();
                    return SuccessMsg("Your address has been updated successfully");
                }
                else
                {
                    tbladdressbook.created_on = DateTime.Now;
                    Context.tblAddressBooks.Add(tbladdressbook);
                    Context.SaveChanges();
                    return SuccessMsg("Your address has been saved successfully");
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public List<tblCreditCardViewModel> GetCreaditCards()
        {
            int id = (int)HttpContext.Current.Session["UserId"];
            return (from tcc in Context.tblCreditCards
                    join tb in Context.tblBillingAddresses
                    on tcc.Fk_billing_address_id equals tb.Id
                    where tcc.Fk_customer_Id == id
                    && tcc.card_auth_status == "I00001"
                    select new tblCreditCardViewModel
                    {
                        Id = tcc.Id,
                        is_default = tcc.is_default,
                        card_type = tcc.card_type,
                        card_num = tcc.card_num,
                        card_expiry = tcc.card_expiry,
                        Name = tb.first_name + " " + tb.last_name
                    }
                 ).ToList();
        }

        public ViewCustomerPaymentModel GetCreaditCardsById(int id)
        {
            try
            {
                return (from cred in Context.tblCreditCards
                        join bill in Context.tblBillingAddresses
                        on cred.Fk_billing_address_id equals bill.Id
                        where (cred.Id == id)
                        select new ViewCustomerPaymentModel() { Id = cred.Id, billingId = bill.Id, first_name = bill.first_name, last_name = bill.last_name, email = bill.email, mobile = bill.mobile, address = bill.address, Country = bill.country_code, state = bill.state, city = bill.city, post_code = bill.post_code }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeDefaultCreditCard(int CreditCardId)
        {
            try
            {
                int id = (int)HttpContext.Current.Session["UserId"];
                //make all credit card not default
                var cCards = Context.tblCreditCards.AsQueryable().Where(cc => cc.Fk_customer_Id == id).ToList();
                if (cCards != null || cCards.Any())
                {
                    cCards.ForEach(x => x.is_default = false);
                    cCards.Where(x => x.Id == CreditCardId).Single().is_default = true;
                    Context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ViewCustomerPaymentModel GetAddressBookDataByFkId()
        {
            try
            {
                int id = (int)HttpContext.Current.Session["UserId"];
                var CustomerEmail = Context.tblCustomers.Where(x => x.Id == id).Select(x => x.email).FirstOrDefault();
                var sameshipment = Context.tblAddressBooks.AsQueryable().Where(x => x.Fk_customer_Id == id && x.is_default == true).FirstOrDefault();
                ViewCustomerPaymentModel objviewmodel = new ViewCustomerPaymentModel();
                objviewmodel.Id = sameshipment.Id;
                objviewmodel.Fk_customer_id = sameshipment.Fk_customer_Id;
                objviewmodel.address = sameshipment.address1 + " " + sameshipment.address2;
                objviewmodel.first_name = sameshipment.first_name;
                objviewmodel.last_name = sameshipment.last_name;
                objviewmodel.Country = sameshipment.country_code;
                objviewmodel.state = sameshipment.state;
                objviewmodel.city = sameshipment.city;
                objviewmodel.post_code = sameshipment.post_code;
                objviewmodel.mobile = sameshipment.mobile1;
                objviewmodel.email = CustomerEmail;
                return objviewmodel;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public IPagedList<tblAddressBook> GetAddressBookData(int? Id, bool Is_Default, int pageNumber = 1)
        {
            try
            {
                int id = (int)HttpContext.Current.Session["UserId"];
                return Context.tblAddressBooks.AsQueryable().Where(x => x.Fk_customer_Id == id && x.is_default == Is_Default).ToList().ToPagedList(pageNumber, MvcApplication.global_Page_Size);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public tblAddressBook GetAddressBookData(int id)
        {
            try
            {
                return Context.tblAddressBooks.AsQueryable().Where(x => x.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public tblCustomer GetTblCustomerData()
        {
            try
            {
                int id = (int)HttpContext.Current.Session["UserId"];
                return Context.tblCustomers.AsQueryable().Where(x => x.Id == id).OrderByDescending(x => x.created_on).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteSingleAddressByID(int id)
        {
            var itemToRemove = Context.tblAddressBooks.SingleOrDefault(x => x.Id == id);

            try
            {
                if (itemToRemove != null)
                {
                    Context.tblAddressBooks.Remove(itemToRemove);
                    Context.SaveChanges();
                }
                return SuccessMsg("Your address has been deleted successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public string ChangeUserPassword(ChangePasswordModel changepasswordmodel)
        {
            try
            {
                tblCustomer cust = new Repository.tblCustomer();
                int customerId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                var tbl = Context.tblCustomers.AsQueryable().Where(x => x.Id == customerId).FirstOrDefault();
                if (changepasswordmodel.confirmpassword == changepasswordmodel.newpassword)
                {
                    tbl.password = changepasswordmodel.confirmpassword;
                    Context.Entry(tbl).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();
                    return SuccessMsg("Password has been changed successfully");
                }
                else
                {
                    return ErrorMsg("Confirm Password and New Password do not match");
                }
                // return SuccessMsg("Password has been changed successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public bool CheckDefaultAddress(string address, string adressBookId)
        {
            try
            {
                if (Context == null)
                    Context = new Context4InshipEntities();
                int id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                int adressBook_Id = Convert.ToInt32(((adressBookId == "" || adressBookId == "SingleAddress" || adressBookId == "Setting") ? "0" : adressBookId));
                List<tblAddressBook> e = Context.tblAddressBooks.AsQueryable().Where(x => x.address_name == address && x.Fk_customer_Id == id && x.Id != (adressBook_Id == 0 ? 0 : adressBook_Id)).ToList();
                if (e.Any())
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckOldPasswordandNewPassword(string Pass)
        {
            try
            {
                int id = (int)HttpContext.Current.Session["UserId"];
                if (Context == null)
                {
                    Context = new Context4InshipEntities();
                }
                var tbl = Context.tblCustomers.AsQueryable().Where(x => x.password == Pass && x.Id == id).FirstOrDefault();

                if (tbl == null)
                {
                    return true;
                }
                else
                {
                    if (tbl.password == Pass)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //  return false;
            }
        }

        public bool CheckPassword(string pass)
        {
            try
            {
                int id = (int)HttpContext.Current.Session["UserId"];
                if (Context == null)
                {
                    Context = new Context4InshipEntities();
                }
                var tbl = Context.tblCustomers.AsQueryable().Where(x => x.password == pass && x.Id == id).FirstOrDefault();

                if (tbl != null)
                {
                    if (tbl.password == pass)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //  return false;
            }
        }

        public string ChangeUserEmail(ChangeEmailId changeemailId)
        {
            try
            {
                tblCustomer cust = new Repository.tblCustomer();
                var tbl = Context.tblCustomers.AsQueryable().Where(x => x.Id == Convert.ToInt32(HttpContext.Current.Session["UserId"])).FirstOrDefault();
                if (changeemailId.confirmemail == changeemailId.newemail)
                {
                    tbl.email = changeemailId.confirmemail;
                    Context.Entry(tbl).State = System.Data.Entity.EntityState.Modified;
                }
                Context.SaveChanges();
                return SuccessMsg("Email id has been changed successfully");
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
        }

        public string UpdateBillingAddressById(ViewCustomerPaymentModel viewcustomerpaymentmodel)
        {
            try
            {
                tblBillingAddress tblbillingaddress = Context.tblBillingAddresses.AsQueryable().Where(x => x.Id == viewcustomerpaymentmodel.billingId).FirstOrDefault();
                tblbillingaddress.Id = viewcustomerpaymentmodel.billingId;
                tblbillingaddress.first_name = viewcustomerpaymentmodel.first_name;
                tblbillingaddress.last_name = viewcustomerpaymentmodel.last_name;
                tblbillingaddress.email = viewcustomerpaymentmodel.email;
                tblbillingaddress.mobile = viewcustomerpaymentmodel.mobile;
                tblbillingaddress.address = viewcustomerpaymentmodel.address;
                tblbillingaddress.country_code = viewcustomerpaymentmodel.Country;
                tblbillingaddress.state = viewcustomerpaymentmodel.state;
                tblbillingaddress.city = viewcustomerpaymentmodel.city;
                tblbillingaddress.post_code = viewcustomerpaymentmodel.post_code;
                //tblbillingaddress.created_on = DateTime.Now;
                Context.Entry(tblbillingaddress).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
                return SuccessMsg(" BillingAddress has baan Updated sucessfully");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CreateUpdatePaymentMethod(ViewCustomerPaymentModel viewcustomerpaymentmodel)
        {
            try
            {
                #region Crate BillingAddress object

                tblBillingAddress tblbillingaddress = new tblBillingAddress();
                tblbillingaddress.first_name = viewcustomerpaymentmodel.first_name;
                tblbillingaddress.last_name = viewcustomerpaymentmodel.last_name;
                tblbillingaddress.email = viewcustomerpaymentmodel.email;
                tblbillingaddress.mobile = viewcustomerpaymentmodel.mobile;
                tblbillingaddress.address = viewcustomerpaymentmodel.address;
                tblbillingaddress.country_code = viewcustomerpaymentmodel.Country;
                tblbillingaddress.state = viewcustomerpaymentmodel.state;
                tblbillingaddress.city = viewcustomerpaymentmodel.city;
                tblbillingaddress.post_code = viewcustomerpaymentmodel.post_code;
                tblbillingaddress.created_on = DateTime.Now;

                #endregion Crate BillingAddress object

                #region Get Customer Detail

                int userId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                var tblcustomer = Context.tblCustomers.AsQueryable().Where(X => X.Id == userId).FirstOrDefault();

                #endregion Get Customer Detail

                #region Generate AuthoizeNet Request

                repo._CustCardAndBilling = viewcustomerpaymentmodel;
                repo._billingAddress = tblbillingaddress;
                repo._CustomerDetail = tblcustomer;
                if (repo.CreateCustomerProfile())
                {
                    is_Success = true;
                    return SuccessMsg("Credit card has been successfully added.");
                }
                else
                {
                    is_Success = false;
                    return repo._msg;
                }

                #endregion Generate AuthoizeNet Request

                #region Commented

                //if (viewcustomerpaymentmodel != null)
                //{
                //    string lastfourdigitsofcard = viewcustomerpaymentmodel.CreditCard.Substring(viewcustomerpaymentmodel.CreditCard.Length - 4);
                //    var exitscardnumber = Context.tblCreditCards.AsQueryable().Where(x => x.card_num == lastfourdigitsofcard).FirstOrDefault();
                //    if (exitscardnumber != null)
                //    {
                //        return ErrorMsg("Card already exits");
                //    }
                //    else
                //    {
                //        if (viewcustomerpaymentmodel.billingId != 0)
                //        {
                //            var tbl = Context.tblBillingAddresses.Where(x => x.Id == viewcustomerpaymentmodel.billingId).FirstOrDefault();
                //            tbl.Id = viewcustomerpaymentmodel.billingId;
                //            tbl.first_name = viewcustomerpaymentmodel.first_name;
                //            tbl.last_name = viewcustomerpaymentmodel.last_name;
                //            tbl.email = viewcustomerpaymentmodel.email;
                //            tbl.mobile = viewcustomerpaymentmodel.mobile;
                //            tbl.address = viewcustomerpaymentmodel.address;
                //            tbl.country_code = viewcustomerpaymentmodel.Country;
                //            tbl.state = viewcustomerpaymentmodel.state;
                //            tbl.city = viewcustomerpaymentmodel.city;
                //            tbl.post_code = viewcustomerpaymentmodel.post_code;
                //            Context.Entry(tbl).State = System.Data.Entity.EntityState.Modified;
                //            Context.SaveChanges();
                //            repo.Fk_billing_address_id = Convert.ToInt32(tbl.Id);
                //            return SuccessMsg("Updated successfully");
                //        }
                //        else
                //        {
                //            tblbillingaddress.first_name = viewcustomerpaymentmodel.first_name;
                //            tblbillingaddress.last_name = viewcustomerpaymentmodel.last_name;
                //            tblbillingaddress.email = viewcustomerpaymentmodel.email;
                //            tblbillingaddress.mobile = viewcustomerpaymentmodel.mobile;
                //            tblbillingaddress.address = viewcustomerpaymentmodel.address;
                //            tblbillingaddress.country_code = viewcustomerpaymentmodel.Country;
                //            tblbillingaddress.state = viewcustomerpaymentmodel.state;
                //            tblbillingaddress.city = viewcustomerpaymentmodel.city;
                //            tblbillingaddress.post_code = viewcustomerpaymentmodel.post_code;
                //            tblbillingaddress.created_on = DateTime.Now;
                //            Context.tblBillingAddresses.Add(tblbillingaddress);//remove this from and save from AuthoizeNetService
                //            Context.SaveChanges();//remove
                //            repo.Fk_billing_address_id = Convert.ToInt32(tblbillingaddress.Id);
                //        }

                //        //var tblcustomer = Context.tblCustomers.AsQueryable().Where(X => X.Id == userId).FirstOrDefault();
                //        var tblcredit = Context.tblCreditCards.AsQueryable().Where(X => X.Fk_customer_Id == userId).FirstOrDefault();
                //        repo._CustomerDetail = tblcustomer;
                //        repo._CustCardAndBilling = viewcustomerpaymentmodel;
                //        if (tblcredit.cust_payment_profile_id != null)
                //        {
                //            repo._CustomerPaymentProfileId = tblcredit.cust_payment_profile_id;
                //        }
                //        if (tblcredit.cust_shipping_profile_id != null)
                //        {
                //            repo._CustomerShipppingProfileId = tblcredit.cust_shipping_profile_id;
                //        }
                //        if (tblcustomer.cust_profile_id != null)
                //        {
                //            repo._CustomerProfileId = tblcustomer.cust_profile_id;
                //            //repo.CreateCustomerPaymentProfile();
                //        }
                //        else
                //        {
                //        }
                //    }
                //}
                //return SuccessMsg("Saved successfully");

                #endregion Commented
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string insertrewardpoint(decimal balance)
        {
            try
            {
                if (balance != 0)
                {
                    tblRewardAmount objreward = new tblRewardAmount();
                    tblRewardPoint objrewardpoint = new tblRewardPoint();
                    int Id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                    objreward.Fk_CustomerId = Id;
                    objreward.credit = Convert.ToDecimal(balance);
                    objreward.debit = Convert.ToDecimal(0);
                    objreward.created_on = DateTime.Now;
                    Context.tblRewardAmounts.Add(objreward);
                    Context.SaveChanges();
                    Context.tblRewardPoints.AsQueryable().Where(x => x.Fk_CustomerId == Id).ToList().ForEach(x => x.is_converted = true);
                    Context.SaveChanges();
                }
                return SuccessMsg("Reward points has been converted successfully");
            }
            catch (Exception ex)
            {
                throw ex;
#pragma warning disable CS0162 // Unreachable code detected
                return ErrorMsg(ex.Message);
#pragma warning restore CS0162 // Unreachable code detected
            }
        }

        public IPagedList<tblRewardPoint> getRewardPointsDetail(int pageNumber)
        {
            try
            {
                int cust_id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                return Context.tblRewardPoints.AsQueryable().Where(x => x.Fk_CustomerId == cust_id).OrderByDescending(x => x.created_on).AsEnumerable().ToPagedList(pageNumber, MvcApplication.global_Page_Size);
            }
            catch (Exception EX)
            {
                throw EX;
            }
        }

        public List<tblPlan> Plans()
        {
            try
            {
                var PlanList = Context.tblPlans.OrderBy(x => x.display_order).ToList();
                return PlanList.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PlansOptionModel> OptionPlan()
        {
            try
            {
                var OptionPlan = (from p in Context.tblPlans join pkoptionlink in Context.tblPlanOptionLinkings on p.Id equals pkoptionlink.Fk_plan_id join pkoption in Context.tblPackagingOptions on pkoptionlink.Fk_packaging_option_id equals pkoption.Id select new { pkoptionlink.price, pkoption.title, pkoption.description, p.Id, pkoption.is_signup, pkoption.is_shipment }).Where(x => x.is_signup == true).ToList();
                List<PlansOptionModel> objPlansOptionModel = new List<PlansOptionModel>();
                foreach (var item in OptionPlan.ToList())
                {
                    objPlansOptionModel.Add(new PlansOptionModel { title = item.title, price = Convert.ToDecimal(item.price), description = item.description, p_id = item.Id, IsSignUp = Convert.ToBoolean(item.is_signup) });
                }
                return objPlansOptionModel.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCustomerPlanLinking PlanLinkingId(int Id)
        {
            try
            {
                var GetCutomerPlanRecord = Context.tblCustomerPlanLinkings.AsQueryable().Where(x => x.Fk_Customer_Id == Id).OrderByDescending(x => x.Id).Select(x => x).FirstOrDefault();
                return GetCutomerPlanRecord;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}