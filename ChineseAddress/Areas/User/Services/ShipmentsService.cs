using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _ChineseAddress.com.Areas.User.Services
{
    public class ShipmentsService : clsCommon, IShipmentService<tblReceivedShipment, tblOrder, UserViewCustomerModel, UserPaymentModel, ViewRecivedShipmentModel, tblAddressBook, ViewPackagingOptionsModel, ViewOrderModel, ShipmentMethodPackage, PacakageOptionArray>
    {
        [Dependency]
        public Context4InshipEntities Context { get; set; }

        public List<tblReceivedShipment> GetRecievedShipment(int ID)
        {
            try
            {
                List<tblReceivedShipment> TblRecivedShipment = Context.tblReceivedShipments.AsQueryable().OrderByDescending(x => x.id).Where(x => x.Fk_Customer_Id == ID).Take(10).ToList();
                return TblRecivedShipment.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblReceivedShipment> GetRecievedShipmentRecords(int ID)
        {
            try
            {
                List<tblReceivedShipment> TblRecivedShipment = Context.tblReceivedShipments.AsQueryable().OrderByDescending(x => x.id).Where(x => x.Fk_Customer_Id == ID).ToList();
                return TblRecivedShipment.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblOrder> GetOrderRecordList(int ID)
        {
            try
            {
                List<tblOrder> OrderList = Context.tblOrders.AsQueryable().Where(x => x.Fk_customer_id == ID && x.status != (int)enumorder.isInctive).OrderByDescending(x => x.id).Take(3).ToList();
                return OrderList.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserViewCustomerModel GetCustomerPlans(int ID)
        {
            try
            {
                UserViewCustomerModel objUserViewCustomerModel = new UserViewCustomerModel();
                var GetCutomerPlanRecord = Context.tblCustomerPlanLinkings.AsQueryable().Where(x => x.Fk_Customer_Id == ID).Select(x => x).FirstOrDefault();
                var GetAddresbookRecord = Context.tblAddressBooks.AsQueryable().Where(x => x.Fk_customer_Id == ID).Select(x => x).FirstOrDefault();
                var GetCustomerRecord = Context.tblCustomers.AsQueryable().Where(x => x.Id == ID).Select(x => x).FirstOrDefault();
                decimal GetPansPrice = Convert.ToDecimal(Context.tblPlans.Max(x => x.price));
                if (GetPansPrice > GetCutomerPlanRecord.plan_amount)
                {
                    objUserViewCustomerModel.PlanPrice = GetPansPrice;
                }
                objUserViewCustomerModel.PlanTitle = GetCutomerPlanRecord.plan_title;
                objUserViewCustomerModel.Email = GetCustomerRecord.email;
                objUserViewCustomerModel.FirstName = GetAddresbookRecord.first_name;
                objUserViewCustomerModel.LastName = GetAddresbookRecord.last_name;
                return objUserViewCustomerModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal yourpoints()
        {
            try
            {
                int id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                return ((Context.tblRewardPoints.AsQueryable().Where(x => x.is_converted == false && x.Fk_CustomerId == id).Sum(X => X.reward_points)) ?? 0.0M);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal yourBalance()
        {
            try
            {
                decimal balance = 0;
                int id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                var balanceList = Context.tblRewardAmounts.AsQueryable().Where(x => x.Fk_CustomerId == id).ToList();
                if (balanceList != null)
                {
                    balance = (balanceList.Sum(x => x.credit) ?? 0.0m) - (balanceList.Sum(x => x.debit) ?? 0.0m);
                }
                return balance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserPaymentModel> GetCustomerPayment(int ID)
        {
            var plan = Context.tblCustomerPlanLinkings.AsQueryable().Where(x => x.Fk_Customer_Id == ID).FirstOrDefault();
            var query_join = (from ti in Context.tblInvoices
                              join to in Context.tblOrders on ti.Id equals to.Fk_invoice_id into tito
                              from to in tito.DefaultIfEmpty()
                              orderby ti.invoice_date descending
                              where ti.Fk_customer_id == ID
                              select new UserPaymentModel()
                              {
                                  PaymentDate = ti.invoice_date,
                                  Method = (ti.payment_method ?? "Card"),
                                  Amount = ti.payment_amount,
                                  Status = ti.paid_status,
                                  InvNo = ti.invoice_number,
                                  FK_Customer_Id = ID,
                                  InvoiceType = ti.invoice_type,
                                  PlanType = ti.plan_type,
                                  OrderReferenceNumber = (ti.invoice_type == "order" ? to.reference_no : plan.plan_title)
                              }).AsEnumerable().Take(5).ToList();
            return query_join;
        }

        public List<ViewRecivedShipmentModel> GetReciveShipmentDetails(int ID, bool isOrderCreated = false)
        {
            try
            {
                Context4InshipEntities Context = new Context4InshipEntities();
                List<ViewRecivedShipmentModel> objReceivedshiment = new List<ViewRecivedShipmentModel>();
                clsCommonServiceRoot objclsCommonServiceRoot = new clsCommonServiceRoot();
                ViewRecivedShipmentModel objrecshipment = new ViewRecivedShipmentModel();
                var Models = Context.tblReceivedShipments.Where(x => x.Fk_Customer_Id == ID).ToList();
                var Model = (from rec in Context.tblReceivedShipments
                             join ord in Context.tblOrders on (rec.Fk_order_id ?? 0) equals ord.id
                             into ro
                             from ord in ro.DefaultIfEmpty()
                             where rec.Fk_Customer_Id == ID
                            && (isOrderCreated ? rec.status == 10 : rec.status != 10)
                             select new
                             {
                                 id = rec.id,
                                 rec.correct_price_status,
                                 rec.sender,
                                 rec.received_date,
                                 rec.length,
                                 rec.weight,
                                 rec.width,
                                 rec.height,
                                 rec.box_condition,
                                 rec.staff_comments,
                                 rec.status,
                                 rec.tracking,
                                 rec.user_notes,
                                 Fk_order_id = (rec.Fk_order_id ?? 0),
                                 rec.Fk_Customer_Id,
                                 rec.is_requested_picture,
                                 reference_no = (ord.reference_no ?? "")
                             }).ToList();
                foreach (var item in Model)
                {

                    int TotalDays = objclsCommonServiceRoot.GetFreeStorageDays(ID, item.received_date);
                    objrecshipment.RecivedShipment_ImagePath = Context.tblShipmentImages.Where(x => x.Fk_shipment_id == item.id).Select(x => x.file_path).ToList<dynamic>();

                    objReceivedshiment.Add(new ViewRecivedShipmentModel() { orderreferenceno = item.reference_no, correctpricestatus = Convert.ToInt32(item.correct_price_status), sender = item.sender, received_date = item.received_date, length = item.length, weight = item.weight, width = item.width, height = item.height, box_condition = item.box_condition, staff_comments = item.staff_comments, status = item.status, tracking = item.tracking, ID = item.id, StorageDays = TotalDays, RecivedShipment_ImagePath = objrecshipment.RecivedShipment_ImagePath, user_notes = (item.user_notes ?? ""), IsUnderSomeProcess = (((item.correct_price_status ?? 0) == 1 || (item.is_requested_picture ?? false) == true) ? true : false) });
                }
                return objReceivedshiment.ToList();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public string GetOrderIdByreferenceNo(string reference)
        {
            tblOrder OrderReferenceNo = Context.tblOrders.Where(x => x.reference_no == reference).FirstOrDefault();
            return Cryptography.Encrypt(Convert.ToString(OrderReferenceNo.id));
        }

        public List<ViewRecivedShipmentModel> GetShipmentDetails()
        {
            try
            {
                int custID = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                List<ViewRecivedShipmentModel> objReceivedshiment = new List<ViewRecivedShipmentModel>();
                objReceivedshiment = (from tblrec in Context.tblReceivedShipments
                                      join tblship in Context.tblShipmentDetails
                                      on tblrec.id equals tblship.Fk_shipment_id
                                      // join tblord in Context.tblOrders on tblrec.Fk_order_id equals tblord.id
                                      where (tblrec.Fk_Customer_Id == custID)
                                      select new ViewRecivedShipmentModel() { description = tblship.description, purchase_price = tblship.purchase_price, quantity = tblship.quantity, Fk_shipment_id = tblship.Fk_shipment_id, user_notes = tblrec.user_notes ?? "" }).ToList();
                foreach (var item in objReceivedshiment)
                {
                    var GetOrderRefernceNumber = (from tbord in Context.tblOrders join tbrecship in Context.tblReceivedShipments on tbord.id equals tbrecship.Fk_order_id select new { tbrecship.id, tbord.reference_no }).Where(x => x.id == item.Fk_shipment_id).FirstOrDefault();
                    if (GetOrderRefernceNumber != null)
                        item.orderreferenceno = GetOrderRefernceNumber.reference_no;
                }

                return objReceivedshiment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblAddressBook GettblAdressData(int ID)
        {
            try
            {
                tblAddressBook objAddress = new tblAddressBook();
                objAddress.Id = ID;
                int custId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                tblAddressBook tblobjAddress = Context.tblAddressBooks.AsQueryable().Where(x => x.Fk_customer_Id == custId && x.Id == ID).FirstOrDefault();
                var tblAddressbooktbldata = (from tbladdress in Context.tblAddressBooks join tblcontry in Context.tblCountryMasters on tbladdress.country_code equals tblcontry.country_code select new { tblcontry.country_name, tbladdress.country_code }).Where(x => x.country_code == tblobjAddress.country_code).FirstOrDefault();
                tblobjAddress.country_code = tblAddressbooktbldata.country_name;
                return tblobjAddress;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ViewPackagingOptionsModel> GetoptionplanPlandata(int ID)
        {
            return
 (from plan in Context.tblPlans
  join customerplan in Context.tblCustomerPlanLinkings on plan.title equals customerplan.plan_title
  join planlink in Context.tblPlanOptionLinkings on plan.Id equals planlink.Fk_plan_id
  join planoption in Context.tblPackagingOptions on planlink.Fk_packaging_option_id equals planoption.Id
  where customerplan.Fk_Customer_Id == ID && planoption.is_shipment == true
  select new ViewPackagingOptionsModel() { title = planoption.title, description = planoption.description, price = planlink.price }).ToList();
        }

        public string CancelRecShipment(int ID)
        {
            try
            {
                tblReceivedShipment objRec = new tblReceivedShipment();
                objRec.id = ID;
                tblReceivedShipment GetRecord = Context.tblReceivedShipments.Where(x => x.id == objRec.id).Select(x => x).FirstOrDefault();
                GetRecord.status = 7;
                Context.Entry(GetRecord).State = EntityState.Modified;
                Context.SaveChanges();
                var GetCustomerEmail = (from tbcust in Context.tblCustomers
                                        join tbRec in Context.tblReceivedShipments on tbcust.Id equals tbRec.Fk_Customer_Id
                                        join tbAdreesbook in Context.tblAddressBooks on tbcust.Id equals tbAdreesbook.Fk_customer_Id
                                        select new { tbcust.email, tbAdreesbook.first_name, tbAdreesbook.is_default, tbAdreesbook.last_name, tbRec.id, tbRec.tracking }).Where(x => x.id == ID && x.is_default == true).FirstOrDefault();
                if (GetCustomerEmail != null)
                {
                    string subject = "chineseaddress - Abandon/Cancel!";
                    Hashtable htTemplate = new Hashtable();
                    htTemplate.Add("#FirstName", GetCustomerEmail.first_name);
                    htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
                    htTemplate.Add("#trackingno#", GetCustomerEmail.tracking);
                    string mailbody = (new clsEmailTemplateService()).GetEmailTemplate("orderabandon", htTemplate);
                    (new clsSendEmailService()).SendEmail(new[] { GetCustomerEmail.email }, null, null, mailbody, subject);
                }
                return "Shipment  has been  canceled successfully";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetRequestPicture(int ID)
        {
            try
            {
                tblReceivedShipment objRec = new tblReceivedShipment();
                objRec.id = ID;
                tblReceivedShipment GetRecord = Context.tblReceivedShipments.Where(x => x.id == objRec.id).Select(x => x).FirstOrDefault();
                if (GetRecord != null)
                {
                    GetRecord.is_requested_picture = true;
                    Context.Entry(GetRecord).State = EntityState.Modified;
                    Context.SaveChanges();
                }
                return "Request for shipments picture  has been  submitted successfully ";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetReturnPanding(int ID)
        {
            try
            {
                tblReceivedShipment objRec = new tblReceivedShipment();
                objRec.id = ID;
                tblReceivedShipment GetRecord = Context.tblReceivedShipments.Where(x => x.id == objRec.id).Select(x => x).FirstOrDefault();
                if (GetRecord != null)
                {
                    GetRecord.status = 8;
                    Context.Entry(GetRecord).State = EntityState.Modified;
                    Context.SaveChanges();
                }
                return "Request for return packages  has been  submited  successfully";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SaveInvoicefilepath(tblReceivedShipment objrecivedship)
        {
            try
            {
                var SaveFilePath = Context.tblReceivedShipments.Where(x => x.id == objrecivedship.id).Select(x => x).FirstOrDefault();
                if (SaveFilePath != null)
                {
                    SaveFilePath.correct_price_file_path = objrecivedship.correct_price_file_path;
                    SaveFilePath.correct_price_status = 1;
                    Context.Entry(SaveFilePath).State = EntityState.Modified;
                    Context.SaveChanges();
                }
                return "Invoice has been uploaded successfully";
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public string ReturnPackageData(ViewRecivedShipmentModel objViewRecivedShipmentModel)
        {
            try
            {
                tblShipmentReturnDetail shipdetail = Context.tblShipmentReturnDetails.Where(x => x.Fk_shipment_id == objViewRecivedShipmentModel.Fk_shipment_id).Select(x => x).FirstOrDefault();
                tblShipmentReturnDetail objtblShipmentReturnDetail = new tblShipmentReturnDetail();
                if (shipdetail == null)
                {
                    if (objViewRecivedShipmentModel.ReturnpackagefileUpload != null)
                    {
                        objtblShipmentReturnDetail.Fk_shipment_id = objViewRecivedShipmentModel.Fk_shipment_id;
                        objtblShipmentReturnDetail.prepaid_label_filename = objViewRecivedShipmentModel.ReturnpackagefileUpload;
                        Context.tblShipmentReturnDetails.Add(objtblShipmentReturnDetail);
                        Context.SaveChanges();
                        //Update recivedshipmenttbl
                        var tblRecivedShipmentData = Context.tblReceivedShipments.Where(x => x.id == objViewRecivedShipmentModel.Fk_shipment_id).FirstOrDefault();
                        tblRecivedShipmentData.status = Convert.ToByte(enumReceivShipmentStatus.Return_Pending);
                        Context.Entry(tblRecivedShipmentData).State = EntityState.Modified;
                        Context.SaveChanges();
                    }
                    else
                    {
                        objtblShipmentReturnDetail.Fk_shipment_id = objViewRecivedShipmentModel.Fk_shipment_id;
                        objtblShipmentReturnDetail.first_name = objViewRecivedShipmentModel.FirstName;
                        objtblShipmentReturnDetail.last_name = objViewRecivedShipmentModel.LastName;
                        objtblShipmentReturnDetail.address1 = objViewRecivedShipmentModel.Address1;
                        objtblShipmentReturnDetail.address2 = objViewRecivedShipmentModel.Address1;
                        objtblShipmentReturnDetail.city = objViewRecivedShipmentModel.Address2;
                        objtblShipmentReturnDetail.state = objViewRecivedShipmentModel.State;
                        objtblShipmentReturnDetail.postal_code = objViewRecivedShipmentModel.Zip;
                        objtblShipmentReturnDetail.mobile = objViewRecivedShipmentModel.Phone;
                        objtblShipmentReturnDetail.rma = objViewRecivedShipmentModel.Rma;
                        Context.tblShipmentReturnDetails.Add(objtblShipmentReturnDetail);
                        Context.SaveChanges();
                        var tblRecivedShipmentData = Context.tblReceivedShipments.Where(x => x.id == objViewRecivedShipmentModel.Fk_shipment_id).FirstOrDefault();
                        tblRecivedShipmentData.status = Convert.ToByte(enumReceivShipmentStatus.Return_Pending); ;
                        Context.Entry(tblRecivedShipmentData).State = EntityState.Modified;
                        Context.SaveChanges();
                    }
                    return "Return  package(s) request has been  submited  successfully ";
                }
                else
                {
                    if (objViewRecivedShipmentModel.ReturnpackagefileUpload != null)
                    {
                        shipdetail.Fk_shipment_id = objViewRecivedShipmentModel.Fk_shipment_id;
                        shipdetail.prepaid_label_filename = objViewRecivedShipmentModel.ReturnpackagefileUpload;
                        Context.Entry(shipdetail).State = EntityState.Modified;
                        Context.SaveChanges();
                        var tblRecivedShipmentData = Context.tblReceivedShipments.Where(x => x.id == objViewRecivedShipmentModel.Fk_shipment_id).FirstOrDefault();
                        tblRecivedShipmentData.status = Convert.ToByte(enumReceivShipmentStatus.Return_Pending); ;
                        Context.Entry(tblRecivedShipmentData).State = EntityState.Modified;
                        Context.SaveChanges();
                    }
                    else
                    {
                        objtblShipmentReturnDetail.Fk_shipment_id = objViewRecivedShipmentModel.Fk_shipment_id;
                        shipdetail.first_name = objViewRecivedShipmentModel.FirstName;
                        shipdetail.last_name = objViewRecivedShipmentModel.LastName;
                        shipdetail.mobile = objViewRecivedShipmentModel.Phone;
                        shipdetail.address1 = objViewRecivedShipmentModel.Address1;
                        shipdetail.address2 = objViewRecivedShipmentModel.Address2;
                        shipdetail.city = objViewRecivedShipmentModel.City;
                        shipdetail.state = objViewRecivedShipmentModel.State;
                        shipdetail.postal_code = objViewRecivedShipmentModel.Zip;
                        shipdetail.mobile = objViewRecivedShipmentModel.Phone;
                        shipdetail.rma = objViewRecivedShipmentModel.Rma;
                        Context.Entry(shipdetail).State = EntityState.Modified;
                        Context.SaveChanges();
                        var tblRecivedShipmentData = Context.tblReceivedShipments.Where(x => x.id == objViewRecivedShipmentModel.Fk_shipment_id).FirstOrDefault();
                        tblRecivedShipmentData.status = Convert.ToByte(enumReceivShipmentStatus.Return_Pending); ;
                        Context.Entry(tblRecivedShipmentData).State = EntityState.Modified;
                        Context.SaveChanges();
                    }
                    return "Return packages  has been submited  successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SaveNotes(tblReceivedShipment objtblReceivedShipment)
        {
            try
            {
                var Reshipmentdata = Context.tblReceivedShipments.Where(x => x.id == objtblReceivedShipment.id).Select(x => x).FirstOrDefault();
                Reshipmentdata.user_notes = objtblReceivedShipment.user_notes;
                Context.Configuration.ValidateOnSaveEnabled = false;
                Context.Entry(Reshipmentdata).State = EntityState.Modified;
                Context.SaveChanges();
                return "Note has been saved successfully";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CreateShipmentOrder(List<ShipmentMethodPackage> objShipmentMethodPackage, List<PacakageOptionArray> objPacakageOptionArray, ViewOrderModel objViewModelOrder, string CouponCode,string Order_Note)
        {
            try
            {
                UserCommanClass objUserCommanClass = new UserCommanClass();
                string[] RecivedshipmentID = objViewModelOrder.ShipmentCheckboxID;
                if (RecivedshipmentID.Length == 0)
                {
                    return "";
                }
                else
                {
                    // Check Create Order//
                    for (int i = 0; i < RecivedshipmentID.Length; i++)
                    {
                        if (RecivedshipmentID[i] != "")
                        {
                            int id = Convert.ToInt32(RecivedshipmentID[i].Replace("id", ""));
                            var GetOrderID = Context.Database.SqlQuery<int>($"select tbord.id from tblOrder tbord inner join tblReceivedShipment tborec on tbord.id=tborec.Fk_order_id where tborec.id={id}").FirstOrDefault<int>();
                            if (GetOrderID != 0)
                                objUserCommanClass.DeleteCreateOrder(Convert.ToInt32(GetOrderID));
                        }
                    }
                    //Use function Add Data TblDeliverAddress
                    ShipmentDeliverdAddress(objViewModelOrder.Fk_Customer_id, objViewModelOrder.Fk_orderAddres_id);
                    //Use function  Add Data tblOrder
                    ShipmentMethodPackage services = objShipmentMethodPackage.FirstOrDefault();
                    int _orderId = CreateOrder(objViewModelOrder, services, CouponCode, Order_Note);
                    //Add Data RecivedShipmentdata//
                    decimal Shipmetpictureprice = 0;
                    tblOrderCharge objtblOrderCharge = new tblOrderCharge();
                    int _count = 1;
                    for (int i = 0; i < RecivedshipmentID.Length; i++)
                    {
                        if (RecivedshipmentID[i] != "")
                        {

                            int id = Convert.ToInt32(RecivedshipmentID[i].Replace("id", ""));
                            // Save FkOrderId in tblReceivedShipment Start//
                            RecivedCheckboxid(objViewModelOrder.Fk_Customer_id, id);
                            // Save FkOrderId in tblReceivedShipment End//
                            //Save Free Storage Days According Charges Start//
                            if (RecivedshipmentID.Length == _count)
                                SaveFreeStorageDays(objViewModelOrder.Fk_Customer_id);
                            //Save Free Storage Days According Charges End//
                            //Shipment picture Charges
                            //var Isrequestedpicture = Context.tblReceivedShipments.Where(x => x.is_requested_picture == true && x.id == id).Select(x => x.is_requested_picture).FirstOrDefault();
                            //Shipment picture Charges by checking images in  images table 
                            int countChargablePictures = Context.tblShipmentImages.Where(x => x.Fk_shipment_id == id && x.IsChargeable == true).Count();
                            if (countChargablePictures > 0)
                            {
                                var GetOrderID = _orderId;
                                var GetPalnoption = Context.Database.SqlQuery<ViewPackagingOptionsModel>("select distinct tblpoptlink.price,tblpacopt.title from tblCustomerPlanLinking tblcustlink inner join tblPlans tbplan on tblcustlink.plan_title = tbplan.title inner join tblReceivedShipment tblrec on tblcustlink.Fk_Customer_id = tblrec.Fk_Customer_Id inner join tblPlanOptionLinking tblpoptlink on tbplan.Id = tblpoptlink.Fk_plan_id inner join tblPackagingOptions tblpacopt on tblpoptlink.Fk_packaging_option_id = tblpacopt.Id where tblrec.Fk_Customer_Id = " + objViewModelOrder.Fk_Customer_id + " and tblpacopt.title ='Shipment Picture'").FirstOrDefault<ViewPackagingOptionsModel>();
                                objtblOrderCharge.charge_name = GetPalnoption.title;
                                Shipmetpictureprice += (decimal)GetPalnoption.price;
                                objtblOrderCharge.sell_price = Shipmetpictureprice;
                                objtblOrderCharge.Fk_order_id = GetOrderID;
                            }

                            _count++;
                        }
                    }
                    if (!string.IsNullOrEmpty(objtblOrderCharge.charge_name))
                    {
                        Context.tblOrderCharges.Add(objtblOrderCharge);
                        Context.SaveChanges();
                    }
                    //insert Smart Packaging Repacking charge shubham

                    if (RecivedshipmentID.Length > 1)
                    {
                        var GetSmartPackagingRepackingCharge = Context.Database.SqlQuery<ViewPackagingOptionsModel>("select distinct tblpoptlink.price,tblpacopt.title from tblCustomerPlanLinking tblcustlink inner join tblPlans tbplan on tblcustlink.plan_title = tbplan.title inner join tblReceivedShipment tblrec on tblcustlink.Fk_Customer_id = tblrec.Fk_Customer_Id inner join tblPlanOptionLinking tblpoptlink on tbplan.Id = tblpoptlink.Fk_plan_id inner join tblPackagingOptions tblpacopt on tblpoptlink.Fk_packaging_option_id = tblpacopt.Id where tblrec.Fk_Customer_Id = " + objViewModelOrder.Fk_Customer_id + " and tblpacopt.title ='SmartPack Repackaging'").FirstOrDefault();
                        tblOrderCharge objOCharges = new tblOrderCharge();
                        objOCharges.Fk_order_id = _orderId;
                        objOCharges.sell_price = GetSmartPackagingRepackingCharge.price;
                        objOCharges.charge_name = GetSmartPackagingRepackingCharge.title;
                        Context.tblOrderCharges.Add(objOCharges);
                        Context.SaveChanges();
                    }
                    //End  Smart Packaging Repacking charge shubham
                    //Use function  Add Data tblorderCahrges
                    OrderCharges(objShipmentMethodPackage, objPacakageOptionArray, objViewModelOrder, RecivedshipmentID);
                    //Function to add order rewards points of user
                    //reward Points start Below function transfer After order Payment Done
                    // AddOrderRewardPoints();
                    //reward Points End
                    // Sum of Sell price column ordercharges tbl
                    var Order_id = Context.tblOrders.OrderByDescending(x => x.id).Select(x => x.id).FirstOrDefault();
                    var Sumbuycolumn = Context.tblOrderCharges.Where(x => x.Fk_order_id == Order_id).Sum(x => x.sell_price);
                    if (Sumbuycolumn != null)
                    {
                        var paybillAmount = Context.tblOrders.OrderByDescending(x => x.id).Select(x => x).FirstOrDefault();
                        paybillAmount.payable_amount = Sumbuycolumn;
                        paybillAmount.is_urgent = (objPacakageOptionArray.Where(c => string.Equals(c.PackageOptionTitle, "Urgent processing", StringComparison.CurrentCultureIgnoreCase)).Count() == 0 ? false : true);
                        Context.Entry(paybillAmount).State = EntityState.Modified;
                        Context.SaveChanges();
                    }
                    return Order_id.ToString();
                }
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message.ToString());
            }
        }



        public void ShipmentDeliverdAddress(int fkcustomerid, int fkorderaddressid)
        {
            var GetAddressBookData = Context.tblAddressBooks.Where(x => x.Fk_customer_Id == fkcustomerid && x.Id == fkorderaddressid).Select(x => x).FirstOrDefault();
            if (GetAddressBookData != null)
            {
                tblDeliveryAddress objtblDeliveryAddress = new tblDeliveryAddress();
                objtblDeliveryAddress.address_name = GetAddressBookData.address_name;
                objtblDeliveryAddress.address1 = GetAddressBookData.address1;
                objtblDeliveryAddress.address2 = GetAddressBookData.address2;
                objtblDeliveryAddress.city = GetAddressBookData.city;
                objtblDeliveryAddress.company = GetAddressBookData.company ?? "";
                objtblDeliveryAddress.country_code = GetAddressBookData.country_code;
                objtblDeliveryAddress.first_name = GetAddressBookData.first_name;
                objtblDeliveryAddress.last_name = GetAddressBookData.last_name;
                objtblDeliveryAddress.postal_code = GetAddressBookData.post_code;
                objtblDeliveryAddress.state = GetAddressBookData.state;
                objtblDeliveryAddress.mobile1 = GetAddressBookData.mobile1;
                objtblDeliveryAddress.mobile2 = GetAddressBookData.mobile2;
                Context.Configuration.ValidateOnSaveEnabled = false;
                Context.tblDeliveryAddresses.Add(objtblDeliveryAddress);
                Context.SaveChanges();
            }
        }

        // Order Charges method//
        public void OrderCharges(List<ShipmentMethodPackage> objShipmentMethodPackage, List<PacakageOptionArray> objPackagingOptions, ViewOrderModel objVieworderModel, string[] RecivedshipmentID)
        {
            ViewOrderModel objViewOrderModel = new ViewOrderModel();
            tblOrderCharge tblOrderChargeList = new tblOrderCharge();
            var Fk_Orderid = Context.tblOrders.OrderByDescending(x => x.id).Select(x => x.id).FirstOrDefault();
            // Check Default Address//
            var GetAddressBookData = Context.tblAddressBooks.Where(x => x.Fk_customer_Id == objVieworderModel.Fk_Customer_id && x.Id == objVieworderModel.Fk_orderAddres_id).Select(x => x).FirstOrDefault();
            var GetisdefaultAddress = (from tbladdress in Context.tblAddressBooks join tbdeladdress in Context.tblDeliveryAddresses on tbladdress.address_name equals tbdeladdress.address_name select new { tbladdress.Id, tbdeladdress.address_name, tbladdress.is_default, tbladdress.Fk_customer_Id }).Where(x => x.is_default == true && x.Fk_customer_Id == objVieworderModel.Fk_Customer_id && x.address_name == GetAddressBookData.address_name).FirstOrDefault();
            if (GetisdefaultAddress == null)
            {
                tblOrderCharge objtblOrderCharge = new tblOrderCharge();
                var GetShiptoAddressCharges = Context.Database.SqlQuery<ViewPackagingOptionsModel>("select distinct tblpoptlink.price,tblpacopt.title from tblCustomerPlanLinking tblcustlink inner join tblPlans tbplan on tblcustlink.plan_title = tbplan.title inner join tblReceivedShipment tblrec on tblcustlink.Fk_Customer_id = tblrec.Fk_Customer_Id inner join tblPlanOptionLinking tblpoptlink on tbplan.Id = tblpoptlink.Fk_plan_id inner join tblPackagingOptions tblpacopt on tblpoptlink.Fk_packaging_option_id = tblpacopt.Id where tblrec.Fk_Customer_Id = " + objVieworderModel.Fk_Customer_id + " and tblpacopt.title ='Ship to Address'").FirstOrDefault<ViewPackagingOptionsModel>();
                objtblOrderCharge.charge_name = GetShiptoAddressCharges.title;
                objtblOrderCharge.sell_price = GetShiptoAddressCharges.price;
                objtblOrderCharge.Fk_order_id = Fk_Orderid;
                Context.tblOrderCharges.Add(objtblOrderCharge);
                Context.SaveChanges();
            }

            //foreach (var item in objShipmentMethodPackage)
            //{
            //    decimal ShipmentPrice;
            //    if (decimal.TryParse(item.shipmentMethodPrice.Trim(), out ShipmentPrice))
            //        ShipmentPrice = Convert.ToDecimal(ShipmentPrice);
            //    tblOrderChargeList.sell_price = ShipmentPrice;
            //    tblOrderChargeList.Fk_order_id = Fk_Orderid;
            //    Context.tblOrderCharges.Add(tblOrderChargeList);
            //    Context.SaveChanges();
            //}

            foreach (var item in objPackagingOptions)
            {
                // Use functionality add Insurance to shipment start//
                if (item.PackageOptionTitle.Equals("Add Insurance to my Shipment") == true)
                {
                    string[] RecID = RecivedshipmentID.Select(x => x.Replace("id", "")).ToArray();
                    int[] RecShipmentID = Array.ConvertAll(RecID, int.Parse);
                    decimal? TotalShipmentPrice = 0;
                    for (int i = 0; i < RecShipmentID.Length; i++)
                    {
                        int ID = RecShipmentID[i];
                        var GetShipmentDetailData = Context.tblShipmentDetails.Where(x => x.Fk_shipment_id == ID).Select(x => x).ToList();
                        GetShipmentDetailData.ForEach(x =>
                        {
                            decimal SumTotalShipment = (x.purchase_price) * (x.quantity);
                            TotalShipmentPrice += SumTotalShipment;
                        });
                    }
                    // Add Insurance Charge Total ReceivedShipmentPrice less than 100 Start//
                    if (TotalShipmentPrice <= 100)
                    {
                        tblOrderChargeList.charge_name = "Insurance";
                        tblOrderChargeList.sell_price = 0.00m;
                        tblOrderChargeList.Fk_order_id = Fk_Orderid;
                        Context.tblOrderCharges.Add(tblOrderChargeList);
                        Context.SaveChanges();
                    }
                    // Add Insurance Charge Total ReceivedShipmentPrice less than 100 End//

                    // Add Insurance Charge Total ReceivedShipmentPrice greater than 100 Start//
                    else if (TotalShipmentPrice > 100 && TotalShipmentPrice <= 50000)
                    {
                        if (TotalShipmentPrice % 100 > 0)
                        {
                            int ModePrice = Convert.ToInt32(TotalShipmentPrice % 100);
                            TotalShipmentPrice = (decimal)(TotalShipmentPrice) - (ModePrice) + 100;
                        }
                        decimal GetInsurance = ((TotalShipmentPrice ?? 0.0m) * 0.90m) / 100;
                        if (GetInsurance <= 2.70m)
                        {
                            GetInsurance = 2.70m;
                        }
                        tblOrderChargeList.charge_name = "Insurance";
                        tblOrderChargeList.sell_price = Convert.ToDecimal(GetInsurance);
                        tblOrderChargeList.Fk_order_id = Fk_Orderid;
                        Context.tblOrderCharges.Add(tblOrderChargeList);
                        Context.SaveChanges();
                    }
                    else
                    {
                        decimal GetInsurance = ((TotalShipmentPrice ?? 0.0m) * 0.90m) / 100;
                        tblOrderChargeList.charge_name = "Insurance";
                        tblOrderChargeList.sell_price = Convert.ToDecimal(GetInsurance);
                        tblOrderChargeList.Fk_order_id = Fk_Orderid;
                        Context.tblOrderCharges.Add(tblOrderChargeList);
                        Context.SaveChanges();
                    }
                    // Add Insurance Charge Total ReceivedShipmentPrice greater than 100 end//
                }
                // Use functionality add Insurance to shipment end//

                // Use functionality add another Charges Start  //
                else if (item.PackageOptionTitle.Equals("Add Insurance to my Shipment") == false)
                {
                    decimal OptionPrice;
                    if (decimal.TryParse(item.PackageOptionPrice.Trim(), out OptionPrice))
                        OptionPrice = Convert.ToDecimal(OptionPrice);
                    tblOrderChargeList.charge_name = item.PackageOptionTitle;
                    decimal data = OptionPrice;
                    tblOrderChargeList.sell_price = OptionPrice;
                    tblOrderChargeList.Fk_order_id = Fk_Orderid;
                    Context.tblOrderCharges.Add(tblOrderChargeList);
                    Context.SaveChanges();
                }
                // Use functionality add another Charges end  //
            }
            var GetSumOrderCharges = Context.tblOrderCharges.AsQueryable().Where(x => x.Fk_order_id == Fk_Orderid).Sum(x => x.sell_price);
            var GetOrdertbl = Context.tblOrders.AsQueryable().OrderByDescending(x => x.id).Select(x => x).FirstOrDefault();
            GetOrdertbl.payable_amount = GetSumOrderCharges;
            Context.Entry(GetOrdertbl).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public int CreateOrder(ViewOrderModel objViewOrderModel, ShipmentMethodPackage services, string CouponCode,string Order_Note)
        {
            string CarrierServicesCode = services.shipmentcode;
            string CarrierName = services.shipmentdescription;
            var GetServicetblcarrierDetail = Context.tblCarriersDetails.AsQueryable().Where(x => x.code == CarrierServicesCode && x.carrier_name == CarrierName).Select(x => x).FirstOrDefault();
            int GetDeliverAddressbookID = Context.tblDeliveryAddresses.OrderByDescending(x => x.id).Select(x => x.id).FirstOrDefault();

            tblOrder objtblOrder = new tblOrder();
            objtblOrder.Fk_delivery_address_id = GetDeliverAddressbookID;
            objtblOrder.coupon_code = CouponCode;
            objtblOrder.Fk_invoice_id = 0;
            objtblOrder.carrier = GetServicetblcarrierDetail.carrier_name;
            objtblOrder.product = GetServicetblcarrierDetail.product;
            objtblOrder.service = GetServicetblcarrierDetail.service;
            objtblOrder.service_code = GetServicetblcarrierDetail.code;
            objtblOrder.estimated_orderprice = decimal.Parse(services.shipmentMethodPrice);
            objtblOrder.pickup_date = DateTime.Now;
            objtblOrder.pickup_cut_off_time = DateTime.Now;
            objtblOrder.booking_time = DateTime.Now;
            objtblOrder.created_on = DateTime.Now;
            //objtblOrder.billing_weight = 25;
            //objtblOrder.is_delivered = true;
            //objtblOrder.signature = "Customer";
            objtblOrder.status = 8;
            objtblOrder.Fk_customer_id = objViewOrderModel.Fk_Customer_id;
            objtblOrder.reference_no = getOrderReferenceNo();
            objtblOrder.order_notes = Order_Note;
            Context.tblOrders.Add(objtblOrder);
            Context.SaveChanges();

            return objtblOrder.id;
        }

        private string getOrderReferenceNo()
        {
            int CountTodayOrder = Context.tblOrders.AsQueryable().Where(x => DbFunctions.TruncateTime(x.created_on) == DbFunctions.TruncateTime(DateTime.Now)).Count();
            return DateTime.Now.ToString("yyyyMMdd") + (CountTodayOrder + 1).ToString();
        }

        public void RecivedCheckboxid(int CustomerID, int id)
        {

            var RecivedShipmentData = Context.tblReceivedShipments.Where(x => x.Fk_Customer_Id == CustomerID && x.id == id).Select(x => x).FirstOrDefault();
            var Fk_Orderid = Context.tblOrders.OrderByDescending(x => x.id).Select(x => x.id).FirstOrDefault();
            if (RecivedShipmentData != null)
            {
                RecivedShipmentData.Fk_order_id = Fk_Orderid;
                // RecivedShipmentData.status = 10;
                Context.Entry(RecivedShipmentData).State = EntityState.Modified;
                Context.SaveChanges();
            }
            // Get Free Storage days and save order charges tbl//

        }
        public void SaveFreeStorageDays(int CustomerID)
        {
            int GetApplyFreeStorageDays = 0;

            int Forstoragedays = 0;
            var GetOrderID = Context.tblOrders.OrderByDescending(x => x.id).Select(x => x.id).FirstOrDefault();
            var GetDatetblRecivedShip = Context.tblReceivedShipments.Where(x => x.Fk_order_id == GetOrderID).Select(x => x.received_date).ToList();
            var Freestoragedays = (from tbplan in Context.tblPlans join tbcustpllink in Context.tblCustomerPlanLinkings on tbplan.title equals tbcustpllink.plan_title select new { tbcustpllink.free_storage_days, tbcustpllink.Fk_Customer_Id }).Where(x => x.Fk_Customer_Id == CustomerID).FirstOrDefault();
            foreach (var item in GetDatetblRecivedShip)
            {
                var GetPalnoption = Context.Database.SqlQuery<ViewPackagingOptionsModel>("select distinct tblpoptlink.price,tblpacopt.title from tblCustomerPlanLinking tblcustlink inner join tblPlans tbplan on tblcustlink.plan_title = tbplan.title inner join tblReceivedShipment tblrec on tblcustlink.Fk_Customer_id = tblrec.Fk_Customer_Id inner join tblPlanOptionLinking tblpoptlink on tbplan.Id = tblpoptlink.Fk_plan_id inner join tblPackagingOptions tblpacopt on tblpoptlink.Fk_packaging_option_id = tblpacopt.Id where tblrec.Fk_Customer_Id = " + CustomerID + " and tblpacopt.title ='Storage'").FirstOrDefault<ViewPackagingOptionsModel>();
                int TotalstorageDays = Convert.ToInt32((DateTime.Now.Date - item.Date).TotalDays);
                int TotalDays = Convert.ToInt32(Freestoragedays.free_storage_days) - TotalstorageDays;
                if (TotalDays < 0)
                {
                    GetApplyFreeStorageDays += Convert.ToInt32(TotalDays * GetPalnoption.price);
                    Forstoragedays = System.Math.Abs(GetApplyFreeStorageDays);
                }
            }
            if (Forstoragedays != 0)
            {
                tblOrderCharge objordercharges = new tblOrderCharge();
                objordercharges.charge_name = "Storage";
                objordercharges.Fk_order_id = GetOrderID;
                objordercharges.sell_price = Forstoragedays;
                Context.tblOrderCharges.Add(objordercharges);
                Context.SaveChanges();
            }
        }
        public string GetCustomerRefNo(int ID)
        {
            Context4InshipEntities DBContext = new Context4InshipEntities();
            return DBContext.tblCustomers.AsQueryable().Where(x => x.Id == ID).Select(X => X.customer_reference).FirstOrDefault<string>();
        }

        public Tuple<bool, string> validateCouponCode(string coupon_code, string country_name)
        {
            System.Data.Entity.Core.Objects.ObjectParameter objIsValidparameter = new System.Data.Entity.Core.Objects.ObjectParameter("isValid", true);
            System.Data.Entity.Core.Objects.ObjectParameter objmsgparameter = new System.Data.Entity.Core.Objects.ObjectParameter("msg", "");
            var ret = Context.usp_IsCouponValidUser(coupon_code, Convert.ToInt32(HttpContext.Current.Session["UserId"]), country_name, objIsValidparameter, objmsgparameter);
            if (Convert.ToBoolean(objIsValidparameter.Value) == true)
            {
                return Tuple.Create(true, Convert.ToString(objmsgparameter.Value));
            }
            return Tuple.Create(false, "This coupon code is invalid or has expired.");
        }

        public string GetCustomerPlan()
        {
            int id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
            return Context.tblCustomerPlanLinkings.AsQueryable().Where(x => x.Fk_Customer_Id == id).Select(x => x.plan_title).FirstOrDefault();
        }
    }
}