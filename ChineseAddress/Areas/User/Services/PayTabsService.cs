using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Configuration;
using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Models;
using System.Web.Script.Serialization;

namespace _ChineseAddress.com.Areas.User.Services
{
    /// <summary>
    /// Summary description for clsPayTab
    /// </summary>
    public class PayTabsService : IPayTabsService
    {
        public PayTabsService()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        public string merchant_email { get; set; } = ConfigurationManager.AppSettings["merchant_email"];
        public string secret_key { get; set; } = ConfigurationManager.AppSettings["secret_key"];
        public string cc_first_name { get; set; }
        public string cc_last_name { get; set; }
        public string phone_number { get; set; }
        public string billing_address { get; set; }

        //USA or CANADA String of 2 characters containing the ISO state code
        public string state { get; set; }
        public string city { get; set; }
        public string postal_code { get; set; }
        public string country { get; set; }
        public string email { get; set; }
        public decimal amount { get; set; }
        public decimal other_charges { get; set; }
        //optional
        public decimal discount { get; set; }
        //optional
        public string reference_no { get; set; }
        public string currency { get; set; }
        public string title { get; set; }
        public string ip_customer { get; set; }
        public string ip_merchant { get; set; }
        public string return_url { get; set; }
        public string address_shipping { get; set; }
        public string city_shipping { get; set; }
        public string state_shipping { get; set; }
        public string postal_code_shipping { get; set; }
        public string country_shipping { get; set; }
        public string quantity { get; set; }
        public string unit_price { get; set; }
        public string products_per_title { get; set; }
        public string ChannelOfOperations { get; set; }
        public string ProductCategory { get; set; }
        public string ProductName { get; set; }
        public string ShippingMethod { get; set; }
        public string DeliveryType { get; set; }
        public string CustomerId { get; set; }
        public string msg_lang { get; set; }
        public string payment_reference { get; set; }
        public string is_tokenization { get; set; }
        public string is_existing_customer { get; set; }
        public string pt_token { get; set; }
        public string pt_customer_email { get; set; }
        public string pt_customer_password { get; set; }

        public bool _isSucceded { get; set; } = true;
        public string _msg { get; set; }
        public string ApiKey { get; set; }

        public void PayTabAuthentication()
        {
            try
            {
                //string merchant_email = ConfigurationManager.AppSettings["merchant_email"];
                //string secret_key = ConfigurationManager.AppSettings["secret_key"];

                string URLAuth = "https://www.paytabs.com/apiv2/validate_secret_key";
                string postString = string.Format("merchant_email={0}&secret_key={1}", merchant_email, secret_key);

                const string contentType = "application/x-www-form-urlencoded";
                System.Net.ServicePointManager.Expect100Continue = false;

                CookieContainer cookies = new CookieContainer();
                HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
                webRequest.Method = "POST";
                webRequest.ContentType = contentType;
                webRequest.CookieContainer = cookies;
                webRequest.ContentLength = postString.Length;

                StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();

                StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
                string responseData = responseReader.ReadToEnd();
                responseReader.Close();
                webRequest.GetResponse().Close();

                var obj = JObject.Parse(responseData);
                string strAccess = (string)obj["response_code"];
                if (strAccess == "4000")
                {
                    _msg = (string)obj["result"];
                    _isSucceded = true;
                }
                else
                {
                    _isSucceded = false;
                    _msg = (string)obj["result"]; ;
                }
            }
            catch (Exception ex)
            {
                _isSucceded = false;
                _msg = ex.Message.ToString();
            }
        }


        public void CreatePayPage(out string paymentURL)
        {
            try
            {
                string URLAuth = "https://www.paytabs.com/apiv2/create_pay_page";

                StringBuilder postString = new StringBuilder();
                postString.Append("merchant_email=" + merchant_email);
                postString.Append("&secret_key=" + secret_key);
                postString.Append("&cc_first_name=" + cc_first_name);
                postString.Append("&cc_last_name=" + cc_last_name);

                postString.Append("&cc_phone_number=" + "000");//we don't have a phone no  initital so,we are passing 000 
                postString.Append("&phone_number=" + phone_number);
                postString.Append("&billing_address=" + billing_address);
                postString.Append("&state=" + state);
                postString.Append("&city=" + city);
                postString.Append("&postal_code=" + postal_code);
                postString.Append("&country=" + country);
                postString.Append("&email=" + email);
                postString.Append("&amount=" + Convert.ToString(amount * Convert.ToDecimal(ConfigurationManager.AppSettings["USDToReal"])));
                postString.Append("&other_charges=0.00");
                postString.Append("&discount=" + discount);
                postString.Append(reference_no != "" ? "&reference_no=" + reference_no + "" : "");
                postString.Append("&currency=" + currency);
                postString.Append("&title=" + title);
                postString.Append("&ip_customer=" + ip_customer);
                postString.Append("&ip_merchant=" + ip_merchant);
                postString.Append("&site_url=" + HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority);
                postString.Append("&return_url=" + return_url);
                postString.Append("&address_shipping=" + address_shipping);
                postString.Append("&city_shipping=" + city_shipping);
                postString.Append("&state_shipping=" + state_shipping);
                postString.Append("&postal_code_shipping=" + postal_code_shipping);
                postString.Append("&country_shipping=" + country_shipping);
                postString.Append("&quantity=" + quantity);
                postString.Append("&unit_price=" + Convert.ToString(Convert.ToDecimal(unit_price) * Convert.ToDecimal(ConfigurationManager.AppSettings["USDToReal"])));
                postString.Append("&products_per_title=" + products_per_title);
                postString.Append("&ChannelOfOperations=" + ChannelOfOperations);
                postString.Append("&ProductCategory=" + ProductCategory);
                postString.Append("&ProductName=" + ProductName);
                postString.Append("&ShippingMethod=" + ShippingMethod);
                postString.Append("&DeliveryType=" + DeliveryType);
                postString.Append("&CustomerId=" + CustomerId);
                postString.Append(msg_lang != "" ? "&msg_lang=" + msg_lang + "" : "");
                postString.Append("&cms_with_version=Asp.net Mvc c# Web Request");


                const string contentType = "application/x-www-form-urlencoded";
                System.Net.ServicePointManager.Expect100Continue = false;

                CookieContainer cookies = new CookieContainer();
                HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
                webRequest.Method = "POST";
                webRequest.ContentType = contentType;
                webRequest.CookieContainer = cookies;
                webRequest.ContentLength = postString.Length;

                StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();

                StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
                string responseData = responseReader.ReadToEnd();
                responseReader.Close();
                webRequest.GetResponse().Close();

                var obj = JObject.Parse(responseData);
                paymentURL = "";
                if ((string)obj["response_code"] == "4012")
                {
                    _isSucceded = true;
                    paymentURL = (string)obj["payment_url"];
                }
                else
                {
                    _isSucceded = false;
                    _msg = "Error  Occured, Error Code :" + (string)obj["response_code"] + " and Error  Message : " + (string)obj["result"];
                }




            }
            catch (Exception ex)
            {
                paymentURL = "";
                _isSucceded = false;
                _msg = ex.Message.ToString();
            }
        }

        public void CreatePayPageWithToken(out string paymentURL)
        {
            try
            {
                string URLAuth = "https://www.paytabs.com/apiv2/create_pay_page";

                StringBuilder postString = new StringBuilder();
                postString.Append("merchant_email=" + merchant_email);
                postString.Append("&secret_key=" + secret_key);
                postString.Append("&cc_first_name=" + cc_first_name);
                postString.Append("&cc_last_name=" + cc_last_name);

                postString.Append("&cc_phone_number=" + "000");//we don't have a phone no  initital so,we are passing 000 
                postString.Append("&phone_number=" + phone_number);
                postString.Append("&billing_address=" + billing_address);
                postString.Append("&state=" + state);
                postString.Append("&city=" + city);
                postString.Append("&postal_code=" + postal_code);
                postString.Append("&country=" + country);
                postString.Append("&email=" + email);
                postString.Append("&amount=" + Convert.ToString(amount * Convert.ToDecimal(ConfigurationManager.AppSettings["USDToReal"])));
                postString.Append("&other_charges=0.00");
                postString.Append("&discount=" + discount);
                postString.Append(reference_no != "" ? "&reference_no=" + reference_no + "" : "");
                postString.Append("&currency=" + currency);
                postString.Append("&title=" + title);
                postString.Append("&ip_customer=" + ip_customer);
                postString.Append("&ip_merchant=" + ip_merchant);
                postString.Append("&site_url=" + HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority);
                postString.Append("&return_url=" + return_url);
                postString.Append("&address_shipping=" + address_shipping);
                postString.Append("&city_shipping=" + city_shipping);
                postString.Append("&state_shipping=" + state_shipping);
                postString.Append("&postal_code_shipping=" + postal_code_shipping);
                postString.Append("&country_shipping=" + country_shipping);
                postString.Append("&quantity=" + quantity);
                postString.Append("&unit_price=" + Convert.ToString(Convert.ToDecimal(unit_price) * Convert.ToDecimal(ConfigurationManager.AppSettings["USDToReal"])));
                postString.Append("&products_per_title=" + products_per_title);
                postString.Append("&ChannelOfOperations=" + ChannelOfOperations);
                postString.Append("&ProductCategory=" + ProductCategory);
                postString.Append("&ProductName=" + ProductName);
                postString.Append("&ShippingMethod=" + ShippingMethod);
                postString.Append("&DeliveryType=" + DeliveryType);
                postString.Append("&CustomerId=" + CustomerId);
                postString.Append("&is_tokenization=" + is_tokenization);
                postString.Append("&is_existing_customer=" + is_existing_customer);
                if (!string.IsNullOrEmpty(pt_token))
                {
                    postString.Append("&pt_token=" + pt_token);
                    postString.Append("&pt_customer_email=" + pt_customer_email);
                    postString.Append("&pt_customer_password=" + pt_customer_password);
                }
                postString.Append(msg_lang != "" ? "&msg_lang=" + msg_lang + "" : "");
                postString.Append("&cms_with_version=Asp.net Mvc c# Web Request");


                const string contentType = "application/x-www-form-urlencoded";
                System.Net.ServicePointManager.Expect100Continue = false;

                CookieContainer cookies = new CookieContainer();
                HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
                webRequest.Method = "POST";
                webRequest.ContentType = contentType;
                webRequest.CookieContainer = cookies;
                webRequest.ContentLength = postString.Length;

                StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();

                StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
                string responseData = responseReader.ReadToEnd();
                responseReader.Close();
                webRequest.GetResponse().Close();

                var obj = JObject.Parse(responseData);
                paymentURL = "";
                if ((string)obj["response_code"] == "4012")
                {
                    _isSucceded = true;
                    paymentURL = (string)obj["payment_url"];
                }
                else
                {
                    _isSucceded = false;
                    _msg = "Error  Occured, Error Code :" + (string)obj["response_code"] + " and Error  Message : " + (string)obj["result"];
                }




            }
            catch (Exception ex)
            {
                paymentURL = "";
                _isSucceded = false;
                _msg = ex.Message.ToString();
            }
        }

        public string ValidateApiKey()
        {
            try
            {
                string URLAuth = "https://www.paytabs.com/api/api_key_valid";
                string postString = string.Format("api_key={0}", ApiKey);

                const string contentType = "application/x-www-form-urlencoded";
                System.Net.ServicePointManager.Expect100Continue = false;

                CookieContainer cookies = new CookieContainer();
                HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
                webRequest.Method = "POST";
                webRequest.ContentType = contentType;
                webRequest.CookieContainer = cookies;
                webRequest.ContentLength = postString.Length;

                StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();

                StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
                string responseData = responseReader.ReadToEnd();
                responseReader.Close();
                webRequest.GetResponse().Close();

                var obj = JObject.Parse(responseData);
                return (string)obj["result"];
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }


        public PayTabsResponseModel VerifyPayment()
        {
            PayTabsResponseModel _PayTabsResponseModel = new PayTabsResponseModel();
            try
            {
                string URLAuth = "https://www.paytabs.com/apiv2/verify_payment";
                string postString = string.Format("merchant_email={0}&secret_key={1}&payment_reference={2}", merchant_email, secret_key, payment_reference);

                const string contentType = "application/x-www-form-urlencoded";
                System.Net.ServicePointManager.Expect100Continue = false;

                CookieContainer cookies = new CookieContainer();
                HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
                webRequest.Method = "POST";
                webRequest.ContentType = contentType;
                webRequest.CookieContainer = cookies;
                webRequest.ContentLength = postString.Length;

                StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();

                StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
                string responseData = responseReader.ReadToEnd();
                responseReader.Close();
                webRequest.GetResponse().Close();

                _PayTabsResponseModel = Newtonsoft.Json.JsonConvert.DeserializeObject<_ChineseAddress.com.Models.PayTabsResponseModel>(responseData);
                string strResponseCode = _PayTabsResponseModel.response_code;

                if (strResponseCode == "100" || strResponseCode == "481" || strResponseCode == "482")
                {
                    _isSucceded = true;
                    _msg = _PayTabsResponseModel.result;
                }
                else
                {
                    _isSucceded = false;
                    _msg = _PayTabsResponseModel.result;
                }
            }
            catch (Exception ex)
            {
                _isSucceded = false;
                _msg = ex.Message.ToString();
            }
            return _PayTabsResponseModel;
        }


        public string LogoutPayTab()
        {
            try
            {
                string URLAuth = "https://www.paytabs.com/api/logout";
                string postString = string.Format("api_key={0}", ApiKey);

                const string contentType = "application/x-www-form-urlencoded";
                System.Net.ServicePointManager.Expect100Continue = false;

                CookieContainer cookies = new CookieContainer();
                HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
                webRequest.Method = "POST";
                webRequest.ContentType = contentType;
                webRequest.CookieContainer = cookies;
                webRequest.ContentLength = postString.Length;

                StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();

                StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
                string responseData = responseReader.ReadToEnd();
                responseReader.Close();
                webRequest.GetResponse().Close();

                var obj = JObject.Parse(responseData);

                string strLogoutError = "";
                if ((string)obj["Error Code"] != null)
                {
                    strLogoutError = "api key not valid";
                }

                return strLogoutError;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}