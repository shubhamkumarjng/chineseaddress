using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Repository;
using Microsoft.Practices.Unity;
using System.Web.Mvc;
using System.Data.Entity.SqlServer;
using System.IO;
using _ChineseAddress.com.Common.Services;
using PagedList;
using PagedList.Mvc;

namespace _ChineseAddress.com.Areas.User.Services
{
    public class WalletService : Admin.Models.clsCommon, IWalletService<tblTopUp, tblTopUpMethod>
    {
        [Dependency]
        public Context4InshipEntities Context { get; set; }
        public bool _isSucceeded { get; set; } = true;
        public string _msg { get; set; }

        public void AddUpdateTopUpWalletRequest(tblTopUp _tblTopUp, bool IsAdmin = false)
        {
            try
            {

                _tblTopUp.Fk_TopUpMethod = Convert.ToInt32(_tblTopUp.TopUpMethodId);


                if (_tblTopUp.Id == 0)
                {
                    _tblTopUp.created_on = DateTime.Now;
                    Context.tblTopUps.Add(_tblTopUp);
                    Context.Configuration.ValidateOnSaveEnabled = false;
                    Context.SaveChanges();

                    if (_tblTopUp.attachment != null)
                    {
                        string fName = uploadFile(_tblTopUp.Id, _tblTopUp.attachment, "RequestTopUpWallet");
                        _tblTopUp.attach_name = fName;
                        Context.Entry(_tblTopUp).Property(x => x.attach_name).IsModified = true;
                        Context.SaveChanges();
                    }
                    if (!IsAdmin)
                        sendEmailOnAddTopUpRequestToAdmin(_tblTopUp, true);
                    //send email to user on Adding new TopUp Request.
                    sendEmailOnAddEditTopUpRequest(_tblTopUp);
                    _msg = SuccessMsg("Top up wallet request has been saved successfully");
                }
                else
                {
                    var etopUp = Context.tblTopUps.AsQueryable().Where(x => x.Id == _tblTopUp.Id).FirstOrDefault();
                    etopUp.Fk_TopUpMethod = _tblTopUp.Fk_TopUpMethod;
                    etopUp.Fk_Customer_Id = _tblTopUp.Fk_Customer_Id;
                    etopUp.transaction_id = _tblTopUp.transaction_id;
                    etopUp.amount = _tblTopUp.amount;
                    etopUp.attachment = _tblTopUp.attachment;
                    etopUp.remark = _tblTopUp.remark;
                    etopUp.admin_remark = _tblTopUp.admin_remark;
                    etopUp.status = _tblTopUp.status;
                    Context.tblTopUps.Attach(etopUp);
                    Context.Entry(etopUp).State = System.Data.Entity.EntityState.Modified;
                    Context.Configuration.ValidateOnSaveEnabled = false;
                    Context.SaveChanges();

                    if (_tblTopUp.attachment != null)
                    {
                        string fName = uploadFile(_tblTopUp.Id, _tblTopUp.attachment, "RequestTopUpWallet");
                        etopUp.attach_name = fName;
                        Context.Entry(etopUp).Property(x => x.attach_name).IsModified = true;
                        Context.SaveChanges();
                    }
                    _tblTopUp = etopUp;
                    if (!IsAdmin)
                    {
                        sendEmailOnAddTopUpRequestToAdmin(_tblTopUp, false);
                        sendEmailOnAddEditTopUpRequest(_tblTopUp, false);
                    }

                    _msg = SuccessMsg("Top up wallet request has been updated successfully");

                }
                if (_tblTopUp.status == (int)eTopUpWalletStatus.Accept)
                {
                    UpdateWalletBalance(_tblTopUp);
                }
            }
            catch (Exception ex)
            {
                _isSucceeded = false;
                _msg = ErrorMsg(ex.Message);
            }
        }

        public List<SelectListItem> getTopUpMethodsAsItems()
        {
            try
            {
                var lstTopUpMethods = Context.tblTopUpMethods.AsQueryable().Where(x => x.status == true).Select(x => new SelectListItem
                {
                    Text = x.method_name,
                    Value = SqlFunctions.StringConvert((double)x.id).Trim()
                }).ToList();
                lstTopUpMethods.Insert(0, new SelectListItem() { Text = "Select Top Up Method", Value = "" });
                return lstTopUpMethods;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblTopUp> getTopUpDetail()
        {
            try
            {
                return Context.tblTopUps.AsEnumerable()
                    .Join(Context.tblTopUpMethods, t => t.Fk_TopUpMethod, tm => tm.id, (t, tm) => new { topup = t, topupMethod = tm })
                    .Join(Context.tblCustomers, t => t.topup.Fk_Customer_Id, c => c.Id, (t, c) => new { topup = t.topup, topupMethod = t.topupMethod, customer = c })
                    .OrderByDescending(x => x.topup.created_on)
                    .Select(ttm => new tblTopUp { CustomerRefNo = ttm.customer.customer_reference, TopUpMethodId = ttm.topupMethod.id.ToString(), TopUpMethodName = ttm.topupMethod.method_name, Fk_TopUpMethod = ttm.topupMethod.id, Id = ttm.topup.Id, transaction_id = ttm.topup.transaction_id, amount = ttm.topup.amount, remark = ttm.topup.remark, admin_remark = ttm.topup.admin_remark, attach_name = ttm.topup.attach_name, status = ttm.topup.status, created_on = ttm.topup.created_on })
                    .AsEnumerable().ToList();
            }
            catch (Exception ex)
            {
                _isSucceeded = false;
                ErrorMsg(ex.Message.ToString());
            }
            return null;
        }
        public IPagedList<tblTopUp> getTopUpDetailByCustomerId(int CustomerId, int pageNumber)
        {
            try
            {
                return Context.tblTopUps
                    .AsQueryable()
                    .Where(x => x.Fk_Customer_Id == CustomerId).
                    AsEnumerable().
                    Join(Context.tblTopUpMethods, t => t.Fk_TopUpMethod, tm => tm.id, (t, tm) => new { topup = t, topupMethod = tm })
                    .OrderByDescending(x => x.topup.created_on)
                    .Select(ttm => new tblTopUp { TopUpMethodId = ttm.topupMethod.id.ToString(), TopUpMethodName = ttm.topupMethod.method_name, Fk_TopUpMethod = ttm.topupMethod.id, Id = ttm.topup.Id, transaction_id = ttm.topup.transaction_id, amount = ttm.topup.amount, remark = ttm.topup.remark, admin_remark = ttm.topup.admin_remark, attach_name = ttm.topup.attach_name, status = ttm.topup.status })
                    .AsEnumerable().ToPagedList(pageNumber, MvcApplication.global_Page_Size);
            }
            catch (Exception ex)
            {
                _isSucceeded = false;
                ErrorMsg(ex.Message.ToString());
            }
            return null;
        }
        public tblTopUp getTopUpDetailById(int id)
        {
            try
            {
                return Context.tblTopUps
                    .AsQueryable()
                    .Where(x => x.Id == id).
                    AsEnumerable().
                    Join(Context.tblTopUpMethods, t => t.Fk_TopUpMethod, tm => tm.id, (t, tm) => new { topup = t, topupMethod = tm })
                    .Join(Context.tblCustomers, t => t.topup.Fk_Customer_Id, c => c.Id, (t, c) => new { topup = t.topup, topupMethod = t.topupMethod, customer = c })
                    .Select(ttm => new tblTopUp { Fk_Customer_Id = ttm.customer.Id, TopUpMethodId = ttm.topupMethod.id.ToString(), TopUpMethodName = ttm.topupMethod.method_name, Fk_TopUpMethod = ttm.topupMethod.id, Id = ttm.topup.Id, transaction_id = ttm.topup.transaction_id, amount = ttm.topup.amount, remark = ttm.topup.remark, admin_remark = ttm.topup.admin_remark, attach_name = ttm.topup.attach_name, status = ttm.topup.status })
                    .AsEnumerable().FirstOrDefault();
            }
            catch (Exception ex)
            {
                _isSucceeded = false;
                _msg = ErrorMsg(ex.Message.ToString());
            }
            return new tblTopUp();
        }

        public void UpdateWalletBalance(tblTopUp _tblTopUp)
        {
            tblRewardAmount objtblRewardAmount = new tblRewardAmount();
            objtblRewardAmount.Fk_CustomerId = _tblTopUp.Fk_Customer_Id;
            objtblRewardAmount.credit = _tblTopUp.amount;
            objtblRewardAmount.created_on = DateTime.Now;
            objtblRewardAmount.type = "TopUp";
            objtblRewardAmount.Fk_TopUp_Id = _tblTopUp.Id;
            Context.tblRewardAmounts.Add(objtblRewardAmount);
            Context.SaveChanges();
        }

        public IEnumerable<tblTopUpMethod> getTopUpMethods()
        {
            return Context.tblTopUpMethods.AsQueryable().Where(x => x.status == true);
        }
        public string getTopUpMethodDescriptionByMehodId(int TopUpMethodId)
        {
            string desc = "";
            try
            {
                desc = Context.tblTopUpMethods.AsQueryable().Where(x => x.id == TopUpMethodId).Select(x => x.method_description).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _isSucceeded = false;
                _msg = ErrorMsg(ex.Message);
            }
            return desc;
        }

        #region extMethods

        public string uploadFile(int id, HttpPostedFileBase postedFiles, string folderName)
        {
            string fileName = "";
            try
            {
                if (postedFiles != null)
                {
                    string imgFolderpath = HttpContext.Current.Server.MapPath($"~/Uploads/{folderName}");
                    if (!Directory.Exists(imgFolderpath))
                        Directory.CreateDirectory(imgFolderpath);
                    string dirPath = HttpContext.Current.Server.MapPath($"~/Uploads/{folderName}/") + Convert.ToString(id);
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }
                    fileName = Guid.NewGuid().ToString() + Path.GetExtension(postedFiles.FileName);
                    string file_path = dirPath + "\\" + fileName;
                    postedFiles.SaveAs(file_path);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fileName;
        }

        public void sendEmailOnAddEditTopUpRequest(tblTopUp _tblTopUp, bool isNew = true)
        {
            try
            {
                tblCustomer customer = Context.tblCustomers.AsQueryable().Where(X => X.Id == _tblTopUp.Fk_Customer_Id).FirstOrDefault();
                string firstName = Context.tblAddressBooks.AsQueryable().Where(x => x.Fk_customer_Id == customer.Id).OrderBy(x => x.created_on).Select(x => x.first_name).FirstOrDefault();
                //Send Sign Email Start
                clsSendEmailService objclsSendEmailService = new clsSendEmailService();
                string subject = "chineseaddress - " + (isNew ? "New" : "Edit") + "  Top Up Wallet Request";
                System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append($"This is an automatic email just to let you know that your request for top up wallet has been received with reference Number <Strong>{_tblTopUp.transaction_id}</Strong> as on <Strong>{_tblTopUp.created_on.ToString("dd MMMM yyyy hh:mm:ss tt")}</Strong>.we will update your wallet balance within 24 hrs.<br/>");
                hashTable.Add("#user#", firstName);
                hashTable.Add("#content#", sb.ToString());
                //Send Sign Email End

                objclsSendEmailService.SendEmail(new[] { customer.email }, null, null, (new clsEmailTemplateService().GetEmailTemplate("commonuser", hashTable)), subject);
            }
            catch (Exception ex)
            {
            }
        }
        public void sendEmailOnAddTopUpRequestToAdmin(tblTopUp _tblTopUp, bool isNew)
        {
            try
            {
                tblCustomer customer = Context.tblCustomers.AsQueryable().Where(X => X.Id == _tblTopUp.Fk_Customer_Id).FirstOrDefault();
                _tblTopUp = getTopUpDetailById(_tblTopUp.Id);
                //Send Sign Email Start
                clsSendEmailService objclsSendEmailService = new clsSendEmailService();

                string subject = "chineseaddress - " + (isNew ? "New" : "Edit") + " Top Up Request";
                System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append($"you received a " + (isNew ? "New" : "Edit") + " Top Up Request" + " with following details:-<br/><br/>");
                sb.AppendFormat("Top Up Method : <strong>{0}</strong><br/><br/>", _tblTopUp.TopUpMethodName);
                sb.AppendFormat("Reference Number : <strong>{0}</strong><br/><br/>", _tblTopUp.transaction_id);
                sb.AppendFormat("Amount : <strong>${0}</strong><br/><br/>", _tblTopUp.amount);
                sb.AppendFormat("Remark : <strong>{0}</strong>", _tblTopUp.remark);
                if (!string.IsNullOrEmpty(_tblTopUp.attach_name))
                {
                    Uri uri = HttpContext.Current.Request.Url;
                    sb.AppendFormat("<br/><br/>Attachment : <a href='{0}' target='_blank'>Click Here</a>", uri.Scheme + "://" + uri.Authority + "/Uploads/RequestTopUpWallet/" + _tblTopUp.Id + "/" + _tblTopUp.attach_name);
                }
                hashTable.Add("#content#", sb.ToString());
                //Send Sign Email End
                objclsSendEmailService.SendEmail(new[] { Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["clientMail"]) }, null, null, (new clsEmailTemplateService().GetEmailTemplate("commonadmin", hashTable)), subject);
            }
            catch (Exception)
            {
            }
        }
        public decimal yourBalance(int CustomerId)
        {
            try
            {
                decimal balance = 0.00m;
                var balanceList = Context.tblRewardAmounts.AsQueryable().Where(x => x.Fk_CustomerId == CustomerId).ToList();
                if (balanceList != null)
                {
                    balance = (balanceList.Sum(x => x.credit) ?? 0.0m) - (balanceList.Sum(x => x.debit) ?? 0.0m);
                }
                return balance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}