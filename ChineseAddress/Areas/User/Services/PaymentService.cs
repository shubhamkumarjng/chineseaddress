using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.IServices;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using Microsoft.Practices.Unity;
using PagedList;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using AutoMapper;
using System.Configuration;
using _ChineseAddress.com.Areas.User.Services;

namespace _ChineseAddress.com.Services
{
    public class PaymentService : _ChineseAddress.com.Areas.Admin.Models.clsCommon, IPaymentService<UserPaymentModel, ViewModelOrderinfo>
    {


        private IAuthorizeNetService _repository;


        public PaymentService(AuthorizeNetService repo)
        {
            _repository = repo;
        }

        [Dependency]
        public Context4InshipEntities Context { get; set; }

        [Dependency]
        public IWalletService<tblTopUp, tblTopUpMethod> _repoWalletService { get; set; }

        //[Dependency]
        //public IPayTabsService _repoPayTab { get; set; }


        #region Properties
        [Dependency]
        public PayTabsService _repoPayTab { get; set; }

        public bool _isSuccess { get; set; }
        public string _msg { get; set; }
        public tblDeliveryAddress _DeliveryAddress { get; set; }
        public List<tblOrderBox> _Packages { get; set; }
        public tblOrder _OrderDetail { get; set; }
        public decimal? _Insurancecharges { get; set; }
        #endregion Properties

        #region OrderPaymentMethods

        public IPagedList<UserPaymentModel> GetCustomerPayment(int pageNumber)
        {
            int ID = (int)HttpContext.Current.Session["UserId"];
            var plan = Context.tblCustomerPlanLinkings.AsQueryable().Where(x => x.Fk_Customer_Id == ID).FirstOrDefault();
            var query_join = (from ti in Context.tblInvoices
                              join to in Context.tblOrders on ti.Id equals to.Fk_invoice_id into tito
                              from to in tito.DefaultIfEmpty()
                              orderby ti.invoice_date descending
                              where ti.Fk_customer_id == ID
                              select new UserPaymentModel()
                              {
                                  PaymentDate = ti.invoice_date,
                                  Method = (ti.payment_method ?? "Card"),
                                  Amount = ti.payment_amount,
                                  Status = ti.paid_status,
                                  InvNo = ti.invoice_number,
                                  FK_Customer_Id = ID,
                                  InvoiceType = ti.invoice_type,
                                  PlanType = ti.plan_type,
                                  OrderId = to.id,
                                  OrderReferenceNumber = (ti.invoice_type == "order" ? to.reference_no : plan.plan_title)
                              }).ToList().ToPagedList(pageNumber, MvcApplication.global_Page_Size);
            return query_join;
        }

        public string ProcessPayment(int orderId, int creditCardId, string type)
        {
            try
            {
                int custId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                decimal payableAmount = 0.0m;

                #region accessData

                tblCustomer objCustomer = Context.tblCustomers.AsQueryable().FirstOrDefault(c => c.Id == custId);
                tblOrder objOrder = Context.tblOrders.AsQueryable().Where(o => o.id == orderId).FirstOrDefault();
                payableAmount = (objOrder.payable_amount ?? 0.0m);
                tblCreditCard objCreditCard = Context.tblCreditCards.AsQueryable().Where(x => x.Id == creditCardId).FirstOrDefault();
                tblInvoice objInvoice = new tblInvoice();
                objInvoice.Fk_customer_id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                objInvoice.Fk_billing_address_id = (objCreditCard.Fk_billing_address_id ?? 0);
                objInvoice.payment_amount = (objOrder.payable_amount ?? 0.0m);
                objInvoice.Fk_credit_card_id = creditCardId;
                objInvoice.paid_on = DateTime.Now;
                objInvoice.custom_guid = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
                objInvoice.invoice_type = "order";

                #endregion accessData

                #region GetWalletBalanceAndAddcalculatepayableAmount

                decimal walletBal = (Context.tblRewardAmounts.AsQueryable().AsParallel().Where(x => x.Fk_CustomerId == custId).Sum(x => x.credit) ?? 0.0m) - (Context.tblRewardAmounts.AsQueryable().AsParallel().Where(x => x.Fk_CustomerId == custId).Sum(x => x.debit) ?? 0.0m);
                if (walletBal >= payableAmount)
                {
                    //AddWalletConsumeAmountInRewardAmount
                    AddWalletConsumeAmountInRewardAmount(payableAmount, custId);
                    //Bypass Authorize Net Paymet and generate invoice from here.
                    //Save payment invoice
                    objInvoice.wallet_consume_amount = payableAmount;
                    objInvoice.Fk_credit_card_id = null;
                    objInvoice.invoice_number = (new clsCommonServiceRoot()).GetInvoiceNumber();
                    objInvoice.invoice_date = DateTime.Now;
                    objInvoice.paid_status = 2;
                    if (walletBal == 0.0m)
                    {
                        objInvoice.payment_method = "Coupon";
                        objInvoice.transaction_response = "Coupon success";
                    }
                    else
                    {
                        objInvoice.payment_method = "wallet";
                        objInvoice.transaction_response = "wallet success";
                    }
                    objInvoice.transaction_id = Guid.NewGuid().ToString().Replace("-", "");
                    objInvoice.transaction_status = "1";
                    Context.Configuration.ValidateOnSaveEnabled = false;
                    Context.tblInvoices.Add(objInvoice);
                    Context.SaveChanges();
                    //update Fk_invoice_id in order table
                    objOrder.Fk_invoice_id = objInvoice.Id;
                    objOrder.status = Convert.ToInt32(Areas.Admin.Models.enumorder.Awaiting_Dispatched);
                    Context.Entry(objOrder).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();

                    #region CallShippingApi

                    CallApiZeroPayableAmount(Convert.ToInt32(objOrder.Fk_delivery_address_id), objOrder.id, objOrder);

                    #endregion CallShippingApi

                    CreateOrderPaymentInvoiceAndSendAttachmentEmail(objInvoice, objOrder, objCustomer);
                    //Generate Invoice and Send Mail to customer

                    _isSuccess = true;
                    return "showMessage('Payment successfully completed.',true);";
                }
                else if (walletBal < payableAmount && walletBal > 0)
                {
                    payableAmount = payableAmount - walletBal;
                    objInvoice.payment_amount = payableAmount;
                    objInvoice.wallet_consume_amount = walletBal;
                    //AddWalletConsumeAmountInRewardAmount
                    AddWalletConsumeAmountInRewardAmount(walletBal, custId);
                }
                else
                {
                    objInvoice.wallet_consume_amount = 0;
                }

                #endregion GetWalletBalanceAndAddcalculatepayableAmount

                #region CallAuthoizeNetMethods

                _repository._InvoiceDetail = objInvoice;
                _repository._CreditCard = objCreditCard;
                _repository._CustomerDetail = objCustomer;
                _repository._orderDetail = objOrder;
                if (_repository.ChargeCustomerProfile())
                {
                    _isSuccess = true;

                    #region CallShippingApi

                    CallShippingApi();

                    #endregion CallShippingApi

                    #region CreateOrderPaymentInvoiceAndSendAttachmentEmail

                    CreateOrderPaymentInvoiceAndSendAttachmentEmail(_repository._InvoiceDetail, _repository._orderDetail, objCustomer);

                    #endregion CreateOrderPaymentInvoiceAndSendAttachmentEmail

                    return _repository._msg;
                }
                else
                {
                    _isSuccess = false;
                    return _repository._msg;
                }

                #endregion CallAuthoizeNetMethods
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ProcessPaymentReturnShipment(int receivedShipmentId, int custId, decimal charge)
        {
            try
            {
                #region accessData
                decimal payableAmount;
                if (Context == null)
                {
                    Context = new Context4InshipEntities();
                }
                tblCustomer objCustomer = Context.tblCustomers.AsQueryable().FirstOrDefault(c => c.Id == custId);
                payableAmount = charge;
                tblInvoice objInvoice = new tblInvoice();
                objInvoice.Fk_customer_id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                //save biiling address end start
                //deafult address to billing address
                //Insert into  tblBillingAddress from  tblAddressBook start
                var tblAddressBook = Context.tblAddressBooks.AsNoTracking().AsQueryable().Where(x => x.Fk_customer_Id == custId && x.is_default == true).FirstOrDefault();
                //Auto  Mapper to Map  tblAddressBook  To tblBillingAddress
                Mapper.Initialize(cfg =>
                {
                    cfg.ReplaceMemberName("address1", "address");
                    cfg.ReplaceMemberName("mobile1", "mobile");
                    cfg.CreateMap<tblAddressBook, tblBillingAddress>();
                });
                Mapper.Configuration.CompileMappings();

                tblBillingAddress tblBillingAddress = AutoMapper.Mapper.Map<tblBillingAddress>(tblAddressBook);
                tblBillingAddress.email = objCustomer.email;
                tblBillingAddress.created_on = DateTime.Now;
                Context.tblBillingAddresses.Add(tblBillingAddress);
                Context.SaveChanges();
                //save biiling address end

                //Create invoce start
                objInvoice.Fk_billing_address_id = tblBillingAddress.Id;
                objInvoice.payment_amount = payableAmount;
                objInvoice.Fk_credit_card_id = null;
                objInvoice.paid_on = DateTime.Now;
                objInvoice.custom_guid = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
                objInvoice.invoice_type = "Return Shipment";

                #endregion accessData


                decimal walletBal = (Context.tblRewardAmounts.AsQueryable().AsParallel().Where(x => x.Fk_CustomerId == custId).Sum(x => x.credit) ?? 0.0m) - (Context.tblRewardAmounts.AsQueryable().AsParallel().Where(x => x.Fk_CustomerId == custId).Sum(x => x.debit) ?? 0.0m);
                if (walletBal >= payableAmount)
                {
                    //AddWalletConsumeAmountInRewardAmount
                    AddWalletConsumeAmountInRewardAmount(payableAmount, custId);
                    //Bypass Authorize Net Paymet and generate invoice from here.
                    //Save payment invoice
                    objInvoice.wallet_consume_amount = payableAmount;
                    objInvoice.Fk_customer_id = custId;
                    objInvoice.Fk_credit_card_id = null;
                    objInvoice.invoice_number = (new clsCommonServiceRoot()).GetInvoiceNumber();
                    objInvoice.invoice_date = DateTime.Now;
                    objInvoice.paid_status = 2;

                    objInvoice.payment_method = "wallet";
                    objInvoice.transaction_response = "wallet success";

                    objInvoice.transaction_id = Guid.NewGuid().ToString().Replace("-", "");
                    objInvoice.transaction_status = "1";
                    Context.Configuration.ValidateOnSaveEnabled = false;
                    Context.tblInvoices.Add(objInvoice);
                    Context.SaveChanges();
                    //update Fk_invoice_id in order table
                    //objOrder.Fk_invoice_id = objInvoice.Id;
                    //objOrder.status = Convert.ToInt32(Areas.Admin.Models.enumorder.Awaiting_Dispatched);
                    //Context.Entry(objOrder).State = System.Data.Entity.EntityState.Modified;
                    //Context.SaveChanges();


                    //CreateOrderPaymentInvoiceAndSendAttachmentEmail(objInvoice, objOrder, objCustomer);
                    //Generate Invoice and Send Mail to customer
                    var receivedShipment = Context.tblReceivedShipments.AsQueryable().Where(x => x.id == receivedShipmentId).FirstOrDefault();
                    receivedShipment.FK_Return_Invoice_Id = objInvoice.Id;
                    Context.Entry(receivedShipment).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();

                    //send email  with attachment

                    CreateReturnShipmentPaymentInvoiceAndSendAttachmentEmail(objInvoice, receivedShipment, objCustomer, tblBillingAddress, charge);

                    _isSuccess = true;
                    _msg = SuccessMsg("Payment successfully completed.");
                }
                else
                {
                    _msg = ErrorMsg("Customer doesn't have a enough amount in their wallet");
                    _isSuccess = false;
                }



            }
            catch (Exception ex)
            {
                _isSuccess = false;
                _msg = ex.Message.ToString();

            }
            return _isSuccess;
        }

        public string ProcessPaymentViaWallet(int orderId, int addressBookId, string type)
        {
            try
            {
                int custId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                decimal payableAmount = 0.0m;
                decimal walletBal = _repoWalletService.yourBalance(custId);

                tblCustomer objCustomer = Context.tblCustomers.AsQueryable().FirstOrDefault(c => c.Id == custId);
                tblOrder objOrder = Context.tblOrders.AsQueryable().Where(o => o.id == orderId).FirstOrDefault();
                payableAmount = (objOrder.payable_amount ?? 0.0m);
                #region Check Wallet Balance

                if (walletBal < objOrder.payable_amount)
                {
                    _isSuccess = false;
                    return ErrorMsg("you don not have sufficient balance in your wallet <a href='/wallet/topup'>click here</a> to recharge you  wallet.");
                }
                #endregion

                #region create billing address ,create invoice



                //Insert into  tblBillingAddress from  tblAddressBook start
                var tblAddressBook = Context.tblAddressBooks.AsQueryable().Where(x => x.Id == addressBookId).FirstOrDefault();
                //Auto  Mapper to Map  tblAddressBook  To tblBillingAddress
                Mapper.Initialize(cfg =>
                {
                    cfg.ReplaceMemberName("address1", "address");
                    cfg.ReplaceMemberName("mobile1", "mobile");
                    cfg.CreateMap<tblAddressBook, tblBillingAddress>();
                });
                Mapper.Configuration.CompileMappings();

                tblBillingAddress tblBillingAddress = AutoMapper.Mapper.Map<tblBillingAddress>(tblAddressBook);
                tblBillingAddress.email = objCustomer.email;
                tblBillingAddress.created_on = DateTime.Now;
                Context.tblBillingAddresses.Add(tblBillingAddress);
                Context.SaveChanges();
                //Insert into  tblBillingAddress from  tblAddressBook end

                tblInvoice objInvoice = new tblInvoice();
                objInvoice.Fk_customer_id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                objInvoice.Fk_billing_address_id = tblBillingAddress.Id;
                objInvoice.payment_amount = payableAmount;
                //objInvoice.Fk_credit_card_id = creditCardId;
                objInvoice.paid_on = DateTime.Now;
                objInvoice.custom_guid = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
                objInvoice.invoice_type = "order";

                #endregion

                #region do Payment and create invoice

                //AddWalletConsumeAmountInRewardAmount
                AddWalletConsumeAmountInRewardAmount(payableAmount, custId);
                //Bypass Authorize Net Paymet and generate invoice from here.
                //Save payment invoice
                objInvoice.wallet_consume_amount = payableAmount;
                objInvoice.Fk_credit_card_id = null;
                objInvoice.invoice_number = (new clsCommonServiceRoot()).GetInvoiceNumber();
                objInvoice.invoice_date = DateTime.Now;
                objInvoice.paid_status = 2;
                if (payableAmount == 0.0m)
                {
                    objInvoice.payment_method = "Coupon";
                    objInvoice.transaction_response = "Coupon success";
                }
                else
                {
                    objInvoice.payment_method = "wallet";
                    objInvoice.transaction_response = "wallet success";
                }
                objInvoice.transaction_id = Guid.NewGuid().ToString().Replace("-", "");
                objInvoice.transaction_status = "1";
                Context.Configuration.ValidateOnSaveEnabled = false;
                Context.tblInvoices.Add(objInvoice);
                Context.SaveChanges();
                //update Fk_invoice_id in order table
                objOrder.Fk_invoice_id = objInvoice.Id;
                objOrder.status = Convert.ToInt32(Areas.Admin.Models.enumorder.Awaiting_Dispatched);
                Context.Entry(objOrder).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();

                #region CallShippingApi

                CallApiZeroPayableAmount(Convert.ToInt32(objOrder.Fk_delivery_address_id), objOrder.id, objOrder);

                #endregion CallShippingApi

                CreateOrderPaymentInvoiceAndSendAttachmentEmail(objInvoice, objOrder, objCustomer);
                //Generate Invoice and Send Mail to customer

                _isSuccess = true;
                return "showMessage('Payment successfully completed.',true);";

                #endregion 
            }
            catch (Exception ex)
            {
                _isSuccess = false;
                return ErrorMsg(ex.Message);
            }
        }



        private void CallApiZeroPayableAmount(int _Fk_Delivery_id, int _Order_id, tblOrder _orderdetails)
        {
            try
            {
                _DeliveryAddress = Context.tblDeliveryAddresses.AsQueryable().Where(x => x.id == _Fk_Delivery_id).FirstOrDefault();
                _Packages = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == _Order_id).ToList();
                clsCarriersService objclsCarriersService = new clsCarriersService();
                objclsCarriersService._ToAddress = _DeliveryAddress;
                objclsCarriersService._Packages = _Packages;
                objclsCarriersService._orderdetails = _orderdetails;
                objclsCarriersService._Insurancecharges = _Insurancecharges;
                //check Flag enable disable shipping API
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsEnabledCreateShipmentApi"]))
                {
                    objclsCarriersService.CallShippingApi();
                    if (objclsCarriersService._IsSuccess)
                    {
                        _OrderDetail = objclsCarriersService._orderdetails;

                        UpdateOrderDispatchedandOrderBoxes();
                    }
                }

            }
            catch (Exception ex)
            {
                //error send email to Admin
                throw ex;
            }
        }
        private void CallShippingApi()
        {
            try
            {
                _DeliveryAddress = Context.tblDeliveryAddresses.AsQueryable().Where(x => x.id == _repository._orderDetail.Fk_delivery_address_id).FirstOrDefault();
                _Packages = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == _repository._orderDetail.id).ToList();
                clsCarriersService objclsCarriersService = new clsCarriersService();
                objclsCarriersService._ToAddress = _DeliveryAddress;
                objclsCarriersService._Packages = _Packages;
                objclsCarriersService._Insurancecharges = _Insurancecharges;
                objclsCarriersService._orderdetails = _repository._orderDetail;
                objclsCarriersService.CallShippingApi();
                if (objclsCarriersService._IsSuccess)
                {
                    _OrderDetail = objclsCarriersService._orderdetails;

                    UpdateOrderDispatchedandOrderBoxes();
                }


            }
            catch (Exception ex)
            {
                //error send email to Admin
                throw ex;
            }
        }

        private void UpdateOrderDispatchedandOrderBoxes()
        {

            //Update tblOrderBoxes Tacking no
            List<tblOrderBox> lstOrderBoxes = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == _OrderDetail.id).ToList();
            foreach (tblOrderBox package in lstOrderBoxes)
            {
                package.tracking_no = _Packages.AsQueryable().Where(x => x.id == package.id).Select(x => x.tracking_no).FirstOrDefault();
            }
            Context.SaveChanges();

            _OrderDetail.status = (int)Areas.Admin.Models.enumorder.Dispatched;
            Context.tblOrders.Attach(_OrderDetail);
            Context.Entry(_OrderDetail).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
            clsCommonServiceRoot _objclsCommonServiceRoot = new clsCommonServiceRoot();
            _objclsCommonServiceRoot.CreateOrderCustomPerfomaInvoice(Convert.ToInt32(_OrderDetail.id));
            AddOrderRewardPoints();
        }

        private void AddOrderRewardPoints()
        {
            try
            {
                int userid = _OrderDetail.Fk_customer_id;
                decimal rewardpoints = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["ReferralOrderCreatePoints"]);
                var GetCustomerEmail = (from tbcust in Context.tblCustomers
                                        join tbAdreesbook in Context.tblAddressBooks on tbcust.Id equals tbAdreesbook.Fk_customer_Id
                                        select new { tbcust.email, tbcust.Id, tbAdreesbook.first_name, tbAdreesbook.is_default, tbAdreesbook.last_name }).Where(x => x.Id == userid && x.is_default == true).FirstOrDefault();
                if (GetCustomerEmail != null)
                {
                    string subject = "chineseaddress - Reward Points!";
                    Hashtable htTemplate = new Hashtable();
                    htTemplate.Add("#FirstName", GetCustomerEmail.first_name);
                    htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
                    htTemplate.Add("#rewardpoints#", rewardpoints);
                    string mailbody = (new clsEmailTemplateService()).GetEmailTemplate("rewardpoint", htTemplate);
                    (new clsSendEmailService()).SendEmail(new[] { GetCustomerEmail.email }, null, null, mailbody, subject);
                }
                Context.tblRewardPoints.Add(new tblRewardPoint() { Fk_CustomerId = userid, is_converted = false, type = "order", created_on = DateTime.Now, reward_points = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["ReferralOrderCreatePoints"]) });
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CreateOrderPaymentInvoiceAndSendAttachmentEmail(tblInvoice objInvoice, tblOrder objOrder, tblCustomer objCustomer)
        {
            try
            {
                HttpServerUtility server = HttpContext.Current.Server;
                tblBillingAddress objBillingAddress = Context.tblBillingAddresses.AsQueryable().FirstOrDefault(x => x.Id == objInvoice.Fk_billing_address_id);
                int totalPackages = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == objOrder.id).Count();
                int totalReceivedShipments = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == objOrder.id).Count();
                string countryName = Context.tblCountryMasters.AsQueryable().AsParallel().Where(x => x.country_code == objBillingAddress.country_code).Select(x => x.country_name).FirstOrDefault();

                #region GetTemplateFromHtmlFileAndMakeInvoice

                StringBuilder sbTemplate = new StringBuilder();
                // sbTemplate.Append(File.ReadAllText(HttpContext.Current.Server.MapPath("~/Areas/User/CreateInvoiceHTML.html")));
                System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                // hashTable.Add("#logo#", HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Content/img/logo.png");
                hashTable.Add("#suite#", objCustomer.customer_reference);
                hashTable.Add("#invoicno#", objInvoice.invoice_number);
                hashTable.Add("#invoicedate#", (objInvoice.invoice_date ?? DateTime.Now).ToString("yyyy/MM/dd"));
                hashTable.Add("#invoicetotal#", String.Format("{0:0.00}", objOrder.payable_amount));
                hashTable.Add("#toname#", objBillingAddress.first_name + " " + objBillingAddress.last_name);
                hashTable.Add("#toaddress#", objBillingAddress.address ?? "");
                hashTable.Add("#tocity#", objBillingAddress.city);
                hashTable.Add("#tocountry#", countryName);
                hashTable.Add("#totelephone#", objBillingAddress.mobile);
                hashTable.Add("#tozip#", objBillingAddress.post_code);
                hashTable.Add("#tostate#", objBillingAddress.state);
                hashTable.Add("#amount#", Convert.ToString((objOrder.payable_amount ?? 0.0m) - (objInvoice.wallet_consume_amount ?? 0.0m)));
                hashTable.Add("#wallet_amount#", Convert.ToString(objInvoice.wallet_consume_amount ?? 0.0m));
                hashTable.Add("#demototal#", Convert.ToString(objOrder.payable_amount));
                hashTable.Add("#ordrefno#", objOrder.reference_no);
                hashTable.Add("#date#", (objOrder.created_on ?? DateTime.Now).ToString("yyyy/MM/dd"));
                hashTable.Add("#trackingno#", (objOrder.tracking_no ?? "N/A"));
                hashTable.Add("#packages#", Convert.ToString(totalPackages));
                hashTable.Add("#shipments#", Convert.ToString(totalReceivedShipments));


                if (!Directory.Exists(server.MapPath("~/Uploads/") + "Orders"))
                {
                    Directory.CreateDirectory(server.MapPath("~/Uploads/") + "Orders");
                }
                if (!Directory.Exists(server.MapPath("~/Uploads/Orders/") + objOrder.id.ToString()))
                {
                    Directory.CreateDirectory(server.MapPath("~/Uploads/Orders/") + objOrder.id.ToString());
                }
                string Invoicetemplate = (new clsEmailTemplateService()).GetEmailTemplate("orderinvoice", hashTable);
                string strInvFilePath = server.MapPath($"~/Uploads/Orders/{objOrder.id.ToString()}/") + objOrder.id.ToString() + "_Invoice.pdf";
                string CssInvoicePath = server.MapPath("~/Content/css/SignUpInvoice.min.css");
                clsHTMLtoPDF objclsHTMLtoPDF = new clsHTMLtoPDF();
                bool OrderInvoice = true;
                objclsHTMLtoPDF.ConvertHTMLToPDF(Invoicetemplate, CssInvoicePath, strInvFilePath, OrderInvoice);

                #endregion GetTemplateFromHtmlFileAndMakeInvoice

                #region SendEmailWithAttachment

                var GetCustomerEmail = (from tbcust in Context.tblCustomers
                                        join tbord in Context.tblOrders on tbcust.Id equals tbord.Fk_customer_id
                                        join tbAddressbook in Context.tblAddressBooks on tbcust.Id equals tbAddressbook.Fk_customer_Id
                                        select new { tbcust.email, tbAddressbook.first_name, tbAddressbook.last_name, tbAddressbook.is_default, tbord.id, tbord.Fk_customer_id, tbord.reference_no, tbord.tracking_no }).Where(x => x.id == objOrder.id && x.is_default == true).FirstOrDefault();
                hashTable.Clear();

                if (GetCustomerEmail != null)
                {
                    hashTable.Add("#FirstName#", GetCustomerEmail.first_name);
                    hashTable.Add("#LastName#", GetCustomerEmail.last_name);
                    hashTable.Add("#trackingno#", GetCustomerEmail.tracking_no);
                    hashTable.Add("#referenceno#", objOrder.reference_no);
                }
                string emailBody = new clsEmailTemplateService().GetEmailTemplate("orderpayment", hashTable);
                new clsSendEmailService().SendEmail(new[] { objCustomer.email }, null, null, emailBody, "chineseaddress - Order Dispatch (invoice with attachment)", new[] { strInvFilePath });

                #region Send Email to Customer order disparched
                clsCommonServiceRoot _objclsCommonServiceRoot = new clsCommonServiceRoot();
                if (_OrderDetail != null)
                    _objclsCommonServiceRoot.SendOrderDispatchedEmail(Convert.ToInt32(_OrderDetail.id), _OrderDetail.tracking_no);

                #endregion Send Email to Customer order disparched
                #endregion SendEmailWithAttachment
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void CreateReturnShipmentPaymentInvoiceAndSendAttachmentEmail(tblInvoice objInvoice, tblReceivedShipment objReceivedShipment, tblCustomer objCustomer, tblBillingAddress objBillingAddress, decimal charge)
        {
            try
            {
                HttpServerUtility server = HttpContext.Current.Server;
                //tblBillingAddress objBillingAddress = Context.tblBillingAddresses.AsQueryable().FirstOrDefault(x => x.Id == objInvoice.Fk_billing_address_id);
                //int totalPackages = Context.tblOrderBoxes.AsQueryable().Where(x => x.Fk_order_id == objOrder.id).Count();
                //int totalReceivedShipments = Context.tblReceivedShipments.AsQueryable().Where(x => x.Fk_order_id == objOrder.id).Count();
                string countryName = Context.tblCountryMasters.AsQueryable().AsParallel().Where(x => x.country_code == objBillingAddress.country_code).Select(x => x.country_name).FirstOrDefault();

                #region GetTemplateFromHtmlFileAndMakeInvoice

                StringBuilder sbTemplate = new StringBuilder();
                // sbTemplate.Append(File.ReadAllText(HttpContext.Current.System.Web.HttpContext.Current.Server.MapPath("~/Areas/User/CreateInvoiceHTML.html")));
                System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                // hashTable.Add("#logo#", HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Content/img/logo.png");
                hashTable.Add("#suite#", objCustomer.customer_reference);
                hashTable.Add("#invoicno#", objInvoice.invoice_number);
                hashTable.Add("#invoicedate#", (objInvoice.invoice_date ?? DateTime.Now).ToString("yyyy/MM/dd"));
                hashTable.Add("#toname#", objBillingAddress.first_name + " " + objBillingAddress.last_name);
                hashTable.Add("#toaddress#", objBillingAddress.address ?? "");
                hashTable.Add("#tocity#", objBillingAddress.city);
                hashTable.Add("#tocountry#", countryName);
                hashTable.Add("#totelephone#", objBillingAddress.mobile);
                hashTable.Add("#tozip#", objBillingAddress.post_code);
                hashTable.Add("#tostate#", objBillingAddress.state);
                hashTable.Add("#amount#", objInvoice.payment_amount.ToString("F"));
                hashTable.Add("#payment_method#", objInvoice.payment_method);
                hashTable.Add("#type#", objInvoice.invoice_type);
                hashTable.Add("#id#", objReceivedShipment.id);
                hashTable.Add("#date#", (objInvoice.paid_on ?? DateTime.Now).ToString("yyyy/MM/dd"));

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/") + "ReturnShipmentInvoices"))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/") + "ReturnShipmentInvoices");
                }
                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/ReturnShipmentInvoices/") + objCustomer.Id))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/ReturnShipmentInvoices/") + objCustomer.Id);
                }

                //
                string Invoicetemplate = (new clsEmailTemplateService()).GetEmailTemplate("return_shipment_invoice", hashTable);
                string strInvFilePath = System.Web.HttpContext.Current.Server.MapPath($"~/Uploads/ReturnShipmentInvoices/{objCustomer.Id}/") + objInvoice.invoice_number.ToString() + "_ReturnShipment_Invoice.pdf";
                string CssInvoicePath = System.Web.HttpContext.Current.Server.MapPath("~/Content/css/SignUpInvoice.min.css");
                clsHTMLtoPDF objclsHTMLtoPDF = new clsHTMLtoPDF();
                bool OrderInvoice = false;
                objclsHTMLtoPDF.ConvertHTMLToPDF(Invoicetemplate, CssInvoicePath, strInvFilePath, OrderInvoice);

                #endregion GetTemplateFromHtmlFileAndMakeInvoice

                #region SendEmailWithAttachment

                var GetCustomerEmail = (from tbcust in Context.tblCustomers
                                        join tbAddressbook in Context.tblAddressBooks on tbcust.Id equals tbAddressbook.Fk_customer_Id
                                        select new { tbcust.Id, tbcust.email, tbAddressbook.first_name, tbAddressbook.last_name, tbAddressbook.is_default }).Where(x => x.Id == objCustomer.Id && x.is_default == true).FirstOrDefault();
                hashTable.Clear();

                //string emailBody = new clsEmailTemplateService().GetEmailTemplate("orderpayment", hashTable);
                //new clsSendEmailService().SendEmail(new[] { objCustomer.email }, null, null, emailBody, "chineseaddress - Order Dispatch (invoice with attachment)", new[] { strInvFilePath });

                //#region Send Email to Customer order disparched
                //clsCommonServiceRoot _objclsCommonServiceRoot = new clsCommonServiceRoot();
                //if (_OrderDetail != null)
                //    _objclsCommonServiceRoot.SendOrderDispatchedEmail(Convert.ToInt32(_OrderDetail.id), _OrderDetail.tracking_no);
                if (GetCustomerEmail != null)
                {
                    string subject = "chineseaddress- Shipment Return Accepted! (invoice with attachment)";
                    clsEmailTemplateService clscommn = new clsEmailTemplateService();
                    Hashtable htTemplate = new Hashtable();
                    htTemplate.Add("#FirstName#", GetCustomerEmail.first_name);
                    htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
                    htTemplate.Add("#id#", objReceivedShipment.id);
                    htTemplate.Add("#charge#", charge);
                    string mailbody = (new clsEmailTemplateService()).GetEmailTemplate("shipmentreturnaccept", htTemplate);
                    (new clsSendEmailService()).SendEmail(new[] { GetCustomerEmail.email }, null, null, mailbody, subject, new[] { strInvFilePath });
                }
                #endregion Send Email to Customer order disparched
                #endregion SendEmailWithAttachment
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void AddWalletConsumeAmountInRewardAmount(decimal consumeAmount, int custId)
        {
            Context.tblRewardAmounts.Add(new tblRewardAmount() { Fk_CustomerId = custId, debit = consumeAmount, credit = 0.0m, created_on = DateTime.Now });
            Context.SaveChanges();
        }

        public ViewModelOrderinfo GetOrderDetail(int OrderId)
        {
            ViewModelOrderinfo objViewModelOrderinfo = new ViewModelOrderinfo();
            try
            {
                decimal balance = 0;
                int id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                var balanceList = Context.tblRewardAmounts.AsQueryable().Where(x => x.Fk_CustomerId == id).ToList();
                if (balanceList != null)
                {
                    balance = (balanceList.Sum(x => x.credit) ?? 0.0m) - (balanceList.Sum(x => x.debit) ?? 0.0m);
                }
                objViewModelOrderinfo = (from to in Context.tblOrders
                                         where to.id == OrderId
                                         select new ViewModelOrderinfo
                                         {
                                             id = to.id,
                                             orderon = to.created_on,
                                             orderno = to.reference_no,
                                             shipmethod = to.carrier,
                                             totalamount = to.payable_amount ?? 0.0m,
                                             status = to.status,
                                             WalletBalance = balance
                                         }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objViewModelOrderinfo;
        }


        public string ProcessUpgradePlanPayment(int PlanId, int creditCardId)
        {
            int custId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);

            var objCustomerPlanLinking = Context.tblCustomerPlanLinkings.AsQueryable().Where(x => x.Fk_Customer_Id == custId).OrderByDescending(x => x.Id).FirstOrDefault();

            tblCreditCard objCreditCard = Context.tblCreditCards.AsQueryable().Where(x => x.Id == creditCardId).FirstOrDefault();

            tblCustomer objCustomer = Context.tblCustomers.AsQueryable().Where(X => X.Id == custId).FirstOrDefault();
            tblBillingAddress objBillingAdress = Context.tblBillingAddresses.AsQueryable().Where(x => x.Id == objCreditCard.Fk_billing_address_id).FirstOrDefault();
            tblPlan objPlan = Context.tblPlans.AsQueryable().Where(X => X.Id == PlanId).FirstOrDefault();

            tblInvoice objInvoice = new tblInvoice();
            objInvoice.Fk_customer_id = custId;
            objInvoice.plan_type = objPlan.title;
            objInvoice.Fk_credit_card_id = objCreditCard.Id;
            objInvoice.Fk_billing_address_id = objCreditCard.Fk_billing_address_id ?? 0;
            objInvoice.paid_on = DateTime.Now;
            objInvoice.payment_amount = Math.Abs((objPlan.price ?? 0.0m) - objCustomerPlanLinking.plan_amount);
            objInvoice.custom_guid = Guid.NewGuid().ToString().Substring(0, 20);
            objInvoice.invoice_number = (new clsCommonServiceRoot()).GetInvoiceNumber();
            objInvoice.invoice_type = "signup";
            objInvoice.plan_type = objPlan.title;
            //Genrate Invoice On Upgrade Plan End//
            #region CallAuthoizeNetMethods
            _repository._InvoiceDetail = objInvoice;
            _repository._CreditCard = objCreditCard;
            _repository._CustomerDetail = objCustomer;
            if (_repository.ChargeCustomerProfile())
            {
                //UPdate Customer Plan
                objCustomerPlanLinking.plan_title = objPlan.title;
                objCustomerPlanLinking.plan_amount = objPlan.price ?? 0.0m;
                objCustomerPlanLinking.free_storage_days = objPlan.free_storage_days;
                Context.Entry(objCustomerPlanLinking).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
                //Genrate Invoice On Upgrade Plan Start//
                ViewCustomerPaymentModel objViewCustomerPaymentModel = new ViewCustomerPaymentModel();
                ClsCommanCustomerSignup objClsCommanCustomerSignup = new ClsCommanCustomerSignup();
                objViewCustomerPaymentModel.InvoiceNumber = objInvoice.invoice_number;
                objViewCustomerPaymentModel.InvoiceDate = DateTime.Now;
                objViewCustomerPaymentModel.CreditCardId = objInvoice.Fk_credit_card_id;
                objViewCustomerPaymentModel.first_name = objBillingAdress.first_name;
                objViewCustomerPaymentModel.last_name = objBillingAdress.last_name;
                objViewCustomerPaymentModel.mobile = objBillingAdress.mobile;
                objViewCustomerPaymentModel.address = objBillingAdress.address;
                objViewCustomerPaymentModel.city = objBillingAdress.city;
                objViewCustomerPaymentModel.state = objBillingAdress.state;
                objViewCustomerPaymentModel.post_code = objBillingAdress.post_code;
                objViewCustomerPaymentModel.Country = objBillingAdress.country_code;
                objViewCustomerPaymentModel.Fk_customer_id = custId;
                objViewCustomerPaymentModel.PlanAmount = objInvoice.payment_amount;
                objViewCustomerPaymentModel.PlanType = objPlan.title;
                objClsCommanCustomerSignup.CreateSignUpInvoice(objViewCustomerPaymentModel);
                _isSuccess = true;
                return "showMessage('Your plan has been upgraded successfully.',true);";
            }
            else
            {
                _isSuccess = false;
                return _repository._msg;
            }

            #endregion CallAuthoizeNetMethods
        }

        //Process Wallet Payment
        public string ProcessTopUpPayment(decimal payableAmount, int BookingAddressId)
        {
            try
            {
                int custId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                tblCustomer objCustomer = Context.tblCustomers.AsQueryable().FirstOrDefault(c => c.Id == custId);

                #region Create Billing Address
                //Insert into  tblBillingAddress from  tblAddressBook start
                var tblAddressBook = Context.tblAddressBooks.AsQueryable().Where(x => x.Id == BookingAddressId).FirstOrDefault();
                //Auto  Mapper to Map  tblAddressBook  To tblBillingAddress
                Mapper.Initialize(cfg =>
                {
                    cfg.ReplaceMemberName("address1", "address");
                    cfg.ReplaceMemberName("mobile1", "mobile");
                    cfg.CreateMap<tblAddressBook, tblBillingAddress>();
                });
                Mapper.Configuration.CompileMappings();

                tblBillingAddress tblBillingAddress = AutoMapper.Mapper.Map<tblBillingAddress>(tblAddressBook);
                tblBillingAddress.email = objCustomer.email;
                tblBillingAddress.created_on = DateTime.Now;
                Context.tblBillingAddresses.Add(tblBillingAddress);
                Context.SaveChanges();
                //Insert into  tblBillingAddress from  tblAddressBook end
                #endregion  

                #region Create Invoice
                //create Invoice of add wallet payment 

                tblInvoice objInvoice = new tblInvoice();
                objInvoice.Fk_customer_id = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                objInvoice.Fk_billing_address_id = tblBillingAddress.Id;
                objInvoice.payment_amount = payableAmount;
                //objInvoice.Fk_credit_card_id = creditCardId;
                objInvoice.paid_on = DateTime.Now;
                objInvoice.custom_guid = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
                objInvoice.invoice_type = "TopUp";
                objInvoice.payment_method = "Credit/Debit Card";
                objInvoice.Fk_credit_card_id = null;
                objInvoice.paid_status = 0;
                objInvoice.invoice_date = DateTime.Now;
                Context.tblInvoices.Add(objInvoice);
                Context.Configuration.ValidateOnSaveEnabled = false;
                Context.SaveChanges();
                #endregion

                #region ProcessPayTabPayment
                string strPaymentURl = PayByPayTabsMethod(objCustomer, tblBillingAddress, objInvoice);
                if (_isSuccess == false)
                {
                    return _msg;
                }
                else
                {
                    return strPaymentURl;
                }
                #endregion

                #region update invoice

                //Save payment invoice

                #endregion
                return "";
            }
            catch (Exception ex)
            {
                _isSuccess = false;
                return ErrorMsg(ex.Message);

            }

        }

        private string PayByPayTabsMethod(tblCustomer customer, tblBillingAddress billingAddress, tblInvoice invoice)
        {
            string strPaymentURl = "";
            try
            {

                /*Pay Tab code start here*/

                string strApiKey = "";
                //Validate Secret Key
                _repoPayTab.PayTabAuthentication();
                if (_repoPayTab._isSucceded == true)
                {
                    //Create pay page
                    tblAddressBook tblAddress = Context.tblAddressBooks.AsQueryable().Where(x => x.Fk_customer_Id == customer.Id).OrderBy(x => x.created_on).Select(x => x).FirstOrDefault();
                    _repoPayTab.cc_first_name = tblAddress.first_name;
                    _repoPayTab.cc_last_name = tblAddress.last_name == "" ? tblAddress.first_name : tblAddress.last_name;
                    _repoPayTab.phone_number = billingAddress.mobile;
                    _repoPayTab.billing_address = billingAddress.first_name + " " + billingAddress.last_name;
                    _repoPayTab.city = billingAddress.city;
                    _repoPayTab.state = billingAddress.state;
                    _repoPayTab.postal_code = billingAddress.post_code;//we hv not postal code for UAE
                    _repoPayTab.country = (billingAddress.country_code == "US" || billingAddress.country_code == "CA" ? billingAddress.country_code : getISO3ByISO2(billingAddress.country_code));
                    _repoPayTab.email = customer.email;
                    _repoPayTab.amount = Convert.ToDecimal(invoice.payment_amount);
                    //_repoPayTab.discount = (string.IsNullOrEmpty(hfDiscount.Value) ? 0 : Math.Abs(Convert.ToDecimal(hfDiscount.Value)));//optional

                    _repoPayTab.reference_no = invoice.custom_guid;//optional
                    _repoPayTab.currency = ConfigurationManager.AppSettings["paytabCurrency"];
                    _repoPayTab.title = "ChineseAddress";
                    _repoPayTab.ip_customer = HttpContext.Current.Request.Params["HTTP_CLIENT_IP"] ?? HttpContext.Current.Request.UserHostAddress;
                    _repoPayTab.ip_merchant = ConfigurationManager.AppSettings["ip_merchant"].Trim();
                    _repoPayTab.unit_price = Convert.ToString(invoice.payment_amount);//if multiple then like= "12.21 || 21.20"
                    _repoPayTab.quantity = "1";
                    _repoPayTab.address_shipping = tblAddress.address1;
                    _repoPayTab.state_shipping = tblAddress.state;
                    _repoPayTab.city_shipping = tblAddress.city;
                    _repoPayTab.postal_code_shipping = tblAddress.post_code;
                    _repoPayTab.country_shipping = getISO3ByISO2(tblAddress.country_code);
                    _repoPayTab.products_per_title = "ChineseAddress TopUp Wallet";//if multiple then "MobilePhone||Charger"
                    _repoPayTab.ChannelOfOperations = "ChineseAddress Wallet";
                    _repoPayTab.ProductCategory = "TopUp Wallet";
                    _repoPayTab.ProductName = "ChineseAddress TopUp Wallet";
                    _repoPayTab.ShippingMethod = "PrePay";
                    _repoPayTab.CustomerId = customer.customer_reference;
                    _repoPayTab.msg_lang = "English";
                    _repoPayTab.return_url = System.Configuration.ConfigurationManager.AppSettings["paytabReturnURL"].ToString() + "?custom_guid=" + invoice.custom_guid;

                    _repoPayTab.is_tokenization = "TRUE";
                    _repoPayTab.is_existing_customer = (string.IsNullOrEmpty(customer.pt_token) ? "FALSE" : "TRUE");
                    _repoPayTab.pt_token = customer.pt_token;
                    _repoPayTab.pt_customer_password = customer.pt_customer_password;
                    _repoPayTab.pt_customer_email = customer.email;
                    _repoPayTab.CreatePayPage(out strPaymentURl);
                    if (_repoPayTab._isSucceded == true)
                    {
                        _isSuccess = true;
                        return strPaymentURl;
                    }
                    else
                    {
                        _isSuccess = false;
                        _msg = ErrorMsg(_repoPayTab._msg);
                        return strPaymentURl;
                    }
                }
                else
                {
                    _isSuccess = false;
                    _msg = ErrorMsg(_repoPayTab._msg);
                    return strPaymentURl;
                }
            }
            catch (Exception ex)
            {
                _isSuccess = false;
                _msg = ErrorMsg(ex.Message);
                return strPaymentURl;
            }
        }

        private string getISO3ByISO2(string country_code)
        {
            return Context.tblCountryMasters.AsQueryable().Where(x => x.country_code == country_code).Select(x => x.country_code_three_digit).FirstOrDefault();
        }
    }
}