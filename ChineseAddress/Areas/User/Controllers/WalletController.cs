using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.Areas.User.IServices;
using Microsoft.Practices.Unity;
using _ChineseAddress.com.Areas.User.Services;
using _ChineseAddress.com.Areas.User.Models;

namespace _ChineseAddress.com.Areas.User.Controllers
{

    [Authorize(Roles = "User")]
    [CustomActionFilter]
    public class WalletController : Controller
    {
        private UserCommanClass objUserCommanClass = new UserCommanClass();

        [Dependency]
        public IShipmentService<tblReceivedShipment, tblOrder, UserViewCustomerModel, UserPaymentModel, ViewRecivedShipmentModel, tblAddressBook, ViewPackagingOptionsModel, ViewOrderModel, ShipmentMethodPackage, PacakageOptionArray> _repoShipmentService { get; set; }

        [Dependency]
        public IWalletService<tblTopUp, tblTopUpMethod> _repositoryD { get; set; }

        [Dependency]
        public IPaymentService<UserPaymentModel, ViewModelOrderinfo> _repoPaymentService { get; set; }

        // GET: User/Wallet
        public ActionResult Index(int pageNumber = 1)
        {
            ViewBag.Balance = _repositoryD.yourBalance(Convert.ToInt32(Session["UserId"]));
            return View(_repositoryD.getTopUpDetailByCustomerId(Convert.ToInt32(Session["UserId"]), pageNumber));
        }

        [HttpGet]
        public ActionResult TopUpWallet(string qs)
        {
            tblTopUp objTopUp = new tblTopUp();
            try
            {
                //Bind Billing Address Drop Down start
                int defaultAddressID = 0;
                ViewBag.AdressList = objUserCommanClass.AddressTableDrpdown(Convert.ToInt32(Session["UserId"]));
                if (ViewBag.AdressList != null)
                {
                    defaultAddressID = Convert.ToInt32(ViewBag.AdressList[0].Value);
                    TempData["defaultAddress"] = defaultAddressID;
                    ViewBag.DefaultAddress = _repoShipmentService.GettblAdressData(defaultAddressID);
                }
                //Bind Billing Address Drop Down end 
                ViewBag.TopUpMethodId = _repositoryD.getTopUpMethodsAsItems();

                if (!string.IsNullOrEmpty(qs))
                {
                    objTopUp = _repositoryD.getTopUpDetailById(Convert.ToInt32(_ChineseAddress.com.Models.Cryptography.Decrypt(qs)));
                    if (!_repositoryD._isSucceeded)
                        TempData["msg"] = _repositoryD._msg;
                }
            }
            catch (Exception ex)
            {
                TempData["msg"] = $"'showMessage({ex.Message.ToString()})',false);";
                throw ex;
            }
            return View(objTopUp);
        }
        [HttpPost]
        public ActionResult TopUpWallet(tblTopUp _tblTopUp)
        {

            _tblTopUp.Fk_Customer_Id = Convert.ToInt32(Session["UserId"]);
            _tblTopUp.status = (int)eTopUpWalletStatus.Pending;
            _repositoryD.AddUpdateTopUpWalletRequest(_tblTopUp);

            TempData["msg"] = _repositoryD._msg;

            if (_repositoryD._isSucceeded)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult TopUpWalletViaPayTab(decimal amount, int BookingAddressId)
        {
            string msg = _repoPaymentService.ProcessTopUpPayment(amount, BookingAddressId);
            if (_repoPaymentService._isSuccess == true)
            {
                Response.Redirect(msg,true);
                //TempData["msg"] = "Top Up payment has been done successfully.";
            }
            else
            {
                TempData["msg"] = msg;
            }
            return RedirectToAction("TopUpWallet");
        }
        public JsonResult getTopUpMethodDescriptionByMehodId(int TopUpMethodId)
        {
            return Json(_repositoryD.getTopUpMethodDescriptionByMehodId(TopUpMethodId), JsonRequestBehavior.AllowGet);
        }

    }
}