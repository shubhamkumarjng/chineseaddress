using _ChineseAddress.com.Areas.User.Services;
using _ChineseAddress.com.Repository;
using Microsoft.Practices.Unity;
using System.Linq;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.User.Controllers
{
    [Authorize(Roles = "User")]
    [CustomActionFilter]
    public class TrackingController : Controller
    {
        [Dependency]
        public TrackingService _repo { get; set; }

        [Dependency]
        public Context4InshipEntities Context { get; set; }

        public TrackingController()
        {
        }

        // GET: User/Tracking
        public ActionResult Index(string Track)
        {
            var isTrackingExists = Context.tblOrders.AsQueryable().Where(x => x.tracking_no == Track).Select(x => x.tracking_no).Union(Context.tblOrderBoxes.AsQueryable().Where(x => x.tracking_no == Track).Select(x => x.tracking_no)).FirstOrDefault();
            if (isTrackingExists != null)
            {
               
                _repo.TrackOrder(Track);
                if (!_repo._isSuccess)
                {
                    ViewBag.Message = $"showMessage('No information found for this tracking number.',false)";
                }
            }
            else
                ViewBag.Message = $"showMessage('Invalid tracking number.',false)";
            return View(_repo._TrackingDetail);
        }
    }
}