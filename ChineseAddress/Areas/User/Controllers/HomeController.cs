using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.Repository;
using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.User.Controllers
{
    [Authorize(Roles = "User")]
    [CustomActionFilter]
    public class HomeController : Controller
    {
        // GET: User/Home
        private IShipmentService<tblReceivedShipment, tblOrder, UserViewCustomerModel, UserPaymentModel, ViewRecivedShipmentModel, tblAddressBook, ViewPackagingOptionsModel, ViewOrderModel, ShipmentMethodPackage, PacakageOptionArray> _repository;

        public HomeController(IShipmentService<tblReceivedShipment, tblOrder, UserViewCustomerModel, UserPaymentModel, ViewRecivedShipmentModel, tblAddressBook, ViewPackagingOptionsModel, ViewOrderModel, ShipmentMethodPackage, PacakageOptionArray> repo)
        {
            _repository = repo;
        }

        public async Task<ActionResult> Index()
        {
            int ID = Convert.ToInt32(Session["UserId"]);
            Task<string> refno = Task.Run(() => _repository.GetCustomerRefNo(ID));
            var ShipmentList = _repository.GetRecievedShipment(ID);
            ViewBag.OrderList = _repository.GetOrderRecordList(ID);
            ViewBag.CustomerDetail = _repository.GetCustomerPlans(ID);
            ViewBag.CustomerPayment = _repository.GetCustomerPayment(ID);
            RewardpointsandBalance();
            ViewBag.ShowAddress = (new clsCommonServiceRoot()).IsUserTopUpWithMinAmount(ID);
            ViewBag.refno = await refno;
            return View(ShipmentList);
        }

        private void RewardpointsandBalance()
        {
            decimal Balance = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRatePerhunderedRewardPoints"]);
            ViewBag.yourpoints = _repository.yourpoints();
            ViewBag.yourcreditAmount = _repository.yourBalance();
            Balance = Balance * 100.00m;
            ViewBag.yourbalance = Convert.ToDecimal(ViewBag.yourpoints) / Balance;
        }
    }
}