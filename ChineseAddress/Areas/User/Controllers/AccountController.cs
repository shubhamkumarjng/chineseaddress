using System.Web.Mvc;
using System.Web.Security;

namespace _ChineseAddress.com.Areas.User.Controllers
{
    public class AccountController : Controller
    {
        // GET: User/Account
        public ActionResult Index()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "Customer", new { area = "" });
        }
    }
}