using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Areas.User.Services;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using PagedList;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.User.Controllers
{
    [Authorize(Roles = "User")]
    [CustomActionFilter]
    public class PaymentController : Controller
    {
        private ISettingService<tblAddressBook, tblCustomer, tblCreditCard, tblPlan, PlansOptionModel, tblRewardPoint, ViewCustomerPaymentModel, tblCreditCardViewModel, tblCustomerPlanLinking> _repositorySetting;
        private IPaymentService<UserPaymentModel, ViewModelOrderinfo> _repository;

        public PaymentController(IPaymentService<UserPaymentModel, ViewModelOrderinfo> repo, SettingService repoSetting)
        {
            _repository = repo;
            _repositorySetting = repoSetting;
        }

        // GET: Payment
        // [Route("user/Payment")]
        [HttpGet]
        public ActionResult Index(string id, string type)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    ViewBag.order_id = id;
                    TempData["PaymentOrerId"] = Cryptography.Decrypt(id);
                }
                if (type != null)
                {
                    TempData["PaymentOf"] = type;
                }
                //get Credit Cards Detail
                int orderId = Convert.ToInt32(TempData["PaymentOrerId"]);
                TempData.Keep("PaymentOrerId");

                ViewModelOrderinfo objViewModelOrderinfo = _repository.GetOrderDetail(orderId);
                List<tblCreditCardViewModel> tblcreditcards = _repositorySetting.GetCreaditCards();
                return View(Tuple.Create(tblcreditcards, objViewModelOrderinfo));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string CreditCardId)
        {
            try
            {
                int CardId = 0;
                int orderId = 0;
                string type = "";
                if (TempData["PaymentOrerId"] != null)
                {
                    TempData.Keep("PaymentOrerId");
                    ViewBag.order_id = Cryptography.Encrypt(Convert.ToString(TempData["PaymentOrerId"]));
                    TempData.Keep("PaymentOrerId");
                    orderId = Convert.ToInt32(TempData["PaymentOrerId"]);
                    TempData.Keep("PaymentOrerId");
                }
                if (TempData["PaymentOf"] != null)
                {
                    type = Convert.ToString(TempData["PaymentOf"]);
                    TempData.Keep("PaymentOf");
                }
                else
                {
                    ViewBag.Message = "showMessage('Error occured while processing your request.',false)";
                }
                if (!string.IsNullOrEmpty(CreditCardId))
                {
                    CardId = Convert.ToInt32(Cryptography.Decrypt(CreditCardId));
                    ViewBag.Message = _repository.ProcessPayment(orderId, CardId, type);
                    if (_repository._isSuccess)
                    {
                        TempData["successPayment"] = ViewBag.Message;
                        return RedirectToAction("OrderDetails", "Order");
                    }
                }
                else
                {
                    ViewBag.Message = "showMessage('Error occured while processing your request.',false)";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            List<tblCreditCardViewModel> tblcreditcards = _repositorySetting.GetCreaditCards();
            return View(tblcreditcards);
        }

        [HttpGet]
        public ActionResult PaymentHistory(int? pageNumber)
        {
            try
            {
                IPagedList<UserPaymentModel> CustomerPayments = _repository.GetCustomerPayment(pageNumber ?? 1);
                return View(CustomerPayments);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult PaymentPlanUpgrade(string id, int? creditCardId, int pageNumber = 1)
        {
            int PlanId = 0;
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.PlanId = id;
                PlanId = Convert.ToInt32(Cryptography.Decrypt(id));
            }
            if (Request.RequestType.ToLower() != "get")
            {
                string msg = _repository.ProcessUpgradePlanPayment(PlanId, creditCardId ?? 0);
                ViewBag.Message = msg;
            }

            List<tblCreditCardViewModel> tblcreditcards = _repositorySetting.GetCreaditCards();
            return View(tblcreditcards);
        }
    }
}