using _ChineseAddress.com.Common.Services;
using System.IO;
using System.Text;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.User.Controllers
{
    public class TestController : Controller
    {
        // GET: User/Test
        //InvoiceService Context = new InvoiceService();
        public ActionResult Index()
        {
            clsSendEmailService objSendEmail = new clsSendEmailService();
            //objSendEmail.SendEmail(new[] { "shubham.kumar@jngitsolutions.com" }, new[] { "shubham.kumar@jngitsolutions.com" }, new[] { "shubham.kumar@jngitsolutions.com" }, "Test Attachment", "Test Attachment", new[] { Server.MapPath("~/uploads/PackagesImages/1/10e2a330-7c71-4efd-a30f-37b4cba85d37.jpg") });
            return View();
        }

        public ActionResult CreateInvoice(int OrderID = 9)
        {
            clsEmailTemplateService objclsEmailTemplateService = new clsEmailTemplateService();
            System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
            //hashTable = Context.GetPerformaInvoiceReport(Convert.ToInt32(OrderID));
            StringBuilder sb = new StringBuilder();

            string htmlTemplate = System.IO.File.ReadAllText(Server.MapPath("~/Areas/User/CreateInvoiceHTML.html"));
            sb.Append(htmlTemplate);
            string Invoicetemplate = objclsEmailTemplateService.GetEmailTemplate("invoice", hashTable);
            if (!Directory.Exists(Server.MapPath("~/Uploads/Invoices" + "/") + OrderID))
            {
                Directory.CreateDirectory(Server.MapPath("~/Uploads/Invoices" + "/") + OrderID);
            }
            using (FileStream fs = new FileStream((Server.MapPath("~/Uploads/Invoices/" + "/" + OrderID + "/") + OrderID + "_Invoice.html"), FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, System.Text.Encoding.UTF8))
                {
                    w.WriteLine(Invoicetemplate);
                }
            }
            return View();
        }
    }
}