using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Areas.User.Services;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.Services;
using PagedList;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.User.Controllers
{
    [Authorize(Roles = "User")]
    [CustomActionFilter]
    public class SettingController : Controller
    {
        // GET: User/Setting

        private ISettingService<tblAddressBook, tblCustomer, tblCreditCard, tblPlan, PlansOptionModel, tblRewardPoint, ViewCustomerPaymentModel, tblCreditCardViewModel, tblCustomerPlanLinking> _repository;
        private IShipmentService<tblReceivedShipment, tblOrder, UserViewCustomerModel, UserPaymentModel, ViewRecivedShipmentModel, tblAddressBook, ViewPackagingOptionsModel, ViewOrderModel, ShipmentMethodPackage, PacakageOptionArray> _repositoryshipment;

        public SettingController(SettingService repo, ShipmentsService repos)
        {
            _repository = repo;
            _repositoryshipment = repos;
        }

        [HttpGet]
        public ActionResult Index()
        {
            CountryServices objcon = new CountryServices();

            ViewBag.country_code = objcon.Country();
            return View(_repository.GetAddressBookData(null, true).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Index(tblAddressBook tbladdressbook)
        {

            var valueToClean = ModelState["address_name"];
            valueToClean.Errors.Clear();

            if (tbladdressbook != null)
            {
                ViewBag.Message = _repository.CreateUpdateNewAddress(tbladdressbook);
            }
            CountryServices objcon = new CountryServices();
            ViewBag.country_code = objcon.Country();
            return View(_repository.GetAddressBookData(null, true).FirstOrDefault());
        }

        [HttpGet]
        public ActionResult SingleAddress(int? Id)
        {
            CountryServices objcon = new CountryServices();
            ViewBag.country_code = objcon.Country();
            if (Id != null)
            {
                if (Id != 0)
                {
                    return View(_repository.GetAddressBookData(Id ?? 0));
                }
            }
            return View();
        }

        public JsonResult CheckAddressname(string addressname, string addressBook_Id)
        {
            bool isMatch = false;

            if (addressname != null)
            {
                isMatch = _repository.CheckDefaultAddress(addressname.Trim(), addressBook_Id);
            }

            return Json(isMatch, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SingleAddress(tblAddressBook tbladdressbook)
        {
            if (tbladdressbook != null)
            {
                tbladdressbook.Fk_customer_Id = Convert.ToInt32(Session["UserId"]);
                ViewBag.Message = _repository.CreateUpdateNewAddress(tbladdressbook);
            }
            ModelState.Clear();
            CountryServices objcon = new CountryServices();
            ViewBag.country_code = objcon.Country();
            return View("SingleAddress");
        }

        public ActionResult AddressBook(int id = 0, int pageNumber = 1)
        {
            if (id != 0)
            {
                ViewBag.Message = _repository.DeleteSingleAddressByID(Convert.ToInt32(id));
            }
            return View(_repository.GetAddressBookData(null, false, pageNumber));
        }

        //public ActionResult AddressBook(int pageNumber = 1)
        //{
        //    return View(_repository.GetAddressBookData(null, false));
        //}
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel ChangePasswordmodel)
        {
            if (ChangePasswordmodel != null)
            {
                ViewBag.Message = _repository.ChangeUserPassword(ChangePasswordmodel);
                ModelState.Clear();
            }
            return View();
        }

        public JsonResult CheckOldPassword(string pass)
        {
            bool isMatch = false;
            if (pass != null)
            {
                isMatch = _repository.CheckPassword(pass);
            }
            return Json(isMatch, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CopmareOldAndNewPassword(string pass)
        {
            bool isMatch = false;
            if (pass != null)
            {
                isMatch = _repository.CheckOldPasswordandNewPassword(pass);
            }
            return Json(isMatch, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChangeEmail()
        {
            ChangeEmailId ch = new ChangeEmailId();
            var chemail = _repository.GetTblCustomerData();
            ch.currentemail = chemail.email;
            return View(ch);
        }

        [HttpPost]
        public ActionResult ChangeEmail(ChangeEmailId changeemailId)
        {
            if (changeemailId != null)
            {
                ViewBag.Message = _repository.ChangeUserEmail(changeemailId);
                ModelState.Clear();
            }
            ChangeEmailId ch = new ChangeEmailId();
            var chemail = _repository.GetTblCustomerData();
            ch.currentemail = chemail.email;
            return View(ch);
        }

        public JsonResult SameshippingAddress()
        {
            CountryServices objcon = new CountryServices();
            ViewCustomerPaymentModel viewcust = _repository.GetAddressBookDataByFkId();
            // ViewBag.Country = objcon.Country();
            return Json(viewcust, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreditCards(int pageNumber = 1)
        {
            if (TempData["success_msg_SingleCreditCard"] != null)
            {
                ViewBag.Message = TempData["success_msg_SingleCreditCard"];
            }
            IEnumerable<tblCreditCardViewModel> tblcreditcards = _repository.GetCreaditCards();
            return View(tblcreditcards);
        }

        public JsonResult ChangeDefaultCreditCard(int CreditCardId)
        {
            _repository.ChangeDefaultCreditCard(CreditCardId);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        //public ActionResult CreditCards(int id)
        //{
        //    _repository.GetCreaditCardsById(id);
        //    return View();
        //}

        public ActionResult SingleCreditCard(int? Id)
        {

            BindCountryDropDown();
            if (Id != 0)
            {
                ViewCustomerPaymentModel viewcust = _repository.GetCreaditCardsById(Id ?? 0);
                return View(viewcust);
            }
            return View();
        }

        [OutputCache(Duration = 216000)]
        private void BindCountryDropDown()
        {
            CountryServices objcon = new CountryServices();
            ViewBag.Country = objcon.Country();
        }

        [HttpPost]
        public ActionResult UpdateSingleCreditCard(ViewCustomerPaymentModel viewcustomerpaymentmodel)
        {
            if (viewcustomerpaymentmodel != null)
            {
                ViewBag.Message = _repository.UpdateBillingAddressById(viewcustomerpaymentmodel);
            }
            ModelState.Clear();
            return RedirectToAction("CreditCards");
        }

        public PartialViewResult _SingleCreditCard(int? Id)
        {
            ViewCustomerPaymentModel viewcust = new ViewCustomerPaymentModel();
            if (Id != null)
            {
                viewcust = _repository.GetCreaditCardsById(Id ?? 0);
            }
            BindCountryDropDown();
            return PartialView("_SingleCreditCard", viewcust);
        }

        [HttpPost]
        public ActionResult SingleCreditCard(ViewCustomerPaymentModel viewcustomerpaymentmodel)
        {
            if (viewcustomerpaymentmodel.Terms == false)
            {
                TempData["CustomerBillingPayment"] = "showMessage('Please accept Terms and Conditions',false)";
                BindCountryDropDown();
                return View(viewcustomerpaymentmodel);
            }
            if (viewcustomerpaymentmodel != null)
            {
                ViewBag.Message = _repository.CreateUpdatePaymentMethod(viewcustomerpaymentmodel);
                if (_repository.is_Success == true)
                {
                    TempData["success_msg_SingleCreditCard"] = ViewBag.Message;
                    return RedirectToAction("CreditCards");
                }
            }
            ModelState.Clear();
            BindCountryDropDown();
            return View("SingleCreditCard");
        }

        public ActionResult Referral()
        {
            int userId = Convert.ToInt32(Session["UserId"]);
            var url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            var Links = url + "?r=" + +userId + "#price_area";
            ViewBag.Link = Links;
            return View();
        }

        [HttpPost]
        public ActionResult Referral(string[] email, string[] name)
        {
            int userId = Convert.ToInt32(Session["UserId"]);
            Context4InshipEntities Contexts = new Context4InshipEntities();
            var GetUserEmail = Contexts.tblCustomers.AsQueryable().Where(x => x.Id == userId).Select(x => x.email).FirstOrDefault();
            var url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            var Links = url + "?r=" + +userId + "#price_area";
            ClsCommanCustomerSignup clscommn = new ClsCommanCustomerSignup();
            string Discount = ConfigurationManager.AppSettings["ReferredSignupMarginPercent"];
            string subject = "chineseaddress - Referal Invitation";
            for (int i = 0; i < email.Length; i++)
            {
                //string body = "Congratulations " + name[i] + " !! You have been invited to join ChineseAddress and get discount on Signup, use the following link for Signup " + Links + " Thank you for using ChineseAddresss.com.";
                Hashtable htTemplate = new Hashtable();
                clsSendEmailService objclsSendEmailService = new clsSendEmailService();
                htTemplate.Add("#name#", name[i]);
                htTemplate.Add("#Links#", Links);
                htTemplate.Add("#ReferalDiscount#", Discount);
                htTemplate.Add("#website#", url);
                htTemplate.Add("#Useremail#", GetUserEmail);
                try
                {
                    objclsSendEmailService.SendEmail(new[] { email[i] }, null, null, (new clsEmailTemplateService().GetEmailTemplate("refferalinvitation", htTemplate)), subject);
                    ViewBag.Message = "showMessage('Referal email successfully sent.',true)";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message.ToString();
                }
            }
            ModelState.Clear();
            ViewBag.Link = Links;
            return View();
        }
        [HttpGet]
        public ActionResult ManagePoints(int pageNumber = 1)
        {
            try
            {
                RewardpointsandBalance();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_repository.getRewardPointsDetail(pageNumber));
        }

        [HttpPost]
        [OutputCache(Duration = 0)]
        public JsonResult ManagePoints()
        {
            try
            {
                //if (Request.RequestType.ToLower() != "get")
                //{
                RewardpointsandBalance();
                decimal convertRatePerhundered = Convert.ToDecimal(ConfigurationManager.AppSettings["ConvertRatePerhunderedRewardPoints"]);
                decimal Minconverpoints = Convert.ToDecimal(ConfigurationManager.AppSettings["MinConvertPoint"]);
                var points = _repositoryshipment.yourpoints();
                if (points >= Minconverpoints)
                {
                    TempData["ConvertPointMSg"] = _repository.insertrewardpoint(Convert.ToDecimal(ViewBag.yourbalance));
                    TempData.Peek("ConvertPointMSg");
                }
                else
                {
                    _ChineseAddress.com.Areas.Admin.Models.clsCommon objcommon = new Admin.Models.clsCommon();
                    TempData["ConvertPointMSg"] = objcommon.ErrorMsg($"You must have minimum {Minconverpoints} points to convert into balance.");
                    TempData.Peek("ConvertPointMSg");
                }
                //}
                //RewardpointsandBalance();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [ChildActionOnly]
        private void RewardpointsandBalance()
        {
            decimal CRPHP = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRatePerhunderedRewardPoints"]);
            ViewBag.yourpoints = _repositoryshipment.yourpoints();
            ViewBag.yourcreditAmount = _repositoryshipment.yourBalance();
            CRPHP = CRPHP * 100.00m;
            ViewBag.yourbalance = Convert.ToDecimal(ViewBag.yourpoints) / CRPHP;
        }

        public ActionResult Upgrade()
        {
            int LoginUser = Convert.ToInt32(Session["UserId"]);
            ViewBag.CurrnetPlan = _repository.PlanLinkingId(LoginUser);
            ClsCommanCustomerSignup clscmmn = new ClsCommanCustomerSignup();
            clscmmn.GetCustomerReference();
            ViewBag.PlansOption = _repository.Plans();
            List<PlansOptionModel> objList = _repository.OptionPlan();
            return View(objList);
        }
    }
}