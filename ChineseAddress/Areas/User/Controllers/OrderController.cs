using _ChineseAddress.com.Areas.Admin.Models;
using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Areas.User.Services;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using PagedList;
using System;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
namespace _ChineseAddress.com.Areas.User.Controllers
{
    [Authorize(Roles = "User")]
    [CustomActionFilter]
    public class OrderController : Controller
    {
        private UserCommanClass objUserCommanClass = new UserCommanClass();

        [Dependency]
        public IShipmentService<tblReceivedShipment, tblOrder, UserViewCustomerModel, UserPaymentModel, ViewRecivedShipmentModel, tblAddressBook, ViewPackagingOptionsModel, ViewOrderModel, ShipmentMethodPackage, PacakageOptionArray> _repoShipmentService { get; set; }

        [Dependency]
        public IPaymentService<UserPaymentModel, ViewModelOrderinfo> _repoPaymentService { get; set; }
        // GET: User/Order
        private IOrder<ViewModelOrderinfo, tblOrderCharge, ViewRecivedShipmentModel, ViewOrderModel, tblReceivedShipment, tblOrderBox> _repository;

        public OrderController(IOrder<ViewModelOrderinfo, tblOrderCharge, ViewRecivedShipmentModel, ViewOrderModel, tblReceivedShipment, tblOrderBox> _repo)
        {
            _repository = _repo;
        }

        private OrderService Context = new OrderService();

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult OrderDetails(int? pageNumber)
        {
            if (TempData["tempOrderConfirm"] != null)
            {
                ViewBag.Message = $"showMessage('{Convert.ToString(TempData["tempOrderConfirm"])}',true)";
            }
            if (TempData["successPayment"] != null)
            {
                ViewBag.Message = TempData["successPayment"];
            }
            int ID = Convert.ToInt32(Session["UserId"]);
            IPagedList<ViewOrderModel> tblorde = _repository.GettblShipmentReturnDetail(ID, pageNumber ?? 1);
            return View(tblorde);
        }

        public ActionResult Orderinformation(string id)
        {
            if (TempData["AcceptTerms"] != null)
            {
                ViewBag.Message = TempData["AcceptTerms"];
            }
            int GetOrderID = 0;
            if (!string.IsNullOrEmpty(id))
            {
                GetOrderID = Convert.ToInt32(Cryptography.Decrypt(id));

                ViewBag.Order_Id = id;
            }
            else
            {
                GetOrderID = Convert.ToInt32(TempData.Peek("OrderID"));
                ViewBag.Order_Id = id;
            }
            if (GetOrderID != 0)
            {
                ViewModelOrderinfo orderInfo = _repository.GetOrderInformation(GetOrderID);
                ViewBag.CreateOrderDetails = _repository.CreateOrderRecivedShipment(GetOrderID);
                decimal sumSellPrice = 0.0m;
                var chagres = _repository.GetTblOrdeChargedByFkId(GetOrderID);
                sumSellPrice = _repository.GetOrderCharges(GetOrderID);
                ViewBag.OrderCharges = chagres;
                ViewBag.OrderShipmentDetails = _repository.GetShipmentDetails();

                ViewBag.OrderBoxes = _repository.GetTblOrdeBoxesByFkId(GetOrderID);
                orderInfo.totalcharges = sumSellPrice;

                if (orderInfo.status == (int)enumorder.Awaiting_Payment)
                {
                    int defaultAddressID = 0;
                    ViewBag.AdressList = objUserCommanClass.AddressTableDrpdown(Convert.ToInt32(Session["UserId"]));
                    if (ViewBag.AdressList != null)
                    {
                        defaultAddressID = Convert.ToInt32(ViewBag.AdressList[0].Value);
                        TempData["defaultAddress"] = defaultAddressID;
                        ViewBag.DefaultAddress = _repoShipmentService.GettblAdressData(defaultAddressID);
                    }
                }

                return View(orderInfo);
            }
            return View();
        }

        public PartialViewResult _shipmentdetails()
        {
            return PartialView("_shipmentdetails");
        }

        [HttpGet]
        public ActionResult CancelOrder()
        {
            int GetOrderID = Convert.ToInt32(TempData.Peek("OrderID"));
            ViewBag.Message = _repository.CancleOrder(GetOrderID);
            return RedirectToAction("ShipmentDetails", "Shipment");
        }

        [HttpPost]
        public ActionResult ConfirmOrder(ViewModelOrderinfo objViewModelOrderinfo, string id)
        {
            if (objViewModelOrderinfo.Terms == true)
            {
                int Orderid = Convert.ToInt32(Cryptography.Decrypt(id));
                TempData["OrderMessage"] = _repository.ConfirmOrder(Orderid);
                if (Convert.ToString(TempData["OrderMessage"]) == "showMessage('Order has been created successfully',true);")
                {
                    TempData["tempOrderConfirm"] = "Your order has been confirmed successfully";
                    return RedirectToAction("OrderDetails", "Order");
                }
                else
                {
                    return RedirectToAction("ShipmentDetails", "Shipment");
                }
            }

            TempData["AcceptTerms"] = "showMessage('Please accept Terms and Conditions',false)";
            return RedirectToAction("Orderinformation", "Order", new { id = id });
        }

        [HttpPost]
        public JsonResult DeleteOrderById(string ID)
        {
            int GetOrderID = Convert.ToInt32(ID);
            _repository.CancleOrder(GetOrderID);
            ViewBag.Message = ID;
            return Json(ViewBag.Message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveNotes(string SaveNotesData, string RecShipId)
        {
            tblReceivedShipment objtblReceivedShipment = new tblReceivedShipment();
            objtblReceivedShipment.id = Convert.ToInt32(RecShipId);
            objtblReceivedShipment.user_notes = SaveNotesData;
            string Message = _repository.SaveNotes(objtblReceivedShipment);
            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Manageorder(int id)
        {
            int ID = Convert.ToInt32(Session["UserId"]);
            ViewBag.CreateOrderDetails = _repository.CreateOrderRecivedShipment(id);
            // ViwBag.Message = _repository.ChangeStatusToProcessingById(id);
            ViewModelOrderinfo orderInfo = _repository.GetOrderInformation(id);
            ViewBag.OrderCharges = _repository.GetTblOrdeChargedByFkId(id);
            ViewBag.OrderShipmentDetails = _repository.GetShipmentDetails();
            ViewBag.GetOrderBoxesList = _repository.GetTblOrdeBoxesByFkId(id);
            return View(orderInfo);
        }

        //process PayNow
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PayNow(string encOrder_Id, int BookingAddressId)
        {
            int orderId = 0;
            if (!string.IsNullOrEmpty(encOrder_Id))
            {
                orderId = Convert.ToInt32(Cryptography.Decrypt(encOrder_Id));
            }
            string _msg = _repoPaymentService.ProcessPaymentViaWallet(orderId, BookingAddressId, "Order");
            if (_repoPaymentService._isSuccess == true)
            {
                TempData["successPayment"] = _msg;
                return RedirectToAction("OrderDetails", "Order");
            }
            else
            {
                TempData["Message"] = _msg;
            }
            //ViewBag.Message = _repoPaymentService.ProcessPayment(orderId, CardId, type);
            //if (_repository._isSuccess)
            //{
            //    TempData["successPayment"] = ViewBag.Message;
            //    return RedirectToAction("OrderDetails", "Order");
            //}
            //TempData["successPayment"] = ViewBag.Message;
            return RedirectToAction("Orderinformation", "Order", new { id = encOrder_Id });
        }

    }
}