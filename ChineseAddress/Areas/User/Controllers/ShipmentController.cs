using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Repository;
using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.User.Controllers
{
    [Authorize(Roles = "User")]
    [CustomActionFilter]
    public class ShipmentController : Controller
    {
        // GET: User/Shipment
        private UserCommanClass objUserCommanClass = new UserCommanClass();

        private IShipmentService<tblReceivedShipment, tblOrder, UserViewCustomerModel, UserPaymentModel, ViewRecivedShipmentModel, tblAddressBook, ViewPackagingOptionsModel, ViewOrderModel, ShipmentMethodPackage, PacakageOptionArray> _repository;

        public ShipmentController(IShipmentService<tblReceivedShipment, tblOrder, UserViewCustomerModel, UserPaymentModel, ViewRecivedShipmentModel, tblAddressBook, ViewPackagingOptionsModel, ViewOrderModel, ShipmentMethodPackage, PacakageOptionArray> repo)
        {
            _repository = repo;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShipmentDetails(int? pageNumber)
        {
            int ID = Convert.ToInt32(Session["UserId"]);
            int defaultAddressID = 0;
            ViewBag.AdressList = objUserCommanClass.AddressTableDrpdown(ID);
            if (ViewBag.AdressList != null)
            {
                defaultAddressID = Convert.ToInt32(ViewBag.AdressList[0].Value);
                TempData["defaultAddress"] = defaultAddressID;
                ViewBag.DefaultAddress = _repository.GettblAdressData(defaultAddressID);
            }
            //ViewBag.optionpackagelist = _repository.GetoptionplanPlandata(ID);
            //List<ViewRecivedShipmentModel> ObjReciveShipmentList = _repository.GetReciveShipmentDetails(ID);
            ViewBag.ShipmentDetails = _repository.GetShipmentDetails();
            ViewBag.PlanTitle = _repository.GetCustomerPlan();
            ViewBag.ReturnShipmentCharge = (new Common.Services.clsCommonServiceRoot()).getPlanChargePriceByCustomerId(ID, "Return Shipment").ToString("F");
            List<ViewRecivedShipmentModel> ObjReciveShipmentList = new List<ViewRecivedShipmentModel>();
            ViewBag.OrderRecords = _repository.GetRecievedShipmentRecords(ID);
            IPagedList<ViewRecivedShipmentModel> ObjOrderCreatedList = _repository.GetReciveShipmentDetails(ID, true).ToPagedList((pageNumber ?? 1), MvcApplication.global_Page_Size);
            if (pageNumber != null)
            {
                ViewBag.triggerOrderCreated = @"$('a[href=""#OrderCreated""]').trigger('click')";
            }
            Parallel.Invoke(() => ViewBag.optionpackagelist = _repository.GetoptionplanPlandata(ID),
                 () => ObjReciveShipmentList = _repository.GetReciveShipmentDetails(ID));
            return View(Tuple.Create(ObjReciveShipmentList, ObjOrderCreatedList));
        }

        //Bind Order Created using jquery
        //public JsonResult ShipmetCreatedOrdersDetail(int pageNumber = 1)
        //{
        //    int ID = Convert.ToInt32(Session["UserId"]);
        //    List<ViewRecivedShipmentModel> ObjReciveShipmentList = _repository.GetReciveShipmentDetails(ID, true);
        //    IPagedList<ViewRecivedShipmentModel> pagedlstreceivedshipment = ObjReciveShipmentList.ToPagedList(pageNumber, MvcApplication.global_Page_Size);
        //    return Json(new object[] { pagedlstreceivedshipment, pagedlstreceivedshipment.TotalItemCount, pagedlstreceivedshipment.Count, pagedlstreceivedshipment.PageNumber, pagedlstreceivedshipment.PageCount }, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult ShipmentDetailss(string referenceNo)
        {
            var id = _repository.GetOrderIdByreferenceNo(referenceNo);
            return RedirectToAction("Orderinformation", "Order", new { id });
        }

        // change address dropdown Get shipment api method//
        [HttpPost]
        public JsonResult GetReciveShipmentDetail(string ID)
        {
            tblAddressBook Data = new tblAddressBook();
            if (ID != "")
            {
                Data = _repository.GettblAdressData(Convert.ToInt32(ID));
            }
            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetShipmentMethodCarrierApi(string ID, string[] ShipmentCheckboxID)
        {
            tblAddressBook Data = new tblAddressBook();
            if (ShipmentCheckboxID != null)
            {
                ViewBag.ShipmentCarrierApi = objUserCommanClass.ShipmentCarrierApi(Convert.ToInt32(ID), ShipmentCheckboxID);
            }
            return Json((ViewBag.ShipmentCarrierApi ?? ""), JsonRequestBehavior.AllowGet);
        }

        // default address Get shipment api method//
        [HttpPost]
        public JsonResult defaultAddressGetShipmentMethod(string ID, string[] ShipmentCheckboxID)
        {
            ViewBag.ShipmentCarrierApi = objUserCommanClass.ShipmentCarrierApi(Convert.ToInt32(ID), ShipmentCheckboxID);
            return Json(ViewBag.ShipmentCarrierApi, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CancelRecivedShipment(string CancelShipmentID)
        {
            ViewBag.Message = _repository.CancelRecShipment(Convert.ToInt32(CancelShipmentID));

            return Json(ViewBag.Message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RequestPicture(string[] requestpicture)
        {
            string message = "";
            if (requestpicture != null)
            {
                for (int i = 0; i < requestpicture.Length; i++)
                {
                    if (requestpicture[i] != "")
                    {
                        var id = requestpicture[i].Replace("id", "");
                        message = _repository.GetRequestPicture(Convert.ToInt32(id));
                    }
                }
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CorrectPrice()
        {
            HttpFileCollectionBase files = Request.Files;
            string message = "";
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFileBase file = files[i];
                string reciviedshipid = "";
                if (Request.Form["recievshipId"] != null)
                {
                    reciviedshipid = Request.Form["recievshipId"];
                }
                string extension = Path.GetExtension(file.FileName);
                string fileName = Guid.NewGuid().ToString().Substring(0, 8) + extension;
                if (!Directory.Exists(Server.MapPath("~/Uploads/CorrectPrice" + "/") + reciviedshipid))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Uploads/CorrectPrice" + "/") + reciviedshipid);
                }
                file.SaveAs(Server.MapPath("~/Uploads/CorrectPrice/" + "/" + reciviedshipid + "/") + fileName.ToString());
                tblReceivedShipment objrecivedship = new tblReceivedShipment();
                objrecivedship.id = Convert.ToInt32(reciviedshipid);
                objrecivedship.correct_price_file_path = fileName.ToString();

                message = _repository.SaveInvoicefilepath(objrecivedship);
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        //ReturnPAckage //
        [HttpPost]
        public ActionResult ReturPackageForm(ViewRecivedShipmentModel objViewRecivedShipmentModel)
        {
            string message = "";
            if (Request.Files.Count != 0)
            {
                HttpFileCollectionBase files = Request.Files;
#pragma warning disable CS0162 // Unreachable code detected
                for (int i = 0; i < files.Count; i++)
#pragma warning restore CS0162 // Unreachable code detected
                {
                    HttpPostedFileBase file = files[i];
                    string[] reciviedshipid = Request.Form["returnpackageid"].Split(',');
                    for (int k = 0; k < reciviedshipid.Length; k++)
                    {
                        if (reciviedshipid[k] != "")
                        {
                            string extension = Path.GetExtension(file.FileName);
                            string fileName = Guid.NewGuid().ToString().Substring(0, 8) + extension;
                            string RecshipID = reciviedshipid[k].Replace("id", "").Trim();
                            if (!Directory.Exists(Server.MapPath("~/Uploads/ReturnPackages" + "/") + RecshipID))
                            {
                                Directory.CreateDirectory(Server.MapPath("~/Uploads/ReturnPackages" + "/") + RecshipID);
                            }
                            file.SaveAs(Server.MapPath("~/Uploads/ReturnPackages/" + RecshipID + "/") + fileName.ToString());
                            objViewRecivedShipmentModel.Fk_shipment_id = Convert.ToInt32(RecshipID);
                            objViewRecivedShipmentModel.ReturnpackagefileUpload = fileName.ToString();
                            message = _repository.ReturnPackageData(objViewRecivedShipmentModel);
                        }
                    }
                    return Json(message, JsonRequestBehavior.AllowGet);
                }
            }
            else if (objViewRecivedShipmentModel.returnpackageid[0] != "")
            {
                // id according save data//
                string[] returnpackageid = objViewRecivedShipmentModel.returnpackageid[0].Split(',');
                for (int i = 0; i < returnpackageid.Length; i++)
                {
                    if (returnpackageid[i] != "")
                    {
                        objViewRecivedShipmentModel.Fk_shipment_id = Convert.ToInt32(returnpackageid[i].Replace("id", ""));
                        message = _repository.ReturnPackageData(objViewRecivedShipmentModel);
                    }
                }
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveNotes(string SaveNotesData, string RecShipId)
        {
            tblReceivedShipment objtblReceivedShipment = new tblReceivedShipment();
            objtblReceivedShipment.id = Convert.ToInt32(RecShipId);
            objtblReceivedShipment.user_notes = SaveNotesData;
            string Message = _repository.SaveNotes(objtblReceivedShipment);
            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateShipmentOrder(string data)
        {
            string GetOrderId = "";
            string message = "";
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            List<ShipmentMethodPackage> ShipmentPackage = JsonConvert.DeserializeObject<List<ShipmentMethodPackage>>(values["ShipmentMethodPackage"].ToString());
            List<PacakageOptionArray> Packageoption = JsonConvert.DeserializeObject<List<PacakageOptionArray>>(values["PacakageOptionArray"].ToString());
            string palnTitle = _repository.GetCustomerPlan();
            string[] Recivedshipmentcheckbox = JsonConvert.DeserializeObject<string[]>(values["ShipmentCheckboxID"].ToString());
            if (Recivedshipmentcheckbox.Length > 1 && palnTitle.ToLower() == "direct-single-shipment")
            {
                return Json("false", JsonRequestBehavior.AllowGet);
            }
            string AddressDrop = JsonConvert.DeserializeObject<string>(values["Addressdropid"].ToString());
            string strCouponCode = Convert.ToString(values["CouponCode"]);
            string strOrder_Notes = Convert.ToString(values["order_notes"]);
            int ID = Convert.ToInt32(Session["UserId"]);
            List<ShipmentMethodPackage> objShipmentMethodPackage = new List<ShipmentMethodPackage>();
            List<PacakageOptionArray> objPacakageOptionArray = new List<PacakageOptionArray>();
            foreach (var item in ShipmentPackage)
            {
                objShipmentMethodPackage.Add(new ShipmentMethodPackage() { shipmentMethodPrice = item.shipmentMethodPrice, shipmentdescription = item.shipmentdescription, shipmentcode = item.shipmentcode });
            }
            foreach (var item in Packageoption)
            {
                objPacakageOptionArray.Add(new PacakageOptionArray() { PackageOptionPrice = item.PackageOptionPrice, PackageOptionTitle = item.PackageOptionTitle });
            }
            ViewOrderModel objViewOrderModel = new ViewOrderModel();
            if (Recivedshipmentcheckbox != null)
            {
                objViewOrderModel.Fk_Customer_id = ID;
                objViewOrderModel.ShipmentCheckboxID = Recivedshipmentcheckbox;
                objViewOrderModel.Fk_orderAddres_id = Convert.ToInt32(AddressDrop);
                GetOrderId = _repository.CreateShipmentOrder(objShipmentMethodPackage, objPacakageOptionArray, objViewOrderModel, strCouponCode, strOrder_Notes);
                TempData["OrderID"] = GetOrderId;
                TempData.Peek("OrderID");
                message = com.Models.Cryptography.Encrypt(GetOrderId);
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult validateCouponCode(string coupon_code, string country_name)
        {
            Tuple<bool, string> isvalid = _repository.validateCouponCode(coupon_code, country_name);
            return Json(isvalid, JsonRequestBehavior.AllowGet);
        }
    }
}