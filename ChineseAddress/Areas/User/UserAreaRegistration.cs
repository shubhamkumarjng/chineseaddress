using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.User
{
    public class UserAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "User";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            #region Area User Folderfiles Routing Start
            context.MapRoute(
            name: "DashBoard",
            url: "DashBoard",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            namespaces: new string[] { "_ChineseAddress.com.Areas.User.Controllers" }
           );

            context.MapRoute(
            name: "CreateOrder",
            url: "CreateOrder/{id}",
            defaults: new { controller = "Shipment", action = "ShipmentDetails", id = UrlParameter.Optional }
           );
            context.MapRoute(
            name: "OrderInformation",
            url: "OrderInformation/{id}",
            defaults: new { controller = "Order", action = "Orderinformation", id = UrlParameter.Optional }
           );
            context.MapRoute(
            name: "Orders",
            url: "Orders/{pageNumber}",
            defaults: new { controller = "Order", action = "OrderDetails", pageNumber = UrlParameter.Optional }
           );
            context.MapRoute(
            name: "PaymentHistory",
            url: "PaymentHistory/{pageNumber}",
            defaults: new { controller = "Payment", action = "PaymentHistory", pageNumber = UrlParameter.Optional }
            );
            context.MapRoute(
            name: "Setting",
            url: "Setting",
            defaults: new { controller = "Setting", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
            name: "Addressbook",
            url: "Addressbook/{id}",
            defaults: new { controller = "Setting", action = "Addressbook", id = UrlParameter.Optional }
            );
            context.MapRoute(
            name: "ChangePassword",
            url: "ChangePassword",
            defaults: new { controller = "Setting", action = "ChangePassword", id = UrlParameter.Optional }
            );
            context.MapRoute(
            name: "CreditCards",
            url: "CreditCards/{pageNumber}",
            defaults: new { controller = "Setting", action = "CreditCards", pageNumber = UrlParameter.Optional }
            );
            context.MapRoute(
            name: "ManagePoints",
            url: "ManagePoints",
            defaults: new { controller = "Setting", action = "ManagePoints", id = UrlParameter.Optional }
           );
            context.MapRoute(
            name: "SingleCreditCard",
            url: "SingleCreditCard",
            defaults: new { controller = "Setting", action = "SingleCreditCard", id = UrlParameter.Optional }
           );
            context.MapRoute(
            name: "SingleAddress",
            url: "SingleAddress",
            defaults: new { controller = "Setting", action = "SingleAddress", id = UrlParameter.Optional }
            );
            context.MapRoute(
            name: "Referral",
            url: "Referral",
            defaults: new { controller = "Setting", action = "Referral", id = UrlParameter.Optional }
            );
            context.MapRoute(
            name: "Payments",
            url: "Payments/{type}/{id}/{pageNumber}",
            defaults: new { controller = "Payment", action = "Index", id = UrlParameter.Optional, type = UrlParameter.Optional, pageNumber = UrlParameter.Optional }
            );
            context.MapRoute(
            name: "Tracking",
            url: "TrackOrder/{Track}",
            defaults: new { controller = "Tracking", action = "Index", Track = UrlParameter.Optional }
            );
            context.MapRoute(
            name: "Upgrade",
            url: "Upgrade",
            defaults: new { controller = "Setting", action = "Upgrade", id = UrlParameter.Optional }
            );
            //context.MapRoute(
            //name: "Newsletter",
            //url: "Newsletter",
            //defaults: new { controller = "Newsletter", action = "Index", id = UrlParameter.Optional }
            //);
            context.MapRoute(
            name: "PaymentPlanUpgrade",
            url: "PaymentPlanUpgrade/{id}/{pageNumber}/{creditCardId}",
            defaults: new { controller = "Payment", action = "PaymentPlanUpgrade", id = UrlParameter.Optional, creditCardId = UrlParameter.Optional, pageNumber = UrlParameter.Optional }
            );

            //Shubham Routes start
            context.MapRoute(
                name: "wallet-TopUp",
                url: "wallet/topup",
                defaults: new { Controller = "wallet", action = "topupwallet", qs = UrlParameter.Optional }
                );
            context.MapRoute(
                name: "wallet",
                url: "wallet",
                defaults: new { Controller = "wallet", action = "Index" }
                );


            //Shubham Routes end

            #endregion Area User Folderfiles Routing Start

            context.MapRoute(
                "User_default",
                "User/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                //namespaces: new[] { "_ChineseAddress.com.Controllers" }
            );
        }
    }
}