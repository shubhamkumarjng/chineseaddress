using System;
using System.Web;
using System.Web.Security;

namespace _ChineseAddress.com.Areas.User.Models
{
    public class clsAuthenticateData
    {
        public void CheckUserAuthenticated()
        {
            HttpSessionStateBase Session = new HttpSessionStateWrapper(HttpContext.Current.Session);
            if (Session["UserId"] == null)
            {
                FormsIdentity fi;
                fi = (FormsIdentity)HttpContext.Current.User.Identity;
                string[] ud = fi.Ticket.UserData.Split(';');
                Session["UserId"] = Convert.ToInt32((string.IsNullOrEmpty(ud[1]) ? "0" : ud[1]));
                Session["UserFullName"] = ud[2];
                Session["UserEmailAddress"] = (ud.Length >= 3 ? "" : ud[3]);
            }
        }
    }
}