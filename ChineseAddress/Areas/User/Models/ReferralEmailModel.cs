using System.ComponentModel.DataAnnotations;

namespace _ChineseAddress.com.Areas.User.Models
{
    public class ReferralEmailModel
    {
        [Required(ErrorMessage = "Email is required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail address")]
        public string email { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string name { get; set; }
    }
}