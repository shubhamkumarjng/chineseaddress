using _ChineseAddress.com.Areas.User.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _ChineseAddress.com.Repository
{
    [MetadataType(typeof(tblAddressBookproperties))]
    public partial class tblAddressBook
    {
    }

    public class tblAddressBookproperties
    {
        public int Id { get; set; }

        public int Fk_customer_Id { get; set; }

        [Required(ErrorMessage = "Address Name is required")]
        [ValidateAddressName(ErrorMessage = "Address Name already exists.")]
        [MaxLength(35, ErrorMessage = "Address Name is maximum 35 characters long")]
        [Display(Name = "Address Name")]
        public string address_name { get; set; }
        [MaxLength(11, ErrorMessage = "First Name cannot be longer than 10 characters.")]
        [Required(ErrorMessage = "First Name is required")]
        public string first_name { get; set; }
        [MaxLength(11, ErrorMessage = "Last Name cannot be longer than 10 characters.")]
        [Required(ErrorMessage = "Last Name is required")]
        public string last_name { get; set; }

        public string company { get; set; }

        [Required(ErrorMessage = "Address 1 is required")]
        [MaxLength(35, ErrorMessage = "Address 1 is maximum 35 characters long")]
        [RegularExpression("^[a-zA-Z0-9-,/;.'&quot; ]*$", ErrorMessage = "Unacceptable character in Address 1 field. Please use only a-z,0-9 or -',/;.&quot;")]
        public string address1 { get; set; }

        [MaxLength(35, ErrorMessage = "Address 2 is maximum 35 characters long")]
        [RegularExpression("^[a-zA-Z0-9-,/;.'&quot; ]*$", ErrorMessage = "Unacceptable character in Address 2 field. Please use only a-z,0-9 or -',/;.&quot;")]
        public string address2 { get; set; }

        [Required(ErrorMessage = "Country is required")]
        public string country_code { get; set; }

        [Required(ErrorMessage = "State is required")]
        public string state { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string city { get; set; }

        [Required(ErrorMessage = "Postal Code is required")]
        public string post_code { get; set; }

        [Required(ErrorMessage = "Mobile Number is required")]
        [MaxLength(15, ErrorMessage = "MobileNumber cannot be longer than 10 digits.")]
        [RegularExpression(@"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-.]+\d$", ErrorMessage = "Entered phone format is not valid.")]
        public string mobile1 { get; set; }
        //[Required]
        [MaxLength(15, ErrorMessage = "MobileNumber cannot be longer than 10 digits.")]
        [RegularExpression(@"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-.]+\d$", ErrorMessage = "Entered phone format is not valid.")]
        public string mobile2 { get; set; }
        public string tax_id { get; set; }
        public bool is_default { get; set; }
        public System.DateTime created_on { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ValidateAddressNameAttribute : ValidationAttribute, IClientValidatable
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!string.IsNullOrEmpty(value.ToString()))
            {
                if ((new SettingService(new _ChineseAddress.com.Services.AuthorizeNetService()).CheckDefaultAddress(value.ToString(), HttpContext.Current.Request.RawUrl.Split('/').Last())))
                    return ValidationResult.Success;
            }
            return new ValidationResult(string.Format(ErrorMessageString, validationContext.DisplayName));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "validateaddressname"
            };

            yield return rule;
        }
    }
}