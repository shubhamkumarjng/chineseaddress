using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.User.Models
{
    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "Old Password  is required")]
        [ValidateCurrentPassword(ErrorMessage = "Old Password  is incorrect")]
        public string oldpassword { get; set; }

        [Display(Name = "New Password")]
        [MinLength(6, ErrorMessage = "New Password must be minimum 6 character")]
        [Required(ErrorMessage = "New Password  is required")]
        [notequalto(ErrorMessage = "Old Password and New Password cannot be same.")]
        public string newpassword { get; set; }

        [MinLength(6, ErrorMessage = "Confirm Password must be minimum 6 character")]
        [Display(Name = "Confirm New Password")]
        [Required(ErrorMessage = "Confirm new Password  is required")]
        [System.ComponentModel.DataAnnotations.Compare("newpassword", ErrorMessage = "Confirm New Password and New Password do not match.")]
        public string confirmpassword { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ValidateCurrentPasswordAttribute : ValidationAttribute, IClientValidatable
    {
        public ValidateCurrentPasswordAttribute()
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!string.IsNullOrEmpty(value.ToString()))
            {
                if ((new Areas.User.Services.SettingService(new _ChineseAddress.com.Services.AuthorizeNetService())).CheckPassword(value.ToString()))
                    return ValidationResult.Success;
            }
            return new ValidationResult(string.Format(ErrorMessageString, validationContext.DisplayName));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "validatecurrentpassword"
            };

            yield return rule;
        }
    }

    //OldPassword & NewPassword cannot be the same
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class notequaltoAttribute : ValidationAttribute, IClientValidatable
    {
        //private const string defaultErrorMessage = "{0} cannot be the same as {1}.";
        //private string otherProperty;
        public notequaltoAttribute()
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!string.IsNullOrEmpty(value.ToString()))
            {
                if ((new Areas.User.Services.SettingService(new _ChineseAddress.com.Services.AuthorizeNetService())).CheckOldPasswordandNewPassword(value.ToString()))
                    return ValidationResult.Success;
            }
            return new ValidationResult(string.Format(ErrorMessageString, validationContext.DisplayName));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "notequalto"
            };

            yield return rule;
        }
    }
}