using System;

namespace _ChineseAddress.com.Areas.User.Models
{
    public class UserPaymentModel
    {
        public int FK_Customer_Id { get; set; }
        public string OrderReferenceNumber { get; set; }
        public string Method { get; set; }
        public decimal Amount { get; set; }
        public int? Status { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string InvNo { get; set; }
        public int? OrderId { get; set; }
        public string InvoiceType { get; set; }
        public string PlanType { get; set; }
    }
}