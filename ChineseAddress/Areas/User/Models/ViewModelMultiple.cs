using System.Collections.Generic;

namespace _ChineseAddress.com.Areas.User.Models
{
    public class ViewModelMultiple
    {
        public ViewModelOrderinfo OrderInfo { get; set; }
        public List<ViewModelShipmentdetail> ShipmentDetail { get; set; }
    }
}