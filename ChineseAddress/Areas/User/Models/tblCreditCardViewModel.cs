using System.ComponentModel.DataAnnotations;

namespace _ChineseAddress.com.Areas.User.Models
{
    public class tblCreditCardViewModel
    {
        public int Id { get; set; }
        public bool is_default { get; set; }
        public string card_type { get; set; }
        public string card_num { get; set; }
        public string card_expiry { get; set; }
        public string Name { get; set; }
        [Display(Name = "Terms and Conditions")]
        public bool Terms { get; set; }
    }
}