using _ChineseAddress.com.Common.Models;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _ChineseAddress.com.Areas.User.Models
{
    public class UserCommanClass
    {
        private Context4InshipEntities Context = new Context4InshipEntities();

        public List<SelectListItem> AddressTableDrpdown(int ID)
        {
            try
            {
                var AddressList = Context.tblAddressBooks.AsQueryable().OrderByDescending(x => x.is_default == true).Where(x => x.Fk_customer_Id == ID).ToList();
                List<SelectListItem> ObjAddressTableList = new List<SelectListItem>();
                foreach (var item in AddressList)
                {
                    ObjAddressTableList.Add(new SelectListItem { Value = Convert.ToString(item.Id), Text = item.address_name });
                }
                return ObjAddressTableList.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ViewModelOrder> ShipmentCarrierApi(int AddressId, string[] shipmentid)
        {
            try
            {
                string[] RecID = shipmentid.Select(x => x.Replace("id", "")).ToArray();
                List<ViewModelOrder> Shipmentcarrier = new List<ViewModelOrder>();
                var CarrierDetails = Context.tblCarriersDetails.AsQueryable().Where(x => x.status == true).Select(x => x).ToList();
                clsCarriersService objclsCarriersService = new clsCarriersService();
                // Get Address form Addressbook table
                tblDeliveryAddress objAddress = new tblDeliveryAddress();
                int custId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);

                var objToAddress = (from tbladdress in Context.tblAddressBooks
                                    join tblcontry in Context.tblCountryMasters
                                    on tbladdress.country_code equals tblcontry.country_code
                                    where tbladdress.Id == AddressId
                                    select new
                                    {
                                        first_name = tbladdress.first_name,
                                        last_name = tbladdress.last_name,
                                        company = tbladdress.company,
                                        address1 = tbladdress.address1,
                                        address2 = (tbladdress.address2 ?? ""),
                                        country_code = tblcontry.country_code,
                                        state = tbladdress.state,
                                        city = tbladdress.city,
                                        postal_code = tbladdress.post_code,
                                        mobile1 = tbladdress.mobile1
                                    }).ToList();

                foreach (var item in objToAddress)
                {
                    objAddress.first_name = item.first_name;
                    objAddress.last_name = item.last_name;
                    objAddress.company = item.company;
                    objAddress.address1 = item.address1;
                    objAddress.address2 = item.address2;
                    objAddress.country_code = item.country_code;
                    objAddress.state = item.state;
                    objAddress.city = item.city;
                    objAddress.postal_code = item.postal_code;
                    objAddress.mobile1 = item.mobile1;
                }
                int[] RecShipmentID = Array.ConvertAll(RecID, int.Parse);
                objclsCarriersService._ToAddress = objAddress;
                objclsCarriersService._ReceivedShipmentIds = RecShipmentID;
                objclsCarriersService.getCarrierPrices();
                //if (objclsCarriersService._msg != null)
                //{
                //    foreach (var item in CarrierDetails)
                //    {
                //        Shipmentcarrier.Add(new ViewModelOrder { carrier = item.carrier_name + "  " + item.service, Amount = "N/A", CarrierCode = item.code, service = item.carrier_name });
                //    }
                //}
                //else if (objclsCarriersService._IsSuccess == true)
                //{
                List<carriersRatesResponseModel> shippingChargesModel = objclsCarriersService._carriersRates.OrderBy(x => x.TotalShipmentCharge).ToList();
                dynamic GetMarginProfit = 0;
                dynamic MarginPrice = 0;
                int i = 0;
                foreach (var carrier in shippingChargesModel)
                {
                    foreach (var item in CarrierDetails)
                    {
                        if (RecShipmentID[0] != 0 && i == 0)
                        {
                            int RecshipID = RecShipmentID[0];
                            var GetCustomerPlan = (from tbrec in Context.tblReceivedShipments join tbcustlink in Context.tblCustomerPlanLinkings on tbrec.Fk_Customer_Id equals tbcustlink.Fk_Customer_Id select new { tbrec.id, tbcustlink.plan_title }).AsQueryable().Where(x => x.id == RecshipID).Select(x => x.plan_title).FirstOrDefault();
                            GetMarginProfit = Context.tblPlans.AsQueryable().Where(x => x.title == GetCustomerPlan).Select(x => x.profit_margin).FirstOrDefault();
                            i++;
                        }
                        if (item.code == carrier.Service_Code)
                        {
                            MarginPrice = (carrier.TotalShipmentCharge * (100 + GetMarginProfit) / 100);
                            Shipmentcarrier.Add(new ViewModelOrder { carrier = item.service, Amount = Convert.ToString(String.Format("$ {0:0.00}", MarginPrice)), total_shipment_price = MarginPrice, CarrierCode = carrier.Service_Code, service = item.carrier_name, BussinessDays = carrier.BussinessDays });//item.carrier_name + "  " +
                        }
                    }
                }
                // }
                return Shipmentcarrier.OrderBy(x => x.total_shipment_price).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteCreateOrder(int OrderId)
        {
            try
            {
                var OrderData = Context.tblOrders.Where(x => x.id == OrderId).Select(x => x).FirstOrDefault();
                if (OrderData != null)
                {
                    Context.tblOrders.RemoveRange(Context.tblOrders.Where(x => x.id == OrderId).ToList());
                    Context.tblOrderCharges.RemoveRange(Context.tblOrderCharges.Where(x => x.Fk_order_id == OrderId).ToList());
                    Context.Database.ExecuteSqlCommand("delete tda from tblDeliveryAddress as tda inner join tblOrder as todr on todr.Fk_delivery_address_id = tda.id where todr.id =" + OrderId.ToString());
                    var UpdateRecivedShipData = Context.tblReceivedShipments.Where(x => x.Fk_order_id == OrderId).ToList();
                    UpdateRecivedShipData.ForEach(x => { x.Fk_order_id = 0; x.status = 1; });
                    Context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class ShipmentMethodPackage
    {
        public string shipmentMethodPrice { get; set; }
        public string shipmentdescription { get; set; }
        public string shipmentcode { get; set; }
    }

    public class PacakageOptionArray
    {
        public string PackageOptionPrice { get; set; }
        public string PackageOptionTitle { get; set; }
    }
}