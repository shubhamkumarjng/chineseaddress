using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace _ChineseAddress.com.Areas.User.Models
{
    public class ViewModelOrderinfo
    {
        public int id { get; set; }
        public DateTime? orderon { get; set; }
        public string orderno { get; set; }
        public string shipmethod { get; set; }
        public int status { get; set; }
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string City { get; set; }
        public string state { get; set; }
        public string Postal_Code { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
        public string trackingnumber { get; set; }
        public decimal totalcharges { get; set; }
        public string totalpackages { get; set; }
        public decimal billableweight { get; set; }
        public decimal totalamount { get; set; }
        public string carrier_logo { get; set; }
        public decimal WalletBalance { get; set; }

        public string order_notes { get; set; }
        public List<tblOrderCharge> OrderChargeDetails { get; set; }
        public string totalshipment { get; set; }

        [Display(Name = "Terms and Conditions")]
        public bool Terms { get; set; }
    }
}