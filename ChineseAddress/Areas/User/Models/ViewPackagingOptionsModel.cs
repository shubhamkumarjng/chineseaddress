namespace _ChineseAddress.com.Areas.User.Models
{
    public class ViewPackagingOptionsModel
    {
        public string title { get; set; }
        public decimal? price { get; set; }
        public string description { get; set; }
        public string ShipmentCheckboxID { get; set; }
        public string Addressdropid { get; set; }
    }
}