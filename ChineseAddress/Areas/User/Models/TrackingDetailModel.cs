using System;
using System.Collections.Generic;

namespace _ChineseAddress.com.Areas.User.Models
{
    public class TrackingDetailModel
    {
        public string TrackingNumber { get; set; }
        public List<Address> AddressDetail { get; set; }
        public string Reciverby { get; set; }
        public class Address
        {
            public DateTime Date { get; set; }
            public string City { get; set; }
            public string Country { get; set; }
            public string PostalCode { get; set; }
            public string State { get; set; }
            public string status { get; set; }
          
        }
    }
}