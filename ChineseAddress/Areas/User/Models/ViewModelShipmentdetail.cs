namespace _ChineseAddress.com.Areas.User.Models
{
    public class ViewModelShipmentdetail
    {
        public int id { get; set; }
        public string tracking { get; set; }
        public string sender { get; set; }
        public string weight { get; set; }
        public string status { get; set; }
        public System.DateTime date { get; set; }
    }
}