namespace _ChineseAddress.com.Areas.User.Models
{
    public class ViewModelOrderCharges
    {
        public string charge_name { get; set; }
        public decimal sell_price { get; set; }
    }
}