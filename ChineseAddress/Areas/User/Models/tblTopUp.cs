using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace _ChineseAddress.com.Repository
{
    [MetadataType(typeof(tblTopUpMetaData))]
    public partial class tblTopUp
    {
        [Required(ErrorMessage = "Top Up Method is required")]
        public string TopUpMethodId { get; set; }
        [RegularExpression(@"^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.png|.PNG|.jpg|.JPG|.jpeg|.JPEG|.bmp|.BMP|.txt|.pdf|.PDF|.doc|.DOC|.xlsx|.XLSX)$", ErrorMessage = "Only '.png |.jpg |.jpeg |.bmp |.txt |.pdf |.doc |.xlsx ' file formats are allowed.")]
        public HttpPostedFileBase attachment { get; set; }

        public string TopUpMethodName { get; set; }

        public string CustomerRefNo { get; set; }
    }

    sealed class tblTopUpMetaData
    {
        [Required(ErrorMessage = "Customer is required")]
        public int Fk_Customer_Id { get; set; }
        public int Fk_TopUpMethod { get; set; }
        [Required(ErrorMessage = "Reference Number is rquired")]
        public string transaction_id { get; set; }

        [DisplayFormat(DataFormatString = "{0:00}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Amount is rquired")]
        public decimal amount { get; set; }
        public string remark { get; set; }
        [Required(ErrorMessage = "Status is required")]
        public int status { get; set; }
        public string admin_remark { get; set; }
        public System.DateTime created_on { get; set; } = DateTime.Now;
    }
}