using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _ChineseAddress.com.Repository;
using System.Web.Mvc;
using PagedList;
using System.Threading.Tasks;

namespace _ChineseAddress.com.Areas.User.IServices
{
    public interface IWalletService<T, T1> where T : class
    {
        bool _isSucceeded { get; set; }
        string _msg { get; set; }
        List<SelectListItem> getTopUpMethodsAsItems();
        void AddUpdateTopUpWalletRequest(T _tblTopUpbool, bool IsAdmin = false);

        List<tblTopUp> getTopUpDetail();
        tblTopUp getTopUpDetailById(int id);
        IPagedList<tblTopUp> getTopUpDetailByCustomerId(int CustomerId, int pageNumber);
        IEnumerable<tblTopUpMethod> getTopUpMethods();
        string getTopUpMethodDescriptionByMehodId(int TopUpMethodId);

        void sendEmailOnAddEditTopUpRequest(tblTopUp _tblTopUp, bool isNew = true);
        void sendEmailOnAddTopUpRequestToAdmin(tblTopUp _tblTopUp, bool isNew);
        void UpdateWalletBalance(tblTopUp _tblTopUp);
        decimal yourBalance(int CustomerId);

    }
}