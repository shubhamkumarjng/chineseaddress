using PagedList;
using System.Collections.Generic;

namespace _ChineseAddress.com.Areas.User.IServices
{
    public interface IOrder<T, T1, T2, T3, T4, T5> where T : class
    {
        T GetOrderInformation(int ID);

        dynamic GetTblOrdeChargedByFkId(int id);
        decimal GetOrderCharges(int id);

        List<T5> GetTblOrdeBoxesByFkId(int ID);

        List<T2> CreateOrderRecivedShipment(int ID);

        List<T2> GetShipmentDetails();

        string CancleOrder(int entity);

        string ConfirmOrder(int entity);

        string SaveNotes(T4 entity);

        IPagedList<T3> GettblShipmentReturnDetail(int ID, int PageNo);

        // string ChangeStatusToProcessingById(int ID);
        //List<T5> GetOrderboxesData(int entity);
    }
}