using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Models;
using PagedList;
using System.Collections.Generic;

namespace _ChineseAddress.com.Areas.User.IServices
{
    public interface ISettingService<T, T1, T2, T3, T4, T5, T6, T7, T8> where T : class
    {
        #region Properites

        bool is_Success { get; set; }

        #endregion Properites

        string CreateUpdateNewAddress(T entity);

        IPagedList<T> GetAddressBookData(int? Id, bool Is_Default, int pageNumber = 1);

        T GetAddressBookData(int Id);

        string DeleteSingleAddressByID(int id);

        string ChangeUserPassword(ChangePasswordModel entity);

        string ChangeUserEmail(ChangeEmailId entity);

        bool CheckDefaultAddress(string entity, string e);

        bool CheckPassword(string entity);

        bool CheckOldPasswordandNewPassword(string entity);

        T1 GetTblCustomerData();

        string CreateUpdatePaymentMethod(ViewCustomerPaymentModel entity);

        string insertrewardpoint(decimal entity);

        IPagedList<T5> getRewardPointsDetail(int pageNumber);

        ViewCustomerPaymentModel GetAddressBookDataByFkId();

        List<T7> GetCreaditCards();

        ViewCustomerPaymentModel GetCreaditCardsById(int entity);

        void ChangeDefaultCreditCard(int CreditCardId);

        List<T3> Plans();

        List<T4> OptionPlan();

        T8 PlanLinkingId(int Id);

        string UpdateBillingAddressById(ViewCustomerPaymentModel entity);
    }
}