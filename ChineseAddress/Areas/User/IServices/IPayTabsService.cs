using _ChineseAddress.com.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _ChineseAddress.com.Areas.User.IServices
{
    interface IPayTabsService
    {
        string ApiKey { get; set; }
        string merchant_email { get; set; }

        string secret_key { get; set; }
        string cc_first_name { get; set; }
        string cc_last_name { get; set; }
        string phone_number { get; set; }
        string billing_address { get; set; }

        //USA or CANADA String of 2 characters containing the ISO state code
        string state { get; set; }
        string city { get; set; }
        string postal_code { get; set; }
        string country { get; set; }
        string email { get; set; }
        decimal amount { get; set; }
        decimal other_charges { get; set; }
        //optional
        decimal discount { get; set; }
        //optional
        string reference_no { get; set; }
        string currency { get; set; }
        string title { get; set; }
        string ip_customer { get; set; }
        string ip_merchant { get; set; }
        string return_url { get; set; }
        string address_shipping { get; set; }
        string city_shipping { get; set; }
        string state_shipping { get; set; }
        string postal_code_shipping { get; set; }
        string country_shipping { get; set; }
        string quantity { get; set; }
        string unit_price { get; set; }
        string products_per_title { get; set; }
        string ChannelOfOperations { get; set; }
        string ProductCategory { get; set; }
        string ProductName { get; set; }
        string ShippingMethod { get; set; }
        string DeliveryType { get; set; }
        string CustomerId { get; set; }
        string msg_lang { get; set; }
        string payment_reference { get; set; }
        string is_tokenization { get; set; }
        string is_existing_customer { get; set; }
        string pt_token { get; set; }
        string pt_customer_email { get; set; }
        string pt_customer_password { get; set; }
        bool _isSucceded { get; set; }
        string _msg { get; set; }

        void PayTabAuthentication();
        void CreatePayPage(out string paymentURL);
        void CreatePayPageWithToken(out string paymentURL);
        string ValidateApiKey();
        PayTabsResponseModel VerifyPayment();
        string LogoutPayTab();
    }
}