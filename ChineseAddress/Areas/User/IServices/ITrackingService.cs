using System.Collections.Generic;

namespace _ChineseAddress.com.Areas.User.IServices
{
    public interface ITrackingService<T> where T : class
    {
        bool _isSuccess { get; set; }
        string _msg { get; set; }
        List<T> _TrackingDetail { get; set; }

        T TrackOrder(string TrackingNumber);
    }
}