using PagedList;
using System.Collections.Generic;

namespace _ChineseAddress.com.Areas.User.IServices
{
    public interface IPaymentService<T, T1> where T : class
    {
        bool _isSuccess { get; set; }

        string _msg { get; set; }

        IPagedList<T> GetCustomerPayment(int pageNumber);

        string ProcessPayment(int orderId, int creditCardId, string type);
        bool ProcessPaymentReturnShipment(int receivedShipmentId, int cust_id, decimal charge);
        string ProcessPaymentViaWallet(int orderId, int addressBookId, string type);

        T1 GetOrderDetail(int OrderId);

        string ProcessUpgradePlanPayment(int PlanId, int creditCardId);

        string ProcessTopUpPayment(decimal payableAmount, int BookingAddressId);
    }

}