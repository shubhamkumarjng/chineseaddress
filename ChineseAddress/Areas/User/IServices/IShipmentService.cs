using System;
using System.Collections.Generic;

namespace _ChineseAddress.com.Areas.User.IServices

{
    public interface IShipmentService<T, T1, T2, T3, T4, T5, T6, T7, T8, T9> where T : class
    {
        List<T> GetRecievedShipment(int ID);

        string SaveNotes(T entity);

        string SaveInvoicefilepath(T objrecivedship);

        decimal yourpoints();

        decimal yourBalance();

        List<T1> GetOrderRecordList(int ID);

        T2 GetCustomerPlans(int ID);

        List<T3> GetCustomerPayment(int ID);

        List<T4> GetReciveShipmentDetails(int ID, bool isOrderCreated = false);

        List<T4> GetShipmentDetails();

        T5 GettblAdressData(int ID);

        List<T6> GetoptionplanPlandata(int ID);

        string CancelRecShipment(int ID);

        string GetRequestPicture(int ID);

        string GetReturnPanding(int ID);

        string ReturnPackageData(T4 objreturn);

        string CreateShipmentOrder(List<T8> entity1, List<T9> entity2, T7 entitys, string CouponCode, string Order_Note);

        string GetCustomerRefNo(int ID);

        string GetOrderIdByreferenceNo(string referenceNo);

        Tuple<bool, string> validateCouponCode(string coupon_code, string country_name);

        List<T> GetRecievedShipmentRecords(int ID);

        string GetCustomerPlan();
    }
}