﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _ChineseAddress.com.Repository
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class Context4InshipEntities : DbContext
    {
        public Context4InshipEntities()
            : base("name=ChineseAddress_Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tblAddressBook> tblAddressBooks { get; set; }
        public virtual DbSet<tblAdminUser> tblAdminUsers { get; set; }
        public virtual DbSet<tblBillingAddress> tblBillingAddresses { get; set; }
        public virtual DbSet<tblcarrierholiday> tblcarrierholidays { get; set; }
        public virtual DbSet<tblCarriersDetail> tblCarriersDetails { get; set; }
        public virtual DbSet<tblCityMaster> tblCityMasters { get; set; }
        public virtual DbSet<tblCountryMaster> tblCountryMasters { get; set; }
        public virtual DbSet<tblCoupon> tblCoupons { get; set; }
        public virtual DbSet<tblCreditCard> tblCreditCards { get; set; }
        public virtual DbSet<tblCustomerPlanLinking> tblCustomerPlanLinkings { get; set; }
        public virtual DbSet<tblDeliveryAddress> tblDeliveryAddresses { get; set; }
        public virtual DbSet<tblEmailNotification> tblEmailNotifications { get; set; }
        public virtual DbSet<tblExceptionlog> tblExceptionlogs { get; set; }
        public virtual DbSet<tblInvoice> tblInvoices { get; set; }
        public virtual DbSet<tblOrder> tblOrders { get; set; }
        public virtual DbSet<tblOrderBox> tblOrderBoxes { get; set; }
        public virtual DbSet<tblOrderCharge> tblOrderCharges { get; set; }
        public virtual DbSet<tblPackagingOption> tblPackagingOptions { get; set; }
        public virtual DbSet<tblPlanOptionLinking> tblPlanOptionLinkings { get; set; }
        public virtual DbSet<tblPlan> tblPlans { get; set; }
        public virtual DbSet<tblPrice> tblPrices { get; set; }
        public virtual DbSet<tblReceivedShipment> tblReceivedShipments { get; set; }
        public virtual DbSet<tblRewardAmount> tblRewardAmounts { get; set; }
        public virtual DbSet<tblRewardPoint> tblRewardPoints { get; set; }
        public virtual DbSet<tblShipmentDetail> tblShipmentDetails { get; set; }
        public virtual DbSet<tblShipmentImage> tblShipmentImages { get; set; }
        public virtual DbSet<tblShipmentReturnDetail> tblShipmentReturnDetails { get; set; }
        public virtual DbSet<tblSignupCouponConsumed> tblSignupCouponConsumeds { get; set; }
        public virtual DbSet<uv_GetExpiryNotificationEmails> uv_GetExpiryNotificationEmails { get; set; }
        public virtual DbSet<view_tblPrices> view_tblPrices { get; set; }
        public virtual DbSet<tblTopUpMethod> tblTopUpMethods { get; set; }
        public virtual DbSet<tblTopUp> tblTopUps { get; set; }
        public virtual DbSet<tblCustomer> tblCustomers { get; set; }
    
        public virtual ObjectResult<tblPrice> sp_ef_calcRates(string service, string code, Nullable<decimal> weight, string country)
        {
            var serviceParameter = service != null ?
                new ObjectParameter("service", service) :
                new ObjectParameter("service", typeof(string));
    
            var codeParameter = code != null ?
                new ObjectParameter("code", code) :
                new ObjectParameter("code", typeof(string));
    
            var weightParameter = weight.HasValue ?
                new ObjectParameter("weight", weight) :
                new ObjectParameter("weight", typeof(decimal));
    
            var countryParameter = country != null ?
                new ObjectParameter("country", country) :
                new ObjectParameter("country", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<tblPrice>("sp_ef_calcRates", serviceParameter, codeParameter, weightParameter, countryParameter);
        }
    
        public virtual ObjectResult<tblPrice> sp_ef_calcRates(string service, string code, Nullable<decimal> weight, string country, MergeOption mergeOption)
        {
            var serviceParameter = service != null ?
                new ObjectParameter("service", service) :
                new ObjectParameter("service", typeof(string));
    
            var codeParameter = code != null ?
                new ObjectParameter("code", code) :
                new ObjectParameter("code", typeof(string));
    
            var weightParameter = weight.HasValue ?
                new ObjectParameter("weight", weight) :
                new ObjectParameter("weight", typeof(decimal));
    
            var countryParameter = country != null ?
                new ObjectParameter("country", country) :
                new ObjectParameter("country", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<tblPrice>("sp_ef_calcRates", mergeOption, serviceParameter, codeParameter, weightParameter, countryParameter);
        }
    
        public virtual int usp_AdminCoupon(Nullable<int> order_id, string couponCode, ObjectParameter returnparameter)
        {
            var order_idParameter = order_id.HasValue ?
                new ObjectParameter("Order_id", order_id) :
                new ObjectParameter("Order_id", typeof(int));
    
            var couponCodeParameter = couponCode != null ?
                new ObjectParameter("CouponCode", couponCode) :
                new ObjectParameter("CouponCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_AdminCoupon", order_idParameter, couponCodeParameter, returnparameter);
        }
    
        public virtual int usp_IsCouponValidUser(string coupon_Code, Nullable<int> customerId, string countryToName, ObjectParameter isValid, ObjectParameter msg)
        {
            var coupon_CodeParameter = coupon_Code != null ?
                new ObjectParameter("Coupon_Code", coupon_Code) :
                new ObjectParameter("Coupon_Code", typeof(string));
    
            var customerIdParameter = customerId.HasValue ?
                new ObjectParameter("CustomerId", customerId) :
                new ObjectParameter("CustomerId", typeof(int));
    
            var countryToNameParameter = countryToName != null ?
                new ObjectParameter("CountryToName", countryToName) :
                new ObjectParameter("CountryToName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_IsCouponValidUser", coupon_CodeParameter, customerIdParameter, countryToNameParameter, isValid, msg);
        }
    }
}
