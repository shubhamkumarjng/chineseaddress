namespace _ChineseAddress.com.IServices
{
    public interface ICustomerServices<T, TS> where T : class
    {
        bool prop_success { get; set; }

        string CustomerSignUp(T entity, int IsReferal);

        TS GetAddressBookData(int ID);

        T GetTblCustomerIdByUserNamePassword(T entity);

        T GetTblCustomerIdByUserNamePasswords(string entity);

        string CustomerPayBillingAddress(TS entity);

        string UpdateUserEmailPassword(string entity);

        bool isUserExists(string email);

        string FacebookSocialapi(string Socialemail, string SocialID);

        string GoogleSocialapi(string Socialemail, string SocialID);

        TS CustomerAdditionalInformation(int ID);
    }
}