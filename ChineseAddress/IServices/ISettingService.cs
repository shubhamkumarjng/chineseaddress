using System.Collections.Generic;

namespace _ChineseAddress.com.IServices
{
    public interface ISettingService<T> where T : class
    {
        List<T> GetAddressbookListById(int entity);

        string CreateUpdateNewAddress(T entity, string txnId);
    }
}