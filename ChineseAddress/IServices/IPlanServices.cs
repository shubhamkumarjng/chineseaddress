using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using System.Collections.Generic;

namespace _ChineseAddress.com.IServices
{
    public interface IPlanServices<Planservice> where Planservice : class
    {
        List<tblPlan> Plans();

        List<PlansOptionModel> OptionPlan();
    }
}