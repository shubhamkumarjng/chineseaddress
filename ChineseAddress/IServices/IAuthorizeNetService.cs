using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;

namespace _ChineseAddress.com.IServices
{
    public interface IAuthorizeNetService
    {
        ViewCustomerPaymentModel _CustCardAndBilling { get; set; }
        tblInvoice _InvoiceDetail { get; set; }
        tblCustomer _CustomerDetail { get; set; }
        tblCreditCard _CreditCard { get; set; }
        tblBillingAddress _billingAddress { get; set; }
        string _CustomerProfileId { get; set; }
        string _CustomerPaymentProfileId { get; set; }
        string _CustomerShipppingProfileId { get; set; }
        string _msg { get; set; }

        //recurringInterval from
        short recurringInterval { get; set; }

        int Fk_billing_address_id { get; set; }

        //Referrer_Id
        int referrer_Id { get; set; }

        //orderId
        tblOrder _orderDetail { get; set; }

        bool CreateCustomerProfile();

        bool ChargeCustomerProfile();

        bool CreateSubscription();
    }
}