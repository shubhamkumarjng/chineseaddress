using System;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Linq;
using System.Net;
using _ChineseAddress.com.Common.Services;

namespace _ChineseAddress.com
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static int global_Page_Size { get; set; }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UnityConfig.RegisterComponents();
            BundleTable.EnableOptimizations = true;
            ModelBinders.Binders.DefaultBinder = new TrimModelBinder();
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            global_Page_Size = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxPageSize"]);
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null || authCookie.Value == "")
            {
                return;
            }

            FormsAuthenticationTicket authTicket;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch
            {
                return;
            }
            string[] roles = authTicket.UserData.Split(';');
            if (Context.User != null)
            {
                Context.User = new GenericPrincipal(Context.User.Identity, roles);
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                Exception exception = Server.GetLastError();
                if (exception != null)
                {
                    if (exception.Message != "Cannot redirect after HTTP headers have been sent.")
                    {
                        HttpCookie authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
                        var userName = Context.User.Identity;
                        string IdentityUser = userName.Name;
                        clsCommonServiceRoot objclsCommonServiceRoot = new clsCommonServiceRoot();
                        objclsCommonServiceRoot.Errorhandling(exception, IdentityUser);
                    }
                }
                //
                // TODO: Log the exception or something 
                //Response.Clear();
                //Server.ClearError();
                //RouteData routeData = new RouteData();
                //routeData.Values["controller"] = "Error";
                //routeData.Values["action"] = "Index";
                //routeData.Values["msg"] = Convert.ToString(exception.Message);
                ////Response.StatusCode = 500;
                //IController controller = new _ChineseAddress.com.Controllers.ErrorController();
                //RequestContext rc = new RequestContext(new HttpContextWrapper(Context), routeData);
                //controller.Execute(rc);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public class TrimModelBinder : DefaultModelBinder
        {
            protected override void SetProperty(ControllerContext controllerContext,
              ModelBindingContext bindingContext,
              System.ComponentModel.PropertyDescriptor propertyDescriptor, object value)
            {
                if (propertyDescriptor.PropertyType == typeof(string))
                {
                    var stringValue = (string)value;
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        value = stringValue.Trim();
                    }
                    else
                    {
                        value = null;
                    }
                }
                base.SetProperty(controllerContext, bindingContext,
                                    propertyDescriptor, value);
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string[] reqAcceptType = HttpContext.Current.Request.AcceptTypes;
            if (reqAcceptType != null)
            {
                if (reqAcceptType.Length <= 3)
                {
                    Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
                    Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
                }
            }

        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            HttpApplication app = sender as HttpApplication;
            string acceptEncoding = app.Request.Headers["Accept-Encoding"];
            System.IO.Stream prevUncompressedStream = app.Response.Filter;
            if (acceptEncoding != null && acceptEncoding.Length > 0)
            {
                acceptEncoding = acceptEncoding.ToLower();
                if (acceptEncoding.Contains("gzip"))
                {
                    app.Response.Filter = new System.IO.Compression.GZipStream(prevUncompressedStream, System.IO.Compression.CompressionMode.Compress);
                    app.Response.AppendHeader("Content-Encoding", "gzip");
                }
            }
            Response.Cache.VaryByHeaders["Accept-Encoding"] = true;
            Response.Headers["Server"] = "changed";
            Response.Headers.Remove("Content-Length");
            Response.Headers.Remove("X-AspNet-Version");
            Response.Headers.Remove("X-AspNetMvc-Version");
            Response.Headers.Remove("X-Frame-Options");
            Response.AddHeader("X-Frame-Options", "AllowAll");

        }
    }
}