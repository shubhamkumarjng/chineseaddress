using System.Web.Mvc;
using System.Web.Routing;

namespace _ChineseAddress.com
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            //routes.LowercaseUrls = true;

            #region Route Files Routing Start

            routes.MapRoute(
            name: "Login",
            url: "Login",
            defaults: new { controller = "Customer", action = "Login", id = UrlParameter.Optional },
            namespaces: new[] { "_ChineseAddress.com.Controllers" }
           );
            routes.MapRoute(
            name: "SignUp",
            url: "SignUp/{id}",
            defaults: new { controller = "Customer", action = "SignUp", id = UrlParameter.Optional },
            namespaces: new[] { "_ChineseAddress.com.Controllers" }
           );
            routes.MapRoute(
            name: "CustomerPayment",
            url: "Payment/{id}",
            defaults: new { controller = "Customer", action = "CustomerPayment", id = UrlParameter.Optional },
            namespaces: new[] { "_ChineseAddress.com.Controllers" }
           );
            routes.MapRoute(
            name: "Index",
            url: "Rates",
            defaults: new { controller = "Rates", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "_ChineseAddress.com.Controllers" }
           );

            routes.MapRoute(
            name: "AboutUs",
            url: "AboutUs",
            defaults: new { controller = "AboutUs", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "_ChineseAddress.com.Controllers" }
           );
            routes.MapRoute(
            name: "Agreement",
            url: "Agreement",
            defaults: new { controller = "Agreement", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "_ChineseAddress.com.Controllers" }
           );
            routes.MapRoute(
            name: "Privacy",
            url: "Privacy",
            defaults: new { controller = "Privacy", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "_ChineseAddress.com.Controllers" }
           );
            routes.MapRoute(
            name: "Faqs",
            url: "Faqs",
            defaults: new { controller = "Faqs", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "_ChineseAddress.com.Controllers" }
           );
            routes.MapRoute(
            name: "Payment",
            url: "Payment/{id}",
            defaults: new { controller = "Payment", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "_ChineseAddress.com.Controllers" }
           );
            routes.MapRoute(
             name: "Newsletter",
             url: "Newsletter/{id}",
             defaults: new { controller = "Newsletter", action = "Index", id = UrlParameter.Optional },
             namespaces: new[] { "_ChineseAddress.com.Controllers" }
            );
            routes.MapRoute(
 name: "PaymentMethods",
 url: "payment-methods",
 defaults: new { controller = "PaymentMethods", action = "Index" },
 namespaces: new[] { "_ChineseAddress.com.Controllers" }
);

            #endregion Route Files Routing Start

            routes.MapRoute(
            name: "Default",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "_ChineseAddress.com.Controllers" }
            );
        }
    }
}