﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _4InShip.com.Repository;
namespace _4InShip.com
{
    public abstract class AutoMapperBase
    {
        protected readonly IMapper _mapper;

        protected AutoMapperBase()
        {
            var config = new MapperConfiguration(x =>
            {
                x.CreateMap<tblAddressBook, tblBillingAddress>();
            });

            _mapper = config.CreateMapper();
        }
    }

}