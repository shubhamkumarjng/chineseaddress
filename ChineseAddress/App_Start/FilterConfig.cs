using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace _ChineseAddress.com
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }

    //public class CustomAuthorization : AuthorizeAttribute
    //{
    //    public string Url { get; set; }

    //    protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
    //    {
    //        filterContext.Result = new RedirectResult(Url + "?returnUrl=" + filterContext.HttpContext.Request.Url.PathAndQuery, true);
    //    }
    //}

    public class CustomAuthorize : AuthorizeAttribute
    {
        public string Url { get; set; }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectResult(Url + "?returnUrl=" + filterContext.HttpContext.Request.Url.PathAndQuery);
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (this.AuthorizeCore(filterContext.HttpContext))
            {
                if (filterContext.HttpContext.Session["AdminRole"] == null)
                {
                    HttpSessionStateBase Session = new HttpSessionStateWrapper(HttpContext.Current.Session);
                    FormsIdentity fi;
                    fi = (FormsIdentity)filterContext.HttpContext.User.Identity;
                    string[] ud = fi.Ticket.UserData.Split(';');
                    Session["AdminRole"] = ud[0].ToString();
                    Session["AdminId"] = int.Parse((string.IsNullOrEmpty(ud[1]) ? "0" : ud[1]));
                    Session["AdminUsername"] = ud[2];
                }
                base.OnAuthorization(filterContext);
            }
            else
            {
                this.HandleUnauthorizedRequest(filterContext);
            }
        }
    }

    public class CustomActionFilter : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            (new _ChineseAddress.com.Areas.User.Models.clsAuthenticateData()).CheckUserAuthenticated();
        }
    }

    //public class IsAuthenticateAdminFilter : ActionFilterAttribute, IActionFilter
    //{
    //    void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        (new _ChineseAddress.com.Areas.Admin.Models.clsAuthenticateAdmin()).CheckUserAuthenticated();
    //    }
    //}
}