using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Areas.User.Models;
using _ChineseAddress.com.Areas.User.Services;
using _ChineseAddress.com.IServices;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.Services;
using Microsoft.Practices.Unity;
using System.Web.Mvc;
using Unity.Mvc5;

namespace _ChineseAddress.com
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IPlanServices<PlansOptionModel>, PlanServices>();
            container.RegisterType<ICustomerServices<ViewCustomerModel, ViewCustomerPaymentModel>, CustomerServices>();
            container.RegisterType<IShipmentService<tblReceivedShipment, tblOrder, UserViewCustomerModel, UserPaymentModel, ViewRecivedShipmentModel, tblAddressBook, ViewPackagingOptionsModel, ViewOrderModel, ShipmentMethodPackage, PacakageOptionArray>, ShipmentsService>();
            container.RegisterType<IOrder<ViewModelOrderinfo, tblOrderCharge, ViewRecivedShipmentModel, ViewOrderModel, tblReceivedShipment, tblOrderBox>, OrderService>();
            container.RegisterType<IPaymentService<UserPaymentModel, ViewModelOrderinfo>, PaymentService>();
            container.RegisterType<ISettingService<tblAddressBook, tblCustomer, tblCreditCard, tblPlan, PlansOptionModel, tblRewardPoint, ViewCustomerPaymentModel, tblCreditCardViewModel, tblCustomerPlanLinking>, SettingService>();
            container.RegisterType<IAuthorizeNetService, AuthorizeNetService>();
            container.RegisterType<ITrackingService<TrackingDetailModel>, TrackingService>();
            container.RegisterType<IWalletService<tblTopUp, tblTopUpMethod>, WalletService>();
            container.RegisterType<IPayTabsService, PayTabsService>();

            container.RegisterType<Common.Services.clsEmailTemplateService>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}