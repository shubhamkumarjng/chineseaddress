﻿using System.Collections.Generic;
using System.Linq;

public static class SkipTake
{
    public static IEnumerable<T> ToSkipTake<T>(this IEnumerable<T> records, int currentPage = 1, int DisplayLength = 10)
    {
        return records.Skip(((currentPage * DisplayLength) - DisplayLength)).Take(DisplayLength);
    }
}