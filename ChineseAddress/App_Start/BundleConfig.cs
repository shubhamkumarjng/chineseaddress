using System.Web.Optimization;

namespace _ChineseAddress.com
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region js Bundling

            //load Async javascript
            bundles.Add(new ScriptBundle("~/bundles/load_async_css").Include("~/Scripts/_loadAsyncCSS.min.js"));
            bundles.Add(new ScriptBundle("~/Areas/Admin/bundles/load_async_css").Include("~/Areas/Admin/Scripts/_loadAsyncCSS.min.js"));

            var jqueryBundle = new ScriptBundle("~/bundles/jquery")
            .Include("~/Scripts/jquery-1.12.4.min.js");
            jqueryBundle.CdnPath = "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js";
            jqueryBundle.CdnFallbackExpression = "window.jquery";
            bundles.Add(jqueryBundle);

            var bootbox = new ScriptBundle("~/bundles/bootbox")
                .Include("~/Scripts/bootbox.min.js");
            bootbox.CdnPath = "https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js";
            bootbox.CdnFallbackExpression = "window.jquery";
            bundles.Add(bootbox);

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate.bundle.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/BasicJS").Include(new[]
            {
                "~/Scripts/Unveil.min.js",
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/respond.min.js",
                "~/Scripts/pgwmodal.min.js",
                "~/Scripts/main.min.js",
                "~/Scripts/js_developer.min.js",
                "~/Scripts/bootbox.min.js"
            }));
            bundles.Add(new ScriptBundle("~/bundles/select2").Include(
                        "~/Scripts/select2.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/home").Include(new[] {
                "~/Scripts/wow.min.js",
                "~/Scripts/jquery.nicescroll.min.js",
                "~/Scripts/jquery.easing.min.js",
                "~/Scripts/jquery.mixitup.min.js",
                "~/Scripts/imagesloaded.pkgd.min.js",
                "~/Scripts/skillset.min.js",
                "~/Scripts/owl.carousel.min.js",
                "~/Scripts/scrollupto.min.js",
                "~/Scripts/jquery.carouFredSel-6.0.4-packed.js",
                "~/Scripts/Carousel.min.js"
                 //"~/Scripts/jquery.cookiebar.min.js"
            }));

            bundles.Add(new ScriptBundle("~/bundles/homemain").Include(
                        "~/Scripts/main.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/fileinput").Include(
                        "~/Scripts/fileinput.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/lightbox/lightgallery").Include(
                "~/Scripts/lightbox/lightbox-bundle.min.js"
                        //"~/Scripts/lightbox/lightgallery.min.js",
                        //"~/Scripts/lightbox/lg-lg-fullscreen.min.js",
                        //"~/Scripts/lightbox/lg-hash.min.js",
                        //"~/Scripts/lightbox/lg-pager.min.js",
                        //"~/Scripts/lightbox/lg-share.min.js",
                        //"~/Scripts/lightbox/lg-thumbnail.min.js",
                        //"~/Scripts/lightbox/lg-zoom.min.js"

                        ));

            #endregion js Bundling

            #region css Bundling

            //bundles.Add(new StyleBundle("~/content/css/home").Include(new[] {
            //    "~/Content/css/style.min.css",
            //          "~/Content/css/animate.min.css",
            //          "~/Content/css/skillset.min.css",
            //          "~/Content/css/owl.carousel.min.css",
            //          "~/Content/css/owl.transitions.min.css",
            //          "~/Content/css/owl.theme.min.css"}
            //          ));

            bundles.Add(new StyleBundle("~/content/css/home").Include(new[] {
                "~/Content/css/home.min.css",
                "~/Content/css/product-slider.min.css" }));
            bundles.Add(new StyleBundle("~/content/css/css_developer").Include(new[] {
                "~/Content/css/css_developer.min.css",
                "~/Content/css/css_developer_header_menu.min.css"
            }));
            bundles.Add(new StyleBundle("~/content/css/basecss").Include(new[] {
                "~/Content/css/bootstrap.min.css",
                "~/Content/css/fonts.googleapis_css_family_Open_Sans_400,300,600.min.css",
                "~/Content/css/fonts.googleapis_css_family_Raleway_400,500,300,600.min.css",
                "~/Content/css/fonts.googleapis_css_family_Roboto_400,100,300,500,700,400italic,300italic.min.css",
                "~/Content/css/font-awesome.min.css",
                "~/Content/css/pgwmodal.min.css",
                "~/Content/css/Security.min.css",
                "~/Content/css/jquery.cookiebar.min.css"
            }));

            bundles.Add(new StyleBundle("~/content/css/mystyle").Include("~/Content/css/myStyle.min.css"));
            bundles.Add(new StyleBundle("~/content/css/select2").Include("~/Content/css/select2.min.css"));
            bundles.Add(new StyleBundle("~/content/css/fileinput").Include("~/Content/css/fileinput.min.css"));
            bundles.Add(new StyleBundle("~/content/css/lightgallery").Include("~/Content/css/lightgallery.min.css"));

            #endregion css Bundling

            #region Admin js Bundling

            bundles.Add(new ScriptBundle("~/Areas/Admin/bundles/layout").Include(
                "~/Areas/Admin/Scripts/bootstrap.min.js",
                "~/Areas/Admin/Scripts/pgwmodal.min.js",
                "~/Areas/Admin/Scripts/select2.min.js",
                "~/Areas/Admin/Scripts/icheck.min.js",
                "~/Areas/Admin/Scripts/app.min.js",
                "~/Areas/Admin/Scripts/fileinput.min.js",
                "~/Areas/Admin/Scripts/demo.min.js",
                "~/Areas/Admin/Scripts/jquery.mousewheel.min.js",
                "~/Areas/Admin/Scripts/bootstrap-datepicker.min.js",
                "~/Areas/Admin/Scripts/JS_developer.min.js"
                ));
            bundles.Add(new ScriptBundle("~/Areas/Admin/bundles/layoutBundle").Include("~/Areas/Admin/Scripts/layout.bundle.min.js"));



            bundles.Add(new ScriptBundle("~/Areas/Admin/bundles/datatables").Include(
                "~/Areas/Admin/Scripts/jquery.dataTables.min.js",
                "~/Areas/Admin/Scripts/dataTables.bootstrap.min.js",
                "~/Areas/Admin/Scripts/dataTables.responsive.min.js",
                "~/Areas/Admin/Scripts/responsive.bootstrap.min.js",
                "~/Areas/Admin/Scripts/dataTables.buttons.min.js",
                "~/Areas/Admin/Scripts/buttons.colVis.min.js"
                ));
            //bundles.Add(new ScriptBundle("~/Areas/Admin/bundles/lightbox/lightgallery").Include(
            //          "~/Areas/Admin/Scripts/lightbox/lightgallery.min.js",
            //           "~/Areas/Admin/Scripts/lightbox/lg-lg-fullscreen.min.js",
            //           "~/Areas/Admin/Scripts/lightbox/lg-hash.min.js",
            //           "~/Areas/Admin/Scripts/lightbox/lg-pager.min.js",
            //           "~/Areas/Admin/Scripts/lightbox/lg-share.min.js",
            //           "~/Areas/Admin/Scripts/lightbox/lg-thumbnail.min.js",
            //           "~/Areas/Admin/Scripts/lightbox/lg-zoom.min.js"));
            bundles.Add(new ScriptBundle("~/Areas/Admin/bundles/lightbox/lightgallery").Include("~/Areas/Admin/Scripts/lightbox/lightbox-bundle.min.js"));

            #endregion Admin js Bundling

            #region Admin Css Bundling

            bundles.Add(new StyleBundle("~/Areas/Admin/css/preloadlayout").Include(
                 //"~/Areas/Admin/css/bootstrap.min.css",
                 "~/Areas/Admin/css/skin-green.min.css",
                 "~/Areas/Admin/css/AdminLTE.min.css",
                "~/Areas/Admin/css/designer_css_developer.min.css"
                ));
            bundles.Add(new StyleBundle("~/Areas/Admin/css/layout").Include(
                 //"~/Areas/Admin/css/font-awesome.min.css",
                 "~/Areas/Admin/css/select2.min.css",
                 "~/Areas/Admin/css/datepicker.min.css",
                 "~/Areas/Admin/css/dataTables.bootstrap.min.css",
                "~/Areas/Admin/css/responsive.bootstrap.min.css",
                "~/Areas/Admin/css/buttons.dataTables.min.css",
                 "~/Areas/Admin/css/desCXZADigner.min.css",
                 "~/Areas/Admin/css/flat/_all.css",
                 "~/Areas/Admin/css/fileinput.min.css",
                "~/Areas/Admin/css/pgwmodal.min.css",
               "~/Areas/Admin/css/css_developer.css"
                ));
            bundles.Add(new StyleBundle("~/Areas/Admin/css/lightgallery").Include("~/Areas/Admin/css/lightgallery.min.css"));

            #endregion Admin Css Bundling
        }
    }
}