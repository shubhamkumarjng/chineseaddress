using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.IServices;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace _ChineseAddress.com.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        private ICustomerServices<ViewCustomerModel, ViewCustomerPaymentModel> _reposatory;

        private CountryServices objCountryServices = new CountryServices();
        private clsCommonServiceRoot objclsCommon;

        public CustomerController(ICustomerServices<ViewCustomerModel, ViewCustomerPaymentModel> repo, clsCommonServiceRoot repoclsCommon)
        {
            _reposatory = repo;
            objclsCommon = repoclsCommon;
        }

        public ActionResult Index()
        {
            ViewBag.CountryBind = objCountryServices.Country();
            return View();
        }

        [HttpGet]
        public ActionResult SignUp(string id)
        {
            if (id != null)
            {
                string Cust_id = Cryptography.Decrypt(id);
                TempData["plan_id"] = Cust_id;
                TempData.Peek("plan_id");
                ViewBag.Country = objCountryServices.Country();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            if (TempData["referrer_id"] != null)
            {
                TempData.Keep("referrer_id");
            }
            ViewBag.Country = objCountryServices.Country();
            return View();
        }

        //--Customer SignUp --//
        [HttpPost]
        public ActionResult SignUp(ViewCustomerModel objViewCustomerModel)
        {
            objViewCustomerModel.Plan_id = Convert.ToInt32(TempData.Peek("plan_id"));

            int referrer_id = 0;
            if (TempData["referrer_id"] != null)
            {
                referrer_id = Convert.ToInt32(TempData["referrer_id"]);
                TempData.Keep("referrer_id");
            }
            ViewBag.Message = _reposatory.CustomerSignUp(objViewCustomerModel, referrer_id);
            if (ViewBag.Message == "showMessage('CouponCode does not exist!',false);")
            {
                ViewBag.Country = objCountryServices.Country();
                return View();
            }
            if (ViewBag.Message == "showMessage('User already exist!',false);")
            {
                ViewBag.Country = objCountryServices.Country();
                return View();
            }
            else if (ViewBag.Message == "Register successfully redirect to user home page")
            {
                TempData["SignUpMessage"] = "showMessage('Thank you for registering with us!',true);";
                return createCookieAndRedirectToUserHome(Convert.ToInt32(Cryptography.Decrypt(Convert.ToString(Session["customerEncriptedId"]))));
            }
            ModelState.Clear();

            string Q = ViewBag.Message;
            string Type = "SignUp";
            ViewBag.Country = objCountryServices.Country();
            return RedirectToAction("CustomerPayment", "Customer", new { Q, Type });
        }

        public JsonResult CheckCoupon(ViewCustomerModel objViewCustomerModel)
        {
            string data = "";
            if (Request.Form["CouponCode"] != null)
            {
                objViewCustomerModel.CuponCode = Request.Form["CouponCode"];

                string SignUpCouponCode = ConfigurationManager.AppSettings["SignUpCouponCode"].ToString();
                string SignUpCouponPlusPlanCode = ConfigurationManager.AppSettings["SignUpCouponPlusPlan"].ToString();
                Context4InshipEntities objContext = new Context4InshipEntities();
                int PlanID = Convert.ToInt32(TempData.Peek("plan_id"));

                var GetPlan = objContext.tblPlans.AsQueryable().Where(x => x.Id == PlanID).Select(x => x).FirstOrDefault();
                if (GetPlan.title == "Basic")
                {
                    if (objViewCustomerModel.CuponCode.ToLower().Trim() == SignUpCouponCode.ToLower().Trim())
                    {
                        data = "";
                        return Json(data, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        data = "Coupon does not exist!";
                    }
                }
                else if (GetPlan.title == "Plus" || GetPlan.title == "Premium")
                {
                    if (objViewCustomerModel.CuponCode.ToLower().Trim() == SignUpCouponPlusPlanCode.ToLower().Trim())
                    {
                        data = "";
                        return Json(data, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        data = "Coupon does not exist!";
                    }
                }
                else
                {
                    data = "Coupon does not exist!";
                }
            }
            else
            {
                data = "Coupon does not exist!";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //-- Get Address Customer show Billing Page and Check AccountExpireDate Method --//
        [HttpGet]
        public JsonResult GetAddressbookData()
        {
            string CustomerID;
            if (Session["customerEncriptedId"] != null)
            {
                CustomerID = Session["customerEncriptedId"].ToString();
                int id = Convert.ToInt32(Cryptography.Decrypt(CustomerID));
                var GetAddressBook = _reposatory.GetAddressBookData(id);
                return Json(GetAddressBook, JsonRequestBehavior.AllowGet);
            }
            else if (Session["CustomerExpDateAccount"] != null)
            {
                CustomerID = Session["CustomerExpDateAccount"].ToString();
                int id = Convert.ToInt32(Cryptography.Decrypt(CustomerID));
                var GetAddressBook = _reposatory.GetAddressBookData(id);
                return Json(GetAddressBook, JsonRequestBehavior.AllowGet);
            }
            return Json(null);
        }



        [HttpGet]
        public ActionResult CustomerPayment()
        {
            ViewCustomerPaymentModel objViewCustomerPaymentModel = BillingInformation();
            Context4InshipEntities objContext = new Context4InshipEntities();
            if (objViewCustomerPaymentModel.referrer_id != 0)
            {
                ViewBag.ReferralMargin = objViewCustomerPaymentModel.PlanAmount - Math.Abs((objViewCustomerPaymentModel.PlanAmount ?? 0.0m) * (100 - Convert.ToDecimal(ConfigurationManager.AppSettings["ReferredSignupMarginPercent"])) / 100);
                if (ViewBag.ReferralMargin != null)
                    objViewCustomerPaymentModel.PayableAmount = objViewCustomerPaymentModel.PayableAmount - Convert.ToDecimal(ViewBag.ReferralMargin);
            }
            BindDropDown();
            return View(objViewCustomerPaymentModel);
        }

        [ChildActionOnly]
        [OutputCache(Duration = 216000)]
        private void BindDropDown()
        {
            ViewBag.Country = objCountryServices.Country();
        }

        [HttpPost]
        public ActionResult CustomerPayment([Bind(Exclude = "Id")]ViewCustomerPaymentModel objViewCustomerPaymentModel)
        {
            if (ModelState.IsValid)
            {
                if (objViewCustomerPaymentModel.Terms == false)
                {
                    TempData["CustomerBillingPayment"] = "showMessage('Please accept Terms and Conditions',false)";
                    BindDropDown();
                    ViewCustomerPaymentModel objCustomerPaymentModel = BillingInformation();
                    return View(objCustomerPaymentModel);
                }
                string CustomerID = "";

                if (Session["customerEncriptedId"] != null)
                {
                    CustomerID = Session["customerEncriptedId"].ToString();
                }
                else if (Session["CustomerExpDateAccount"] != null)
                {
                    CustomerID = Session["CustomerExpDateAccount"].ToString();
                }
                int id = Convert.ToInt32(Cryptography.Decrypt(CustomerID));
                objViewCustomerPaymentModel.Fk_customer_id = id;
                TempData["CustomerBillingPayment"] = _reposatory.CustomerPayBillingAddress(objViewCustomerPaymentModel);
                if (_reposatory.prop_success)
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();
                    //Customer is Activated here
                    Context4InshipEntities ContextCustomer = new Context4InshipEntities();
                    var Cust_Status = ContextCustomer.tblCustomers.AsQueryable().Where(x => x.Id == id).FirstOrDefault();
                    Cust_Status.status = true;
                    ContextCustomer.Entry(Cust_Status).State = System.Data.Entity.EntityState.Modified;
                    ContextCustomer.SaveChanges();
                    TempData["SignUpMessage"] = "showMessage('Thank you for registering with us!',true);";
                    return createCookieAndRedirectToUserHome(Convert.ToInt32(id));
                }
            }
            else
                TempData["CustomerBillingPayment"] = "showMessage('Please fill required fields.',false)";
            BindDropDown();
            return View();
        }

        //--- Customer Login Method--//

        [HttpGet]
        public ActionResult Login(string login)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("User"))
                {
                    FormsIdentity fi;
                    fi = (FormsIdentity)User.Identity;
                    string[] ud = fi.Ticket.UserData.Split(';');
                    Session["UserId"] = Convert.ToInt32((string.IsNullOrEmpty(ud[1]) ? "0" : ud[1]));
                    Session["UserFullName"] = ud[2];
                    Session["UserEmailAddress"] = (ud.Length >= 3 ? "" : ud[3]);
                    return RedirectToAction("Index", new RouteValueDictionary(new { controller = "User/Home", action = "Index" }));
                }
            }
            if (Request.QueryString != null)
            {
                if (Request.QueryString["signup"] == "successfully")
                {
                    ViewBag.Message = "showMessage('You has been registered successfully',true)";
                }
            }
            if (Request.QueryString != null)
            {
                if (Request.QueryString["ReturnUrl"] != null)
                {
                    TempData["RetUrl"] = Request.QueryString["ReturnUrl"];
                }
            }
            ViewCustomerModel tbl = new ViewCustomerModel();
            if (Request.Cookies["UserPassword"] != null)
            {
                string e = Cryptography.Decrypt(Request.Cookies["UserPassword"].Values["UserName"]);
                string p = Cryptography.Decrypt(Request.Cookies["UserPassword"].Values["Password"]);
                tbl.email = e;
                tbl.password = p;
            }
            return View(tbl);
        }
        [HttpPost]
        public ActionResult Login(ViewCustomerModel tblcustomer, string IsRemember)
        {
            if (tblcustomer != null)
            {
                var user = _reposatory.GetTblCustomerIdByUserNamePassword(tblcustomer);
                if (user != null)
                {
                    Session["LoginUserId"] = user.Id;

                    if (user != null && user.AccountExpDate != "")
                    {
                        if (Convert.ToDateTime(user.AccountExpDate).Date < DateTime.Now.Date)
                        {
                            string EncriptID = Cryptography.Encrypt(Convert.ToString(user.Id));
                            Session["CustomerExpDateAccount"] = EncriptID;
                            string Q = EncriptID;
                            string Type = "Expire";
                            TempData["Message"] = "showMessage('Complete your payment in order to login your account!',false);";
                            return RedirectToAction("CustomerPayment", "Customer", new { Q, Type });
                        }
                        else if (IsRemember == "true")
                        {
                            HttpContext.Request.Cookies.Remove("UserPassword");
                            HttpCookie Cookie = new HttpCookie("UserPassword");
                            string em = Cryptography.Encrypt(tblcustomer.email);
                            Cookie.Values.Add("UserName", em);
                            string ps = Cryptography.Encrypt(tblcustomer.password);
                            Cookie.Values.Add("Password", ps);
                            Cookie.Expires = DateTime.Now.AddDays(100);
                            Response.Cookies.Add(Cookie);
                        }
                        return createCookieAndRedirectToUserHome(user.Id);
                    }
                    else if (user.AccountExpDate == "")
                    {
                        string EncriptID = Cryptography.Encrypt(Convert.ToString(user.Id));
                        Session["CustomerExpDateAccount"] = EncriptID;
                        string Q = EncriptID;
                        string Type = "SignUp";
                        TempData["Message"] = "showMessage('Complete your payment in order to login your account!',false);";
                        return RedirectToAction("CustomerPayment", "Customer", new { Q, Type });
                    }
                }
                else
                {
                    ViewBag.Message = "showMessage('Invalid username or password!',false);";
                }
            }
            return View();
        }

        public ActionResult ForgetPassword()
        {
            return View("Login");
        }

        [HttpPost]
        public ActionResult ForgetPassword([Bind(Include = "email")]ViewCustomerModel emambmbnil)
        {
            string emails = Convert.ToString(emambmbnil.email);
            var user = _reposatory.GetTblCustomerIdByUserNamePasswords(emails);
            if (user != null)
            {
                if (user.status == true)
                {
                    string pass = _reposatory.UpdateUserEmailPassword(emails);
                    clsEmailTemplateService clscommn = new clsEmailTemplateService();
                    clsSendEmailService clssendemailservice = new clsSendEmailService();
                    string subject = "chineseaddress - Forgot Password";
                    string recoverpasswored = "Your Password Is:" + pass;
                    Hashtable dicTemplate = new Hashtable();
                    dicTemplate.Add("#FirstName#", user.FirstName);
                    dicTemplate.Add("#LastName#", user.LastName);
                    dicTemplate.Add("#email#", emails);
                    dicTemplate.Add("#recoverpasswored#", pass);
                    try
                    {
                        //clssendemailservice.SendEmail(new[] { emails }, null, null, body, subject);
                        clssendemailservice.SendEmail(new[] { emails }, null, null, (new clsEmailTemplateService().GetEmailTemplate("forgotpassword", dicTemplate)), subject);
                        ViewBag.Message = "showMessage('Your Password has been send to your Registered Email Address',true); ";
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = ex.Message.ToString();
                    }
                }
                else
                {
                    ViewBag.Message = "showMessage('Your account is currently inactive. For more information pls contact us',false)";
                }
            }
            else
            {
                ViewBag.Message = "showMessage('User does not exist!',false);";
            }
            return View("Login");
        }

        private ActionResult createCookieAndRedirectToUserHome(int customerId)
        {
            try
            {
                HttpCookie cookie = HttpContext.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
                if (cookie == null)
                {
                    cookie = new HttpCookie(FormsAuthentication.FormsCookieName);

                    HttpContext.Response.Cookies.Add(cookie);
                }
                Context4InshipEntities Context = new Context4InshipEntities();
                var customerDetail = (from cd in Context.tblCustomers
                                      join ab in Context.tblAddressBooks
                                      on cd.Id equals ab.Fk_customer_Id
                                      where cd.Id == customerId
                                      select new { cd.Id, cd.email, name = (ab.first_name + " " + ab.last_name) }).FirstOrDefault();
                string UserData = "User" + ";" + customerDetail.Id + ";" + customerDetail.name + ";" + customerDetail.email;
                string UserName = customerDetail.email;
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, UserName, DateTime.Now, DateTime.Now.AddDays(1), true, UserData, FormsAuthentication.FormsCookiePath);
                cookie.Value = FormsAuthentication.Encrypt(ticket);
                cookie.Expires = DateTime.Now.AddDays(30);
                HttpContext.Response.Cookies.Set(cookie);
                HttpContext.User = new GenericPrincipal(HttpContext.User.Identity, UserData.Split(';'));

                if (TempData["RetUrl"] != null)
                {
                    if (!Convert.ToString(TempData["RetUrl"]).Split('/').Last().ToLower().Contains("signout"))
                        Response.Redirect(Convert.ToString(TempData["RetUrl"]), true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToRoute("DashBoard");
        }

        public JsonResult isEmailExists(string email)
        {
            bool isExists = _reposatory.isUserExists(email);
            return Json(isExists);
        }

        #region Call Function check facebook social api

        public ActionResult FaceBookSignUp(string FacebookEmail, string FacebookID)
        {
            var message = _reposatory.FacebookSocialapi(FacebookEmail, FacebookID);
            if (message == "Social email matched customer email")
            {
                dynamic redirectUrl = null;
                dynamic redirect = SocialApiCheckLogin(redirectUrl);
                return Json(new { redirect.Data });
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion Call Function check facebook social api

        #region Call Function check google social api

        public ActionResult GoogleSignUp(string GoogleEmail, string GoogleID)
        {
            var message = _reposatory.GoogleSocialapi(GoogleEmail, GoogleID);
            if (message == "Social email matched customer email")
            {
                dynamic redirectUrl = null;
                dynamic redirect = SocialApiCheckLogin(redirectUrl);
                return Json(new { redirect.Data });
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion Call Function check google social api

        #region Call function SocailApi check according login

        private ActionResult SocialApiCheckLogin(dynamic redirectUrl)
        {
            ViewCustomerModel tblcustomer = new ViewCustomerModel();
            string IsRemember = "";
            var user = _reposatory.GetTblCustomerIdByUserNamePassword(tblcustomer);
            if (user != null)
            {
                Session["LoginUserId"] = user.Id;

                if (user != null && user.AccountExpDate != "")
                {
                    if (Convert.ToDateTime(user.AccountExpDate).Date < DateTime.Now.Date)
                    {
                        string EncriptID = Cryptography.Encrypt(Convert.ToString(user.Id));
                        Session["CustomerExpDateAccount"] = EncriptID;
                        string Q = EncriptID;
                        string Type = "Expire";
                        TempData["Message"] = "showMessage('Complete your payment in order to login your account!',false);";
                        redirectUrl = new UrlHelper(Request.RequestContext).Action("CustomerPayment", "Customer", new { Q, Type });
                        return Json(new { Url = redirectUrl });
                        //return RedirectToAction("CustomerPayment", "Customer", new { Q, Type });
                    }
                    else if (IsRemember == "true")
                    {
                        HttpContext.Request.Cookies.Remove("UserPassword");
                        HttpCookie Cookie = new HttpCookie("UserPassword");
                        string em = Cryptography.Encrypt(tblcustomer.email);
                        Cookie.Values.Add("UserName", em);
                        string ps = Cryptography.Encrypt(tblcustomer.password);
                        Cookie.Values.Add("Password", ps);
                        Cookie.Expires = DateTime.Now.AddDays(100);
                        Response.Cookies.Add(Cookie);
                    }
                    HttpCookie cookie = HttpContext.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
                    if (cookie == null)
                    {
                        cookie = new HttpCookie(FormsAuthentication.FormsCookieName);

                        HttpContext.Response.Cookies.Add(cookie);
                    }
                    Context4InshipEntities Context = new Context4InshipEntities();
                    var customerDetail = (from cd in Context.tblCustomers
                                          join ab in Context.tblAddressBooks
                                          on cd.Id equals ab.Fk_customer_Id
                                          where cd.Id == user.Id
                                          select new { cd.Id, cd.email, name = (ab.first_name + " " + ab.last_name) }).FirstOrDefault();
                    string UserData = "User" + ";" + customerDetail.Id + ";" + customerDetail.name + ";" + customerDetail.email;
                    string UserName = customerDetail.email;
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, UserName, DateTime.Now, DateTime.Now.AddDays(1), true, UserData, FormsAuthentication.FormsCookiePath);
                    cookie.Value = FormsAuthentication.Encrypt(ticket);
                    cookie.Expires = DateTime.Now.AddDays(30);
                    HttpContext.Response.Cookies.Set(cookie);
                    HttpContext.User = new GenericPrincipal(HttpContext.User.Identity, UserData.Split(';'));

                    if (TempData["RetUrl"] != null)
                    {
                        if (!Convert.ToString(TempData["RetUrl"]).Split('/').Last().ToLower().Contains("signout"))
                            Response.Redirect(Convert.ToString(TempData["RetUrl"]), true);
                    }
                    // return RedirectToAction("Index", new RouteValueDictionary(new { controller = "User/Home", action = "Index" }));
                    redirectUrl = new UrlHelper(Request.RequestContext).Action("/DashBoard");
                    return Json(new { Url = redirectUrl });
                }
                else if (user.AccountExpDate == "")
                {
                    string EncriptID = Cryptography.Encrypt(Convert.ToString(user.Id));
                    Session["CustomerExpDateAccount"] = EncriptID;
                    string Q = EncriptID;
                    string Type = "SignUp";
                    TempData["Message"] = "showMessage('Complete your payment in order to login your account!',false);";
                    // return RedirectToAction("CustomerPayment", "Customer", new { Q, Type });
                    redirectUrl = new UrlHelper(Request.RequestContext).Action("CustomerPayment", "Customer", new { Q, Type });
                    return Json(new { Url = redirectUrl });
                }
            }
            return Json(new { Url = redirectUrl });
        }

        #endregion Call function SocailApi check according login

        public ViewCustomerPaymentModel BillingInformation()
        {
            string CustomerID;
            ViewCustomerPaymentModel objViewCustomerPaymentModel = new ViewCustomerPaymentModel();
            if (Session["customerEncriptedId"] != null)
            {
                CustomerID = Session["customerEncriptedId"].ToString();
                int id = Convert.ToInt32(Cryptography.Decrypt(CustomerID));
                objViewCustomerPaymentModel = _reposatory.CustomerAdditionalInformation(id);
            }
            else if (Session["CustomerExpDateAccount"] != null)
            {
                CustomerID = Session["CustomerExpDateAccount"].ToString();
                int id = Convert.ToInt32(Cryptography.Decrypt(CustomerID));
                objViewCustomerPaymentModel = _reposatory.CustomerAdditionalInformation(id);
            }
            return objViewCustomerPaymentModel;
        }
    }
}