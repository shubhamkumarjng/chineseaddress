using _ChineseAddress.com.Repository;
using System.Linq;
using System.Web.Mvc;

namespace _ChineseAddress.com.Controllers
{ 
    public class CommonChildController : Controller
    {
        // GET: CommonChild
        public FileResult DownloadCustomproforma(int Id)
        {
            return new FilePathResult("~/Uploads/Orders/" + Id + "/" + "" + Id + "_Customproforma.pdf", "Application/pdf");
        }

        public FileResult DownloadInvoicelabel(int Id)
        {
            Context4InshipEntities contxt = new Context4InshipEntities();
            var TrackingNumber = contxt.Database.SqlQuery<string>("select tracking_no from tblorder where id =" + Id).FirstOrDefault<string>();
            return new FilePathResult("~/Uploads/Orders/" + Id + "/" + "" + TrackingNumber + "_label.pdf", "Application/pdf");
        }

        public FileResult DownloadInvoice(int Id)
        {
            return new FilePathResult("~/Uploads/Orders/" + Id + "/" + "" + Id + "_Invoice.pdf", "Application/pdf");
        }
        //DownloadReturnShipmentInvoice
        public FileResult DownloadSignUpInvoice(string id)
        {
            string []SplitInvoiceNumberAndCustomerID = id.Split(',');
            Context4InshipEntities contxt = new Context4InshipEntities();
            return new FilePathResult("~/Uploads/SignupInvoices/" + SplitInvoiceNumberAndCustomerID[1] + "/" + "" + SplitInvoiceNumberAndCustomerID[0] + "_SignupInvoice.pdf", "Application/pdf");
        }
        public FileResult DownloadReturnShipmentInvoice(string id)
        {
            string[] SplitInvoiceNumberAndCustomerID = id.Split(',');
            Context4InshipEntities contxt = new Context4InshipEntities();
            return new FilePathResult("~/Uploads/ReturnShipmentInvoices/" + SplitInvoiceNumberAndCustomerID[1] + "/" + "" + SplitInvoiceNumberAndCustomerID[0] + "_ReturnShipment_Invoice.pdf", "Application/pdf");
        }
        public FileResult DownloadTopUpInvoice(string id)
        {
            string[] SplitInvoiceNumberAndCustomerID = id.Split(',');
            Context4InshipEntities contxt = new Context4InshipEntities();
            return new FilePathResult("~/Uploads/TopUpInvoices/" + SplitInvoiceNumberAndCustomerID[0] + "/" + "" + SplitInvoiceNumberAndCustomerID[1] + "_TopUp_Invoice.pdf", "Application/pdf");
        }

        [HttpPost]
        public ActionResult TrackingNumber(string TrackingNumber)
        {
            dynamic GetOrdertrackinLink = "";
            string tarckno = "";
            if (TrackingNumber != "")
            {
                Context4InshipEntities Context = new Context4InshipEntities();
                //var GetCarrier = Context.tblOrders.AsQueryable().Where(x => x.tracking_no == TrackingNumber).Select(x => x).FirstOrDefault();

                var isTrackingExists = Context.tblOrders.AsQueryable().Where(x => x.tracking_no == TrackingNumber).Select(x => x.tracking_no).Union(Context.tblOrderBoxes.AsQueryable().Where(x => x.tracking_no == TrackingNumber).Select(x => x.tracking_no)).FirstOrDefault();
                if (isTrackingExists != null)
                {
                    //GetOrdertrackinLink = Context.tblCarriersDetails.AsQueryable().Where(x => x.carrier_name == GetCarrier.carrier).Select(x => x.tracking_link).FirstOrDefault();
                    //tarckno = GetOrdertrackinLink.Replace("#tracking_no#", TrackingNumber);
                    //Response.RedirectToRoute("Tracking", new { Track = TrackingNumber });
                    tarckno = TrackingNumber;
                    //return RedirectToAction("Index", "Tracking");
                }
            }
            return Json(tarckno, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult TrackingOrderReturnURl(string TrackingNumber)
        {
            dynamic GetOrdertrackinLink = "";
            string url = "";
            if (TrackingNumber != "")
            {
                Context4InshipEntities Context = new Context4InshipEntities();
                //var GetCarrier = Context.tblOrders.AsQueryable().Where(x => x.tracking_no == TrackingNumber).Select(x => x).FirstOrDefault();

                var ExistsTrackingNumber = Context.tblOrders.AsQueryable().Where(x => x.tracking_no == TrackingNumber).Select(x => x.tracking_no).Union(Context.tblOrderBoxes.AsQueryable().Where(x => x.tracking_no == TrackingNumber).Select(x => x.tracking_no)).FirstOrDefault();
                if (ExistsTrackingNumber != null)
                {
                    //GetOrdertrackinLink = Context.tblCarriersDetails.AsQueryable().Where(x => x.carrier_name == GetCarrier.carrier).Select(x => x.tracking_link).FirstOrDefault();
                    //tarckno = GetOrdertrackinLink.Replace("#tracking_no#", TrackingNumber);
                    //Response.RedirectToRoute("Tracking", new { Track = TrackingNumber });
                    var  carrierCode=Context.tblOrders.AsQueryable().Where(x => x.tracking_no == TrackingNumber).Select(x => x.service_code).FirstOrDefault();
                    if (string.IsNullOrEmpty(carrierCode))
                    {
                        carrierCode = (from o in Context.tblOrders
                                       join ob in Context.tblOrderBoxes
                                       on o.id equals ob.Fk_order_id
                                       where ob.tracking_no == TrackingNumber
                                       select o.service_code).FirstOrDefault();
                    }
                    string tracking_link = Context.tblCarriersDetails.AsQueryable().Where(x => x.code == carrierCode).Select(x => x.tracking_link).FirstOrDefault();
                    if (tracking_link.Contains("#tracking_no#"))
                    {
                        tracking_link = tracking_link.Replace("#tracking_no#", TrackingNumber);
                    }
                    url = tracking_link;
                    //return RedirectToAction("Index", "Tracking");
                }
            }
            return Json(url, JsonRequestBehavior.AllowGet);
        }
    }
}