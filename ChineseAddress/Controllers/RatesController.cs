using _ChineseAddress.com.Common.Models;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.IServices;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using _ChineseAddress.com.Services;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NCalc;
using DevTrends.MvcDonutCaching;

namespace _ChineseAddress.com.Controllers
{
    public class RatesController : Controller
    {
        [Dependency]
        public Context4InshipEntities context { get; set; }

        private IPlanServices<PlansOptionModel> _reposatory;

        public RatesController(IPlanServices<PlansOptionModel> repo)
        {
            _reposatory = repo;
        }

        // GET: Rates
        [HttpGet]
        [DonutOutputCache(Duration = 60, Location = System.Web.UI.OutputCacheLocation.ServerAndClient, Options = OutputCacheOptions.IgnoreQueryString)]
        public ActionResult Index()
        {
            BindCountries();
            return View(_reposatory.Plans());
        }

        [OutputCache(Duration = 2500)]
        private void BindCountries()
        {
            List<SelectListItem> lstCounties = (new CountryServices()).Country();
            lstCounties.RemoveAt(0);
            ViewBag.Country = lstCounties;
        }

        [HttpPost]
        public JsonResult Index(string countryCode, decimal weight, decimal length, decimal width, decimal height)
        {
            clsCarriersService objCarriersService = new clsCarriersService(); try
            {
                tblCountryMaster objCountryMaster = context.tblCountryMasters.AsQueryable().Where(x => x.country_code == countryCode).Select(x => x).FirstOrDefault();
                objCarriersService._ToAddress = new tblDeliveryAddress() { country_code = countryCode, postal_code = objCountryMaster.postal_code, city = objCountryMaster.postal_code };

                objCarriersService._Packages = new List<tblOrderBox>() { new tblOrderBox() { actual_weight = weight, billable_weight = weight, length = length, width = width, height = height } };
                objCarriersService.getCarrierPrices();
                if (objCarriersService._IsSuccess)
                {
                    //Context4InshipEntities context = new Context4InshipEntities();
                    List<tblCarriersDetail> lstreceivedShipment = context.tblCarriersDetails.AsQueryable().Where(x => x.status == true).Select(x => x).ToList();
                    List<carriersRatesResponseModel> lstResponse = new List<carriersRatesResponseModel>();
                    foreach (carriersRatesResponseModel item in objCarriersService._carriersRates)
                    {
                        var carrierDetail = lstreceivedShipment.Where(x => x.code.Contains(item.Service_Code)).FirstOrDefault();

                        if (carrierDetail != null)
                        {
                            item.Service = carrierDetail.service;
                            lstResponse.Add(item);
                        }
                    }

                    return Json(lstResponse.AsEnumerable().OrderBy(x => x.TotalShipmentCharge), JsonRequestBehavior.AllowGet);
                }
            }
            catch (System.Exception ex)
            {
                Response.Write(ex.Message.ToString());
                throw ex;
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(Duration = 60)]
        public JsonResult GetAllActiveCarriers()
        {
            IEnumerable<tblCarriersDetail> lstCarriersDetail = context.tblCarriersDetails.AsEnumerable().Where(x => x.status == true);
            List<carriersRatesResponseModel> lstResponse = new List<carriersRatesResponseModel>();
            foreach (tblCarriersDetail item in lstCarriersDetail)
            {
                lstResponse.Add(new carriersRatesResponseModel() { Service = item.service }); /*item.carrier_name + " " +*/
            }
            return Json(lstResponse, JsonRequestBehavior.AllowGet);
        }
    }
}