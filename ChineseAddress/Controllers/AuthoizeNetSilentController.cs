using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace _ChineseAddress.com.Controllers
{
    public class AuthoizeNetSilentController : Controller
    {
        private Context4InshipEntities Context;
        private tblInvoice CustomerInvoice;
        private string CustomerEmail;

        public AuthoizeNetSilentController()
        {
            Context = new Context4InshipEntities();
        }

        // GET: AuthoizeNetSilent
        [HttpPost]
        public void Index()
        {
            try
            {
                if (Request.Params != null)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.Params["x_subscription_id"])))
                    {
                        #region CommentCode

                        //    if (string.Equals(Request.Params["x_description"], "Test transaction for ValidateCustomerPaymentProfile.", StringComparison.CurrentCultureIgnoreCase))
                        //    {
                        //        LogAllParams("Validate Customer Payment Profile");
                        //        if (!string.IsNullOrEmpty(Convert.ToString(Request.Params["x_email"])))
                        //        {
                        //            CustomerEmail = Convert.ToString(Request.Params["x_email"]);
                        //            CustomerInvoice = (from tc in Context.tblCustomers
                        //                               join ti in Context.tblInvoices on tc.Id equals ti.Fk_customer_id
                        //                               where tc.email == CustomerEmail
                        //                               orderby ti.invoice_date descending
                        //                               select ti).FirstOrDefault();
                        //
                        //        }

                        //        if (Request.Params["x_response_code"] != null)
                        //        {
                        //            if (Request.Params["x_response_code"].ToString() == "1")
                        //            {
                        //                if (Request.Params["x_response_reason_code"] != null)
                        //                {
                        //                    if (Request.Params["x_response_reason_code"].ToString() == "1")
                        //                    {
                        //                        CustomerInvoice.paid_status = 1;
                        //                        CustomerInvoice.payment_method = Convert.ToString(Request.Params["x_card_type"]);
                        //                        Context.Entry(CustomerInvoice).State = System.Data.Entity.EntityState.Modified;
                        //                        Context.SaveChanges();

                        //                        TimeZoneInfo timeZoneInfoPST = TimeZoneInfo.FindSystemTimeZoneById("Pacific SA Standard Time");
                        //                        DateTime newDateTime = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfoPST);
                        //                        DateTime expiryDate = new DateTime();
                        //                        if (newDateTime.Hour >= 1)
                        //                        {
                        //                            expiryDate = DateTime.Now.AddDays(1);
                        //                        }
                        //                        else
                        //                        {
                        //                            expiryDate = DateTime.Now;
                        //                        }
                        //                        tblCustomer objCustomer = Context.tblCustomers.AsQueryable().Where(X => X.email == CustomerEmail).FirstOrDefault();
                        //                        objCustomer.account_expiry_date = expiryDate;
                        //                        Context.Entry(objCustomer).State = System.Data.Entity.EntityState.Modified;
                        //                        Context.SaveChanges();
                        //                    }
                        //                    else
                        //                    {
                        //                        LogFailedValdateCustomerPaymentProfile();
                        //                        PaymentFailed(Request.Params["x_response_reason_code"], Request.Params["x_response_reason_text"], "Validate Customer Payment Profile");
                        //                    }
                        //                }
                        //                else
                        //                {
                        //                    LogFailedValdateCustomerPaymentProfile();
                        //                    PaymentFailed(null, null, "Validate Customer Payment Profile");
                        //                }
                        //            }
                        //            else
                        //            {
                        //                LogFailedValdateCustomerPaymentProfile();
                        //                PaymentFailed(null, null, "Validate Customer Payment Profile");
                        //            }

                        //        }
                        //        else
                        //        {
                        //            LogFailedValdateCustomerPaymentProfile();
                        //            PaymentFailed(null, null, "Validate Customer Payment Profile");
                        //        }
                        //    }
                        //}
                        //else
                        //{

                        #endregion CommentCode

                        LogAllParams("Silent Subscription  All Response ");
                        if (Request.Params["x_subscription_id"] != null)
                        {
                            string subId = Request.Params["x_subscription_id"].ToString();
                            CustomerInvoice = Context.tblInvoices.AsQueryable().Where(c => c.subscription_id == subId).OrderByDescending(c => c.subscription_id).FirstOrDefault();
                            CustomerEmail = (from ti in Context.tblInvoices
                                             join c in Context.tblCustomers on ti.Fk_customer_id equals c.Id
                                             where ti.subscription_id == subId
                                             select c.email).FirstOrDefault();

                            if (Request.Params["x_response_code"] != null)
                            {
                                if (Request.Params["x_response_code"].ToString() == "1")
                                {
                                    if (Request.Params["x_response_reason_code"] != null)
                                    {
                                        if (Request.Params["x_response_reason_code"] == "1")
                                        {
                                            int customerId = Convert.ToInt32(Request.Params["x_cust_id"]);
                                            string SubsId = Convert.ToString(Request.Params["x_subscription_id"]);
                                            if (Convert.ToInt32(Request.Params["x_subscription_paynum"]) == 1)
                                            {
                                                tblInvoice objInvoice = Context.tblInvoices.AsQueryable().Where(
                                                    x => x.Fk_customer_id == customerId
                                                    && x.subscription_id == SubsId).Select(x => x).
                                                OrderByDescending(x => x.invoice_date).FirstOrDefault();
                                                objInvoice.invoice_date = DateTime.Now;
                                                objInvoice.paid_status = 2;
                                                objInvoice.transaction_id = Request.Params["x_trans_id"].ToString();
                                                objInvoice.transaction_status = Request.Params["x_response_reason_code"];
                                                objInvoice.transaction_response = Request.Params["x_response_reason_text"];
                                                Context.Entry(objInvoice).State = System.Data.Entity.EntityState.Modified;
                                                Context.SaveChanges();

                                                tblCustomer objCustomer = Context.tblCustomers.Where(x => x.Id == customerId).FirstOrDefault();
                                                objCustomer.account_expiry_date = (objInvoice.paid_on ?? DateTime.Now).AddDays(7);
                                                Context.tblCustomers.Attach(objCustomer);
                                                Context.Entry(objCustomer).Property(x => x.account_expiry_date).IsModified = true;
                                                Context.SaveChanges();
                                            }
                                            else
                                            {
                                                tblInvoice objInvoice = Context.tblInvoices.AsQueryable().Where(x => x.Fk_customer_id == customerId && x.subscription_id == SubsId).Select(x => x).OrderByDescending(x => x.paid_on).FirstOrDefault();
                                                objInvoice.invoice_number = (new clsCommonServiceRoot()).GetInvoiceNumber();
                                                objInvoice.invoice_date = DateTime.Now;
                                                objInvoice.paid_status = 2;
                                                objInvoice.transaction_id = Request.Params["x_trans_id"].ToString();
                                                objInvoice.transaction_status = Request.Params["x_response_reason_code"];
                                                objInvoice.transaction_response = Request.Params["x_response_reason_text"];
                                                Context.tblInvoices.Add(objInvoice);
                                                Context.SaveChanges();

                                                tblCustomer objCustomer = Context.tblCustomers.Where(x => x.Id == customerId).FirstOrDefault();
                                                objCustomer.account_expiry_date = (objCustomer.account_expiry_date ?? DateTime.Now).AddDays(7);
                                                Context.tblCustomers.Attach(objCustomer);
                                                Context.Entry(objCustomer).Property(x => x.account_expiry_date).IsModified = true;
                                                Context.SaveChanges();
                                            }
                                            StringBuilder sb = new StringBuilder();
                                            sb.AppendLine("***** Start Silent Subscription Response " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                                            sb.AppendLine("Transaction ID: " + Request.Params["x_trans_id"].ToString());
                                            sb.AppendLine("Response Code: " + Request.Params["x_response_reason_code"]);
                                            sb.AppendLine("Description: " + Request.Params["x_response_reason_text"]);
                                            sb.AppendLine("***** End Silent Subscription Response *****");
                                            CreatePaymentResponses(sb.ToString());
                                        }
                                        else
                                        {
                                            LogFailedSubscription();
                                            PaymentFailed($"{Request.Params["x_response_reason_code"]}", $"{Request.Params["x_response_reason_text"]}");
                                        }
                                    }
                                    else
                                    {
                                        LogFailedSubscription();
                                        PaymentFailed($"x_response_reason_code : null", "Error");
                                    }
                                }
                                else
                                {
                                    LogFailedSubscription();
                                    PaymentFailed($"x_response_code : {Request.Params["x_response_code"].ToString()}", "Error");
                                }
                            }
                            else
                            {
                                LogFailedSubscription();
                                PaymentFailed("null", "null");
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private bool PaymentFailed(string code, string description, string ResponseApi = "Subscription")
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"***** Start Silent {ResponseApi} Response Error Occured " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
            sb.Append("Transaction Failed");
            sb.Append("Error Code: " + code);
            sb.Append("Error message: " + description);
            sb.AppendLine($"***** End Silent {ResponseApi} Response Error Occured *****");
            CreatePaymentResponses(sb.ToString());
            return false;
        }

        private void LogAllParams(string resonseApi)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"***** Start Silent Page {resonseApi} Response " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
            var items = Request.Params.AllKeys.SelectMany(
                Request.Params.GetValues, (k, v) => new { key = k, value = v }
                );

            foreach (var item in items)
            {
                sb.AppendLine(string.Format("{0} = {1}", item.key, item.value));
            }
            sb.AppendLine($"***** End Silent Page {resonseApi} Response *****");

            CreatePaymentResponses(sb.ToString());
        }

        private void LogFailedValdateCustomerPaymentProfile()
        {
            if (CustomerInvoice != null)
            {
                CustomerInvoice.paid_status = 0;
                Context.Entry(CustomerInvoice).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
            }
            if (!string.IsNullOrEmpty(CustomerEmail))
            {
                tblCustomer customerDetail = Context.tblCustomers.AsQueryable().Where(c => c.email == CustomerEmail).FirstOrDefault();
                customerDetail.account_expiry_date = DateTime.Now.AddDays(-1);
                Context.Entry(customerDetail).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
            }
        }

        private void LogFailedSubscription()
        {
            try
            {
                if (CustomerInvoice != null)
                {
                    CustomerInvoice.paid_status = 0;
                    Context.Entry(CustomerInvoice).State = System.Data.Entity.EntityState.Modified;
                    Context.Entry(CustomerInvoice).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();
                }
                if (!string.IsNullOrEmpty(CustomerEmail))
                {
                    tblCustomer customerDetail = Context.tblCustomers.AsQueryable().Where(c => c.email == CustomerEmail).FirstOrDefault();
                    customerDetail.account_expiry_date = DateTime.Now.AddDays(-1);
                    Context.Entry(customerDetail).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();
                }
                System.Collections.Hashtable ht = new System.Collections.Hashtable();
                ht.Add("#header#", "chineseaddress-Subscription Failed");
                ht.Add("#content#", "Your subscription has been failed.contact custmer care for more details.");
                (new clsSendEmailService()).SendEmail(new[] { CustomerEmail }, null, null, (new clsEmailTemplateService()).GetEmailTemplate("common", ht), "chineseaddress-Subscription Failed");
            }
            catch
            {
            }
        }

        private void CreatePaymentResponses(string Keyvaluelist)
        {
            try
            {
                StreamWriter _testData = new StreamWriter(Server.MapPath("~/IPN/AuthorizeNetIPN_Silent.txt"), true);
                _testData.WriteLine(Keyvaluelist); // Write the file.
                _testData.Flush();
                _testData.Close();     // Close the instance of StreamWriter.
                _testData.Dispose(); // Dispose from memory.
            }
            catch
            {
            }
        }
    }
}