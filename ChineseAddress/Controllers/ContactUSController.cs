using _ChineseAddress.com.Common.Services;
using System.Web.Mvc;

namespace _ChineseAddress.com.Controllers
{
    public class ContactUSController : Controller
    {
        // GET: ContactUS
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ContactUs(string name, string email, string phone, string message)
        {
            clsEmailTemplateService objEmailTemplateService = new clsEmailTemplateService();
            System.Collections.Hashtable objHashTable = new System.Collections.Hashtable();
            objHashTable.Add("#name#", name);
            objHashTable.Add("#phone#", phone);
            objHashTable.Add("#email#", email);
            objHashTable.Add("#message#", message);
            string body = objEmailTemplateService.GetEmailTemplate("contactus", objHashTable);
            string ret = (new clsSendEmailService()).SendEmail(new string[] { System.Configuration.ConfigurationManager.AppSettings["ClientMail"].ToString() }, null, null, body, "Enquiry");
            return Json(ret, JsonRequestBehavior.AllowGet);
        }
    }
}