using _ChineseAddress.com.Common.Models;
using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.FedExRateWebService;
using _ChineseAddress.com.Repository;
using PdfSharp;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using _ChineseAddress.com.Common;
namespace _ChineseAddress.com.Controllers
{
    public class TestController : Controller
    {
        // GET: Test

        //public tblDeliveryAddress _ToAddress { get; set; }
        //public List<tblOrderBox> _Packages { get; set; }

        [HttpGet]
        public ActionResult Index()
        {



            return View();
        }
        private static byte[] ConvertHexToBytes(string hex)
        {
            byte[] bytes = new byte[hex.Length / 2];
            for (int i = 0; i < hex.Length; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }
            return bytes;
        }

        public JsonResult SaveImageByCam()
        {
            string url = Session["CapturedImage"].ToString();
            Session["CapturedImage"] = null;
            return Json(url, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveImage()
        {
            if (Request.InputStream.Length > 0)
            {
                using (StreamReader reader = new StreamReader(Request.InputStream))
                {
                    string hexString = Server.UrlEncode(reader.ReadToEnd());
                    string imageName = DateTime.Now.ToString("dd-MM-yy-hh-mm-ss");
                    string imagePath = string.Format("~/Uploads/WebCam/{0}.png", imageName);
                    System.IO.File.WriteAllBytes(Server.MapPath(imagePath), ConvertHexToBytes(hexString));
                    Session["CapturedImage"] = "https://www.chineseaddress.com/Uploads/WebCam/" + imagePath;
                }
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult PayTabs()
        {
            ViewBag.merchant_id = System.Configuration.ConfigurationManager.AppSettings["merchantid"];
            ViewBag.paytabReturnURL = System.Configuration.ConfigurationManager.AppSettings["paytabReturnURL"];
            ViewBag.merchantPassword = System.Configuration.ConfigurationManager.AppSettings["merchantPassword"];
            ViewBag.secret_key = System.Configuration.ConfigurationManager.AppSettings["secret_key"];
            ViewBag.paytabCurrency = System.Configuration.ConfigurationManager.AppSettings["paytabCurrency"];


            return View();
        }

        public ActionResult PayTabsResult()
        {
            return View();
        }




    }
}