using System.Web.Mvc;

namespace _ChineseAddress.com.Controllers
{
    [RoutePrefix("Error")]
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index(string msg)
        {
            System.Exception exception = Server.GetLastError();
            if (exception != null)
            {
                msg = exception.Message.ToString();
            }
            if (!string.IsNullOrEmpty(msg))
                ViewBag.Error = msg;
            else
            {
                ViewBag.Error = "Page not found.";
            }

            return View();
        }

        [Route("~/Error/404")]
        public ActionResult Error404(string msg)
        {
            if (!string.IsNullOrEmpty(msg))
                ViewBag.Error = msg;
            else
            {
                ViewBag.Error = "Page not found.";
            }

            return View();
        }
    }
}