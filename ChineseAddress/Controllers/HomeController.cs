using _ChineseAddress.com.IServices;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Services;
using DevTrends.MvcDonutCaching;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace _ChineseAddress.com.Controllers
{
    public class HomeController : Controller
    {
        private IPlanServices<PlansOptionModel> _reposatory;


        public HomeController(IPlanServices<PlansOptionModel> repo)
        {
            _reposatory = repo;
        }
        //[OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        [DonutOutputCache(Duration = 10,Location =System.Web.UI.OutputCacheLocation.Any, Options = OutputCacheOptions.NoCacheLookupForPosts, VaryByParam = "r")]
        public ActionResult Index(int? r)
        {
            var requestedETag = Request.Headers["If-None-Match"];
            var responseETag = DateTime.Now.ToString("ddMMyyyy"); // lookup or generate etag however you want
            if (requestedETag == responseETag)
                new HttpStatusCodeResult(HttpStatusCode.NotModified);

            Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
            Response.Cache.SetETag(responseETag);
            if (r != null)
            {
                ViewBag.IsReferral = true;
                TempData["referrer_id"] = r;
            }
            (new Services.ClsCommanCustomerSignup()).GetCustomerReference();
            PlanServices Obj = new PlanServices();

            ViewBag.PlansOption = _reposatory.Plans();
            List<PlansOptionModel> objList = _reposatory.OptionPlan();
            return View(objList);
        }

        public ActionResult Address()
        {
            return View();
        }
    }
}