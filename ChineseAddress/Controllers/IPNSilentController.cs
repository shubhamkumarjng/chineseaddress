using _ChineseAddress.com.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using _ChineseAddress.com.Areas.User.IServices;
using _ChineseAddress.com.Common.Services;
using Microsoft.Practices.Unity;
using _ChineseAddress.com.Areas.User.Services;

namespace _ChineseAddress.com.Controllers
{
    public class IPNSilentController : Controller
    {
        [Dependency]
        public Context4InshipEntities Context { get; set; }

        [Dependency]
        public PayTabsService _repoPayTabsService { get; set; }

        [Dependency]
        public IWalletService<tblTopUp, tblTopUpMethod> _repositoryUserWalletService { get; set; }

        _ChineseAddress.com.Areas.Admin.Models.clsCommon clsCommon = new Areas.Admin.Models.clsCommon();
        public string fileName { get; set; }
        StringBuilder sb = new StringBuilder();
        private tblInvoice CustomerInvoice;

        // GET: IPNSilent
        public ActionResult IPNPayTabs()
        {
            try
            {
                fileName = "PayTabsIPN.txt";
                //string payment_reference = "120027";
                if (!string.IsNullOrEmpty(Convert.ToString(Request.Form["payment_reference"])))//|| payment_reference != ""
                {
                    string p_id = Convert.ToString(Request.Form["payment_reference"]);
                    string custom_guid = Convert.ToString(Request.QueryString["custom_guid"]);




                    //if (p_id == null)
                    //{
                    //    p_id = payment_reference;
                    //}

                    //Verify_payment
                    _repoPayTabsService.payment_reference = p_id;
                    _repoPayTabsService.reference_no = custom_guid;
                    _ChineseAddress.com.Models.PayTabsResponseModel _PayTabsResponseModel = _repoPayTabsService.VerifyPayment();
                    CustomerInvoice = Context.tblInvoices.AsQueryable().Where(x => x.custom_guid == custom_guid).FirstOrDefault();

                    if (string.IsNullOrEmpty(Convert.ToString(CustomerInvoice.transaction_id)))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(Request.Form["pt_token"])))
                        {
                            //Log paytabs pt_token,pt_customer_email and pt_customer_password
                            sb.Clear();
                            sb.AppendLine($"***** Start PayTabs log pt_token,pt_customer_email and pt_customer_password on " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                            sb.AppendLine("pt_token:" + Convert.ToString(Request.Form["pt_token"]));
                            sb.AppendLine("pt_customer_email: " + Convert.ToString(Request.Form["pt_customer_email"]));
                            sb.AppendLine("pt_customer_password: " + Convert.ToString(Request.Form["pt_customer_password"]));
                            sb.AppendLine($"***** End PayTabs log pt_token,pt_customer_email and pt_customer_password *****");
                            CreatePaymentResponses(sb.ToString(), fileName);
                            //update pt_token and pt_customer_password in tblcustomer
                            tblCustomer tblCustomer = Context.tblCustomers.AsQueryable().Where(x => x.Id == CustomerInvoice.Fk_customer_id).FirstOrDefault();
                            if (tblCustomer != null)
                            {
                                tblCustomer.pt_token = Convert.ToString(Request.Form["pt_token"]);
                                tblCustomer.pt_customer_password = Convert.ToString(Request.Form["pt_customer_password"]);
                                Context.Entry(tblCustomer).State = System.Data.Entity.EntityState.Modified;
                                Context.Configuration.ValidateOnSaveEnabled = false;
                                Context.SaveChanges();
                            }
                        }

                        //Log payment_reference with custom_guid before Verify_Payment
                        sb.Clear();
                        sb.AppendLine($"***** Start PayTabs Response Before Verify_Payment call on " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                        sb.AppendLine("payment_reference:" + p_id);
                        sb.AppendLine("custom_guid: " + custom_guid);
                        sb.AppendLine($"***** End PayTabs Before Verify_Payment call Response *****");
                        CreatePaymentResponses(sb.ToString(), fileName);

                        if (_repoPayTabsService._isSucceded == true)
                        {
                            //Log payment_reference with custom_guid before Verify_Payment
                            sb.Clear();
                            sb.AppendLine($"***** Start PayTabs Response After Verify_Payment call on " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                            sb.AppendLine("payment_reference:" + p_id);
                            sb.AppendLine("custom_guid || reference_no: " + _PayTabsResponseModel.reference_no);
                            sb.AppendLine("result: " + _PayTabsResponseModel.result);
                            sb.AppendLine("response_code: " + _PayTabsResponseModel.response_code);
                            sb.AppendLine("transaction_id: " + _PayTabsResponseModel.transaction_id);
                            sb.AppendLine($"***** End PayTabs After Verify_Payment call Response *****");
                            CreatePaymentResponses(sb.ToString(), fileName);


                            //Update Inoice
                            if (CustomerInvoice != null)
                            {
                                CustomerInvoice.transaction_status = _PayTabsResponseModel.response_code;
                                CustomerInvoice.transaction_response = _PayTabsResponseModel.result;
                                CustomerInvoice.transaction_id = _PayTabsResponseModel.transaction_id;
                                CustomerInvoice.paid_status = 2;
                                CustomerInvoice.invoice_date = DateTime.Now;
                                CustomerInvoice.invoice_number = (new Common.Services.clsCommonServiceRoot()).GetInvoiceNumber();
                                Context.Entry(CustomerInvoice).State = System.Data.Entity.EntityState.Modified;
                                Context.Configuration.ValidateOnSaveEnabled = false;
                                Context.SaveChanges();
                                TempData["msg"] = clsCommon.SuccessMsg(_PayTabsResponseModel.result);
                            }

                            //Add Top Up 
                            AddTopUpAndSendEmail(CustomerInvoice, "Credit/Debit Card");
                            CreateTopUpInvoiceandSendEmail(CustomerInvoice);
                        }
                        else
                        {

                            if (CustomerInvoice != null)
                            {
                                CustomerInvoice.transaction_status = _PayTabsResponseModel.response_code;
                                CustomerInvoice.transaction_response = _PayTabsResponseModel.result;
                                CustomerInvoice.paid_status = 0;
                                CustomerInvoice.invoice_date = DateTime.Now;
                                Context.Entry(CustomerInvoice).State = System.Data.Entity.EntityState.Modified;
                                Context.Configuration.ValidateOnSaveEnabled = false;
                                Context.SaveChanges();
                                TempData["msg"] = clsCommon.ErrorMsg(_PayTabsResponseModel.result);
                            }
                            sb.Clear();
                            sb.AppendLine($"***** Start PayTabs Response After Verify_Payment call on " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                            sb.AppendLine("payment_reference:" + p_id);
                            sb.AppendLine("custom_guid || reference_no: " + _PayTabsResponseModel.reference_no);
                            sb.AppendLine("result: " + _PayTabsResponseModel.result);
                            sb.AppendLine("response_code: " + _PayTabsResponseModel.response_code);
                            sb.AppendLine("transaction_id: " + _PayTabsResponseModel.transaction_id);
                            sb.AppendLine($"***** End PayTabs After Verify_Payment call Response *****");
                            CreatePaymentResponses(sb.ToString(), fileName);


                        }

                    }
                }
                else
                {
                    //Log p_id with custom_guid
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine($"***** Start PayTabs Response Before Verify_Payment call on " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                    sb.AppendLine("Error payment_reference not found");
                    sb.AppendLine("custom_guid: " + Convert.ToString(Request.QueryString["custom_guid"]));
                    sb.AppendLine($"***** End PayTabs Before Verify_Payment call Response *****");
                    CreatePaymentResponses(sb.ToString(), fileName);
                }
            }
            catch (Exception ex)
            {
                TempData["msg"] = "Error occured in prcessing payment.Please contact customer care.";
                CreatePaymentResponses(ex.Message.ToString(), fileName);
            }
            return RedirectToAction("Index", "wallet", new { area = "user" });
        }

        public void CreatePaymentResponses(string Keyvaluelist, string fileName)
        {
            try
            {
                StreamWriter _testData = new StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/IPN/" + fileName), true);
                _testData.WriteLine(Keyvaluelist); // Write the file.
                _testData.Flush();
                _testData.Close();     // Close the instance of StreamWriter.
                _testData.Dispose(); // Dispose from memory.
            }
            catch
            {
            }
        }
        private void AddTopUpAndSendEmail(tblInvoice invoice, string PaymentMetodCode)
        {
            try
            {


                int topUpMethodId = Context.tblTopUpMethods.AsQueryable().Where(x => x.method_code == PaymentMetodCode).Select(x => x.id).FirstOrDefault();
                tblTopUp objTopUp = new tblTopUp();
                objTopUp.remark = "Online payment via " + PaymentMetodCode;
                objTopUp.Fk_TopUpMethod = topUpMethodId;
                objTopUp.transaction_id = invoice.transaction_id;
                objTopUp.amount = invoice.payment_amount;
                objTopUp.Fk_Customer_Id = invoice.Fk_customer_id;
                objTopUp.status = (int)eTopUpWalletStatus.Accept;
                objTopUp.created_on = DateTime.Now;
                Context.tblTopUps.Add(objTopUp);
                Context.Configuration.ValidateOnSaveEnabled = false;
                Context.SaveChanges();
                _repositoryUserWalletService.UpdateWalletBalance(objTopUp);
                try
                {
                    SendEmails(objTopUp);
                }
                catch (Exception ex)
                {
                    CreatePaymentResponses(ex.Message.ToString(), fileName);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public void SendEmails(tblTopUp _tblTopUp)
        {
            if (_tblTopUp.status == (int)eTopUpWalletStatus.Accept)
            {
                tblCustomer customer = Context.tblCustomers.AsQueryable().Where(X => X.Id == _tblTopUp.Fk_Customer_Id).FirstOrDefault();
                string firstName = Context.tblAddressBooks.AsQueryable().Where(x => x.Fk_customer_Id == customer.Id).OrderBy(x => x.created_on).Select(x => x.first_name).FirstOrDefault();

                _tblTopUp = _repositoryUserWalletService.getTopUpDetailById(_tblTopUp.Id);
                //Send Sign Email Start
                clsSendEmailService objclsSendEmailService = new clsSendEmailService();

                string subject = "chineseaddress - Top Up Request Approved";
                System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append($"This is an automatic email just to let you know that your top Up request with  reference no <strong>" + _tblTopUp.transaction_id + "</strong> has been approved.<br/>Your updated balance is $" + _repositoryUserWalletService.yourBalance(_tblTopUp.Fk_Customer_Id).ToString("F") + ".");
                hashTable.Add("#user#", firstName);
                hashTable.Add("#content#", sb.ToString());
                //Send Sign Email End
                objclsSendEmailService.SendEmail(new[] { Convert.ToString(customer.email) }, null, null, (new clsEmailTemplateService().GetEmailTemplate("commonuser", hashTable)), subject);
            }
        }

        //Create Top Up  Invoice
        public void CreateTopUpInvoiceandSendEmail(tblInvoice objInvoice)
        {
            try
            {
                tblCustomer objCustomer = Context.tblCustomers.AsQueryable().Where(X => X.Id == objInvoice.Fk_customer_id).FirstOrDefault();
                tblBillingAddress objBillingAddress = Context.tblBillingAddresses.AsQueryable().FirstOrDefault(x => x.Id == objInvoice.Fk_billing_address_id);

                string countryName = Context.tblCountryMasters.AsQueryable().AsParallel().Where(x => x.country_code == objBillingAddress.country_code).Select(x => x.country_name).FirstOrDefault();

                #region GetTemplateFromHtmlFileAndMakeInvoice

                StringBuilder sbTemplate = new StringBuilder();
                // sbTemplate.Append(File.ReadAllText(HttpContext.Current.System.Web.HttpContext.Current.Server.MapPath("~/Areas/User/CreateInvoiceHTML.html")));
                System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                // hashTable.Add("#logo#", HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Content/img/logo.png");
                hashTable.Add("#suite#", objCustomer.customer_reference);
                hashTable.Add("#invoicno#", objInvoice.invoice_number);
                hashTable.Add("#invoicedate#", (objInvoice.invoice_date ?? DateTime.Now).ToString("yyyy/MM/dd"));
                hashTable.Add("#toname#", objBillingAddress.first_name + " " + objBillingAddress.last_name);
                hashTable.Add("#toaddress#", objBillingAddress.address ?? "");
                hashTable.Add("#tocity#", objBillingAddress.city);
                hashTable.Add("#tocountry#", countryName);
                hashTable.Add("#totelephone#", objBillingAddress.mobile);
                hashTable.Add("#tozip#", objBillingAddress.post_code);
                hashTable.Add("#tostate#", objBillingAddress.state);
                hashTable.Add("#amount#", objInvoice.payment_amount.ToString("F"));
                hashTable.Add("#payment_method#", objInvoice.payment_method);
                hashTable.Add("#type#", objInvoice.invoice_type);
                hashTable.Add("#date#", (objInvoice.paid_on ?? DateTime.Now).ToString("yyyy/MM/dd"));

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/") + "TopUpInvoices"))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/") + "TopUpInvoices");
                }
                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/TopUpInvoices/") + objCustomer.Id))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/TopUpInvoices/") + objCustomer.Id);
                }
                string Invoicetemplate = (new clsEmailTemplateService()).GetEmailTemplate("topupinvoice", hashTable);
                string strInvFilePath = System.Web.HttpContext.Current.Server.MapPath($"~/Uploads/TopUpInvoices/{objCustomer.Id}/") + objInvoice.invoice_number.ToString() + "_TopUp_Invoice.pdf";
                string CssInvoicePath = System.Web.HttpContext.Current.Server.MapPath("~/Content/css/SignUpInvoice.min.css");
                clsHTMLtoPDF objclsHTMLtoPDF = new clsHTMLtoPDF();
                bool OrderInvoice = false;
                objclsHTMLtoPDF.ConvertHTMLToPDF(Invoicetemplate, CssInvoicePath, strInvFilePath, OrderInvoice);

                #endregion GetTemplateFromHtmlFileAndMakeInvoice

                //#region SendEmailWithAttachment

                //var GetCustomerEmail = (from tbcust in Context.tblCustomers
                //                        join tbAddressbook in Context.tblAddressBooks on tbcust.Id equals tbAddressbook.Fk_customer_Id
                //                        select new { tbcust.Id, tbcust.email, tbAddressbook.first_name, tbAddressbook.last_name, tbAddressbook.is_default }).Where(x => x.Id == objInvoice.Fk_customer_id && x.is_default == true).FirstOrDefault();
                //hashTable.Clear();

                //if (GetCustomerEmail != null)
                //{
                //    hashTable.Add("#FirstName#", GetCustomerEmail.first_name);
                //    hashTable.Add("#LastName#", GetCustomerEmail.last_name);
                //    hashTable.Add("#referenceno#", );
                //}
                //string emailBody = new clsEmailTemplateService().GetEmailTemplate("orderpayment", hashTable);
                //new clsSendEmailService().SendEmail(new[] { objCustomer.email }, null, null, emailBody, "chineseaddress - Order Dispatch (invoice with attachment)", new[] { strInvFilePath });

                //#region Send Email to Customer order disparched
                //clsCommonServiceRoot _objclsCommonServiceRoot = new clsCommonServiceRoot();
                //if (_OrderDetail != null)
                //    _objclsCommonServiceRoot.SendOrderDispatchedEmail(Convert.ToInt32(_OrderDetail.id), _OrderDetail.tracking_no);

                //#endregion Send Email to Customer order disparched
                //#endregion SendEmailWithAttachment
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}