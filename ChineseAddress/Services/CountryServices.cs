using _ChineseAddress.com.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace _ChineseAddress.com.Services
{
    public class CountryServices
    {
        private Context4InshipEntities Context = new Context4InshipEntities();

        public List<SelectListItem> Country()
        {
            var CountryList = Context.tblCountryMasters.ToList().Where(x => x.status == true);
            var CtList = new List<SelectListItem>();
            CtList.Add(new SelectListItem() { Text = "Select Country", Value = "" });
            foreach (var item in CountryList.ToList())
            {
                CtList.Add(new SelectListItem() { Text = item.country_name, Value = item.country_code });
            }
            return CtList.ToList();
        }
    }
}