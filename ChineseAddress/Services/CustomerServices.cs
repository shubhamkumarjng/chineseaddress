using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.IServices;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _ChineseAddress.com.Services
{
    public class CustomerServices : Areas.Admin.Models.clsCommon, ICustomerServices<ViewCustomerModel, ViewCustomerPaymentModel>
    {
        [Dependency]
        public Context4InshipEntities Context { get; set; }
        public tblPlan getSelectedPlanPrice { get; set; }
        private IAuthorizeNetService _repoAuthorizeNetService;

        /// <summary>
        /// constructor
        /// </summary>
        public CustomerServices(AuthorizeNetService repo)
        {
            _repoAuthorizeNetService = repo;
        }

        public bool prop_success { get; set; } = true;

        public string CustomerSignUp(ViewCustomerModel objViewCustomerModel, int referer_id = 0)
        {
            try
            {
                var CheckEmail = Context.tblCustomers.Where(x => x.email == objViewCustomerModel.email).Select(x => x.email).FirstOrDefault();
                string SignUpCouponPlusPlanCode = ConfigurationManager.AppSettings["SignUpCouponPlusPlan"].ToString();
                decimal SignUpCouponPlusPlanValue = Convert.ToDecimal(ConfigurationManager.AppSettings["SignUpCouponPlusPlanValue"]);
                string SignUpCouponCode = ConfigurationManager.AppSettings["SignUpCouponCode"].ToString();
                decimal SignUpCouponValue = Convert.ToDecimal(ConfigurationManager.AppSettings["SignUpCouponValue"]);
                decimal planPricewithrefermargin = 0.0m;
                //int SignUpFreeTrialPeriod = Convert.ToInt32(ConfigurationManager.AppSettings["SignUpFreeTrialPeriod"]);
                //DateTime CouponCodeTrailTo = Convert.ToDateTime(ConfigurationManager.AppSettings["SignUpMaxTrialDate"]);
                // DateTime SignUpTrailCouponMonth = DateTime.Now.AddMonths(SignUpFreeTrialPeriod);
                // DateTime AccountExpDate;
                //if (SignUpTrailCouponMonth <= CouponCodeTrailTo)
                //{
                //    AccountExpDate = SignUpTrailCouponMonth;
                //}
                //else
                //{
                //    AccountExpDate = CouponCodeTrailTo;
                //}

                if (CheckEmail == null && objViewCustomerModel.email != null && objViewCustomerModel.password != null && objViewCustomerModel.Address != null)
                {
                    tblCustomer objtblCustomer = new tblCustomer();
                    ClsCommanCustomerSignup objClsCommanCustomerSignup = new ClsCommanCustomerSignup();
                    objtblCustomer.email = objViewCustomerModel.email;
                    objtblCustomer.password = objViewCustomerModel.password;
                    objtblCustomer.account_expiry_date = DateTime.MaxValue;
                    objtblCustomer.created_on = DateTime.Now;
                    objtblCustomer.status = false;
                    getSelectedPlanPrice = Context.tblPlans.Where(x => x.Id == objViewCustomerModel.Plan_id).Select(x => x).SingleOrDefault();
                    //IsReferal START
                    if (referer_id != 0)
                    {
                        planPricewithrefermargin = Math.Abs((getSelectedPlanPrice.price ?? 0.0m) * (100 - Convert.ToDecimal(ConfigurationManager.AppSettings["ReferredSignupMarginPercent"])) / 100);
                    }
                    //IsReferal End
                    decimal amountToBePaid = 0;
                    if (!string.IsNullOrEmpty(objViewCustomerModel.CuponCode) && !string.IsNullOrEmpty(SignUpCouponCode))
                    {
                        if (getSelectedPlanPrice.title == "Basic")
                        {
                            if (objViewCustomerModel.CuponCode.ToLower().Trim() == SignUpCouponCode.ToLower().Trim())
                            {
                                amountToBePaid = 0.00m;
                            }
                        }
                        // amountToBePaid = Convert.ToDecimal(getSelectedPlanPrice) - SignUpCouponValue;
                    }
                    if (!string.IsNullOrEmpty(objViewCustomerModel.CuponCode) && !string.IsNullOrEmpty(SignUpCouponPlusPlanCode))
                    {
                        if (objViewCustomerModel.CuponCode.ToLower().Trim() == SignUpCouponPlusPlanCode.ToLower().Trim())
                        {
                            if (getSelectedPlanPrice.title == "Plus" || getSelectedPlanPrice.title == "Premium")
                            {
                                decimal PlusPlainPercentageAmount = Convert.ToDecimal(getSelectedPlanPrice.price * SignUpCouponPlusPlanValue / 100);
                                if (referer_id != 0)
                                {
                                    PlusPlainPercentageAmount = Math.Abs(PlusPlainPercentageAmount * (100 - Convert.ToDecimal(ConfigurationManager.AppSettings["ReferredSignupMarginPercent"])) / 100);
                                }
                                amountToBePaid = PlusPlainPercentageAmount;
                                SignUpCouponPlusPlanValue = PlusPlainPercentageAmount;
                            }
                        }
                    }
                    //amountToBePaid = SignUpCouponValue;
                    HttpSessionStateBase Session = new HttpSessionStateWrapper(HttpContext.Current.Session);

                    //Add referrer id in tblCustomerTable Start
                    if (referer_id != 0)
                    {
                        objtblCustomer.referrer_id = referer_id;
                    }
                    //Add referrer id in tblCustomerTable End


                    if ((amountToBePaid <= 0 && objViewCustomerModel.CuponCode != null && objViewCustomerModel.CuponCode.ToLower().Trim() == SignUpCouponCode.ToLower().Trim() && getSelectedPlanPrice.title == "Basic") || (amountToBePaid <= 0 && getSelectedPlanPrice.title.ToLower() == "starter-plan"))//changesCA
                    {
                        if (referer_id != 0)
                        {
                            Context.tblRewardPoints.Add(new tblRewardPoint() { Fk_CustomerId = referer_id, is_converted = false, type = "signup", created_on = DateTime.Now, reward_points = Convert.ToDecimal(ConfigurationManager.AppSettings["ReferralSignUpPoints"]) });
                        }
                        string referenceNumber = objClsCommanCustomerSignup.GetCustomerReference();
                        objtblCustomer.customer_reference = referenceNumber;
                        objtblCustomer.status = true;
                        objtblCustomer.payment_status = true;
                        Context.tblCustomers.Add(objtblCustomer);
                        Context.SaveChanges();
                        string CustomerID = Cryptography.Encrypt(Convert.ToString(objtblCustomer.Id));
                        Session["customerEncriptedId"] = CustomerID;
                        objClsCommanCustomerSignup.tblcustomerPlanLinking(objViewCustomerModel);
                        objClsCommanCustomerSignup.tblAddressBookSaveData(objViewCustomerModel);
                        objClsCommanCustomerSignup.SaveBillingAdrress();
                        objClsCommanCustomerSignup.tblSignCouponConsume(SignUpCouponCode, SignUpCouponValue, objViewCustomerModel);
                        tblBillingAddress objbillingAddress = Context.tblBillingAddresses.Where(x => x.email == objtblCustomer.email).Select(x => x).FirstOrDefault();
                        //Create Customer Invoice start 
                        tblInvoice objInvoice = new tblInvoice();
                        objInvoice.Fk_customer_id = objtblCustomer.Id;
                        objInvoice.Fk_billing_address_id = objbillingAddress.Id;
                        objInvoice.Fk_credit_card_id = null;
                        objInvoice.invoice_number = (new clsCommonServiceRoot()).GetInvoiceNumber();
                        objInvoice.invoice_date = DateTime.Now;
                        objInvoice.invoice_type = "signup";
                        objInvoice.paid_status = 2;
                        objInvoice.paid_on = DateTime.Now;
                        if (!string.IsNullOrEmpty(objViewCustomerModel.CuponCode))
                            objInvoice.payment_method = "coupon";
                        else
                            objInvoice.payment_method = "free";
                        objInvoice.payment_amount = 0.0m;
                        objInvoice.custom_guid = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
                        objInvoice.transaction_status = "1";
                        objInvoice.transaction_response = "success";
                        objInvoice.plan_type = getSelectedPlanPrice.title;
                        Context.tblInvoices.Add(objInvoice);
                        Context.SaveChanges();
                        ViewCustomerPaymentModel objPaymentModel = new ViewCustomerPaymentModel();
                        objPaymentModel.Fk_customer_id = objtblCustomer.Id;
                        objPaymentModel.first_name = objbillingAddress.first_name;
                        objPaymentModel.last_name = objbillingAddress.last_name;
                        objPaymentModel.address = objbillingAddress.address;
                        objPaymentModel.post_code = objbillingAddress.post_code;
                        objPaymentModel.city = objbillingAddress.city;
                        objPaymentModel.state = objbillingAddress.state;
                        objPaymentModel.Country = objbillingAddress.country_code;
                        objPaymentModel.mobile = objbillingAddress.mobile;
                        objPaymentModel.PlanType = getSelectedPlanPrice.title;
                        objPaymentModel.InvoiceNumber = objInvoice.invoice_number;
                        objPaymentModel.InvoiceDate = objInvoice.invoice_date;
                        objPaymentModel.PlanAmount = objInvoice.payment_amount;
                        objPaymentModel.PlanType = getSelectedPlanPrice.title;
                        objClsCommanCustomerSignup.CreateSignUpInvoice(objPaymentModel);
                        //Create customer invoice end 
                        //Send Sign Email Start
                        clsSendEmailService objclsSendEmailService = new clsSendEmailService();
                        string subject = "chineseaddress - SignUp";
                        System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                        hashTable.Add("#FirstName#", objViewCustomerModel.FirstName);
                        hashTable.Add("#LastName#", objViewCustomerModel.LastName);
                        hashTable.Add("#username#", objViewCustomerModel.email);
                        hashTable.Add("#password#", objViewCustomerModel.password);
                        //Send Sign Email End
                        try
                        {
                            objclsSendEmailService.SendEmail(new[] { objViewCustomerModel.email }, null, null, (new clsEmailTemplateService().GetEmailTemplate("signup", hashTable)), subject);
                        }
                        catch (Exception)
                        {
                        }
                        return "Register successfully redirect to user home page";
                    }
                    else if (objViewCustomerModel.CuponCode == null || objViewCustomerModel.CuponCode.ToLower().Trim() == SignUpCouponPlusPlanCode.ToLower().Trim())
                    {
                        if (objViewCustomerModel.CuponCode != null)
                            if (getSelectedPlanPrice.title == "Basic" && objViewCustomerModel.CuponCode.ToLower().Trim() == SignUpCouponPlusPlanCode.ToLower().Trim())
                            {
                                return ErrorMsg("CouponCode does not exist!");
                            }
                        objtblCustomer.status = true;
                        objtblCustomer.account_expiry_date = null;
                        string referenceNumber = objClsCommanCustomerSignup.GetCustomerReference();
                        objtblCustomer.customer_reference = referenceNumber;
                        Context.tblCustomers.Add(objtblCustomer);
                        Context.SaveChanges();
                        objClsCommanCustomerSignup.tblcustomerPlanLinking(objViewCustomerModel);
                        objClsCommanCustomerSignup.tblAddressBookSaveData(objViewCustomerModel);
                        if (objViewCustomerModel.CuponCode != null)
                            objClsCommanCustomerSignup.tblSignCouponConsume(SignUpCouponPlusPlanCode, SignUpCouponPlusPlanValue, objViewCustomerModel);
                        string CustomerID = Cryptography.Encrypt(Convert.ToString(objtblCustomer.Id));
                        Session["customerEncriptedId"] = CustomerID;

                        return CustomerID;
                    }
                    return ErrorMsg("CouponCode does not exist!");
                }
                else
                {
                    return ErrorMsg("User already exist!");
                }
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message.ToString());
            }
        }
        public ViewCustomerPaymentModel GetAddressBookData(int ID)
        {
            try
            {
                tblCustomer objtblCustomer = new tblCustomer();
                var CustomerEmail = Context.tblCustomers.Where(x => x.Id == ID).Select(x => x.email).FirstOrDefault();
                var AdressBookData = Context.tblAddressBooks.Where(x => x.Fk_customer_Id == ID).Select(x => x).FirstOrDefault();
                objtblCustomer.Id = ID;
                ViewCustomerPaymentModel objViewCustomerPaymentModel = new ViewCustomerPaymentModel();
                if (AdressBookData != null)
                {
                    objViewCustomerPaymentModel.first_name = AdressBookData.first_name;
                    objViewCustomerPaymentModel.last_name = AdressBookData.last_name;
                    objViewCustomerPaymentModel.email = CustomerEmail;
                    objViewCustomerPaymentModel.city = AdressBookData.city;
                    objViewCustomerPaymentModel.mobile = AdressBookData.mobile1;
                    objViewCustomerPaymentModel.address = AdressBookData.address1;
                    objViewCustomerPaymentModel.Country = AdressBookData.country_code;
                    objViewCustomerPaymentModel.state = AdressBookData.state;
                    objViewCustomerPaymentModel.post_code = AdressBookData.post_code;
                }
                return objViewCustomerPaymentModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ViewCustomerModel GetTblCustomerIdByUserNamePassword(ViewCustomerModel tblcustomer)
        {
            try
            {
                var GetEmail = "";
                if (HttpContext.Current.Session["SocialEmail"] != null)
                {
                    GetEmail = Convert.ToString(HttpContext.Current.Session["SocialEmail"]);
                    var GetCustomerEmail = Context.tblCustomers.AsQueryable().Where(x => x.email == GetEmail).Select(x => x).FirstOrDefault();
                    tblcustomer.email = GetCustomerEmail.email;
                    tblcustomer.Id = GetCustomerEmail.Id;
                    tblcustomer.AccountExpDate = Convert.ToString(GetCustomerEmail.account_expiry_date);
                    tblcustomer.password = GetCustomerEmail.password;
                    HttpContext.Current.Session["SocialEmail"] = null;
                    return tblcustomer;
                }
                else
                {
                    var Data = Context.tblCustomers.AsQueryable().FirstOrDefault(x => x.email == tblcustomer.email && x.password == tblcustomer.password && x.status == true);

                    if (Data != null)
                    {
                        if (Data.password != tblcustomer.password)
                        {
                            return tblcustomer = null;
                        }
                        else
                        {
                            tblcustomer.email = Data.email;
                            tblcustomer.Id = Data.Id;
                            tblcustomer.AccountExpDate = Convert.ToString(Data.account_expiry_date);
                            tblcustomer.password = Data.password;
                            return tblcustomer;
                        }
                    }
                    else
                    {
                        return tblcustomer = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ViewCustomerModel GetTblCustomerIdByUserNamePasswords(string emails)
        {
            try
            {
                return (new clsCommonServiceRoot()).getCustomerViaEmail(emails);
                //ViewCustomerModel objViewCustomerModel = new ViewCustomerModel();
                //var data = Context.tblCustomers.AsQueryable().Where(x => x.email == emails).Select(x => x).FirstOrDefault();

                //if (data != null)
                //{
                //    objViewCustomerModel.status = data.status;
                //    objViewCustomerModel.email = data.email;
                //    return objViewCustomerModel;
                //}
                //return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateUserEmailPassword(string emails)
        {
            try
            {
                var useremail = Context.tblCustomers.AsQueryable().Where(x => x.email == emails).FirstOrDefault();
                return useremail.password;
            }
            catch (Exception ex)
            {
                return ErrorMsg(ex.Message.ToString());
            }
        }

        public ViewCustomerPaymentModel CustomerAdditionalInformation(int ID)
        {
            ViewCustomerPaymentModel objViewCustomerPaymentModel = new ViewCustomerPaymentModel();
            var Plans = Context.tblCustomerPlanLinkings.AsQueryable().Where(x => x.Fk_Customer_Id == ID).Select(x => x.plan_title).FirstOrDefault();
            tblSignupCouponConsumed objtblSignupCouponConsumed = Context.tblSignupCouponConsumeds.Where(x => x.Fk_CustomerId == ID).Select(x => x).FirstOrDefault();
            decimal CustomerPaidAmount = Context.tblCustomerPlanLinkings.Where(x => x.Fk_Customer_Id == ID).OrderByDescending(x => x.created_on).Select(x => x.plan_amount).FirstOrDefault();
            objViewCustomerPaymentModel.PlanAmount = CustomerPaidAmount;
            Decimal? CouponValue = Context.Database.SqlQuery<Decimal?>($"select tscc.redeemed_Amount from tblcustomer as tcust inner join tblSignupCouponConsumed as tscc on tcust.Id = tscc.Fk_CustomerId left join tblInvoice as tinv on tcust.Id = tinv.Fk_customer_id where  isnull(tinv.paid_status,0) = 0 and tcust.id ={ID}").FirstOrDefault();
            if (CouponValue != null)
            {
                CustomerPaidAmount = CustomerPaidAmount - Convert.ToDecimal(CouponValue);
            }

            objViewCustomerPaymentModel.PayableAmount = CustomerPaidAmount;
            objViewCustomerPaymentModel.CouponValue = (objtblSignupCouponConsumed == null ? 0.0m : objtblSignupCouponConsumed.redeemed_Amount ?? 0.00m);
            objViewCustomerPaymentModel.Plan = Plans;
            objViewCustomerPaymentModel.referrer_id = Context.tblCustomers.AsQueryable().Where(x => x.Id == ID).Select(x => x.referrer_id).FirstOrDefault() ?? 0;
            return objViewCustomerPaymentModel;
        }

        public string CustomerPayBillingAddress(ViewCustomerPaymentModel objViewCustomerPaymentModel)
        {
            try
            {
                #region Create Billing Address object

                tblBillingAddress objbillingaddress = new tblBillingAddress();
                objbillingaddress.first_name = objViewCustomerPaymentModel.first_name;
                objbillingaddress.last_name = objViewCustomerPaymentModel.last_name;
                objbillingaddress.mobile = objViewCustomerPaymentModel.mobile;
                objbillingaddress.address = objViewCustomerPaymentModel.address;
                objbillingaddress.country_code = objViewCustomerPaymentModel.Country;
                objbillingaddress.city = objViewCustomerPaymentModel.city;
                objbillingaddress.post_code = objViewCustomerPaymentModel.post_code;
                objbillingaddress.state = objViewCustomerPaymentModel.state;
                objbillingaddress.email = objViewCustomerPaymentModel.email;
                objbillingaddress.created_on = DateTime.Now;
                _repoAuthorizeNetService._billingAddress = objbillingaddress;
                //save this Biiling when customer Profile createdS
                //Context.tblBillingAddresses.Add(objbillingaddress);
                //Context.SaveChanges();

                #endregion Create Billing Address object


                var GetPLanTitle = Context.tblCustomerPlanLinkings.AsQueryable().Where(x => x.Fk_Customer_Id == objViewCustomerPaymentModel.Fk_customer_id).FirstOrDefault();
                tblSignupCouponConsumed objtblSignupCouponConsumed = Context.tblSignupCouponConsumeds.AsQueryable().Where(x => x.Fk_CustomerId == objViewCustomerPaymentModel.Fk_customer_id).Select(x => x).FirstOrDefault();
                decimal CustomerPaidAmount = Context.tblCustomerPlanLinkings.AsQueryable().Where(x => x.Fk_Customer_Id == objViewCustomerPaymentModel.Fk_customer_id).OrderByDescending(x => x.created_on).Select(x => x.plan_amount).FirstOrDefault();

                #region getReferrerId If Exists
                if (objViewCustomerPaymentModel.referrer_id != 0)
                {
                    CustomerPaidAmount = (Math.Abs(CustomerPaidAmount * (100 - Convert.ToDecimal(ConfigurationManager.AppSettings["ReferredSignupMarginPercent"])) / 100));
                    _repoAuthorizeNetService.referrer_Id = objViewCustomerPaymentModel.referrer_id;
                }

                #endregion getReferrerId If Exists
                #region getCouponValue
                Decimal? CouponValue = Context.Database.SqlQuery<Decimal?>($"select tscc.redeemed_Amount from tblcustomer as tcust inner join tblSignupCouponConsumed as tscc on tcust.Id = tscc.Fk_CustomerId left join tblInvoice as tinv on tcust.Id = tinv.Fk_customer_id where  isnull(tinv.paid_status,0) = 0 and tcust.id ={objViewCustomerPaymentModel.Fk_customer_id}").FirstOrDefault();
                if (CouponValue != null)
                {
                    CustomerPaidAmount = Math.Round(CustomerPaidAmount - Math.Round(Convert.ToDecimal(CouponValue), 2), 2);
                }

                #endregion getCouponValue



                #region Create Invoice object

                tblInvoice objInvoice = new tblInvoice();
                //Id of order or subsction id dynamimically with plan
                objInvoice.Fk_customer_id = objViewCustomerPaymentModel.Fk_customer_id;
                //pass addresss from Auhorize Net Servicve
                //objInvoice.Fk_billing_address_id = objbillingaddress.Id;
                objInvoice.plan_type = GetPLanTitle.plan_title;
                objInvoice.custom_guid = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
                objInvoice.payment_amount = CustomerPaidAmount;
                objInvoice.paid_on = DateTime.Now;  //paid_on remove check
                objInvoice.invoice_date = DateTime.Now;
                objInvoice.invoice_type = "signup";

                //Create invoice when custoer payment profile is created
                //Context.Configuration.ValidateOnSaveEnabled = false;
                //Context.tblInvoices.Add(objInvoice);
                //Context.SaveChanges();

                #endregion Create Invoice object

                #region CreditCardComment

                //  Save Data in creadit card tbl//

                //tblCreditCard objcreditcard = new tblCreditCard();
                //objcreditcard.Fk_customer_Id = objViewCustomerPaymentModel.Fk_customer_id;
                //objcreditcard.card_type = "Master Card";
                //objcreditcard.card_num = objViewCustomerPaymentModel.CreditCard;
                //objcreditcard.card_expiry = DateTime.Now.AddYears(10).ToString();
                //objcreditcard.cust_name = objViewCustomerPaymentModel.first_name + " " + objViewCustomerPaymentModel.last_name;
                //objcreditcard.cust_address = objViewCustomerPaymentModel.address;
                //objcreditcard.cust_post_code = objViewCustomerPaymentModel.post_code;
                //objcreditcard.cust_email = objViewCustomerPaymentModel.email;
                //objcreditcard.cust_mobile = objViewCustomerPaymentModel.mobile;
                //objcreditcard.cust_country_code = objViewCustomerPaymentModel.Country;
                //objcreditcard.created_on = DateTime.Now;
                //objcreditcard.card_auth_id = "1100134876";
                //objcreditcard.card_auth_status = "transection successfully";
                //objcreditcard.card_auth_response = "Success";
                //Context.tblCreditCards.Add(objcreditcard);
                //Context.SaveChanges();

                #endregion CreditCardComment

                #region Create cutomer Profile and subscription

                tblCustomer objCustomer = new tblCustomer();
                //step 1 Pass properties
                _repoAuthorizeNetService._CustCardAndBilling = objViewCustomerPaymentModel;
                _repoAuthorizeNetService._InvoiceDetail = objInvoice;
                objCustomer = Context.tblCustomers.Where(x => x.Id == objViewCustomerPaymentModel.Fk_customer_id).FirstOrDefault();
                _repoAuthorizeNetService._CustomerDetail = objCustomer;
                //remove this from here as now we are save billing address on customer profile cration.
                //_repoAuthorizeNetService.Fk_billing_address_id = objbillingaddress.Id;

                bool is_profileCreated = _repoAuthorizeNetService.CreateCustomerProfile();
                if (is_profileCreated)
                {
                    bool is_SubscriptionCreated = _repoAuthorizeNetService.ChargeCustomerProfile();
                    if (!is_SubscriptionCreated)
                    {
                        prop_success = false;
                        return _repoAuthorizeNetService._msg;
                    }
                }
                else
                {
                    prop_success = false;
                    if (!string.IsNullOrEmpty(_repoAuthorizeNetService._msg))
                        return _repoAuthorizeNetService._msg;
                    else
                        ErrorMsg("Error occured plese contact customer care.");
                }

                #endregion Create cutomer Profile and subscription

                #region sendEmail


                clsSendEmailService objclsSendEmailService = new clsSendEmailService();
                var GetCustomerDetails = Context.tblCustomers.AsQueryable().Where(x => x.Id == objViewCustomerPaymentModel.Fk_customer_id).FirstOrDefault();
                if (GetCustomerDetails != null)
                {
                    string subject = "chineseaddress - SignUp";
                    System.Collections.Hashtable hashTable = new System.Collections.Hashtable();
                    hashTable.Add("#FirstName#", objViewCustomerPaymentModel.first_name);
                    hashTable.Add("#LastName#", objViewCustomerPaymentModel.last_name);
                    hashTable.Add("#username#", objViewCustomerPaymentModel.email);
                    hashTable.Add("#password#", GetCustomerDetails.password);
                    try
                    {
                        objclsSendEmailService.SendEmail(new[] { objCustomer.email }, null, null, (new clsEmailTemplateService().GetEmailTemplate("signup", hashTable)), subject);
                    }
                    catch { }

                    #endregion sendEmail
                    #region Create Customer Payment Invoice
                    // Create Customer Signup Payment Invoice Start//
                    var GetInvoiceNumber = Context.tblInvoices.AsQueryable().Where(x => x.Fk_customer_id == objViewCustomerPaymentModel.Fk_customer_id).FirstOrDefault();
                    objViewCustomerPaymentModel.InvoiceNumber = GetInvoiceNumber.invoice_number;
                    objViewCustomerPaymentModel.InvoiceDate = GetInvoiceNumber.invoice_date;
                    objViewCustomerPaymentModel.PlanAmount = GetInvoiceNumber.payment_amount;
                    objViewCustomerPaymentModel.PlanType = GetInvoiceNumber.plan_type;
                    ClsCommanCustomerSignup objClsCommanCustomerSignup = new ClsCommanCustomerSignup();
                    objClsCommanCustomerSignup.CreateSignUpInvoice(objViewCustomerPaymentModel);
                    // Create Customer Signup Payment Invoice End//
                    #endregion
                    prop_success = true;
                    return "showMessage('You has been registered successfully',true)";
                }
                return null;
            }
            catch (Exception ex)
            {
                return $"showMessage('{ex.Message.ToString()}',false)";
                throw ex;
            }
        }

        public bool isUserExists(string email)
        {
            if (Context == null)
            {
                return true;
            }
            var isExists = Context.tblCustomers.AsQueryable().Where(c => c.email.ToLower() == email.ToLower()).ToList();
            if (isExists.Any())
            {
                return false;
            }
            return true;
        }

        public string FacebookSocialapi(string SocialEmail, string SocialID)
        {
            var GetCustomerEmail = Context.tblCustomers.AsQueryable().Where(x => x.email == SocialEmail).Select(x => x).FirstOrDefault();
            string message = "";
            if (GetCustomerEmail != null)
            {
                if (GetCustomerEmail.facebook_id == null)
                {
                    GetCustomerEmail.facebook_id = SocialID;
                }
                Context.Entry(GetCustomerEmail).State = EntityState.Modified;
                Context.SaveChanges();
                HttpContext.Current.Session["SocialEmail"] = GetCustomerEmail.email;
                message = "Social email matched customer email";
            }
            else
            {
                message = "Social email not matched customer email";
            }
            return message;
        }

        public string GoogleSocialapi(string SocialEmail, string SocialID)
        {
            var GetCustomerEmail = Context.tblCustomers.AsQueryable().Where(x => x.email == SocialEmail).Select(x => x).FirstOrDefault();
            string message = "";
            if (GetCustomerEmail != null)
            {
                if (GetCustomerEmail.google_id == null)
                {
                    GetCustomerEmail.google_id = SocialID;
                }
                Context.Entry(GetCustomerEmail).State = EntityState.Modified;
                Context.SaveChanges();
                HttpContext.Current.Session["SocialEmail"] = GetCustomerEmail.email;
                message = "Social email matched customer email";
            }
            else
            {
                message = "Social email not matched customer email";
            }
            return message;
        }

        //public string CheckCustomerExistFirsttime(int Fk_CustomerID)
        //{
        //    bool CheckCustomerExisttblInvoice = Context.tblInvoices.Any(x => x.Fk_customer_id == Fk_CustomerID);
        //    bool C
        //}
    }
}