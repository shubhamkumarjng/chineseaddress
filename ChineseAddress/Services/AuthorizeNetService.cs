using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.IServices;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace _ChineseAddress.com.Services
{
    public class AuthorizeNetService : Areas.Admin.Models.clsCommon, IAuthorizeNetService
    {
        [Dependency]
        public Context4InshipEntities Context { get; set; }

        #region Properties

        public ViewCustomerPaymentModel _CustCardAndBilling { get; set; }
        public tblInvoice _InvoiceDetail { get; set; }
        public tblCustomer _CustomerDetail { get; set; }
        public tblCreditCard _CreditCard { get; set; }
        public tblBillingAddress _billingAddress { get; set; }
        public string _CustomerProfileId { get; set; }
        public string _CustomerPaymentProfileId { get; set; }
        public string _CustomerShipppingProfileId { get; set; }
        public string _msg { get; set; }
        public int _CustomerId { get; set; }

        //recurringInterval from
        public short recurringInterval { get; set; } = 1;

        private bool is_updated { get; set; } = true;

        //Billing Address Id
        public int Fk_billing_address_id { get; set; }

        //Referrer_Id
        public int referrer_Id { get; set; }

        //OrderId
        public tblOrder _orderDetail { get; set; }

        #endregion Properties

        #region AuthoizeNetMethods

        /// <summary>
        /// SetupMerchantAccountandEnvironment
        /// </summary>
        private void SetupMerchantAccountandEnvironment()
        {
            string CheckAthorizeEnvoirmentStatus = ConfigurationManager.AppSettings["AthorizEnvironment"];
            if (CheckAthorizeEnvoirmentStatus == "SANDBOX")
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ConfigurationManager.AppSettings["AuthorizeNetLoginId"],
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ConfigurationManager.AppSettings["AuthorizeNetTransactionKey"],
            };
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
        }

        /// <summary>
        /// Create Customer Profile before Start its subscription and Profile Payments
        /// </summary>
        /// <param name="_CreditCardDetail"></param>
        /// <param name="_BillingAddressDetail"></param>
        public bool CreateCustomerProfile()
        {
            tblCreditCard objCreditCard = new tblCreditCard();

            try
            {
                if (!is_CreditCardExists())
                {
                    if (!string.IsNullOrEmpty(_CustomerDetail.cust_profile_id))
                    {
                        //Add Customer Payment Profile and update it to tblCreditCard.
                        _CustomerProfileId = _CustomerDetail.cust_profile_id;
                        Task<bool> resCreateCustomerPaymentProfile = CreateCustomerPaymentProfile();
                        Task.WaitAll(resCreateCustomerPaymentProfile);
                        bool res = resCreateCustomerPaymentProfile.Result;
                        return res;
                    }
                    else
                    {
                        //SetupMerchantAccountandEnvironment
                        SetupMerchantAccountandEnvironment();

                        ///Add Credit Card Detail
                        var creditCard = new creditCardType
                        {
                            cardNumber = _CustCardAndBilling.CreditCard,
                            expirationDate = _CustCardAndBilling.ExpireMonth + _CustCardAndBilling.ExpireYear,
                            cardCode = _CustCardAndBilling.CVCCode
                        };

                        //standard api call to retrieve response
                        paymentType cc = new paymentType { Item = creditCard, };

                        ///Add Credit Card To Payment Profile
                        List<customerPaymentProfileType> paymentProfileList = new List<customerPaymentProfileType>();

                        ///Bill To Single Address
                        customerAddressType billingAddress = new customerAddressType();
                        billingAddress.firstName = _CustCardAndBilling.first_name;
                        billingAddress.lastName = _CustCardAndBilling.last_name;
                        billingAddress.address = _CustCardAndBilling.address;
                        billingAddress.city = _CustCardAndBilling.city;
                        billingAddress.zip = _CustCardAndBilling.post_code;

                        customerPaymentProfileType ccPaymentProfile = new customerPaymentProfileType();
                        ccPaymentProfile.payment = cc;
                        ccPaymentProfile.billTo = billingAddress;

                        paymentProfileList.Add(ccPaymentProfile);

                        ///Add Billing Address or tblAdrressBook Address
                        List<customerAddressType> addressInfoList = new List<customerAddressType>();
                        ///Create Single Address
                        customerAddressType Address = new customerAddressType();
                        Address.firstName = _CustCardAndBilling.first_name;
                        Address.lastName = _CustCardAndBilling.last_name;
                        Address.address = _CustCardAndBilling.address;
                        Address.city = _CustCardAndBilling.city;
                        Address.zip = _CustCardAndBilling.post_code;

                        addressInfoList.Add(Address);

                        ///Create customer Profile
                        customerProfileType customerProfile = new customerProfileType();
                        customerProfile.merchantCustomerId = Convert.ToString(_CustomerDetail.Id);
                        customerProfile.email = _CustomerDetail.email;
                        customerProfile.paymentProfiles = paymentProfileList.ToArray();
                        customerProfile.shipToList = addressInfoList.ToArray();

                        _CustomerId = _CustomerDetail.Id;

                        var request = new createCustomerProfileRequest { profile = customerProfile, validationMode = (validationModeEnum)Convert.ToInt32(ConfigurationManager.AppSettings["AuthorizeNetValidationMode"]) };
                        var controller = new createCustomerProfileController(request);          // instantiate the contoller that will call the service
                        controller.Execute();

                        createCustomerProfileResponse response = controller.GetApiResponse();   // get the response from the service (errors contained if any)

                        //validate

                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("***** Start Create Customer Profile " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                        if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
                        {
                            if (response != null && response.messages.message != null)
                            {
                                //Api response
                                _CustomerProfileId = response.customerProfileId;
                                _CustomerPaymentProfileId = response.customerPaymentProfileIdList[0];
                                _CustomerShipppingProfileId = response.customerShippingAddressIdList[0];
                                string[] splitResposne = null;
                                if (response.validationDirectResponseList.Length > 0)
                                {
                                    splitResposne = response.validationDirectResponseList[0].Split('|');
                                }
                                sb.AppendLine($"CustomerPaymentProfileId : {_CustomerPaymentProfileId}");
                                sb.AppendLine($"CustomerShipppingProfileId : {_CustomerShipppingProfileId}");
                                sb.AppendLine($"CustomerPaymentProfileId : {_CustomerPaymentProfileId}");
                                sb.AppendLine("***** End Create Customer Profile *****");

                                CreatePaymentResponses(sb.ToString());
                                //Parallel.Invoke(() => CreatePaymentResponses(sb.ToString()));
                                //CreatePaymentResponses(sb.ToString());
                                //start Save billing Adsress
                                Context.tblBillingAddresses.Add(_billingAddress);
                                Context.SaveChanges();
                                //end Save billing Adsress

                                //Bind tblCrediCardg
                                Fk_billing_address_id = _billingAddress.Id;
                                objCreditCard.Fk_customer_Id = _CustomerDetail.Id;
                                if (splitResposne != null)
                                {
                                    if (splitResposne.Length > 51)
                                    {
                                        objCreditCard.card_type = splitResposne[51];
                                    }
                                    else
                                        objCreditCard.card_type = "";
                                }
                                else
                                    objCreditCard.card_type = "";
                                objCreditCard.Fk_billing_address_id = Fk_billing_address_id;
                                objCreditCard.card_num = _CustCardAndBilling.CreditCard.Substring(_CustCardAndBilling.CreditCard.Length - 4);
                                objCreditCard.card_expiry = _CustCardAndBilling.ExpireMonth + "/" + _CustCardAndBilling.ExpireYear;
                                objCreditCard.cust_payment_profile_id = _CustomerPaymentProfileId;
                                objCreditCard.cust_shipping_profile_id = _CustomerShipppingProfileId;
                                objCreditCard.is_default = true;
                                objCreditCard.card_auth_status = response.messages.message[0].code;
                                objCreditCard.card_auth_response = response.messages.message[0].text;
                                objCreditCard.created_on = DateTime.Now;
                                Context.tblCreditCards.Add(objCreditCard);
                                Context.SaveChanges();
                                _CreditCard = objCreditCard;
                                // _InvoiceDetail.Fk_credit_card_id = objCreditCard.Id;
                                //Insert CustomerProfileId into tblCustomer

                                _CustomerDetail = Context.tblCustomers.AsQueryable().FirstOrDefault(x => x.Id == _CustomerDetail.Id);
                                _CustomerDetail.cust_profile_id = _CustomerProfileId;
                                Context.Entry(_CustomerDetail).State = System.Data.Entity.EntityState.Modified;
                                Context.SaveChanges();
                                return true;
                            }
                            else
                            {
                                PaymentFailed("null", "null", "Create Customer Profile");

                                _msg = ErrorMsg("Error occured while creating custmer profile");
                                SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                                return false;
                            }
                        }
                        else if (response != null)
                        {
                            PaymentFailed(response.messages.message[0].code, response.messages.message[0].text, "Create Customer Profile");

                            _msg = ErrorMsg($"{response.messages.message[0].text}");
                            SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                            return false;
                        }
                    }
                }
                else
                {
                    _msg = ErrorMsg("Credit Card already exists.");
                    return false;
                }
                //else
                //{
                //    if (is_updated)
                //    {
                //        string cardNum = _CustCardAndBilling.CreditCard.Substring(_CustCardAndBilling.CreditCard.Length - 4);
                //        objCreditCard = Context.tblCreditCards.AsQueryable().FirstOrDefault(x => x.card_num == cardNum && x.Fk_customer_Id == _CustomerDetail.Id);
                //        _CreditCard = objCreditCard;
                //        _CustomerPaymentProfileId = objCreditCard.cust_payment_profile_id;
                //        _CustomerShipppingProfileId = objCreditCard.cust_shipping_profile_id;
                //        _CustomerDetail = Context.tblCustomers.FirstOrDefault(x => x.Id == _CustomerDetail.Id);
                //        _CustomerProfileId = _CustomerDetail.cust_profile_id;
                //        return true;
                //    }
                //    else
                //    {
                //        _msg = ErrorMsg("Error occured while updateing credit card");
                //        return false;
                //    }
                //}
            }
            catch (Exception ex)
            {
                _msg = ErrorMsg(ex.Message.ToString());
                SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                return false;
            }
            return false;
        }

        /// <summary>
        /// Chage Customer Profile
        /// </summary>
        public bool ChargeCustomerProfile()
        {
            try
            {
                //SetupMerchantAccountandEnvironment
                SetupMerchantAccountandEnvironment();

                //create a customer payment profile
                customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
                profileToCharge.customerProfileId = _CustomerDetail.cust_profile_id;
                _CustomerId = _CustomerDetail.Id;
                profileToCharge.paymentProfile = new paymentProfile { paymentProfileId = _CreditCard.cust_payment_profile_id };

                var transactionRequest = new transactionRequestType
                {
                    transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // refund type
                    amount = _InvoiceDetail.payment_amount,
                    profile = profileToCharge
                };

                var request = new createTransactionRequest { transactionRequest = transactionRequest, refId = _InvoiceDetail.custom_guid.ToString() };

                // instantiate the collector that will call the service
                var controller = new createTransactionController(request);
                controller.Execute();

                // get the response from the service (errors contained if any)
                var response = controller.GetApiResponse();
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("***** Start Charge Customer Profile " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                //validate
                if (response != null)
                {
                    if (response.messages.resultCode == messageTypeEnum.Ok)
                    {
                        if (response.transactionResponse.messages != null)
                        {
                            _InvoiceDetail.wallet_consume_amount = _InvoiceDetail.wallet_consume_amount;
                            _InvoiceDetail.invoice_number = (new clsCommonServiceRoot()).GetInvoiceNumber();
                            _InvoiceDetail.invoice_date = DateTime.Now;
                            _InvoiceDetail.Fk_billing_address_id = (_CreditCard.Fk_billing_address_id ?? 0);
                            _InvoiceDetail.paid_status = 2;
                            _InvoiceDetail.Fk_credit_card_id = _CreditCard.Id;
                            _InvoiceDetail.payment_method = response.transactionResponse.accountType;
                            _InvoiceDetail.transaction_id = response.transactionResponse.transId;
                            _InvoiceDetail.transaction_status = response.transactionResponse.messages[0].code;
                            _InvoiceDetail.transaction_response = response.transactionResponse.messages[0].description;
                            Context.tblInvoices.Add(_InvoiceDetail);
                            Context.SaveChanges();
                            if (_InvoiceDetail.invoice_type.ToLower() == "signup")
                            {
                                AddReferralPoints();
                                _CustomerDetail.payment_status = true;
                                _CustomerDetail.account_expiry_date = DateTime.MaxValue;
                                _CustomerDetail.status = true;
                                Context.Entry(_CustomerDetail).State = System.Data.Entity.EntityState.Modified;
                                Context.SaveChanges();
                            }
                            else if (_InvoiceDetail.invoice_type.ToLower() == "order")
                            {
                                _orderDetail.Fk_invoice_id = _InvoiceDetail.Id;
                                _orderDetail.status = Convert.ToInt32(Areas.Admin.Models.enumorder.Awaiting_Dispatched);
                                Context.Entry(_orderDetail).State = System.Data.Entity.EntityState.Modified;
                                Context.SaveChanges();
                            }
                            sb.AppendLine("Transaction ID: " + response.transactionResponse.transId);
                            sb.AppendLine("Response Code: " + response.transactionResponse.responseCode);
                            sb.AppendLine("Description: " + response.transactionResponse.messages[0].description);
                            sb.AppendLine("Auth Code: " + response.transactionResponse.authCode);
                            sb.AppendLine("***** End Charge Customer Profile *****");
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed. Consider applying the 'await' operator to the result of the call.
                            CreatePaymentResponsesAsync(sb.ToString());
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed. Consider applying the 'await' operator to the result of the call.
                            _msg = SuccessMsg("Payment successfully completed.");
                            return true;
                        }
                        else
                        {
                            if (response.transactionResponse.errors != null)
                            {
                                PaymentFailed(response.transactionResponse.errors[0].errorCode, response.transactionResponse.errors[0].errorText, "Charge Customer Profile");

                                _msg = ErrorMsg($"{response.transactionResponse.errors[0].errorText}");
                                SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                            }
                            else
                            {
                                PaymentFailed("null", "null", "Charge Customer Profile");
                                _msg = ErrorMsg("Error occured while chargeing customer  profile");
                                SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                            }
                            RemoveDebitWalletConsumeAmount();
                        }
                    }
                    else
                    {
                        if (response.transactionResponse != null && response.transactionResponse.errors != null)
                        {
                            PaymentFailed(response.transactionResponse.errors[0].errorCode, response.transactionResponse.errors[0].errorText, "Charge Customer Profile");

                            _msg = ErrorMsg($"{response.transactionResponse.errors[0].errorText}");
                            SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                        }
                        else
                        {
                            PaymentFailed("null", "null", "Charge Customer Profile");
                            _msg = ErrorMsg("Error occured while chareing custmer profile");
                            SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                        }
                        RemoveDebitWalletConsumeAmount();
                    }
                }
                else
                {
                    RemoveDebitWalletConsumeAmount();
                    PaymentFailed("null ", "null", "Charge Customer Profile");
                    _msg = ErrorMsg("Error occured while chareing custmer profile");
                    SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                }
            }
            catch (Exception ex)
            {
                RemoveDebitWalletConsumeAmount();
                _msg = ErrorMsg(ex.Message.ToString());
                SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
            }
            return false;
        }

        /// <summary>
        /// Create Subscription Invoice ,Credit Card ,Customer
        /// </summary>
        /// <returns></returns>
        public bool CreateSubscription()
        {
            try
            {
                //SetupMerchantAccountandEnvironment
                SetupMerchantAccountandEnvironment();

                paymentScheduleTypeInterval interval = new paymentScheduleTypeInterval();

                interval.length = 7;                        // months can be indicated between 1 and 12
                interval.unit = ARBSubscriptionUnitEnum.days;

                paymentScheduleType schedule = new paymentScheduleType
                {
                    interval = interval,
                    startDate = DateTime.Now,      // start date should be tomorrow
                    totalOccurrences = 9999,                          // 999 indicates no end date
                    trialOccurrences = 0
                };

                customerProfileIdType customerProfile = new customerProfileIdType()
                {
                    customerProfileId = _CustomerProfileId,
                    customerPaymentProfileId = _CustomerPaymentProfileId,
                    customerAddressId = _CustomerShipppingProfileId
                };

                ///Bill To Single Address
                nameAndAddressType billingAddress = new nameAndAddressType();
                billingAddress.firstName = _CustCardAndBilling.first_name;
                billingAddress.lastName = _CustCardAndBilling.last_name;
                //billingAddress.address = _CustCardAndBilling.address;
                //billingAddress.city = _CustCardAndBilling.city;
                //billingAddress.zip = _CustCardAndBilling.post_code;

                ARBSubscriptionType subscriptionType = new ARBSubscriptionType()
                {
                    amount = _InvoiceDetail.payment_amount,
                    trialAmount = 0.00m,
                    paymentSchedule = schedule,
                    profile = customerProfile
                };

                var request = new ARBCreateSubscriptionRequest { subscription = subscriptionType, refId = _InvoiceDetail.custom_guid };

                var controller = new ARBCreateSubscriptionController(request);          // instantiate the contoller that will call the service
                controller.Execute();

                ARBCreateSubscriptionResponse response = controller.GetApiResponse();   // get the response from the service (errors contained if any)

                StringBuilder sb = new StringBuilder();
                //validate
                sb.AppendLine("***** Start Create Customer Subscription " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response != null && response.messages.message != null)
                    {
                        sb.AppendLine("Subscription ID : " + response.subscriptionId.ToString());
                        sb.AppendLine("***** End Create Customer Subscription *****");
                        CreatePaymentResponses(sb.ToString());
                        tblInvoice objInvoice = _InvoiceDetail;
                        objInvoice.subscription_id = response.subscriptionId.ToString();
                        objInvoice.Fk_billing_address_id = Fk_billing_address_id;
                        objInvoice.invoice_number = (new clsCommonServiceRoot()).GetInvoiceNumber();
                        objInvoice.Fk_credit_card_id = _CreditCard.Id;
                        objInvoice.payment_method = _CreditCard.card_type;
                        objInvoice.paid_on = DateTime.Now;
                        objInvoice.invoice_date = DateTime.Now;
                        objInvoice.paid_status = 2;
                        objInvoice.transaction_id = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);
                        objInvoice.transaction_status = "1";
                        objInvoice.transaction_response = "success";
                        Context.Configuration.ValidateOnSaveEnabled = false;
                        Context.tblInvoices.Add(objInvoice);
                        Context.SaveChanges();

                        TimeZoneInfo timeZoneInfoPST = TimeZoneInfo.FindSystemTimeZoneById("Pacific SA Standard Time");
                        DateTime newDateTime = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfoPST);
                        DateTime expiryDate = new DateTime();
                        if (newDateTime.Hour >= 1)
                        {
                            expiryDate = DateTime.Now.AddDays(1);
                        }
                        else
                        {
                            expiryDate = DateTime.Now;
                        }
                        //Add Points into Rewards Points Table
                        AddReferralPoints();
                        //Add Customer Expiry Date
                        tblCustomer objCustomer = Context.tblCustomers.Where(X => X.Id == objInvoice.Fk_customer_id).FirstOrDefault();
                        objCustomer.account_expiry_date = expiryDate;
                        objCustomer.payment_status = true;
                        Context.Entry(objCustomer).State = System.Data.Entity.EntityState.Modified;
                        //Below line used for insert two columns
                        Context.SaveChanges();

                        return true;
                    }
                    else
                    {
                        PaymentFailed("null", "null", "Create Customer Subscription");
                        _msg = ErrorMsg("Error occured while creating customer subscription");
                        SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                        return false;
                    }
                }
                else if (response != null)
                {
                    PaymentFailed(response.messages.message[0].code, response.messages.message[0].text, "Create Customer Subscription");
                    _msg = ErrorMsg($"{response.messages.message[0].text}");
                    SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                    return false;
                }
                else
                {
                    PaymentFailed("null", "null", "Create Customer Subscription");
                    _msg = ErrorMsg("Error occured while creating customer subscription");
                    SendEmailPaymentFailed(_CustomerId, _msg.Substring(_msg.IndexOf("'"), _msg.LastIndexOf("'") - _msg.IndexOf("'")));
                    return false;
                }
            }
            catch (Exception ex)
            {
                _msg = ErrorMsg(ex.Message.ToString());
                return false;
            }
        }

        private void AddReferralPoints()
        {
            if (referrer_Id != 0)
            {
                Context.tblRewardPoints.Add(new tblRewardPoint() { Fk_CustomerId = referrer_Id, is_converted = false, type = "signup", created_on = DateTime.Now, reward_points = Convert.ToDecimal(ConfigurationManager.AppSettings["ReferralSignUpPoints"]) });
                var GetCustomerEmail = (from tbcust in Context.tblCustomers
                                        join tbAddressbook in Context.tblAddressBooks on tbcust.Id equals tbAddressbook.Fk_customer_Id
                                        select new { tbcust.email, tbAddressbook.first_name, tbAddressbook.is_default, tbAddressbook.last_name, tbcust.Id }).Where(x => x.Id == referrer_Id && x.is_default == true).FirstOrDefault();
                if (GetCustomerEmail != null)
                {
                    string reward_points = ConfigurationManager.AppSettings["ReferralSignUpPoints"];
                    string subject = "chineseaddress - Referrer Sign Up!";
                    Hashtable htTemplate = new Hashtable();
                    htTemplate.Add("#FirstName#", GetCustomerEmail.first_name);
                    htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
                    htTemplate.Add("#rewardpoint#", reward_points);
                    string mailbody = (new clsEmailTemplateService()).GetEmailTemplate("refersignup", htTemplate);
                    (new clsSendEmailService()).SendEmail(new[] { GetCustomerEmail.email }, null, null, mailbody, subject);
                }
            }
        }

        /// <summary>
        /// Add New Customer Payment profile into custmer profile
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CreateCustomerPaymentProfile()
        {
            try
            {
                Task<bool> taskCreateCustomerShippingProfile = Task.Run(() => CreateCustomerShippingProfile());
                //SetupMerchantAccountandEnvironment
                SetupMerchantAccountandEnvironment();

                paymentType cc = new paymentType() { Item = GetCreateCreditCardObject(_CustCardAndBilling.CreditCard, _CustCardAndBilling.ExpireMonth + _CustCardAndBilling.ExpireYear, _CustCardAndBilling.CVCCode) };
                customerPaymentProfileType PaymentProfile = new customerPaymentProfileType()
                {
                    payment = cc,
                    billTo = GetBillingAddress()
                };
                var request = new createCustomerPaymentProfileRequest
                {
                    customerProfileId = _CustomerProfileId,
                    paymentProfile = PaymentProfile,
                    validationMode = (validationModeEnum)Convert.ToInt32(ConfigurationManager.AppSettings["AuthorizeNetValidationMode"])
                };

                //Prepare Request
                var controller = new createCustomerPaymentProfileController(request);
                controller.Execute();
                bool res = await taskCreateCustomerShippingProfile;
                if (!res)
                {
                    return res;
                }
                //Send Request to EndPoint
                createCustomerPaymentProfileResponse response = controller.GetApiResponse();
                if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response != null && response.messages.message != null)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("***** Start Create Customer Payment Profile " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                        sb.AppendLine($"customerPaymentProfileId = {response.customerPaymentProfileId}");
                        sb.AppendLine("***** End Create Customer Payment Profile *****");
                        CreatePaymentResponses(sb.ToString());
                        string[] splitResposne = response.validationDirectResponse.Split('|');
                        _CustomerPaymentProfileId = response.customerPaymentProfileId;
                        Context.tblBillingAddresses.Add(_billingAddress);
                        Context.SaveChanges();
                        tblCreditCard objCreditCard = new tblCreditCard();
                        objCreditCard.Fk_customer_Id = _CustomerDetail.Id;
                        objCreditCard.Fk_billing_address_id = _billingAddress.Id;

                        if (splitResposne != null)
                        {
                            if (splitResposne.Length > 51)
                            {
                                objCreditCard.card_type = splitResposne[51];
                            }
                            else
                                objCreditCard.card_type = "";
                        }
                        else
                            objCreditCard.card_type = "";
                        objCreditCard.card_num = _CustCardAndBilling.CreditCard.Substring(_CustCardAndBilling.CreditCard.Length - 4);
                        objCreditCard.card_expiry = _CustCardAndBilling.ExpireMonth + "/" + _CustCardAndBilling.ExpireYear;
                        objCreditCard.cust_payment_profile_id = _CustomerPaymentProfileId;
                        objCreditCard.cust_shipping_profile_id = _CustomerShipppingProfileId;
                        objCreditCard.is_default = false;
                        objCreditCard.card_auth_status = response.messages.message[0].code;
                        objCreditCard.card_auth_response = response.messages.message[0].text;
                        objCreditCard.created_on = DateTime.Now;
                        Context.Configuration.ValidateOnSaveEnabled = false;
                        Context.tblCreditCards.Add(objCreditCard);
                        Context.SaveChanges();

                        return true;
                    }
                }
                else
                {
                    if (response != null)
                    {
                        if (response.messages.message[0].code == "E00039")
                        {
                            PaymentFailed(response.messages.message[0].code, response.messages.message[0].text + " " + "Duplicate ID: " + response.customerPaymentProfileId, "Create Customer Payment Profile");
                            _msg = $"{response.messages.message[0].text}";
                            SendEmailPaymentFailed(_CustomerId, _msg);
                            return false;
                        }
                        else
                        {
                            PaymentFailed(response.messages.message[0].code, response.messages.message[0].text, "Create Customer Payment Profile");
                            _msg = $"{response.messages.message[0].text}";
                            SendEmailPaymentFailed(_CustomerId, _msg);
                            return false;
                        }
                    }
                    else
                    {
                        PaymentFailed("null", "null", "Create Customer Payment Profile");
                        _msg = "Error occured while creating customer payment profile";
                        SendEmailPaymentFailed(_CustomerId, _msg);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                _msg = ErrorMsg(ex.Message.ToString());
                return false;
            }
            return false;
        }

#pragma warning disable CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.

        /// <summary>
        /// CreateCustomerShippingProfile
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CreateCustomerShippingProfile()
#pragma warning restore CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        {
            try
            {
                //SetupMerchantAccountandEnvironment
                SetupMerchantAccountandEnvironment();
                customerAddressType Address = new customerAddressType();
                Address.firstName = _CustCardAndBilling.first_name;
                Address.lastName = _CustCardAndBilling.last_name;
                Address.address = _CustCardAndBilling.address;
                Address.city = _CustCardAndBilling.city;
                Address.zip = _CustCardAndBilling.post_code;

                var request = new createCustomerShippingAddressRequest
                {
                    customerProfileId = _CustomerProfileId,
                    address = Address,
                };

                //Prepare Request
                var controller = new createCustomerShippingAddressController(request);
                controller.Execute();
                createCustomerShippingAddressResponse response = controller.GetApiResponse();
                if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response != null && response.messages.message != null)
                    {
                        _CustomerShipppingProfileId = response.customerAddressId;
                        StringBuilder sb = new StringBuilder();
                        //sb.AppendLine("***** Start Create Customer Shipping Profile " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                        //sb.AppendLine("Success, customerAddressId : " + response.customerAddressId);
                        //sb.AppendLine("***** End Create Customer Shipping Profile *****");
                        //Task taskWriteResponse = Task.Run(() => CreatePaymentResponses(sb.ToString()));
                        return true;
                    }
                    else
                    {
                        PaymentFailed("null", "null", "Create Customer Shipping Profile");
                        _msg = "Error occured while creating customer shipping profile";
                        SendEmailPaymentFailed(_CustomerId, _msg);
                        return false;
                    }
                }
                else if (response != null)
                {
                    if (response.messages.message.Length > 0)
                    {
                        if (response.messages.message[0].code == "E00039")
                        {
                            _CustomerShipppingProfileId = response.customerAddressId;
                            //StringBuilder sb = new StringBuilder();
                            //sb.AppendLine("***** Start Create Customer Shipping Profile " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
                            //sb.AppendLine("Success, customerAddressId : " + response.customerAddressId);
                            //sb.AppendLine("***** End Create Customer Shipping Profile *****");
                            //await Task.Run(() => CreatePaymentResponsesAsync(sb.ToString()));
                            return true;
                        }
                        else
                        {
                            PaymentFailed(response.messages.message[0].code, response.messages.message[0].text, "Create Customer Shipping Profile");
                            _msg = $"{response.messages.message[0].text}";
                            SendEmailPaymentFailed(_CustomerId, _msg);
                            return false;
                        }
                    }
                    else
                    {
                        PaymentFailed(response.messages.message[0].code, response.messages.message[0].text, "Create Customer Shipping Profile");
                        _msg = $"{response.messages.message[0].text}";
                        SendEmailPaymentFailed(_CustomerId, _msg);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                _msg = ex.Message.ToString();
                return false;
            }
            return false;
        }

        /// <summary>
        /// Update Customer Payment Profile
        /// </summary>
        /// <param name="creditCard"></param>
        /// <param name="billtoAddress"></param>
        /// <returns></returns>
        public bool UpdateCustomerpaymentProfile(creditCardType creditCard, customerAddressType billtoAddress)
        {
            //tblCreditCard objCreditCard = new tblCreditCard();
            //objCreditCard = Context.tblCreditCards.AsQueryable().FirstOrDefault(x => x.card_num == _CustCardAndBilling.CreditCard && x.Fk_customer_Id == _CustomerDetail.Id);
            //_CustomerPaymentProfileId = objCreditCard.cust_payment_profile_id;
            //_CustomerShipppingProfileId = objCreditCard.cust_shipping_profile_id;
            //_CustomerProfileId = _CustomerDetail.cust_profile_id;

            //SetupMerchantAccountandEnvironment
            SetupMerchantAccountandEnvironment();

            ///Add Credit Card Detail

            //standard api call to retrieve response
            paymentType cc = new paymentType { Item = creditCard };

            ///Add Credit Card To Payment Profile
            var paymentProfile = new customerPaymentProfileExType
            {
                payment = cc,
                customerPaymentProfileId = _CustomerPaymentProfileId,
                billTo = billtoAddress
            };

            var request = new updateCustomerPaymentProfileRequest();
            request.customerProfileId = _CustomerProfileId;
            request.paymentProfile = paymentProfile;
            request.validationMode = (validationModeEnum)Convert.ToInt32(ConfigurationManager.AppSettings["AuthorizeNetValidationMode"]);

            // instantiate the controller that will call the service
            var controller = new updateCustomerPaymentProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (_CreditCard != null)
                {
                    _CreditCard.card_expiry = _CustCardAndBilling.ExpireMonth + "/" + _CustCardAndBilling.ExpireYear;
                    _CreditCard.Fk_billing_address_id = Fk_billing_address_id;
                    Context.Entry(_CreditCard).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();
                }
                _msg = "Customer payment profile updated successfully";
                return true;
            }
            else if (response != null)
            {
                _msg = "Error occured while updating customer payment profile";
                return false;
            }
            return false;
        }

        #endregion AuthoizeNetMethods

        #region PrivateMethods

        private bool is_CreditCardExists()
        {
            string cardNum = _CustCardAndBilling.CreditCard.Substring(_CustCardAndBilling.CreditCard.Length - 4);
            string cardExpiry = _CustCardAndBilling.ExpireMonth.ToString() + "/" + _CustCardAndBilling.ExpireYear.ToString();
            if (Context.tblCreditCards.AsQueryable().FirstOrDefault(x => x.card_num == cardNum && x.Fk_customer_Id == _CustomerDetail.Id) != null)
            {
                return true;
                //if (Context.tblCreditCards.AsQueryable().FirstOrDefault(x => x.card_num == cardNum && x.Fk_customer_Id == _CustomerDetail.Id && x.card_expiry == cardExpiry) != null)
                //    return true;
                //else
                //{
                //    BindProperties();
                //    //update Credit card of this profile
                //    is_updated = UpdateCustomerpaymentProfile(GetCreateCreditCardObject(cardNum, cardExpiry.Replace("/", "")), GetBillingAddress());
                //    return true;
                //}
            }
            else
            {
                return false;
            }
        }

        private void CreatePaymentResponses(string Keyvaluelist)
        {
            try
            {
                StreamWriter _testData = new StreamWriter(HttpContext.Current.Server.MapPath("~/IPN/AuthorizeNetIPN.txt"), true);
                _testData.WriteLineAsync(Keyvaluelist); // Write the file.
                _testData.Flush();
                _testData.Close();     // Close the instance of StreamWriter.
                _testData.Dispose(); // Dispose from memory.
            }
            catch
            {
            }
        }

        private async Task CreatePaymentResponsesAsync(string Keyvaluelist)
        {
            try
            {
                StreamWriter _testData = new StreamWriter(HttpContext.Current.Server.MapPath("~/IPN/AuthorizeNetIPN.txt"), true);
                await _testData.WriteLineAsync(Keyvaluelist); // Write the file.
                _testData.Flush();
                _testData.Close();     // Close the instance of StreamWriter.
                _testData.Dispose(); // Dispose from memory.
            }
            catch
            {
            }
        }

        private bool PaymentFailed(string code, string description, string API)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("***** Start " + API + " " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " *****");
            sb.AppendLine("Transaction Failed");
            sb.AppendLine("Error Code: " + code);
            sb.AppendLine("Error message: " + description);
            sb.AppendLine("***** End " + API + " *****");
            CreatePaymentResponses(sb.ToString());
            return false;
        }

        private void BindProperties()
        {
            string cardNum = _CustCardAndBilling.CreditCard.Substring(_CustCardAndBilling.CreditCard.Length - 4);
            string cardExpiry = _CustCardAndBilling.ExpireMonth.ToString() + "/" + _CustCardAndBilling.ExpireYear.ToString();

            _CustomerDetail = Context.tblCustomers.FirstOrDefault(x => x.Id == _CustCardAndBilling.Fk_customer_id);
            _CreditCard = Context.tblCreditCards.FirstOrDefault(x => x.card_num == cardNum);
            _CustomerProfileId = _CustomerDetail.cust_profile_id;
            if (_CreditCard != null)
            {
                _CustomerPaymentProfileId = _CreditCard.cust_payment_profile_id;
                _CustomerShipppingProfileId = _CreditCard.cust_shipping_profile_id;
            }
        }

        private creditCardType GetCreateCreditCardObject(string CNO, string expiry, string ccv = "")
        {
            if (ccv == "")
                return new creditCardType() { cardNumber = CNO, expirationDate = expiry };
            else
                return new creditCardType() { cardNumber = CNO, expirationDate = expiry, cardCode = ccv };
        }

        private customerAddressType GetBillingAddress()
        {
            return new customerAddressType() { firstName = _CustCardAndBilling.first_name, lastName = _CustCardAndBilling.last_name, address = _CustCardAndBilling.address, city = _CustCardAndBilling.city, state = _CustCardAndBilling.state, zip = _CustCardAndBilling.post_code, phoneNumber = _CustCardAndBilling.mobile };
        }

        /// <summary>
        /// This function call when error occured in Charge customer profile.With this function used wallet remove from table
        /// </summary>
        private void RemoveDebitWalletConsumeAmount()
        {
            try
            {
                if (_InvoiceDetail.wallet_consume_amount != 0)
                {
                    var objRewardAmount = Context.tblRewardAmounts.AsQueryable().OrderByDescending(x => x.created_on).Where(x => x.debit == _InvoiceDetail.wallet_consume_amount).FirstOrDefault();
                    if (objRewardAmount != null)
                    {
                        Context.tblRewardAmounts.Remove(objRewardAmount);
                        Context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion PrivateMethods

        #region Customer Payment Failed Email

        public void SendEmailPaymentFailed(int Customer_id, string _Msg)
        {
            var GetCustomerEmail = (from tbcust in Context.tblCustomers
                                    join tbAdreesbook in Context.tblAddressBooks on tbcust.Id equals tbAdreesbook.Fk_customer_Id
                                    select new { tbcust.email, tbcust.Id, tbAdreesbook.first_name, tbAdreesbook.is_default, tbAdreesbook.last_name }).Where(x => x.Id == Customer_id && x.is_default == true).FirstOrDefault();
            clsSendEmailService objclsSendEmailService = new clsSendEmailService();
            string subject = "chineseaddress - Payment Failed!";
            Hashtable htTemplate = new Hashtable();
            htTemplate.Add("#FirstName#", GetCustomerEmail.first_name);
            htTemplate.Add("#LastName#", GetCustomerEmail.last_name);
            htTemplate.Add("#failedMsg#", _Msg);
            objclsSendEmailService.SendEmail(new[] { GetCustomerEmail.email }, null, null, (new clsEmailTemplateService().GetEmailTemplate("paymentfailed", htTemplate)), subject);
        }

        #endregion Customer Payment Failed Email
    }
}