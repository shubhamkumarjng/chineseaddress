using _ChineseAddress.com.Common.Services;
using _ChineseAddress.com.Models;
using _ChineseAddress.com.Repository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Web;
using PdfSharp.Pdf;
using PdfSharp;

namespace _ChineseAddress.com.Services
{
    public class ClsCommanCustomerSignup
    {
        private Context4InshipEntities Context = new Context4InshipEntities();

        public void SaveBillingAdrress()
        {
            try
            {
                var tblAddressBooksAddress = Context.tblAddressBooks.OrderByDescending(x => x.Id).FirstOrDefault();
                var customerEmail = Context.tblCustomers.OrderByDescending(x => x.Id).FirstOrDefault();
                tblBillingAddress objtblBillingAddress = new tblBillingAddress();
                objtblBillingAddress.email = customerEmail.email;
                objtblBillingAddress.first_name = tblAddressBooksAddress.first_name;
                objtblBillingAddress.last_name = tblAddressBooksAddress.last_name;
                objtblBillingAddress.mobile = tblAddressBooksAddress.mobile1;
                objtblBillingAddress.address = tblAddressBooksAddress.address1;
                objtblBillingAddress.country_code = tblAddressBooksAddress.country_code;
                objtblBillingAddress.state = tblAddressBooksAddress.state;
                objtblBillingAddress.country_code = tblAddressBooksAddress.country_code;
                objtblBillingAddress.city = tblAddressBooksAddress.city;
                objtblBillingAddress.post_code = tblAddressBooksAddress.post_code;
                objtblBillingAddress.created_on = DateTime.Now;
                Context.tblBillingAddresses.Add(objtblBillingAddress);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void tblAddressBookSaveData(ViewCustomerModel objtbAddressBook)
        {
            try
            {
                var fk_customer_ID = Context.tblCustomers.Select(x => x.Id).OrderByDescending(x => x).FirstOrDefault();
                tblAddressBook objtblAddressBook = new tblAddressBook();
                objtblAddressBook.address_name = "My Account Address";
                objtblAddressBook.Fk_customer_Id = fk_customer_ID;
                objtblAddressBook.first_name = objtbAddressBook.FirstName;
                objtblAddressBook.last_name = objtbAddressBook.LastName;
                objtblAddressBook.company = objtbAddressBook.Company;
                objtblAddressBook.address1 = objtbAddressBook.Address;
                objtblAddressBook.mobile1 = objtbAddressBook.MobileNumber;
                objtblAddressBook.country_code = objtbAddressBook.Country;
                objtblAddressBook.city = objtbAddressBook.City;
                objtblAddressBook.state = objtbAddressBook.State;
                objtblAddressBook.post_code = objtbAddressBook.ZipCode;
                objtbAddressBook.Company = objtbAddressBook.Company;
                objtblAddressBook.is_default = true;
                objtblAddressBook.created_on = DateTime.Now;
                Context.Configuration.ValidateOnSaveEnabled = false;
                Context.tblAddressBooks.Add(objtblAddressBook);
                Context.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void tblcustomerPlanLinking(ViewCustomerModel objViewCustomerModel)
        {
            try
            {
                var fk_customer_ID = Context.tblCustomers.Select(x => x.Id).OrderByDescending(x => x).FirstOrDefault();
                tblCustomerPlanLinking objtblCustomerPlanLinking = new tblCustomerPlanLinking();
                var list = (from planlist in Context.tblPlans.Where(x => x.Id == objViewCustomerModel.Plan_id) select planlist).SingleOrDefault();
                objtblCustomerPlanLinking.Fk_Customer_Id = fk_customer_ID;
                objtblCustomerPlanLinking.plan_amount = Convert.ToDecimal(list.price);
                objtblCustomerPlanLinking.plan_title = list.title;
                objtblCustomerPlanLinking.free_storage_days = list.free_storage_days;
                objtblCustomerPlanLinking.created_on = DateTime.Now;
                Context.tblCustomerPlanLinkings.Add(objtblCustomerPlanLinking);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void tblSignCouponConsume(string SignUpCouponCode, decimal SignUpCouponValue, ViewCustomerModel objViewCustomerModel)
        {
            tblSignupCouponConsumed objtblSignupCouponConsumed = new tblSignupCouponConsumed();
            try
            {
                if (objViewCustomerModel.CuponCode != null)
                {
                    if (SignUpCouponCode.ToLower() == objViewCustomerModel.CuponCode.ToLower() && objViewCustomerModel.CuponCode != null)
                    {
                        var fk_customer_ID = Context.tblCustomers.Select(x => x.Id).OrderByDescending(x => x).FirstOrDefault();
                        var CustPlanLink_id = Context.tblCustomerPlanLinkings.Select(x => x.Fk_Customer_Id).OrderByDescending(x => x).FirstOrDefault();
                        objtblSignupCouponConsumed.Fk_CustomerId = fk_customer_ID;
                        objtblSignupCouponConsumed.redeemed_Amount = SignUpCouponValue;
                        objtblSignupCouponConsumed.coupon_Code = objViewCustomerModel.CuponCode;
                        objtblSignupCouponConsumed.redeemed_on = DateTime.Now;
                        Context.tblSignupCouponConsumeds.Add(objtblSignupCouponConsumed);
                        Context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SendEmail(string to, string replyTo, string body, string subject)
        {
            string returnString = "";
            try
            {
                MailMessage email = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                // draft the email
                string Clientmail = ConfigurationManager.AppSettings["ClientMail"].ToString();
                email.To.Add(new MailAddress(to));
                email.From = new MailAddress(Clientmail);
                email.Subject = subject;
                email.Body = body;
                email.IsBodyHtml = true;
                smtp.Send(email);

                returnString = "Email Successfully Send";
            }
            catch (Exception ex)
            {
                returnString = "Error: " + ex.ToString();
            }
            return returnString;
        }
        public string CreateSignUpInvoice(ViewCustomerPaymentModel objviewmodel)
        {
            ViewCustomerModel objmodel = new ViewCustomerModel();
            //var InvoiceNumber = Context.tblInvoices.AsQueryable().OrderByDescending(x=>x.invoice_number).Where(x => x.Fk_customer_id == objviewmodel.Fk_customer_id).Select(x => x).FirstOrDefault();
            // var InvoiceNumber = Context.tblInvoices.AsQueryable().Where(x => x.Fk_customer_id == objviewmodel.Fk_customer_id).Select(x => x).FirstOrDefault();
            var CreaditCardDetails = Context.tblCreditCards.AsQueryable().Where(x => x.Id == objviewmodel.CreditCardId).Select(x => x).FirstOrDefault();
            var CustmorPlans = Context.tblCustomerPlanLinkings.AsQueryable().Where(x => x.Fk_Customer_Id == objviewmodel.Fk_customer_id).Select(x => x).FirstOrDefault();
            var CustomerSuitNo = Context.tblCustomers.AsQueryable().Where(x => x.Id == objviewmodel.Fk_customer_id).FirstOrDefault();
            string country_name = Context.tblCountryMasters.AsQueryable().Where(x => x.country_code == objviewmodel.Country).Select(x => x.country_name).FirstOrDefault();
            Hashtable objhasTable = new Hashtable();
            //objhasTable.Add("#logo#", HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Content/img/logo.png");
            objhasTable.Add("#invoicenumber#", objviewmodel.InvoiceNumber);
            objhasTable.Add("#invoicedate#", objviewmodel.InvoiceDate);
            objhasTable.Add("#fromAddress1#", "3133 Tianhe Ave");
            objhasTable.Add("#fromAddress2#", CustomerSuitNo.customer_reference);
            objhasTable.Add("#fromcity#", "Guangzhou");
            objhasTable.Add("#fromstate#", "Guangzhou");
            objhasTable.Add("#fromzipcode#", "510000");
            objhasTable.Add("#fromcountry#", "China");
            objhasTable.Add("#fromphoneno#", "+8618871488894");
            objhasTable.Add("#firstName#", objviewmodel.first_name);
            objhasTable.Add("#lastName#", objviewmodel.last_name);
            objhasTable.Add("#address1#", objviewmodel.address);
            objhasTable.Add("#city#", objviewmodel.city);
            objhasTable.Add("#state#", objviewmodel.state);
            objhasTable.Add("#postalcode#", (objviewmodel.post_code ?? "-----"));
            objhasTable.Add("#country#", country_name);
            objhasTable.Add("#mobileNo#", objviewmodel.mobile);
            //if (CreaditCardDetails != null)
            //{
            //    objhasTable.Add("#cardno#", (CreaditCardDetails == null ? "-----" : "xxxxx" + CreaditCardDetails.card_num));
            //}
            //else if(objviewmodel.CreditCard!=null)
            //{
            //    objhasTable.Add("#cardno#", (objviewmodel.CreditCard == null ? "-----" : "xxxxx"+ objviewmodel.CreditCard.Substring(objviewmodel.CreditCard.Length - 4, 4)));
            //}
            //else
            //    objhasTable.Add("#cardno#", "-----");
            //if (CreaditCardDetails != null)
            //{
            //    objhasTable.Add("#date#", (CreaditCardDetails == null ? "-----" : CreaditCardDetails.card_expiry));
            //}
            //else if (objviewmodel.ExpireYear != null || objviewmodel.ExpireMonth != null)
            //{
            //    objhasTable.Add("#date#", (objviewmodel.ExpireMonth + "/" + objviewmodel.ExpireYear));
            //}
            //else
            //    objhasTable.Add("#date#", "-------");
            var paid_on = Context.tblInvoices.AsQueryable().Where(x => x.invoice_number == objviewmodel.InvoiceNumber).Select(x => x.paid_on).FirstOrDefault();
                
            objhasTable.Add("#date#", paid_on);
            objhasTable.Add("#paymentamount#", String.Format("{0:0.00}", objviewmodel.PlanAmount));
            objhasTable.Add("#plan#", objviewmodel.PlanType);
            objhasTable.Add("#type#", "signup");

            string Invoicetemplate = (new clsEmailTemplateService()).GetEmailTemplate("signupinvoice", objhasTable);
            var path = HttpContext.Current.Server.MapPath("~/Uploads/SignupInvoices/" + objviewmodel.Fk_customer_id);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                clsHTMLtoPDF objclsHTMLtoPDF = new clsHTMLtoPDF();
                string FilePath = HttpContext.Current.Server.MapPath("~/Uploads/SignupInvoices/" + objviewmodel.Fk_customer_id + "/" + objviewmodel.InvoiceNumber + "_SignUpInvoice.pdf");
                string CssFilePath = HttpContext.Current.Server.MapPath("~/Content/css/SignUpInvoice.min.css");
                bool OrderInvoice = false;
                objclsHTMLtoPDF.ConvertHTMLToPDF(Invoicetemplate, CssFilePath, FilePath, OrderInvoice);
            }
            else
            {
                clsHTMLtoPDF objclsHTMLtoPDF = new clsHTMLtoPDF();
                string FilePath = HttpContext.Current.Server.MapPath("~/Uploads/SignupInvoices/" + objviewmodel.Fk_customer_id + "/" + objviewmodel.InvoiceNumber + "_SignUpInvoice.pdf");
                string CssFilePath = HttpContext.Current.Server.MapPath("~/Content/css/SignUpInvoice.min.css");
                bool OrderInvoice = false;
                objclsHTMLtoPDF.ConvertHTMLToPDF(Invoicetemplate, CssFilePath, FilePath, OrderInvoice);
            }
            return null;
        }
        public string GetCustomerReference()
        {
            try
            {
                string strCustRefe = Convert.ToString(Context.tblCustomers.AsQueryable().OrderByDescending(x => x.Id).Select(x => x.customer_reference).FirstOrDefault());
                if (strCustRefe == null)
                {
                    return "100-001";
                }
                string refNo = Convert.ToString(Convert.ToInt32(strCustRefe.Replace("-", "")) + 1);
                return $"{refNo.Substring(0, refNo.Length - 3).PadRight(3, '0')}-{refNo.Substring(refNo.Length - 3)}";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Emailtemplate(Dictionary<string, string> dic)
        {
            string template = @"
<html>
<head>
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
</head>
<body>
    <div style=""float: left;margin: 53px 0 0;width: 100%;"">
        <div style=""background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
            margin: auto;width: 50%;border-color: #2396FB !important; min-height: 239px !important; border-radius: none !important;"">
            <div style=""border-bottom: 1px solid transparent;border-top-left-radius: 3px;border-top-right-radius: 3px;padding: 10px 15px;background-color: #d6351e !important; color: #fff;font-size: 18px;"""">
                #header#
            </div>
            <div style=""padding: 15px;"">
                <div>
                    <p style=""font-size: 14px; line-height: 26px;"">
                        #content#<br>
                    </p>
                </div>
            </div>

             </div>
    </div>
</body>
</html>";
            foreach (KeyValuePair<string, string> keyvalue in dic)
            {
                template = template.Replace(keyvalue.Key, keyvalue.Value);
            }
            return template;
        }
    }
}